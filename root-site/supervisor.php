<?php
/* OBM SuperVisor 2.2 by miki
 * 2021.12.13
 * */

$version = "2.5.6";

#$warned_files = array('mainpage_sidebar1.php','mainpage_sidebar2.php','mainpage_sidebar3.php','mainpage_content1.php','mainpage_content2.php','mainpage_footer.php','mainpage_header.php');
$protected_files = array(
    'private.map',
    'public.map',
    '.htaccess',
    'local_vars.php.inc'
);
$excluded_local_dirs = array(
    '/local',
    '/jobs'
);
$protected_dirs = array(
    '',
    '/private',
    '/public',
    '/local',
    '/templates',
    '/jobs',
    '/jobs/run',
    '/jobs/run/lib'
); // protected against replace dir



$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
require_once('/etc/openbiomaps/system_vars.php.inc');
#require_once('server_vars.php.inc');

if (defined('OB_WEB_PRE'))
    $ob_web_pre = OB_WEB_PRE;
else
    $ob_web_pre = '';

if (defined('USE_NON_STANDARD_HTTP_PORTS'))
    $non_standard_ports = USE_NON_STANDARD_HTTP_PORTS;
else
    $non_standard_ports = false;

# Access port
$this_port = isset($_SERVER['HTTP_X_FORWARDED_PORT']) ? $_SERVER['HTTP_X_FORWARDED_PORT'] : $_SERVER['SERVER_PORT'];
$this_host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME'];

if ($this_port != 80 and $this_port != 443 and isset($_SERVER['HTTP_X_FORWARDED_HOST']))
    $HOST = $this_host.":".$this_port.$ob_web_pre;

elseif ($non_standard_ports)
    $HOST = $this_host.":".$this_port.$ob_web_pre;

else
    $HOST = $this_host.$ob_web_pre;

require_once(constant('OB_RESOURCES') . 'includes/vendor/autoload.php');
require_once('supervisor_funcs.php');

if (!$BID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

session_start();

if (file_exists('local_vars.php.inc')) {
    // define PROJECTTABLE
    require_once('local_vars.php.inc');
    define('ADMIN_LEVEL','project');    // project admin
    $_GET['project'] = PROJECTTABLE;
    $project = PROJECTTABLE;
    #unset($_SERVER['PHP_AUTH_USER']);
    $_SESSION['authenticated'] = true;
} else {
    if (isset($_GET['project'])) {
        $project = $_GET['project'];
        define('ADMIN_LEVEL','system-project'); // system admin in projects
    } else {
        define('ADMIN_LEVEL','global'); // system admin in global
    }
}

use Mistralys\Diff\Diff;

if (defined('PROJECTTABLE')) {
    if (!isset($_SESSION['Tid']) or !has_access('master')) {
        $_SESSION['authenticated'] = false;
    }
}

// Log-in
$htpasswdFile = '/etc/openbiomaps/.htpasswd';
if (!isset($_SESSION['authenticated']))
    $_SESSION['authenticated'] = false;

if (isset($_POST['username'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    $lines = file($htpasswdFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    foreach ($lines as $line) {
        list($fileUsername, $filePasswordHash) = explode(':', $line, 2);

        #if ($fileUsername === $username && password_verify($password, $filePasswordHash)) {
        #    $authenticated = true;
        #    break;
        #}
        #echo crypt($password, $filePasswordHash, CRYPT_MD5 );
        #if ($fileUsername === $username && crypt($password, $filePasswordHash) === $filePasswordHash) {
        #    $authenticated = true;
        #    break;
        #}
        if ($fileUsername === $username) {
            // Hash részek szétválasztása
            $parts = explode('$', $filePasswordHash);
            $salt = $parts[2];

            // Hash újragenerálása a megadott jelszóval és salt-tal
            $generatedHash = apache_md5($password, $salt);

            if ($filePasswordHash === $generatedHash) {
                $_SESSION['authenticated'] = true;
                break;
            }
        }
    }
}

if (!$_SESSION['authenticated']) {
echo '<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Supervisor</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
        }
        .login-container {
            background-color: #fff;
            padding: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            border-radius: 8px;
            width: 300px;
            text-align: center;
        }
        .login-container h2 {
            margin-bottom: 20px;
        }
        .login-container input[type="text"], .login-container input[type="password"] {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #ccc;
            border-radius: 4px;
        }
        .login-container input[type="submit"] {
            background-color: #28a745;
            color: #fff;
            padding: 10px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            width: 100%;
        }
        .login-container input[type="submit"]:hover {
            background-color: #218838;
        }
    </style>
</head>
<body>

<div class="login-container">
    <h2>Supervisor log-in</h2>
    <form action="" method="post">
        <input type="text" name="username" placeholder="username" required>
        <input type="password" name="password" placeholder="password" required>
        <input type="submit" value="Log-in">
    </form>
</div>

</body>
</html>';
exit;
}


// Self Ajax function
// Update x_vars.php.inc
//
if (isset($_POST['put_file_content'])) {

    $allowed_names = array('system_vars.php.inc'=>'/etc/openbiomaps/','local_vars.php.inc'=>OB_ROOT.'projects/');

    if (array_key_exists($_POST['name'],$allowed_names)) {

        #echo var_dump( $_GET );

        if (isset( $_GET['project'] )) {
            $path = $allowed_names[$_POST['name']].$_GET['project'].'/'.'local_vars.php.inc';
        } else {
            $path = $allowed_names[$_POST['name']].'system_vars.php.inc';
        }

        $content = $_POST['put_file_content'];
        if (file_put_contents($path,$content)===false) {
            echo 'Permission denied??';
        } else {
            echo 'Ok';
        }
    }

    exit;
}

if (isset($_POST['retired'])) {
    # replace index.php to abandoned.php
    # do some settings in local_vars.php:
    # Contact person
    # Retire date
    # Data availability
    # ...?
}
?>

<!DOCTYPE html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="Pragma" content="no-cache">
    <title>obm - supervisor</title>
    <link rel="shortcut icon" href="/favicon.ico">
    <!--<link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo $HOST ?>/js/ui/jquery-ui.css" type="text/css" />-->
    <!--<link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo  $HOST ?>/css/pure-min.css" type="text/css" />-->
    <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/grids-responsive-min.css"  crossorigin="anonymous" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/jquery.min.js"></script>
    <!--<script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/ui/jquery-ui.min.js"></script>-->
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo $HOST ?>/js/datetimepicker/jquery.datetimepicker.css"/ >
    <script src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>

    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo $HOST ?>/css/Font-Awesome-4.7.0/css/font-awesome.min.css" type="text/css" />
    <!-- codemirror for code editing -->
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo $HOST ?>/css/codemirror.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo $HOST ?>/css/monokai.css" type="text/css" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/lib/codemirror.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/mode/clike/clike.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/mode/javascript/javascript.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/mode/css/css.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/mode/php/php.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/mode/sql/sql.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/mode/htmlmixed/htmlmixed.js"></script>

    <!-- codemirror addons -->
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/keymap/vim.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/addon/dialog/dialog.js"></script>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo $HOST ?>/js/addon/dialog/dialog.css" type="text/css">
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/addon/search/search.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/addon/search/searchcursor.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/addon/search/jump-to-line.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/addon/mode/loadmode.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/addon/edit/matchbrackets.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo $HOST ?>/js/addon/selection/active-line.js"></script>

    <script>
    var myCodeMirror;
    $( function() {
        jQuery('#day').datetimepicker({ timepicker:false,format:'Y/m/d'});
        jQuery('#from').datetimepicker({ datepicker:false,format:'H:i'});
        //$( "#day" ).datepicker( {"showAnim": "clip","dateFormat":"yy/mm/dd" });
    });
    function wait() {
         document.body.style.cursor = 'wait';
    }
    $(document).ready(function() {
        $(".editor-open").click(function(e){
            e.preventDefault();
            $('#editor-div').toggle(500);
            if ($(".collapsable").is(":visible")) {
                $(".collapsable").toggle(500);
            }
        });
        $(".collapsable-button").click(function(e) {
            $(".collapsable").toggle(500);
            if ($('#editor-div').is(":visible")) {
                $('#editor-div').toggle(500);
            }
        });
        myCodeMirror = CodeMirror.fromTextArea(document.getElementById("editorArea"),{
            matchBrackets: true,
            styleActiveLine: true,
            theme : 'monokai',
            lineNumbers: false,
            indentUnit: 4,
            smartIndent: true,
            showFormatButton: true,
            autoCloseTags: true,
            autoCloseBrackets: true,
            enableSearchTools: true,
            enableCodeFolding: true,
            lineWrapping: true,
        });
        $(".retired-project").click(function(e){
            e.preventDefault();
            let formData = new FormData();
            formData.append("retired", "<?php isset($_GET['project']) ? $_GET['project'] : '' ?>");
            var request = new Request('supervisor.php<?php if (isset($_GET['project'])) echo '?project='.$_GET['project'] ?>', {
                method: 'POST',
                body: formData,
            });
        });

        $('body').on('click','#editor-save',function(e){
            let code = myCodeMirror.getValue();
            let name = $("#editor-div").data("name");

            let formData = new FormData();
            formData.append("put_file_content", code);
            formData.append("name", name);

            var request = new Request('supervisor.php<?php if (isset($_GET['project'])) echo '?project='.$_GET['project'] ?>', {
                method: 'POST',
                body: formData,
            });

            fetch(request)
            .then(function(response) {
                // Handle response we get from the API
                alert(response.statusText);
            });
        });

    });
    </script>
    <style type="text/css">
        body {
            margin-left:30px;
        }
        h1,h2 {
            margin-left:-30px !important;
            color: #444;
        }
        h3 {
            color: #500273;
        }
        .command-type {
            font-weight: normal;
            font-family: monospace;
        }
        .done-title {
            color: darkgray;
        }
       .num {
            float: left;
            color: gray;
            font-size: 13px;
            font-family: monospace;
            text-align: right;
            margin-right: 6pt;
            padding-right: 6pt;
            border-right: 1px solid gray;}
        code {
            white-space:nowrap;
        }
        .diff td {
            vertical-align : top;
            white-space    : pre;
            white-space    : pre-wrap;
            font-family    : monospace;
        }
        .diffDeleted {
            border: 1px solid rgb(255,192,192);
            background: rgb(255,224,224);
        }
        .diffInserted {
            border: 1px solid rgb(192,255,192);
            background: rgb(224,255,224);
        }
        .tooltip {
            position: relative;
            display: inline-block;
        }
        .tooltip .tooltiptext {
            visibility: hidden;
            width: 520px;
            background-color: #555;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            left: 50%;
            margin-left: -260px;
            opacity: 0;
            transition: opacity 0.3s;
            font-size:85%;
        }
        .tooltip .tooltiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }
        .tooltip:hover .tooltiptext {
            visibility: visible;
            opacity: 1;
        }
        #editor-div {
            display:none;
        }
        .collapsable {
            display:none;
        }
        .button-xsmall {
            font-size: 70%;
        }

        .button-small {
            font-size: 85%;
        }

        .button-large {
            font-size: 110%;
        }

        .button-xlarge {
            font-size: 125%;
        }
        .button-success,
        .button-error,
        .button-warning,
        .button-secondary {
            color: white;
            border-radius: 4px;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
            background: rgb(66, 184, 221);
        }

        .button-success {
            background: rgb(28, 184, 65);
            /* this is a green */
        }

        .button-error {
            background: rgb(202, 60, 60);
            /* this is a maroon */
        }

        .button-warning {
            background: rgb(223, 117, 20);
            /* this is an orange */
        }

        .button-primary {
            /* this is a light blue */
        }
    </style>
</head>
<body style='padding:10px'>

<?php

if (!function_exists('error_clear_last')) {
    function error_clear_last() {
        set_error_handler('var_dump', 0);
        @$undef_var;
        restore_error_handler();
    }
}
/*function rglob($pattern, $flags = 0) {
    $files = glob($pattern, $flags);
    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
    $files = array_merge(
        [],
        ...[$files, rglob($dir . "/" . basename($pattern), $flags)]
    );
    return $files;
    }
}*/
/*function rglob($folder, $regPattern) {
    $dir = new RecursiveDirectoryIterator($folder);
    $ite = new RecursiveIteratorIterator($dir);
    $files = new RegexIterator($ite, $regPattern, RegexIterator::GET_MATCH);
    $fileList = array();
    foreach($files as $file) {
        $fileList = array_merge($fileList, $file);
    }
    return $fileList;
}*/
/*function rglob($dir, &$results = array()) {
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (!is_dir($path)) {
            $results[] = $path;
        } else if ($value != "." && $value != "..") {
            rglob($path, $results);
            $results[] = $path;
        }
    }

    return $results;
}*/

if (isset($_POST['submitSchedule'])) {

    $mfile = OB_ROOT.'projects/maintenance.conf';

    if (isset($_POST['m_project']))
        $mfile = OB_ROOT.'projects/'.$_POST['m_project'].'/maintenance.conf';

    $h = fopen($mfile,"w");
    if ($h) {
        fwrite($h,"# maintenance scheduler\n\n");
        fwrite($h,"## maintenance address\n");
        fwrite($h,$_POST['m_ip']."\n\n");
        fwrite($h,"## start day\n");
        fwrite($h,$_POST['m_day']."\n\n");
        fwrite($h,"## from\n");
        fwrite($h,$_POST['m_from']."\n\n");
        fwrite($h,"## length\n");
        fwrite($h,$_POST['m_length']." ".$_POST['m_length-unit']."\n");
        fclose($h);
    }
}

if (isset($_GET['superspecies_update'])) {

    // csv url
    // $csv_url = 'https://openbiomaps.gitlab.io/superspecies/superspecies.csv';
    // $csv_file = OB_RESOURCES . '../root-site/superspecies/superspecies.csv';

    // sql url
    if (!file_exists(OB_RESOURCES . '../root-site/superspecies')) {
        mkdir(OB_RESOURCES . '../root-site/superspecies', 0777, false);
    }

    $psql_res = trim(exec('which psql'));
    if (trim($psql_res)!='') {
        $sql_url = 'https://superspecies.gitlab.io/superspecies/master/superspecies.sql.dump';
        $sql_file = OB_RESOURCES . '../root-site/superspecies/superspecies.sql.dump';
    } else {
        $sql_url = 'https://superspecies.gitlab.io/superspecies/master/superspecies.sql';
        $sql_file = OB_RESOURCES . '../root-site/superspecies/superspecies.sql';
    }


    $data = file_get_contents($sql_url);
    if (!file_put_contents($sql_file,$data)) {
        echo "<div style='padding:10px;background-color:red;color:white'>Failed to write $sql_file.</div>";
    } else {
        echo "<div style='padding:10px;background-color:green;color:white'>$sql_file downloaded.</div>";
    }

    $gisdb_names = defined('gisdata_dbs') ? gisdata_dbs : array('gisdata');
    $gisdb_hosts = defined('gisdb_hosts') ? gisdb_hosts : array('biomaps_db');
    $gisdb_ports = defined('gisdb_ports') ? gisdb_ports : array(5432);

    for ($i=0; $i<count($gisdb_hosts); $i++) {
        $gisdb_host = $gisdb_hosts[$i];
        $gisdb_name = $gisdb_names[$i];
        if (!$GID = PGconnectSQL(biomapsdb_user,biomapsdb_pass,$gisdb_name,$gisdb_host))
            echo "<div style='padding:10px;background-color:red;color:white'>Unsuccessful connect to $gisdb_name on $gisdb_host.</div>";

        if (!file_exists($sql_file)) {
            echo "<div style='padding:10px;background-color:red;color:white'>superspecies.sql file dump not found</div>";
        }
        else {
            $cmd = "BEGIN;DROP TABLE IF EXISTS shared.superspecies;DROP TABLE IF EXISTS public.superspecies";
            pg_query($GID,$cmd);


            if (trim($psql_res)!='') {
                #$command = "export PGPASSWORD='".biomapsdb_pass."';psql -h ".$gisdb_host." -U ".biomapsdb_user." -d ".$gisdb_name." -f $sql_file";
                $command = "export PGPASSWORD='".biomapsdb_pass."';pg_restore -c -h ".$gisdb_host." -U ".biomapsdb_user." -d ".$gisdb_name." $sql_file";
                $res = 1;
                $last_line = system($command, $retval);
                if ($retval===false) {
                    $res = false;
                }
            } else {
                // no local psql available (Docker), very slow
                $commands = implode(' ',file($sql_file));
                $res = pg_query($GID,$commands);
            }

            if ($res) {
                pg_query($GID,"COMMIT");
                echo "<div style='padding:10px;background-color:green;color:white'>Superspecies updated.</div>";

                $cmd = "ALTER TABLE public.superspecies OWNER TO ".biomapsdb_user;
                $res = pg_query($GID,$cmd);
                $cmd = "GRANT SELECT ON public.superspecies TO PUBLIC";
                $res = pg_query($GID,$cmd);
                $cmd = "SET search_path TO public,shared;ALTER TABLE public.superspecies SET SCHEMA shared";
                $res = pg_query($GID,$cmd);

                if ($res) {
                    echo "<div style='padding:10px;background-color:green;color:white'>Superspecies moved to shared.</div>";
                } else {
                    echo "<div style='padding:10px;background-color:green;color:white'>Failed to move Superspecies to shared.</div>";
                }
            } else {
                pg_query($GID,"ROLLBACK");
                echo "<div style='padding:10px;background-color:orange;color:white'>Superspecies update failed: ".pg_last_error($GID)."</div>";
            }
        }
    }
}


if (isset($_GET['translation_update'])) {
    //GitHub url
    #$t_url = 'https://raw.githubusercontent.com/OpenBioMaps/translations/master/global_project_translations.csv';
    #$t_file = OB_RESOURCES . '../root-site/translations/global_project_translations.csv';

    # https://gitlab.com/openbiomaps/localization/-/raw/weblate/web-app/en.po
    $t_urls = array('en'=>'https://gitlab.com/openbiomaps/localization/-/raw/weblate/web-app/en.po',
                    'hu'=>'https://gitlab.com/openbiomaps/localization/-/raw/weblate/web-app/hu.po',
                    'es'=>'https://gitlab.com/openbiomaps/localization/-/raw/weblate/web-app/es.po',
                    'ro'=>'https://gitlab.com/openbiomaps/localization/-/raw/weblate/web-app/ro.po',
                    'ru'=>'https://gitlab.com/openbiomaps/localization/-/raw/weblate/web-app/ru.po',
                    'pt'=>'https://gitlab.com/openbiomaps/localization/-/raw/weblate/web-app/pt.po',
                    'hr'=>'https://gitlab.com/openbiomaps/localization/-/raw/weblate/web-app/hr.po',
                    'de'=>'https://gitlab.com/openbiomaps/localization/-/raw/weblate/web-app/de.po');

    $cmd = "BEGIN;DELETE FROM translations WHERE scope='global'";
    pg_query($BID,$cmd);

    $lu = array();
    $lf = array();

    foreach ($t_urls as $k=>$t_url ) {

        $data = file_get_contents($t_url);

        $t_file = OB_RESOURCES . '../root-site/translations/' . $k .'.po';
        file_put_contents($t_file,$data);



        if (!file_exists($t_file)) {
            echo "<div style='padding:10px;background-color:orange;color:white'>$k translation file not found</div>";
        }
        elseif (!is_writable($t_file)) {
            echo "<div style='padding:10px;background-color:orange;color:white'>$k.po file is not writable</div>";
        }
        else {

            $fileHandler = new Sepia\PoParser\SourceHandler\FileSystem($t_file);
            $poParser = new Sepia\PoParser\Parser($fileHandler);

            $catalog  = $poParser->parse();
            $data = $catalog->getEntries();

            $cmd = "UPDATE translations SET id=1000000+nextval('translations_id_seq');ALTER SEQUENCE translations_id_seq RESTART;UPDATE translations SET id = DEFAULT;";
            pg_query($BID,$cmd);


            #$rows['LANG']['str_foo'] = 'bar';
            $err = 0;
            $pass = 0;

            foreach ($data as $row) {
                $const = $row->getMsgId();
                $translation = $row->getMsgStr();
                $lang = $k;
                #$scope = $row->getRefrence()[0];
                $scope = 'global';

                if ($translation=='')
                        $cmd = sprintf('INSERT INTO translations ("scope","lang","const","translation") VALUES (%s,%s,%s,\'\')',quote($scope),quote($lang),quote($const));
                else
                        $cmd = sprintf('INSERT INTO translations ("scope","lang","const","translation") VALUES (%s,%s,%s,%s)',quote($scope),quote($lang),quote($const),quote($translation));

                $res = pg_query($BID,$cmd);
                $e = pg_last_error($BID);
                if ($e) {
                    $err++;
                } else {
                    $pass++;
                }
            }
            if ($err)
                $lf[] = $lang;
            if ($pass)
                $lu[] = $lang;

            unlink ($t_file);
        }
    }

    if (!count($lf) and count($lu)) {
        pg_query($BID,"COMMIT");
        echo "<div style='padding:10px;background-color:green;color:white'>Translations [".implode(',',$lu)."] updated.</div>";
    } else {
        pg_query($BID,"ROLLBACK");
        echo "<div style='padding:10px;background-color:orange;color:white'>Translations [".implode(',',$lf)."] update failed.</div>";
    }
}


if (isset($_GET['message_template_update'])) {

    echo message_template_update();
}


use AFM\Rsync\Rsync;
if (isset($_GET['auto_sync'])) {

    if (file_exists(OB_ROOT.'projects/'.$_GET['project']."/app.version")) {
        $filename = OB_ROOT.'projects/'.$_GET['project']."/app.version";
        $handle = fopen($filename, "r");
        $current_app_version = fread($handle, filesize($filename));
        fclose($handle);
    } else {
        $current_app_version = "<4.0";
    }
    $not = false;
    if (preg_match('/\<?(\d+)\.(\d+)\.(\d+)/',$current_app_version,$m)) {
        if ($m[1]<6 and $m[2]<3 and $m[3]<13) {
            echo 'Auto sync cannot be used with app older than 5.2.13';
            $not = true;
        }
    }

    if (!$not) {
        $rsync_exec = trim(exec('which rsync'));
        $origin = OB_RESOURCES;
        $target = OB_ROOT.'projects/'.$_GET['project'].'/';

        $config = array(
            'delete_from_target' => true,
            'archive',
            'dry_run',
            'exclude' => ['.htaccess','local_vars.php.inc','run_supervisor_cmds.*','/local','pwa/manifest.json','supervisor_time_log_file','/jobs','private','/public','/templates'],
        );
        $rsync = new Rsync($config);
        ob_start();
        $rsync->sync($origin, $target);
        $rsync_messages = ob_get_contents();
        ob_end_clean();
        echo preg_replace('/\n/','<br>',$rsync_messages);
    }
}

if (isset($_GET['auto_update'])) {
    $contents = yaml_parse_file(OB_ROOT_SITE.'supervisor.yml');

    $next_app_version = '';
    if (file_exists(OB_RESOURCES."app.version")) {
        $filename = OB_RESOURCES."app.version";
        $handle = fopen($filename, "r");
        $next_app_version = fread($handle, filesize($filename));
        fclose($handle);
    }

    $not = false;
    $cmd = sprintf("SELECT 1 FROM supervisor WHERE project=%s AND status='false'",quote($_GET['project']));
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        echo '<div style="padding:10px;background-color:orange;color:white">Automatic update cannot be used as long as there are incomplete previous updates! Use the manual update.</div>';
        $not = true;
    }

    if (isset($_GET['update_version'])) {
        $next_app_version = $_GET['update_version'];
        $not = false;
    }

    if (!$not) {
        $out = "";
        foreach ($contents as $revision) {

            $app_version = '<5.2.13';
            $connection = '';
            extract($revision, EXTR_OVERWRITE);
            $project = $_GET['project'];

            // how to update previous versions?
            // The update order should be reversed!
            if (trim($next_app_version) != trim($app_version)) {
                continue;
            }

            if (!is_array($commands)) $commands = array($commands);

            if ($database == 'project') {
                $c = 0;
                for($i = 0; $i<count($commands); $i++) {
                    $cmd_id = $i+1;

                    # Magic words substitutions
                    $commands[$i] = preg_replace('/%project%/',$_GET['project'],$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_user%/',biomapsdb_user,$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_pass%/',biomapsdb_pass,$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_host%/',biomapsdb_host,$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_name%/',biomapsdb_name,$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_port%/',biomapsdb_port,$commands[$i]);
                    $displayed_command = preg_replace("/%/",'&#37;',$commands[$i]);

                    if ($type == 'sql')
                        $out .= run_sql($cmd_id,$project,$database,$commands[$i],$title,$project,$connection);
                    elseif ($type == 'php')
                        $out .= run_php($cmd_id,$project,$database,$commands[$i],$title,$project,$connection);
                    elseif ($type == 'shell')
                        $out .= run_cmd($cmd_id,$project,$database,$commands[$i],$title,$project,$connection);
                }
            }
        }
        echo $out;
        opcache_reset();
    }
}

if (isset($_POST['project-settings-update'])) {

    $vars = array();
    foreach($_POST as $k=>$v) {
        if (!in_array($k,$_SESSION['valid_pc']))
            continue;

        if ($k == 'project_table') continue;

        $vars[] = sprintf('"%s"=%s',$k,quote($v));
    }

    echo project_settings_update($vars,$project);

}

if (isset($_POST['RUN-CMD']) and isset($_POST['SQLC']) and isset($_POST['saveSQL'])) {

    $PROJECT = isset($_POST['PROJECT']) ? $_POST['PROJECT'] : '';
    $DATABASE = isset($_POST['DATABASE']) ? $_POST['DATABASE'] : '';
    $project = isset($_POST['project']) ? $_POST['project'] : '';
    echo save_cmd_status($_POST['SQLC'],$PROJECT,$DATABASE,$_POST['RUN-CMD'],$_POST['REVISION'],$project);
}
elseif (isset($_POST['RUN-CMD']) and isset($_POST['SQLC']) and !isset($_POST['saveSQL'])) {

    $PROJECT = isset($_POST['PROJECT']) ? $_POST['PROJECT'] : '';
    $DATABASE = isset($_POST['DATABASE']) ? $_POST['DATABASE'] : '';
    $project = isset($_POST['project']) ? $_POST['project'] : '';
    echo run_cmd($_POST['SQLC'],$PROJECT,$DATABASE,$_POST['RUN-CMD'],$_POST['REVISION'],$project);
    opcache_reset();

}
if (isset($_POST['RUN-PHP']) and isset($_POST['SQLC']) and isset($_POST['saveSQL'])) {

    $PROJECT = isset($_POST['PROJECT']) ? $_POST['PROJECT'] : '';
    $DATABASE = isset($_POST['DATABASE']) ? $_POST['DATABASE'] : '';
    $project = isset($_POST['project']) ? $_POST['project'] : '';
    echo save_php_status($_POST['SQLC'],$PROJECT,$DATABASE,$_POST['RUN-PHP'],$_POST['REVISION'],$project);

}
elseif (isset($_POST['RUN-PHP']) and isset($_POST['SQLC']) and !isset($_POST['saveSQL'])) {

    $PROJECT = isset($_POST['PROJECT']) ? $_POST['PROJECT'] : '';
    $DATABASE = isset($_POST['DATABASE']) ? $_POST['DATABASE'] : '';
    $project = isset($_POST['project']) ? $_POST['project'] : '';
    echo run_php($_POST['SQLC'],$PROJECT,$DATABASE,$_POST['RUN-PHP'],$_POST['REVISION'],$project);
    opcache_reset();
}

if (isset($_POST['RUN-SQL']) and isset($_POST['SQLC']) and !isset($_POST['saveSQL'])) {

    $CONNECTION = isset($_POST['CONNECTION']) ? $_POST['CONNECTION'] : '';
    $PROJECT = isset($_POST['PROJECT']) ? $_POST['PROJECT'] : '';
    $DATABASE = isset($_POST['DATABASE']) ? $_POST['DATABASE'] : '';
    $project = isset($_POST['project']) ? $_POST['project'] : '';
    echo run_sql($_POST['SQLC'],$PROJECT,$DATABASE,$_POST['RUN-SQL'],$_POST['REVISION'],$project,$CONNECTION);
}
elseif (isset($_POST['RUN-SQL']) and isset($_POST['SQLC']) and isset($_POST['saveSQL'])) {

    $PROJECT = isset($_POST['PROJECT']) ? $_POST['PROJECT'] : '';
    $DATABASE = isset($_POST['DATABASE']) ? $_POST['DATABASE'] : '';
    $project = isset($_POST['project']) ? $_POST['project'] : '';
    echo save_sql_status($_POST['SQLC'],$PROJECT,$DATABASE,$_POST['RUN-SQL'],$_POST['REVISION'],$project);

}



// Fullscreen functions with back button

if (isset($_GET['show_diff'])) {

    chdir(OB_ROOT.'projects');

    $projectbutton = "";
    if (isset($_GET['project'])) {
        $projectbutton = "<a class='pure-button' href='?project={$_GET['project']}&manual_sync'>{$_GET['project']}</a>";
    }
    echo "<span style='float:right'>OBM Supervisor $version</span>";
    echo "<h2><a class='pure-button pure-button-primary' href='?'>system</a> $projectbutton<hr></h2>";

    echo "<hr>";

    //chdir(OB_ROOT.'includes');

   if (!isset($_GET['directory'])) {
        $_GET['directory'] = '';
    }

    $dirs =  array(
        'includes'=>'includes',
    );

    $chunks = explode('/', $_GET['directory']);
    $s_directory = array(OB_RESOURCES);
    $p_directory = array(OB_ROOT.'projects/'.$_GET['project']);
    $up = "";

    #if (count($chunks)<=1 and preg_match('/^\/*$/',$chunks[0]))
    #    $s_directory[] = 'pages';

    //$chunks[0] = preg_replace('/^\/*/','/',$chunks[0]);

    foreach ($chunks as $i => $chunk) {
        if ($chunk=='') continue;
        if ($chunk == '..') {
            $up = 1;
            continue;
        }

        if (isset($dirs[$chunk])) {
            $s_directory[] = $dirs[$chunk];
            $p_directory[] = $chunk;
        } else {
            $s_directory[] = $chunk;
            $p_directory[] = $chunk;
        }
    }
    if ($up) {
        if (count($s_directory) > 1) {
            array_pop($s_directory);
            array_pop($p_directory);
        }
    }
    $s_directory = implode('/',$s_directory);
    $p_directory = implode('/',$p_directory);

    $chdir_to_p_dir = chdir($p_directory);

    // http://code.iamkate.com/php/diff-implementation/
    #require_once OB_RESOURCES.'includes/class.Diff.php';

    echo "<div style='background-color:lightgray;font-family:monospace'>".$p_directory.'/'.$_GET['show_diff']." &nbsp; ";
    echo $s_directory.'/'.$_GET['show_diff']."</div>";

    // compare two files line by line
    $d = Diff::compareFiles($p_directory.'/'.$_GET['show_diff'], $s_directory.'/'.$_GET['show_diff']);
    echo $d->toHTMLTable();


    echo "</body></html>";
    exit;


} elseif (isset($_GET['check_file'])) {

    chdir(OB_ROOT.'projects');

    $projectbutton = "";
    if (isset($_GET['project'])) {
        $projectbutton = "<a class='pure-button' href='?project={$_GET['project']}'>{$_GET['project']}</a>";
    }
    echo "<span style='float:right'>OBM Supervisor $version</span>";
    echo "<h2><a class='pure-button pure-button-primary' href='?'>system</a> $projectbutton<hr></h2>";

    echo "<hr>";

    if (!isset($_GET['directory'])) {
        $_GET['directory'] = '';
    }

    $dirs =  array(
        'includes'=>'includes',
    );

    $chunks = explode('/', $_GET['directory']);
    $s_directory = array(OB_RESOURCES);
    $p_directory = array(OB_ROOT.'projects/'.$_GET['project']);
    $up = "";

    if (count($chunks)<=1 and preg_match('/^\/*$/',$chunks[0]))
        $s_directory[] = 'pages';


    //$chunks[0] = preg_replace('/^\/*/','/',$chunks[0]);

    foreach ($chunks as $i => $chunk) {
        if ($chunk=='') continue;
        if ($chunk == '..') {
            $up = 1;
            continue;
        }

        if (isset($dirs[$chunk])) {
            $s_directory[] = $dirs[$chunk];
            $p_directory[] = $chunk;
        } else {
            $s_directory[] = $chunk;
            $p_directory[] = $chunk;
        }
    }
    if ($up) {
        if (count($s_directory) > 1) {
            array_pop($s_directory);
            array_pop($p_directory);
        }
    }
    $s_directory = implode('/',$s_directory);
    $p_directory = implode('/',$p_directory);

    $chdir_to_p_dir = chdir($p_directory);


    echo "<div style='background-color:lightgray;font-family:monospace'>".$p_directory.'/'.$_GET['check_file']."</div>";
    preview_file($p_directory.'/'.$_GET['check_file']);

    echo "</body></html>";
    exit;
}
elseif (isset($_GET['VIEWSQL']) and isset($_GET['SQLC'])) {
    $sql_done = array();

    $sqlid = $_GET['SQLC'];

    $connection = isset($_GET['CONNECTION']) ? $_GET['CONNECTION'] : '';

    $projectbutton = "";
    if ($_GET['DATABASE'] == 'project' and isset($_GET['PROJECT'])) {
        $projectbutton = "<a class='pure-button' href='?project={$_GET['PROJECT']}&manual_update'>{$_GET['PROJECT']}</a>";
    }
    echo "<span style='float:right'>OBM Supervisor $version</span>";
    echo "<h2><a class='pure-button pure-button-primary' href='?'>system</a> $projectbutton<hr></h2>";

    echo "<script>
    function getContent(){
        document.getElementById('RUN-SQL').value = document.getElementById('HighLightSQL').innerText;
    }
    </script>";

    echo "<form method='post' class='pure-form pure-form-aligned' onsubmit='return getContent()'>";
    echo "<input type='hidden' name='CONNECTION' value='".$connection."'>";
    echo " <fieldset>
        <legend>Update command edit/execute form</legend>";
    echo "<div class='pure-control-group'>";
        echo "<label for='REVISION'>Revision</label>";
        echo "<input id='REVISION' name='REVISION' readonly value='".$_GET['REVISION']."'>";
    echo "</div>";
    echo "<div class='pure-control-group'>";
        echo "<label for='DATABASE'>Database</label>";
        echo "<input id='DATABASE' name='DATABASE' readonly value='".$_GET['DATABASE']."'>";
    echo "</div>";
    echo "<div class='pure-control-group'>";
        echo "<label for='SQLC'>Command ID</label>";
        echo "<input id='SQLC' name='SQLC' readonly value='".$_GET['SQLC']."'>";
    echo "</div>";
    echo "<div class='pure-control-group'>";
        echo "<label for='PROJECT'>Project</label>";
        echo "<input id='PROJECT' name='PROJECT' readonly value='".$_GET['PROJECT']."'>";
    echo "</div>";
    if (isset($_GET['project'])) {
        echo "<div class='pure-control-group'>";
            echo "<label for='project'></label>";
            echo "<input id='project' name='project' readonly value='".$_GET['project']."'>";
        echo "</div>";
    }

    #require_once(OB_RESOURCES.'/includes/vendor/autoload.php');
    $viewsql = $_GET['VIEWSQL'];

    if ($_GET['TYPE'] == 'sql') {
        $highlighted_cmd = SqlFormatter::format(base64_decode($viewsql));
        $highlighted_cmd = preg_replace("/0x27/","'",$highlighted_cmd);

        $viewsql = base64_decode($viewsql);
        $viewsql = preg_replace("/0x27/","'",$viewsql);

        echo "<div class='pure-control-group'>";
            echo "<label for='RUN-SQL'>SQL command</label>";
            echo "<textarea id='RUN-SQL' name='RUN-SQL' style='display:none'>".$viewsql."</textarea>";
            echo "<div id='HighLightSQL' class='pure-u-1-2' contenteditable='TRUE' style='border:1px dotted #999'>".$highlighted_cmd."</div>";
        echo "</div>";

    } elseif($_GET['TYPE'] == 'php') {

        $view = preg_replace("/ /","+",$viewsql);
        $viewsql = preg_replace("/¤/","",base64_decode($view));
        $highlighted_cmd = preg_replace("/¤/","",base64_decode($view));
        $highlighted_cmd = preg_replace("/\n/","<br>",$highlighted_cmd);
        $highlighted_cmd = preg_replace("/\\\\n/","<br>",$highlighted_cmd);
        echo "<div class='pure-control-group'>";
            echo "<label for='RUN-PHP'>PHP command</label>";
            echo "<textarea id='RUN-PHP' name='RUN-PHP' style='display:none'>".$viewsql."</textarea>";
            echo "<div id='HighLightCMD' class='pure-u-1-2' contenteditable='TRUE' style='border:1px dotted #999'>".$highlighted_cmd."</div>";
        echo "</div>";

    } else {
        # TYPE == shell
        $view = preg_replace("/ /","+",$viewsql);

        $viewsql = preg_replace("/¤/","",base64_decode($view));
        $viewsql = preg_replace("/&/","&amp;",$viewsql);
        $viewsql = preg_replace("/</","&lt;",$viewsql);
        $viewsql = preg_replace("/>/","&gt;",$viewsql);
        //$viewsql = preg_replace("/\r\n/","\n",$viewsql);
        //$viewsql = preg_replace("/\\\\n/","\n",$viewsql);

        $highlighted_cmd = preg_replace("/¤/","",base64_decode($view));
        $highlighted_cmd = preg_replace("/&/","&amp;",$highlighted_cmd);
        $highlighted_cmd = preg_replace("/</","&lt;",$highlighted_cmd);
        $highlighted_cmd = preg_replace("/>/","&gt;",$highlighted_cmd);
        $highlighted_cmd = preg_replace("/\n/","<br>",$highlighted_cmd);
        $highlighted_cmd = preg_replace("/\\\\n/","<br>",$highlighted_cmd);
        echo "<div class='pure-control-group'>";
            echo "<label for='RUN-CMD'>SHELL command</label>";
            echo "<textarea id='RUN-CMD' name='RUN-CMD' style='display:none'>".$viewsql."</textarea>";
            echo "<div id='HighLightCMD' class='pure-u-1-2' contenteditable='TRUE' style='border:1px dotted #999'>".$highlighted_cmd."</div>";
        echo "</div>";
    }


    echo "<div class='pure-controls'>";
        echo "<button type='submit' name='submitSQL' class='pure-button pure-button-primary'>execute</button> ";
        echo "<button type='submit' name='saveSQL' class='pure-button'>mark as ready</button>";
    echo "</div>";
    echo "</fieldset></form>";

    echo "</body></html>";
    exit;
}


// Global system admin

if (ADMIN_LEVEL == 'global') {
    #               #
    #   root page   #
    #               #
    echo "<h1 style='background-color:#0078e7;color:white;padding:15px;margin:0'>OBM Supervisor $version</h1>";

    #curl --header "Content-Type: application/json" "https://gitlab.com/api/v4/projects/10223587/repository/commits/master"
    $res = wget("https://gitlab.com/api/v4/projects/10223587/repository/commits/master",array("Content-Type: application/json"));
    if (is_json($res)) {
        if (file_exists(OB_RESOURCES."/app.version")) {
            $filename = OB_RESOURCES."/app.version";
            $handle = fopen($filename, "r");
            $next_version = fread($handle, filesize($filename));
            fclose($handle);
        } else {
            $next_version = "<4.0";
        }

        $res = json_decode($res,true);
        echo "Last update: v.".$next_version." ({$res['created_at']}) - <i>{$res['title']}</i>";
    }

    echo "<h2>System <button class='pure-button button-xsmall editor-open'><i class='fa fa-code'></i></button><hr></h2>";
    $filename = "/etc/openbiomaps/system_vars.php.inc";
    $handle = fopen($filename, "r");
    $contents = fread($handle, filesize($filename));
    fclose($handle);
    echo "<div id='editor-div' data-name='system_vars.php.inc' data-path=''>";
    echo "<div id='editor-div-controls' style='margin-bottom:4px'>
                <button class='pure-button button-warning' id='editor-save'>Save</button>
            </div>";
    echo "<textarea id='editorArea'>$contents</textarea>";
    echo "</div>";

    echo "<h2>Projects<hr></h2>";

    chdir(OB_ROOT.'projects');
    echo "<ul>";
    foreach (glob("[^.]*",GLOB_ONLYDIR) as $dirname) {
        echo "<li><a href='?project=$dirname'>$dirname</a></li>";
    }
    echo "</ul>";

    # Traslations
    echo "<br><h2>Remote resources updates<hr></h2>";
    echo "<a href='?translation_update' class='pure-button pure-button-primary' style='margin-right: 15px;'>update global translations from GitLAB</a>";
    echo "<a href='?message_template_update' class='pure-button pure-button-primary' style='margin-right: 15px;'>update message templates from GitHub</a>";
    echo "<a href='?superspecies_update' class='pure-button pure-button-primary' style='margin-right: 15px;'>update superspecies from GitLab</a><br>";


    # Maintenance
    echo "<br><h2>Global maintenance schedule<hr></h2>";

    $capture = array("day"=>0,"from"=>0,"length"=>0,"address"=>0);
    $address = $_SERVER['REMOTE_ADDR'];
    $schedule_day = "";
    $from = "";
    $length = "";
    $mconf = 'maintenance.conf';

    if (file_exists($mconf)) {
        $contents = file($mconf);

        foreach ($contents as $line) {

            if ($capture['day'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $schedule_day = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['from'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $from = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['length'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $length = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['address'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $address = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }

            if (preg_match('/^## start day/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['day'] = 1;
            }
            elseif (preg_match('/^## from/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['from'] = 1;
            }
            elseif (preg_match('/^## length/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['length'] = 1;
            }
            elseif (preg_match('/^## maintenance address/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['address'] = 1;
            }
        }
    }

    echo "<form method='post' class='pure-form'>";
    echo " <fieldset>
        <legend>Set maintenance time window</legend>";
        echo " <input id='ip' name='m_ip' placeholder=\"maintener's IP address\" value='$address'>";
        echo " <input id='day' name='m_day' placeholder='day' value='$schedule_day'>";
        echo " <input id='from' name='m_from' placeholder='time start' value='$from'>";
        $int = array(1,2,3,4,5,6,7,8,9,10,11,12,15,24,30,36,45,48);
        $time_options = "";
        $m = array();
        preg_match("/^(\d+)(\s+)?(\w+)$/",$length,$m);
        foreach($int as $i) {
            if (isset($m[1]) and $i == $m[1])
                $time_options .= "<option selected>$i</option>";
            else
                $time_options .= "<option>$i</option>";

        }
        echo " <select id='length' name='m_length' placeholder='length'>$time_options</select>";
        $min_sel = "";
        if (isset($m[3]) and $m[3]=='minutes') $min_sel='selected';
        echo " <select id='length-unit' name='m_length-unit' placeholder='length-unit'><option>hours</option><option $min_sel>minutes</option></select>";

        echo " <button type='submit' name='submitSchedule' class='pure-button pure-button-primary'>set</button>";
    echo "</fieldset></form>";

    # SQL updates
    echo "<br><h2>Global updates<hr></h2>";

    echo " <a class='pure-button pure-button-primary' onclick='wait()' href='?show_completed_updates'>Show completed updates</a>";

    $t = [];
    foreach (glob('../server_confs_templates/db_structure/biomaps_*') as $f) {
        $t[] = filemtime($f);
    }
    sort($t);
    $last_biomaps_db_update = array_pop($t);

    $show_completed = (isset($_GET['show_completed_updates'])) ? true : false;

    chdir(OB_ROOT_SITE);

    $contents = yaml_parse_file('supervisor.yml');
    foreach ($contents as $revision) {

        $connection = '';
        extract($revision, EXTR_OVERWRITE);
        if (!is_array($commands)) $commands = array($commands);

        $update_timestamp = strtotime($date);

        if ($database == 'biomaps' or $database == 'gisdata') {
            $out = "<ul style='list-style-type:none;font-size:80%;line-height:200%'>";
            $c = 0;
            for($i = 0; $i<count($commands); $i++) {
                $cmd_id = $i+1;
                $displayed_command = preg_replace("/%/",'&#37;',$commands[$i]);
                $cmd = sprintf("SELECT status FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%s",
                    quote(trim($database)),quote(trim($title)),quote($cmd_id));
                $res = pg_query($BID,$cmd);
                if (pg_num_rows($res)) {
                    $title_fix = preg_replace("/&/","%26",$title);
                    $row = pg_fetch_assoc($res);
                    if ($row['status']=='f') {
                        $out .= sprintf("<li><span style='color:red'>Failed:</span> <a href='?TYPE=$type&REVISION=$title_fix&DATABASE=$database&SQLC=$cmd_id&VIEWSQL=%s'>$displayed_command</a></li>",
                            base64_encode($commands[$i]));
                        $c++;
                    }
                    else {
                        $out .= sprintf("<li><span style='color:green'>Done:</span> <a href='?TYPE=$type&REVISION=$title_fix&DATABASE=$database&SQLC=$cmd_id&VIEWSQL=%s'>$displayed_command</a></li>",
                            base64_encode($commands[$i]));
                    }
                } else {
                    // No status
                    $out .= sprintf("<li><span style='color:darkorange'>Not yet run: </span>$cmd_id. <a href='?TYPE=$type&REVISION=$title_fix&DATABASE=$database&SQLC=$cmd_id&VIEWSQL=%s&CONNECTION=$connection'>%s</a></li>",
                        base64_encode($commands[$i]),$commands[$i]);
                    $c++;
                }
            }
            // Mark all update as DONE which older then the last update on biomaps_db files
            if ($update_timestamp < $last_biomaps_db_update and !$show_completed) $c = 0;

            $out .= "</ul>";
            if ($c or $show_completed) {
                if ($c)
                    $out = "<h3>$title - v.$app_version ($date)</h3><h4 class='command-type'>$type update on $database</h4>".$out;
                else
                    $out = "<h4 style='color:gray'>$title - v.$app_version ($date)</h4><h4 class='command-type'>$type update on $database</h4>".$out;
            }
            else
                $out = "<h4 class='done-title'>$title - v.$app_version ($date)</h4>";

            echo $out;
        }
    }
}
elseif (isset($_GET['project'])) {
    #                   #
    #    Project page   #
    #                   #

    chdir(OB_ROOT.'projects');


    touch($_GET['project'].'/supervisor_time_log_file');

    $capture = array("day"=>0,"from"=>0,"length"=>0,"address"=>0);
    $address = $_SERVER['REMOTE_ADDR'];
    $schedule_day = "";
    $from = "";
    $length = "";

    $mconf = $_GET['project']."/maintenance.conf";

    if (file_exists($mconf)) {
        $contents = file($mconf);
        foreach ($contents as $line) {

            if ($capture['day'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $schedule_day = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['from'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $from = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['length'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $length = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }
            elseif ($capture['address'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
                $address = trim($line);
                $capture = array_fill_keys(array_keys($capture), 0);
            }

            if (preg_match('/^## start day/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['day'] = 1;
            }
            elseif (preg_match('/^## from/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['from'] = 1;
            }
            elseif (preg_match('/^## length/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['length'] = 1;
            }
            elseif (preg_match('/^## maintenance address/',$line)) {
                $capture = array_fill_keys(array_keys($capture), 0);
                $capture['address'] = 1;
            }
        }
    }

    $mset  = "<form method='post' class='pure-form'><input type='hidden' name='m_project' value='{$_GET['project']}'>";

    $mset .= " <fieldset>
        <legend>Set maintenance time window</legend>";
        $mset .= " <input id='ip' name='m_ip' placeholder=\"maintener's IP address\" value='$address'>";
        $mset .= " <input id='day' name='m_day' placeholder='day' value='$schedule_day'>";
        $mset .= " <input id='from' name='m_from' placeholder='time start' value='$from'>";
        $int = array(1,2,3,4,5,6,7,8,9,10,11,12,15,24,30,36,45,48);
        $time_options = "";
        $m = array();
        preg_match("/^(\d+)(\s+)?(\w+)$/",$length,$m);
        foreach($int as $i) {
            if (isset($m[1]) and $i == $m[1])
                $time_options .= "<option selected>$i</option>";
            else
                $time_options .= "<option>$i</option>";

        }
        $mset .= " <select id='length' name='m_length' placeholder='length'>$time_options</select>";
        $min_sel = "";
        if (isset($m[3]) and $m[3]=='minutes') $min_sel='selected';
        $mset .= " <select id='length-unit' name='m_length-unit' placeholder='length-unit'><option>hours</option><option $min_sel>minutes</option></select>";

        $mset .= " <button type='submit' name='submitSchedule' class='pure-button pure-button-primary'>set</button>";
    $mset .= "</fieldset></form>";


    echo "<span style='float:right'>OBM Supervisor $version</span>";

    $cmd = sprintf("SELECT protocol,domain FROM projects WHERE project_table=%s",quote($_GET['project']));
    $res = pg_query($BID,$cmd);
    $domain = "projects/".$_GET['project'];
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $domain = $row['protocol'].'://'.$row['domain'];
    }

    if (ADMIN_LEVEL == 'system-project')
        echo "<h2><a class='pure-button pure-button-primary' href='?'>system</a> <a href='$domain'>visit {$_GET['project']}</a></h2>";
    else
        echo "<h2><a href='$domain'>visit {$_GET['project']}</a></h2>";


    if (ADMIN_LEVEL == 'system-project') {
        echo "<div style='margin-left:-30px'>";

        $ts = array();
        foreach (glob("[^.]*",GLOB_ONLYDIR) as $dirname) {
            $style = '';
            if ($dirname == $_GET['project'])
                $style = 'font-weight:bold';
            $ts[] = "<a class='pure-button' href='?project=$dirname' style='$style'>$dirname</a>";
        }
        echo implode(" ",$ts);
        echo "<hr></div>";
    }


    $_SESSION['domain'] = $domain.'/';
    echo "<h2>
        <br>Project settings
            <button class='collapsable-button pure-button button-xsmall' title='settings'><i class='fa fa-database'></i></button>
            <button class='pure-button button-xsmall editor-open' title='local variables'><i class='fa fa-code'></i></button>
            <button class='pure-button button-xsmall retired-project' title='Project down'><i class='fa fa-thumbs-down'></i></button> ";

    if (ADMIN_LEVEL == 'project')
        echo "<a class='pure-button button-xsmall file-manager' title='file manager' href='$domain/simple_file_manager.php'><i class='fa fa-file-o'></i></a>";

    echo "<hr></h2>";

    $filename = $_GET['project']."/local_vars.php.inc";
    $handle = fopen($filename, "r");
    $contents = fread($handle, filesize($filename));
    fclose($handle);
    echo "<div id='editor-div' data-name='local_vars.php.inc' data-path=''>";
    echo "<div id='editor-div-controls' style='margin-bottom:4px'>
                <button class='pure-button button-warning' id='editor-save'>Save</button>
            </div>";
    echo "<textarea id='editorArea'>$contents</textarea>";
    echo "</div>";


    echo "<div class='collapsable'>";
    echo "<form method='post' class='content pure-form pure-form-stacked'><fieldset>
        <div class='pure-g'>";

    $cmd = sprintf("SELECT * FROM projects WHERE project_table=%s",quote($_GET['project']));
    $res = pg_query($BID,$cmd);
    $_SESSION['valid_pc'] = array();
    $stage = '';
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        foreach($row as $k=>$v){
            $_SESSION['valid_pc'][] = $k;
            $readonly = '';
            $title = '';
            $required = '';
            $date = '';
            $type = 'text';
            $pattern = '';

            if ($k == 'project_table') $readonly = 'readonly';
            elseif ($k == 'admin_uid') {
                $required = 'required';
                $type = 'number';
            }
            elseif ($k == 'creation_date') $type = 'date';
            elseif ($k == 'running_date') $type = 'date';
            elseif ($k == 'email') $type = 'email';
            elseif ($k == 'stage') {
                $stage = $v;
                $pattern = 'pattern="(stable)|(experimental)|(testing)|(abandoned)"';
                $title = 'stable | experimental | testing | abandoned';
            }
            elseif ($k == 'protocol') {
                $pattern = 'pattern="(http)|(https)"';
                $title = 'http | https';
                $required = 'required';
            }
            elseif ($k == 'local_project') {
                $pattern = 'pattern="t(rue)?|f(alse)?"';
                $title = 'true | false';
                if ($v == 't') $v = 'true';
                elseif ($v == 'f') $v = 'false';
            }
            elseif ($k == 'domain') $title = $_SERVER['SERVER_NAME']."/...";
            elseif ($k == 'alternate_id') $pattern = 'pattern="\{(.+),?(.+)?\}"';
            elseif ($k == 'rum') {
                $pattern = 'pattern="[+-0][+-0][+-0]"';
                $title = 'PUBLICNESS as Read (+-0) Upload (+-0) Modify (+-0). E.g. +0-';
            }
            elseif ($k == 'licence') $title = 'E.g. ODbL, ODC-By, PDDL';

            if ($k == 'projecttable') continue;

            printf('
            <div class="pure-u-1 pure-u-md-1-5">
                <label for="multi-%1$s" style="font-weight:bold">%1$s</label><input title="%4$s" type="%6$s" %3$s %5$s %7$s name="%1$s" id="multi-%1$s" class="pure-u-23-24" value="%2$s" placeholder="%4$s" />
            </div>',
                $k,$v,$readonly,$title,$required,$type,$pattern);
        }
    }
    echo '</div></fieldset>
        <button type="submit" name="project-settings-update" class="pure-button button-warning">Submit</button></form></div>';

    if (file_exists($_GET['project']."/app.version")) {
        $filename = $_GET['project']."/app.version";
        $handle = fopen($filename, "r");
        $current_app_version = fread($handle, filesize($filename));
        fclose($handle);
    } else {
        $current_app_version = "<4.0";
    }

    $next_app_version = '';
    if (file_exists(OB_RESOURCES."app.version")) {
        $filename = OB_RESOURCES."app.version";
        $handle = fopen($filename, "r");
        $next_app_version = fread($handle, filesize($filename));
        fclose($handle);
    }

    echo "Application stage: $stage / ";
    echo "Application version: $current_app_version  /  Available version: $next_app_version";

    echo "<h2><br>Project maintenance schedule<hr></h2>";
    echo $mset;

    echo "<h2><br>File updates<hr></h2>";

    if (isset($_GET['manual_sync'])) {

        echo "<br><a class='pure-button button-warning' href='?project={$_GET['project']}&auto_sync'>Automatic sync</a></br><br>";

        //chdir(OB_ROOT.'includes');

        // default direcotry
        if (!isset($_GET['directory'])) {
            $_GET['directory'] = '';
        }

        $dirs =  array(
            'includes'=>'includes',
        );

        $chunks = explode('/', $_GET['directory']);
        $s_directory = array(OB_RESOURCES);
        $p_directory = array(OB_ROOT.'projects/'.$_GET['project']);
        $up = "";

        //if (count($chunks)<=1 and preg_match('/^\/*$/',$chunks[0]))
        //    $s_directory[] = 'pages';

        //$chunks[0] = preg_replace('/^\/*/','/',$chunks[0]);

        foreach ($chunks as $i => $chunk) {
            if ($chunk=='') continue;
            if ($chunk == '..') {
                $up = 1;
                continue;
            }

            if (isset($dirs[$chunk])) {
                $s_directory[] = $dirs[$chunk];
                $p_directory[] = $chunk;
            } else {
                $s_directory[] = $chunk;
                $p_directory[] = $chunk;
            }
        }
        if ($up) {
            if (count($s_directory) > 1) {
                array_pop($s_directory);
                array_pop($p_directory);
            }
        }
        $s_directory = implode('/',$s_directory);
        $p_directory = implode('/',$p_directory);

        $chdir_to_p_dir = false;
        if (file_exists($p_directory) and is_dir($p_directory))
            $chdir_to_p_dir = chdir($p_directory);

        $updir = explode('/',$_GET['directory']);
        array_pop($updir);
        $updir = implode('/',$updir);

        $ts_reg = array();
        $ts = array("<a class='pure-button' onclick='wait()' href='?project={$_GET['project']}&directory={$_GET['directory']}&manual_sync'>.</a>","<a class='pure-button' onclick='wait()' href='?project={$_GET['project']}&directory=$updir&manual_sync'>..</a>");
        //foreach (glob("*",GLOB_ONLYDIR ) as $dirname) {
        if ($chdir_to_p_dir) {
            //$results = rglob("./");
            /*$rii = new RecursiveIteratorIterator(new RecursiveDirectoryIterator('./'));
            $files = array();
            foreach ($rii as $file) {
                if ($file->isDir()){
                    continue;
                }
                $files[] = $file->getPathname();
            }*/
            foreach (glob('*') as $dirname) {
                //$dirname = preg_replace("/^\.\//","",$dirname);
                $ok = 0;
                if (is_link($dirname)) $ok = 1;
                elseif (is_dir($dirname)) $ok = 1;

                if (!$ok) continue;

                $style = '';
                if (isset($_GET['directory']) and $dirname == $_GET['directory'])
                    $style = 'font-weight:bold';
                if (!file_exists($dirname))
                    $style = "background-color:red";

                if (!in_array($_GET['directory']."/$dirname",$excluded_local_dirs)) {
                    $c_directory = preg_replace('/\/pages/','',$s_directory);
                    $odirname = $dirname;

                    if (OB_RESOURCES == $c_directory and $dirname == 'includes')
                        $odirname = 'includes';

                    if (!is_dir("$c_directory/$odirname")) {
                        $ts[] = $dirname;
                    } else {
                        $output = shell_exec( "diff -qr $c_directory/$odirname $p_directory/$dirname" );
                        $diff = 0;
                        $expl = array();
                        foreach(preg_split("/((\r?\n)|(\r\n?))/", $output) as $line){
                             if (preg_match('/^Only in/',$line)) {
                                 if (preg_match('/examples$/',$line)) continue;
                                 elseif (preg_match('/\.swp$/',$line)) continue;
                                 elseif (preg_match('/\.swo$/',$line)) continue;
                                 elseif (preg_match('/private$/',$line)) continue;
                                 elseif ($dirname=='private' and preg_match('/cache.php$/',$line)) continue;
                                 elseif ($dirname=='private' and preg_match('/proxy.php$/',$line)) continue;
                                 elseif ($dirname=='public' and preg_match('/cache.php$/',$line)) continue;
                                 elseif ($dirname=='public' and preg_match('/proxy.php$/',$line)) continue;
                                 $diff++;
                                 $expl[] = $line;
                             }
                             elseif (preg_match('/^Files /',$line)) {
                                 if (preg_match('/(public|private)\.map differ$/',$line)) continue;
                                 elseif (preg_match('/\.swp differ$/',$line)) continue;
                                 elseif (preg_match('/\.swo differ$/',$line)) continue;
                                 elseif (preg_match('/r-server\.log differ$/',$line)) continue;
                                 elseif (preg_match('/\.htaccess differ$/',$line)) continue;
                                 $diff++;
                                 $expl[] = $line;
                             }
                        }
                        $ts_reg[] =  $dirname;
                        if ($diff) {
                            $output = implode("<br>", $expl);
                            $ts[] = "<div class='tooltip'><a class='pure-button' style='background-color:violet' href='?project={$_GET['project']}&directory={$_GET['directory']}/$dirname&manual_sync' style='$style'>$dirname</a><span class='tooltiptext'>$output</span></div>";
                        } else
                            $ts[] = "<a class='pure-button' href='?project={$_GET['project']}&directory={$_GET['directory']}/$dirname&manual_sync' style='$style'>$dirname</a>";
                    }
                }
            }
        }

        // search for missing local directories
        // Protected dirs cannot be added, but why?
        $chdir_to_s_dir = chdir($s_directory);
        if ($chdir_to_s_dir) {

            foreach (glob("[^.]*",GLOB_ONLYDIR) as $dirname) {
            //foreach (glob("*" ) as $dirname) {
                if (is_dir($dirname) and !in_array($dirname, $ts_reg) and !in_array("{$_GET['directory']}/$dirname",$protected_dirs))
                    $ts[] = "<a class='pure-button' href='?project={$_GET['project']}&directory={$_GET['directory']}/$dirname&replace_directory&manual_sync' style='background-color:aliceblue'>$dirname</a>";
            }
        }

        $chdir_to_p_dir = false;
        if (file_exists($p_directory) and is_dir($p_directory))
            $chdir_to_p_dir = chdir($p_directory);

        if (isset($_GET['replace_broken_link'])) {
            if (is_link($p_directory) and !file_exists($p_directory)) {
                if (is_dir($s_directory)) {
                    $done = unlink($p_directory) ? $done = recurse_copy($s_directory,$p_directory) : $done = false;
                }
                if (!$done)
                    echo "<span style='background-color:orange'>Replace link failed!</span>";
            }
        }
        elseif (isset($_GET['replace_directory']) and !in_array($_GET['directory'],$protected_dirs)) {
            if (is_dir($p_directory)) {
                //unlink($p_directory) ? mkdir($p_directory) : $done = false;
                $done = recurse_unlink($p_directory);
                $done = recurse_copy($s_directory,$p_directory);
                if (!$done)
                    echo "<div style='background-color:orange'>Copy directory failed!</div>";
            } elseif (!is_file($p_directory)) {
                $done = recurse_copy($s_directory,$p_directory);
                if (!$done)
                    echo "<div style='background-color:orange'>Copy directory failed!</div>";
                else
                    echo "<div style='background-color:aliceblue'>Copy directory done!</div>";
            }
        }


        $all_action = "";
        if (!is_dir($p_directory)) {
            if (is_link($p_directory) and !file_exists($p_directory))
                echo "<span style='color:red'>$p_directory is a broken link</span> <a href='?project={$_GET['project']}&directory={$_GET['directory']}&replace_broken_link&manual_sync'>[replace it]</a><br>";
            else {
                echo "$p_directory directory is not exists<br>";
            }
        } elseif (!is_writable($p_directory)) {
            echo "$p_directory directory is not writable<br>";
        } else
            $all_action = "[<a href='?project={$_GET['project']}&directory={$_GET['directory']}&update_all_files&manual_sync'>update all files</a>]";

        echo implode(" ",$ts);
        echo "<hr>";

        if (is_dir($s_directory)) {
            // remove this dangeorus link for those folders which countains private/custom files
            if (!in_array($_GET['directory'],$protected_dirs) and basename($s_directory)!=='private')
                echo "<i>$s_directory</i> [<a href='?project={$_GET['project']}&directory={$_GET['directory']}&replace_directory&manual_sync'>replace entire local directory</a>]<br>";
        }


        //debug
        //echo $s_directory;

        if (is_dir($s_directory) and chdir($s_directory)) {
            echo "<table class='pure-table'>";
            echo "<thead><tr><th>source file</th><th>project file</th><th>action $all_action</th></tr></thead></tbody>";

            //$dir = opendir($s_directory);
            //while(false !== ( $filename = readdir($dir)) ) {
                //if (!is_file($filename) and !is_link($filename)) continue;
                //if (( $filename == '.' ) || ( $filename == '..' )) continue;

            // list files
            $files_array = glob("*.{map,inc,php,css,js,gif,md,png,jpg,jpeg,svg,json,sql,ico,version,mustache}",GLOB_BRACE);
            usort($files_array, function($b, $a) { return filemtime($a) - filemtime($b); });
            error_clear_last();
            foreach ($files_array as $filename) {
                if (in_array($filename,$protected_files)) {
                    echo "<tr><td></td><td>$filename</td><td>protected file</td></tr>";
                    continue;
                }
                echo "<tr>";
                echo "<td><a href='?project={$_GET['project']}&directory={$_GET['directory']}&check_file=$filename&manual_sync'>$filename</a> ";
                // stat
                if (isset($_GET['stat']) and $_GET['stat']=='size')
                    echo "(" . filesize($filename) . ")";
                elseif (isset($_GET['stat']) and $_GET['stat']=='md5')
                    echo "(" . md5_file($filename) . ")";

                echo "</td>";

                if (is_dir($p_directory) and is_writable($p_directory)) {

                    $project_file = "$p_directory/$filename";
                    if (basename($p_directory) != 'private') {

                        $copy_done = 2;
                        $errors = array();
                        if (isset($_GET['update_file']) and $filename == $_GET['update_file'] and $_GET['action']=='replace') {
                            unlink($project_file) ? $copy_done = copy($filename,$project_file) : $copy_done = false;

                        } elseif (isset($_GET['update_file']) and $filename == $_GET['update_file'] and $_GET['action']=='copy') {
                            $copy_done = copy($filename,$project_file);

                        } elseif (isset($_GET['update_all_files'])) {
                            if (is_link($project_file)) {
                                unlink($project_file) ? $copy_done = copy($filename,$project_file) : $copy_done = false;
                                $errors = error_get_last();
                                if ($errors and count($errors) and $errors['message']!='Undefined variable: undef_var') {
                                    echo "<br />COPY ERROR: ".$errors['type'];
                                    echo "<br />".$errors['message'];
                                }
                            }
                            else {
                                $copy_done = copy($filename,$project_file);
                                $errors = error_get_last();
                                if ($errors and count($errors) and $errors['message']!='Undefined variable: undef_var') {
                                    echo "<br />COPY ERROR: ".$errors['type'];
                                    echo "<br />".$errors['message'];
                                }
                            }
                        }
                        error_clear_last();
                    } else {
                        echo "<br /><span style='color:red'>COPY in private directory is not possible</span>";
                        $copy_done = false;
                    }

                    if (is_file($project_file)) {
                        // local file exists
                        if (is_link($project_file)) {
                            echo "<td><span style='color:violet'>IT IS A SYMLINK!</span></td>";
                            if ($copy_done == 2)
                                echo "<td>[<a href='?project={$_GET['project']}&update_file=$filename&action=replace&directory={$_GET['directory']}&manual_sync'>replace file</a>]</td>";
                            elseif ($copy_done===false)
                                echo "<td><span style='color:red'>replace symlink failed</span></td>";
                            else
                                echo "<td>replace done</td>";

                        } else {
                            if (md5_file($filename) != md5_file($project_file)) {
                                echo "<td><span style='color:orange'>The files are <a href='?project={$_GET['project']}&show_diff=$filename&directory={$_GET['directory']}&manual_sync'>DIFFERENT</a>!</span></td>";
                                if (!is_writable($project_file))
                                    echo "<td>file is not writable</td>";
                                else
                                    echo "<td>[<a href='?project={$_GET['project']}&update_file=$filename&action=copy&directory={$_GET['directory']}&manual_sync'>replace file</a>]</td>";

                            } else {
                                if ($copy_done===false)
                                    echo "<td><span style='color:red'>action failed</span></td><td></td>";
                                else
                                    echo "<td><span style='color:green'>up to date</span></td><td></td>";
                            }
                        }
                    } else {
                        // broken link
                        if (is_link($project_file) ) {
                            echo "<td><span style='color:violet'>IT IS A BROKEN SYMLINK!</span></td>";

                            if ($copy_done===false)
                                echo "<td><span style='color:red'>copy failed</span></td>";
                            elseif ($copy_done == 2) {
                                if (is_writable($project_file))
                                    echo "<td>[<a href='?project={$_GET['project']}&update_file=$filename&action=copy&directory={$_GET['directory']}&manual_sync'>copy file</a>]</td>";
                                else
                                    echo "<td>not writable</td>";
                            }
                            else
                                echo "<td><span style='color:green'>copy done</span></td>";
                        } else {
                            //missing files
                            echo "<td><span style='color:violet'>Local file is missing!</span></td>";
                            if ($copy_done===false)
                                echo "<td><span style='color:red'>copy failed</span></td>";
                            elseif ($copy_done == 2) {
                                echo "<td>[<a href='?project={$_GET['project']}&update_file=$filename&action=copy&directory={$_GET['directory']}&manual_sync'>copy file</a>]</td>";
                            }
                            else
                                echo "<td><span style='color:green'>copy done</span></td>";

                        }
                    }
                } else {
                    echo "<td>$p_directory not writable</td><td></td>";

                }
                echo "</tr>";
            }
            echo "</tbody></table>";
        } else {
            echo "$s_directory directory does not exists or not readable.";
        }


    } else {
        echo "<br><a class='pure-button button-success' onclick='wait()' href='?project={$_GET['project']}&manual_sync'>Manual sync</a> &nbsp; <a class='pure-button button-warning' onclick='wait()' href='?project={$_GET['project']}&auto_sync'>Automatic sync</a></br>";
    }

    echo "<h2><br>Project updates<hr></h2>";
    chdir(OB_ROOT_SITE);
    $contents = yaml_parse_file('supervisor.yml');



    if (isset($_GET['manual_update'])) {

        $uv = (isset($_GET['update_version'])) ? "&update_version='".$_GET['update_version']."'" : '';
        $update_version = (isset($_GET['update_version'])) ? $_GET['update_version'] : '';
        echo "<form>
            <input type='hidden' name='manual_update'>
            <input type='hidden' name='show_completed_updates'>
            <input type='hidden' name='project' value='{$_GET['project']}'>
            <a class='pure-button button-warning' onclick='wait()' href='?project={$_GET['project']}&auto_update$uv'>Automatic update</a>";
        echo " <a class='pure-button pure-button-primary' onclick='wait()' href='?project={$_GET['project']}&show_completed_updates&manual_update'>Show completed updates</a>";
        echo " <input id='update-version' type='text' name='update_version' value='$update_version' pattern='<?[0-9]+\.[0-9]+\.[0-9]+' placeholder='$next_app_version'><button type='submit'>Set update version</button></form><br>";

        $out = array();
        foreach ($contents as $revision) {

            $app_version = '<5.2.13';


            #var_dump($revision);
            #echo "<br>";
            $connection = '';
            extract($revision, EXTR_OVERWRITE);
            $project = $_GET['project'];

            $update_timestamp = strtotime($date);

            if (isset($_GET['update_version']) and $app_version != $_GET['update_version']) continue;

            if (!isset($out[$app_version])) {
                $out[$app_version] = array();
            }

            if (!is_array($commands)) $commands = array($commands);

            if ($database != 'project') {
                $commands = array();
                $date = "";
                $app_version = "";
                $type = "";
                $title = "";
            }
            if ($database == 'project') {
                $ul = array("title"=>$title, "date"=>$date, "type"=>$type, "database"=>$database, "commands"=>array(), "c_commands"=>array());
                #echo ":$title: <br>";
                #var_dump($commands);
                #echo "<br>";

                for($i = 0; $i<count($commands); $i++) {
                    if ($commands[$i] == '' or $commands[$i] == null) continue;
                    $cmd_id = $i+1;
                    $c = 0;

                    # Magic words substitutions
                    $commands[$i] = preg_replace('/%project%/',$_GET['project'],$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_user%/',biomapsdb_user,$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_pass%/',biomapsdb_pass,$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_host%/',biomapsdb_host,$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_name%/',biomapsdb_name,$commands[$i]);
                    $commands[$i] = preg_replace('/%biomapsdb_port%/',biomapsdb_port,$commands[$i]);
                    $displayed_command = preg_replace("/%/",'&#37;',$commands[$i]);

                    $cmd = sprintf("SELECT status FROM supervisor WHERE database=%s AND revision=%s AND sqlid=%s AND project=%s",quote(trim($database)),quote(trim($title)),quote($cmd_id),quote($project));
                    $res = pg_query($BID,$cmd);

                    if (pg_num_rows($res)) {
                        $row = pg_fetch_assoc($res);
                        if ($row['status']=='f') {
                            $ul["commands"][] = sprintf("<li><span style='color:red'>Failed:</span> <a href='?TYPE=$type&REVISION=$title&PROJECT=$project&DATABASE=$database&SQLC=$cmd_id&VIEWSQL=%s'>%s</a></li>",base64_encode($commands[$i]),$displayed_command);
                        } else {
                            $ul["c_commands"][] = sprintf("<li><span style='color:green'>Done:</span> <a href='?TYPE=$type&REVISION=$title&PROJECT=$project&DATABASE=$database&SQLC=$cmd_id&VIEWSQL=%s'>%s</a></li>",base64_encode($commands[$i]),$displayed_command);
                        }
                    } else {
                        $ul["commands"][] = sprintf('<li><span style="color:darkorange">Not yet run:</span> %1$d. <a href="?TYPE=%7$s&REVISION=%2$s&PROJECT=%3$s&DATABASE=%4$s&SQLC=%1$d&VIEWSQL=%5$s&CONNECTION=%8$s">%6$s</a></li>',
                            $cmd_id,$title,$project,$database,base64_encode($commands[$i]),$commands[$i],$type,$connection );
                #echo "-$title- <br>";
                    }
                }
                $out[$app_version][] = $ul;
            }
        }

        $show_completed = (isset($_GET['show_completed_updates'])) ? true : false;

        foreach ($out as $k => $v) {
            #$c = 0;
            #foreach($v as $update) {
            #    if (count($update["commands"])) {
            #        $c++;
            #    }
            #}

            #if ($show_completed)
            #    echo "<b>Updates for $k</b><br>";


            // Mark all update as DONE which older then the last app version
            if (!$show_completed and version_compare($k, $current_app_version) <= 0) continue;

            foreach($v as $u) {
                if (count($u["commands"])) {
                    echo "<h3>{$u["title"]} v.$k ({$u['date']})</h3><h4 class='command-type'>{$u["type"]} update on {$u["database"]}</h4>";

                    echo "<ul style='list-style-type:none;font-size:80%;line-height:200%'>";

                    foreach ($u["commands"] as $commands) {
                        echo $commands;
                    }
                    if ($show_completed)
                        foreach ($u["c_commands"] as $commands) {
                            echo $commands;
                        }
                    echo "</ul>";
                } elseif(count($u['c_commands']) and $show_completed) {
                    echo "<h3 class='done-title'>{$u["title"]}</h3><h4 class='command-type'>{$u["type"]} update on {$u["database"]}</h4>";

                    echo "<ul style='list-style-type:none;font-size:80%;line-height:200%'>";

                    foreach ($u["c_commands"] as $commands) {
                        echo $commands;
                    }
                    echo "</ul>";
                }
                else {
                    if ($u["type"] == "files")
                    echo "<b>File updates for v.$k at {$u['date']}.</b><br>";
                }
            }
        }
    } else {
        echo "<br><a class='pure-button button-warning' onclick='wait()' href='?project={$_GET['project']}&auto_update'>Automatic update</a></br>";
        echo "<br><a class='pure-button button-success' onclick='wait()' href='?project={$_GET['project']}&manual_update'>Manual update</a></br>";

    }
}

/*
            $files = scandir(getenv("OB_LIB_DIR").'../templates/initial/images/');
            foreach($files as $f) {
                if (is_file(getenv("OB_LIB_DIR")."../templates/initial/images/$f")) {
                    copy(getenv("OB_LIB_DIR")."../templates/initial/images/$f","$new_project_dir/images/$f") ? $e+=1 : print "<span class='err'>$f err</span><br>";
                }
            }
 */
?>
</body>
</html>
