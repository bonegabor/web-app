
$(document).ready(function() {
    /* Drop profile */
    $('body').on('click','#drop_my_account',function(e){
        e.preventDefault();
        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog("option", "title", "Drop OBM Account");
        $( "#dialog-confirm" ).dialog({"buttons":[
            {
                text: obj.confirmation_yes,
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post(projecturl+'includes/update_profile.php', {name:'dropmyaccount',text:1 },
                        function(data){
                            var retval = jsendp(data);
                            if (retval['status']=='error') {
                                alert(obj.confirmation_email_failed);
                            } else if (retval['status']=='fail') {
                                console.log(retval['data']);
                                alert(obj.confirmation_email_failed);
                            } else if (retval['status']=='success') {
                                alert(obj.confirmation_email_sent);
                            }
                    }); 
                    $( this ).dialog( "close" );
                }
            },
            {
                text: obj.confirmation_no,
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ], "height":200});
        $( "#dialog-confirm" ).html( "<div style='font-size:150%;color:red'>"+obj.drop_profile_confirm_text+"</div>" );
        $( "#dialog-confirm" ).height('auto');

    });

    //profile setting personal options
    $("body").on('click',".profile-settings-toggle",async function(){
        try {
            const option_name = $(this).prop('id');
            
            $(this).find('i').toggleClass('fa-toggle-on').toggleClass('fa-toggle-off');    
            $(this).toggleClass('button-success').toggleClass('button-passive');    
            
            var e = ($(this).find('i').hasClass('fa-toggle-on'));
            
            const data = await $.post('ajax',{
                action: 'set_option_bool',
                option_name: option_name,
                option_value: e
            });
            
            var retval = jsendp(data);
            if (retval['status']=='error' || retval['status']=='fail') {
                throw retval['message'];
            }
        }
        catch (err) {
            alert(err);
        }
        //$('#tab_basic').load(projecturl+'includes/profile.php?options=get_own_profile');
    });

    /* show saved queries page on profile
     * drop selected queries
     * */
    $("#tab_basic").on("click","#dropsq",function(e) {
        var d = [];
        $(".dropsq:checked").each(function() {
            d.push(this.value);
        });
        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog("option", "title", "Saved query");
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: obj.str_yes_sure,
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'dropsq':d},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['message']);
                            console.log(retval['data']);
                        } else if (retval['status']=='success') {
                            
                            $('#tab_basic').load(projecturl+'includes/profile.php?options=sharedqueries');
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: obj.str_no_no,
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).text( "Really want to drop the selected "+d.length+" queries?" );
        $( "#dialog-confirm" ).height('auto');
    });

    /* show saved results page on profile
     * drop selected results
     * */
    $("#tab_basic").on("click","#droprq",function(e) {
        var d = [];
        $(".droprq:checked").each(function() {
            d.push(this.value);
        });
        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog("option", "title", "...");
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: "Yes, I'm sure!",
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'droprq':d},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['message']);
                            console.log(retval['data']);
                        } else if (retval['status']=='success') {
                            
                            $('#tab_basic').load(projecturl+'includes/profile.php?options=savedresults');
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: "Oh, no!",
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).text( "Really want to drop the selected "+d.length+" results?" );
        $( "#dialog-confirm" ).height('auto');
    });

    // profile page - drop own imports
    $("#body").on("click","#dropiups",function(e) {
        var d = [];
        $(".dropiups:checked").each(function() {
            d.push(this.value);
        });
        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog("option", "title", obj.str_intr_uploads);
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: obj.str_yes_sure,
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'drop_import':d},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['message']);
                            console.log(retval['data']);
                        } else if (retval['status']=='success') {
                            
                            $('#tab_basic').load(projecturl+'includes/profile.php?options=saveduploads');
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: obj.str_no_no,
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).text( "Really want to drop the selected "+d.length+" interrupted uploads?" );
        $( "#dialog-confirm" ).height('auto');
    });

    // save import as template
    $("#body").on("click","#saveiups",function(e) {
        var d;
        var i;
        $(".dropiups:checked").each(function() {
            d = this.value;
            i = $("#name_"+this.value).val();
            $.post("ajax",{'save_import_template':d,'name':i},function(data){
            });
        });
        //setTimeout(function(){location.reload();},1000);
    });

    /* profile api key manage */
    $("#body").on('click','.dropapikey',function() {
        var widgetId = $(this).attr('id').substring($(this).attr('id').indexOf('_')+1,$(this).attr('id').length);
        var tokenType = $(this).data('tokentype');
        $.post("ajax",{'drop_apikey':widgetId,'tokenType':tokenType},function(data){
            //$('#tab_basic').load('includes/profile.php?',function(result){});
            $('#tab_basic').load(projecturl+'includes/profile.php?options=apikeys');
        });
    });



    /* 
     *
     * box_load_selection module's functions 
     * move them to its js file !
     *
     *
     * */

    /* drop polygon */
    $("body").on('click','.droppolygon',function(){
        $(this).css('background-color','gray');

        var id = $(this).attr('id');
        var row_id = id.substring(0, id.indexOf('_')).match(/\d+$/)[0];
        var widgetId = id.substring(id.indexOf('_')+1, id.length);
        var name = $("#ope-name"+row_id+"_"+widgetId).val();

        var isOpen = $( "#dialog-confirm" ).dialog( "isOpen" );
        if(!isOpen) $( "#dialog-confirm" ).dialog( "open" );
        $( "#dialog-confirm" ).dialog("option", "title", "Polygon");
        $( "#dialog-confirm" ).dialog( "option", "buttons",   [
            {
                text: obj.str_yes_sure,
                icons: { primary: "ui-icon-trash" },
                click: function() {
                    $.post("ajax",{'polygon_drop':widgetId},function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['message']);
                            console.log(retval['data']);
                        } else if (retval['status']=='success') {
                            $(".z_"+widgetId).hide();
                        }
                    });
                    $( this ).dialog( "close" );
                }
            },
            {
                text: obj.str_no_no,
                icons: { primary: "ui-icon-cancel" },
                click: function() {
                    $( this ).dialog( "close" );
                }
            }   
        ]);
        $( "#dialog-confirm" ).text( "Really want to drop `"+name+"` polygon?" );
        $( "#dialog-confirm" ).height('auto');
    });
    /* FILTERBOX
     * This called when somebody read shp files using filterbox_load_layer()
     * steps:
     * 1.: move files to a temporary place
     * 2.: read data from files
     * 3.: draw polygon
     * */
    $('body').on('click', '#shp_upload', function(e){
        e.preventDefault();
        //get fa icon id
        var xicon = $(this).find('i').attr('id');
        //we are get only the first element of the form 'form'!
        var formData = new FormData()
        var shp_label = $('#shp_label').val();

        var files = document.getElementById("shpfile").files;
        for(var i in files) {
            formData.append("files[]", files[i]);
        }
        formData.append("load-files", 1);
        //formData.append("filename", 'shpfile');
        formData.append("ext", 'shp');
        $.ajax({
            url: 'ajax',
            type: 'POST',
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            success: function (data) {
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    alert(retval['message']);
                } else if (retval['status']=='fail') {
                    alert(retval['data']);
                } else if (retval['status']=='success') {
                    //homokórás, forgó ikon kell ide...
                    //$("#"+xicon).removeClass('fa-upload');
                    //$("#"+xicon).addClass('fa-spinner fa-pulse');
                    $.post("ajax", {'shaperead':1,'shp_label':shp_label},
                    function(data){
                        var retval = jsendp(data);
                        if (retval['status']=='error') {
                            alert(retval['message']);
                        } else if (retval['status']=='fail') {
                            alert(retval['message']);
                            console.log(retval['data']);
                        } else if (retval['status']=='success') {
                            alert('Processing shp OK!');
                        }

                        $('#tab_basic').load(projecturl+'includes/profile.php?options=get_own_profile');
                        //nem csinálja meg a szelekciót, csak felrajzolja!!!
                        //zoomol
                        //features_length = drawPolygonFromWKT(data,1);
                        //lbuf_reread();
                    });
                }
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        });
        return false;
    });

    $('body').on('input', '#ds-dataset', function(e){
        //console.log(dynamic_content);

        if (isNaN($(this).val())) {
            //console.log('Not number');
            // Not a number
            //$(".ds-autohide").show();
                //$(this).prop('required',true);
            $(".ds-autohide-input").each(function() {
                $(this).prop('disabled',false);
                $(this).prop('required',true);
            
            });
        } else {
            //console.log('Is number');
            // It is a number
            //$(".ds-autohide").hide();
            $(".ds-autohide-input").each(function() {

                $(this).prop('disabled',true);
                $(this).prop('required',false);
                //$(this).removeAttr('required');
            });
            //var jw1 = dynamic_content.jw1;
            //var cds = jw1[$(this).val()];
            //$('#tab_basic').find('#ds-description').html(cds.latestVersion.metadataBlocks.citation.fields[3].value[0].dsDescriptionValue.value);
        }
    });

});
