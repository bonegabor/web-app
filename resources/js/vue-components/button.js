const vButton = Vue.component('v-button', {
  props: {
    fa: String,
    classes: String,
    title: String,
    onClick: {
      type: Function,
      required: true
    },
    type: {
        type: String,
        default: 'button'
    }
  },
  template: `
    <button :class="['pure-button ' + classes]" @click="onClick" :type="type" :title="[title]">
      <i v-if="fa" :class="['fa', 'fa-' + fa ]"> </i> 
      <slot></slot>
    </button>
  `
})

export { vButton }