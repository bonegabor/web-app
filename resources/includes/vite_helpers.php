<?php
// Helpers here serve as example. Change to suit your needs.
const VITE_HOST = 'http://localhost:3000';
$pages = [
    'taxonomy' => ['path' => getenv('PROJECT_DIR') . 'local/includes/admin_pages/taxonomy/', 'url' => protocol() . '://'. URL . '/local/includes/admin_pages/taxonomy/']
];
$current_page = $_SESSION['current_page']['includes'];

$page = $pages[$current_page];
// For a real-world example check here:
// https://github.com/wp-bond/bond/blob/master/src/Tooling/Vite.php
// https://github.com/wp-bond/boilerplate/tree/master/app/themes/boilerplate

// you might check @vitejs/plugin-legacy if you need to support older browsers
// https://github.com/vitejs/vite/tree/main/packages/plugin-legacy



// Prints all the html entries needed for Vite

function vite(string $entry): string
{
    return "\n" . jsTag($entry)
        . "\n" . jsPreloadImports($entry)
        . "\n" . cssTag($entry);
}


// Some dev/prod mechanism would exist in your project

function isDev(string $entry): bool
{
    return (defined('DEV_MODE') && constant('DEV_MODE') === true);
}


// Helpers to print tags

function jsTag(string $entry): string
{
    $url = isDev($entry)
        ? VITE_HOST . '/' . $entry
        : assetUrl($entry);

    if (!$url) {
        return '';
    }
    if (isDev($entry)) {
        return '<script type="module" src="' . VITE_HOST . '/@vite/client"></script>' . "\n"
            . '<script type="module" src="' . $url . '"></script>';
    }
    return '<script type="module" src="' . $url . '"></script>';
}

function jsPreloadImports(string $entry): string
{
    if (isDev($entry)) {
        return '';
    }

    $res = '';
    foreach (importsUrls($entry) as $url) {
        $res .= '<link rel="modulepreload" href="'
            . $url
            . '">';
    }
    return $res;
}

function cssTag(string $entry): string
{
    // not needed on dev, it's inject by Vite
    if (isDev($entry)) {
        return '';
    }

    $tags = '';
    foreach (cssUrls($entry) as $url) {
        $tags .= '<link rel="stylesheet" href="'
            . $url
            . '">';
    }
    return $tags;
}


// Helpers to locate files

function getManifest(): array
{
    global $page;

    $content = file_get_contents($page['path'] . '/.vite/manifest.json');
    return json_decode($content, true);
}

function assetUrl(string $entry): string
{
    global $page;

    debug($page, __FILE__, __LINE__);
    $manifest = getManifest();

    return isset($manifest[$entry])
        ?  $page['url'] . $manifest[$entry]['file']
        : '';
}

function importsUrls(string $entry): array
{
    global $page;
    $urls = [];
    $manifest = getManifest();

    if (!empty($manifest[$entry]['imports'])) {
        foreach ($manifest[$entry]['imports'] as $imports) {
            $urls[] = $page . $manifest[$imports]['file'];
        }
    }
    return $urls;
}

function cssUrls(string $entry): array
{
    global $page;
    $urls = [];
    $manifest = getManifest();

    if (!empty($manifest[$entry]['css'])) {
        foreach ($manifest[$entry]['css'] as $file) {
            $urls[] = $page['url'] . $file;
        }
    }
    return $urls;
}
