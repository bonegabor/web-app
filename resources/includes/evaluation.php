<?php
/* Ajax function
 * IMPROVEMENTS NEEDED!!!
 *
 * */

require_once(getenv('OB_LIB_DIR').'db_funcs.php');
session_start();

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

track_visitors('evaluation');

pg_query($ID,'SET search_path TO system,public');
pg_query($GID,'SET search_path TO system,public');

#$_SESSION['allow-validation'] = 1;
#if (!isset($_SESSION['allow-validation'])) return;

$M = new Messenger();

// ez a target megoldás hekkelhető...
$target         = $_POST['target'];
$post_id        = $_POST['id'];
$value          = $_POST['value'];
$post_comment   = $_POST['comment'];
$rel_id         = "NULL";

!isset($_SESSION['Tid'])  ? $mfid = "NULL":  $mfid = $_SESSION['Tid'];
!isset($_SESSION['Tname'])? $mfname = "NULL":$mfname = $_SESSION['Tname'];

// Eval types
$acce = array('+','-','ok','accept','refuse','m','z','p');

$url = URL;

$eval_table = $_SESSION['current_query_table'] ?? PROJECTTABLE;

// Evaluation of data
if (in_array($value,$acce)) {
    if ($target=='data') {
        $table = $eval_table;
        //unset($_SESSION["slide"][slide."$post_id"]);
        $cmd = sprintf("SELECT uploader_id FROM %s LEFT JOIN system.uploadings ON (obm_uploading_id=id) WHERE obm_id=%d",$eval_table,preg_replace('/[^0-9]+/','',$post_id));
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $R = new Role($row['uploader_id']);
            $data_owner = ($R->is_user) ? $R->get_members() : false;
        }
    } 
    elseif ($target == 'upload') {
        // mode levelben kell lenni a data vote-hoz
        //if (!rst('mod')) exit;
        $table = 'uploadings';

        $cmd = sprintf("SELECT uploader_id FROM system.uploadings WHERE id=%d",preg_replace('/[^0-9]+/','',$post_id));
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);
            $R = new Role($row['uploader_id']);
            $data_owner = ($R->is_user) ? $R->get_members() : false;
        }
    } 
    elseif ($target == 'user') {
        if(!isset($_SESSION['Tid'])) exit;
        $table = 'users';
        $data_owner = new User($post_id, 'user');
        $mail_text = str_follow_this_link.": <a href='http://$url/profile/{$data_owner->user_id}/'>".str_profile."</a>";
        $post_id = $data_owner->user_id;
    }
    else {
        $user_id = '';
        log_action('Eval what?',__FILE__,__LINE__);
    }      
    
    //the user should not receive notification if he was the author of the comment 
    if (is_object($data_owner)) {
        $recipients['owner'] = ($mfid != $data_owner->user_id) ? [$data_owner] : [];
        $recipients['commenters'] = (get_option('comment_notification_to_all_commenters') !== 'false')
        ? array_filter(get_commenters($post_id, $table), function($u) use ($mfid, $data_owner) { 
            return ($u->user_id != $mfid && $u->user_id != $data_owner->user_id); 
        })
        : [];
        $hash = $data_owner->hash;
    } else {
        $hash = '';
        $recipients = array();
    }
    
    $words = compact('post_id', 'eval_table', 'mfname', 'post_comment', 'hash');
    $reputation_cost = 0;

    if ($value == 'ok') $val = 0;
    elseif($value=='accept') $val = '0';
    elseif($value=='refuse') $val = '0';
    elseif($value=='-') { $val = '-1'; }#$reputation_cost = -0.1; $user_comment = 'reputation cost due to negative evaluation'; }
    elseif($value=='+') $val = '+1';
    elseif($value=='m') { $val = '-1'; }#$reputation_cost = -0.1; $user_comment = 'reputation cost due to negative evaluation'; }
    elseif($value=='z') $val = '0';
    elseif($value=='p') $val = '+1';

    /*if ($value=='accept') {
            $post_comment .= '<div class=\'accepted\'>accepted</div>';
    }
    elseif ($value=='refuse') {
            $post_comment .= '<div class=\'refused\'>refused</div>';
    }*/


    if ($val === '-1' and $post_comment == '') {
        $reputation_cost = '-0.5';
        $user_comment = "reputation cost due to negative evaluation without comment";
    }

    if (isset($_SESSION['evaluates'])) {
        if(in_array("$mfid-$table-$post_id",$_SESSION['evaluates'])) {
            echo common_message('error',t(str_itisalready_evaluated)."!");
            #exit;
        }
    } else { 
        $_SESSION['evaluates'] = array();
    }

    //if ($table == 'users') {
    //  $schema = "public";
    //  $db = $BID;
    //  $cmd = sprintf("INSERT INTO %s.evaluations (\"table\",row,user_id,user_name,valuation,comments,related_id) VALUES ('%s',%d,%d,'%s','%d',%s,%s)",
    //        $schema, $table, $post_id, $mfid, $mfname, $val, quote($post_comment), quote($rel_id));
    //}
    if ($reputation_cost !== 0) {
        
        $cmd = sprintf("INSERT INTO public.evaluations (\"table\",row,user_id,user_name,valuation,comments,related_id,related_project,related_table) VALUES ('users',%d,%d,'%s','%f',%s,%s,%s,%s)",
            $_SESSION['Tid'], $mfid, $mfname, $reputation_cost, quote($user_comment), quote($post_id),quote(PROJECTTABLE),quote($table));
        $res = pg_query($BID,$cmd);    
        //
        //Ehhez kell még majd hogy csak még értékeletlen adatnál legyen ez a költség. AZtán ha a negatív értékelés meg lett erősítve, akkor az értékelő kétszeresen kapja vissza a pontját.

    }

    if ($table != 'users') {
        $schema = "system";
        $db = $GID;
        $cmd = sprintf("INSERT INTO %s.evaluations (\"table\",row,user_id,user_name,valuation,comments,related_id) VALUES ('%s',%d,%d,'%s','%d',%s,%s)",
            $schema, $table, $post_id, $mfid, $mfname, $val, quote($post_comment), quote($rel_id));
    
    }

    
    $res = pg_query($db,$cmd);    
    if ($row = pg_affected_rows($res)) {
        $from = new User($_SESSION['Tid']);
        
        $_SESSION["evaluates"][] = "$mfid-$table-$post_id";
        echo common_message('ok',t(str_thx_for_opinion).'!');

        foreach ($recipients as $group => $recips) {
            foreach ($recips as $to) {
                
                $M->send_comment_notification(
                    $from, 
                    $to, 
                    "{$target}_evaluation_{$group}",
                    $words);
            }
        }
    } 
    else {
        echo common_message('fail','SQL error');
    }
        
    /*if ($target=='data') {
            
            $ID_COL=$_SESSION['st_col']['ID_C'];

            if (!isset($_SESSION['Tname'])) $name = 'unknown';
            else $name =  $_SESSION['Tname'];

            $cmd = sprintf("UPDATE %s SET validation_=array_append(validation_,'%s'), modifier_id='%d',comments_=array_append(comments_,'$val evalulation [$name]') 
                WHERE %s='%d'",PROJECTTABLE,"$val",$mfid,$ID_COL,$post_id);
            if ( PGsqlcmd($ID,$cmd) ) {
                unset($_SESSION["slide$post_id"]);
                print 'ok';
                return;
            } else {
                print 'SQL err';
                return;
            }

    } elseif ($target == 'upload') {
            //only for logined users
            if(!isset($_SESSION['Tid'])) {
                include('logout.php');
                exit;
            }

            $cmd = sprintf("UPDATE system.uploadings SET validation=array_append(validation,'%s') 
                WHERE id='%d'","$value"."1",$post_id);
            if ( PGsqlcmd($ID,$cmd) ) {
                print 'ok';
                return;
            } else {
                print 'SQL error';
                return;
            }
    }*/
    exit;
}
echo common_message('fail',"Invalid request");
exit;

/* Returns an array of Users who have commented the data with:
 *  @param $id integer The id of the data record
 *  @param $table string The name of the record's table
 */
function get_commenters($id, $table) {
    global $GID, $BID;

    $u = new userdata($id, 'hash');
    
    $schema = ($table == 'users') ? "public" : "system";
    $db = ($table == 'users') ? $BID : $GID;
    if ($table == 'users') {
        $db = $BID;
        $cmd = sprintf("SELECT DISTINCT user_id FROM %s.evaluations WHERE \"table\" = %s AND user_id = %s;", $schema, quote($table), $u->get_userid($id) );

    } else {
        $db = $GID;
        $cmd = sprintf("SELECT DISTINCT user_id FROM %s.evaluations WHERE \"table\" = %s AND \"row\" = %d AND user_id != 0;", $schema, quote($table), $u->get_userid($id) );

    }
    
    if (! $res = pg_query($db, $cmd)) {
        log_action('get_commenters query error: ' . $cmd,__FILE__,__LINE__);
        return false;
    }
    $results = pg_fetch_all($res);
    if ( empty($results) ) {
        return [];
    }
    $users = array_map(function($u) {
        return new User($u['user_id']);
    }, $results);
    
    return $users;
}
?>
