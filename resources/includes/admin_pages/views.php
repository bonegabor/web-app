<?php
$table = PROJECTTABLE;

$cmd = sprintf('SELECT schemaname as table_schema, viewname as table_name, definition as view_definition, viewowner
    FROM pg_catalog.pg_views
    WHERE schemaname NOT IN (\'pg_catalog\', \'information_schema\')
    AND schemaname IN (\'%1$s\',\'public\')
    AND (viewowner = \'%1$s_admin\' OR (viewname LIKE \'%1$s_%2$s\' OR viewname=\'%1$s\'))
    ORDER BY schemaname, viewname',
    PROJECTTABLE, '%');


$res = pg_query($ID,$cmd);
echo "<button class='main_table_refresh' id='dbcols' data-subpage='views'><i class='fa fa-refresh'></i> ".t(str_refresh)."</button>";
echo "<h4>Edit views</h4>";
echo "<ul style=''>";
$x = 1;
while ($row = pg_fetch_assoc($res)) {
    $col = ($row['viewowner'] != PROJECTTABLE.'_admin') ? 'color:red' : '';
    echo sprintf('
    <li style=\'list-style-type:none\'>
        <span id=\'hn-%4$s\' data-name=\'%1$s.%2$s\' style=\'cursor:pointer\' onclick=\'toggle_visible("h%4$s");loadCodeEditor("h%4$se","sql")\'><i class=\'fa fa-lg fa-plus-square-o\'></i></span>
            
        <input id=\'hn-%4$s-newname\' title=\'Click to edit name\' value=\'%1$s.%2$s\' style=\'border:none;'.$col.'\'>
        <span title=\'Change name\' class=\'span-button\' style=\'vertical-align:unset\' onClick=\'editViewName("hn-%4$s")\'><i class=\'fa fa-pencil\'></i></span> &nbsp;  
        <span title=\'Drop view: %1$s.%2$s\' class=\'span-button\' style=\'vertical-align:unset\' onClick=\'dropView("hn-%4$s")\'><i class=\'fa fa-trash\'></i></span>
        <div style=\'display:table-cell;\'>
        <div id=\'h%4$s\' class=\'hidden\' style=\'margin-left:20px;background-color:#3e3e3e;padding:1px;border-radius:3px\'>
            <button class=\'pure-button button-warning\' onclick=\'saveEditor("h%4$se");saveView("h%4$se","hn-%4$s")\'>'.t(str_save).'</button>
            <textarea id=\'h%4$se\' style=\'max-width:500px;\' >%3$s</textarea></div></div></li>',
            $row['table_schema'],$row['table_name'],$row['view_definition'],$x);
    $x++;
}

$cmd = sprintf('SELECT * FROM pg_matviews 
    WHERE schemaname IN ( \'%1$s\',\'public\' ) AND (matviewname LIKE \'%1$s_%2$s\' OR matviewname=\'%1$s\' OR matviewowner=\'%1$s_admin\') ORDER BY matviewname',
    PROJECTTABLE,'%');

$res = pg_query($ID,$cmd);
$x = 1;
while ($row = pg_fetch_assoc($res)) {

    $col = ($row['matviewowner'] != PROJECTTABLE.'_admin') ? 'color:red' : '';
    echo sprintf('
    <li style=\'list-style-type:none\'>
        <span id=\'hm-%4$s\' data-name=\'%1$s.%2$s\' style=\'cursor:pointer\' onclick=\'toggle_visible("hmx%4$s");loadCodeEditor("hmx%4$se","sql")\'><i class=\'fa fa-lg fa-plus-square-o\'></i></span>
            
        <input id=\'hm-%4$s-newname\' title=\'Click to edit name\' value=\'%1$s.%2$s\' style=\'border:none;'.$col.'\'>
        <span title=\'Change name\' class=\'span-button\' style=\'vertical-align:unset\' onClick=\'editViewName("hm-%4$s")\'><i class=\'fa fa-pencil\'></i></span> &nbsp;  
        <span title=\'Refresh materealized view\' class=\'span-button\' style=\'vertical-align:unset\' onClick=\'refreshView("hm-%4$s")\'><i class=\'fa fa-refresh\'></i></span> &nbsp;  
        <span title=\'Drop view: %1$s.%2$s\' class=\'span-button\' style=\'vertical-align:unset\' onClick=\'dropView("hm-%4$s")\'><i class=\'fa fa-trash\'></i></span>
        <div style=\'display:table-cell;\'>
        <div id=\'hmx%4$s\' class=\'hidden\' style=\'margin-left:20px;background-color:#3e3e3e;padding:1px;border-radius:3px\'>
            <button class=\'pure-button button-warning\' onclick=\'saveEditor("hmx%4$se");saveView("hmx%4$se","hm-%4$s")\'>'.t(str_save).'</button>
            <textarea id=\'hmx%4$se\' style=\'max-width:500px;\' >%3$s</textarea></div></div></li>',
            $row['schemaname'],$row['matviewname'],$row['definition'],$x);
    $x++;
}



echo "</ul>";

// List of joining tables
$cmd = sprintf("SELECT t.table_schema,t.table_name
FROM information_schema.tables t 
LEFT JOIN information_schema.role_table_grants g ON (t.table_name=g.table_name AND t.table_schema=g.table_schema)
WHERE 
    t.table_schema not in ('information_schema','pg_catalog','temporary_tables','system') AND
    grantee IN ('%s_admin', 'PUBLIC') AND 
    privilege_type='SELECT'
ORDER BY t.table_schema,t.table_name",PROJECTTABLE);
$res = pg_query($GID,$cmd);
$options = array("<option></option>");
while ($row = pg_fetch_assoc($res)) {
    $options[] = "<option>".$row['table_schema'].".".$row['table_name']."</option>";
}

echo "<h4>Create view</h4>";

$tables = getProjectTables(true);

echo "<div style='display:table'>";
echo "<div style='display:table-row'>";
echo "<div style='display:table-cell;text-align:right'>";
echo "<select id='schema'><option>public</option><option>".PROJECTTABLE."</option></select> .";
echo "</div>"; // table-cell
echo "<div style='display:table-cell;padding:10px'>";
echo "<input id='view_name' placeholder='view name' pattern='[A-Za-z_0-9]{32}' >";
echo "</div>"; // table-cell
echo "</div>"; // table-row

echo "<div style='display:table-row'>";
echo "<div style='display:table-cell;padding:10px'>";
echo "<label for='mainTable'>Choose a base table: </label>";
echo "<select id='mainTable'><option></option>".selected_option($tables,'')."</select>";
echo "</div>"; // table-cell

echo "<div style='display:table-cell;padding:10px'>";
echo "<label for='selectJoinedTable'>Choose a joining table: </label>";
echo sprintf("<select id='selectJoinedTable'>%s</select>",implode("",$options));
echo "</div>"; // table-cell
echo "</div>"; // table-row

echo "<div style='display:table-row'>";
echo "<div style='display:table-cell;padding:10px'>";
echo "<label for='selectBaseColumn'>Select columns from the base table: </label>";
echo "<select id='selectBaseColumn' multiple style='width:100%'></select>";
echo "</div>"; // table-cell

echo "<div style='display:table-cell;padding:10px'>";
echo "<label for='selectJoinedColumn'>Select columns from the joined table: </label>";
echo "<select id='selectJoinedColumn' multiple style='width:100%'></select>";
echo "</div>"; // table-cell
echo "</div>"; // table-row
echo "</div>"; // table


$join_table = "X";

# View template
$viewcmd = sprintf('CREATE VIEW xxx.xxx AS
    SELECT 
-- COLUMNS FROM THE MAIN TABLE
xxx,
-- COLUMNS FROM THE JOINED TABLE
-- MAIN TABLE REFERENCE
    FROM xxx.xxx d
-- JOIN STATEMENT
-- LEFT JOIN xxx vf ON (d.X = vf.X);');

echo "<div style='display:table'>";
echo "<div style='display:table-row'>";
echo "<div style='display:table-cell;padding:10px'>";
echo "Connecting tables using these columns:<br>";
echo "<select id='mainTableJoinColumn'></select> = ";
echo "<select id='joined1TableJoinColumn'></select><br>";

$cmd_schema = sprintf('SELECT 1
  FROM information_schema.tables
  WHERE table_schema = %1$s',
        quote($table));
$create_schema = "";
$res = pg_query($ID,$cmd_schema);
if (!pg_num_rows($res)) {
    $create_schema = sprintf('- The %1$s schema is created.<br>',$table); 
}

echo "<br>Should we create this view as a table that will replace the original table in the web application? "; 
echo "Yes: <input type='checkbox' id='moveTable' value='on'><br>";
echo "The following actions will take place:<br>";
echo sprintf('
  %2$s
  - The %1$s table will be moved to the %1$s schema.<br>
  - <i>Instead of Rules</i> for <b>insert</b>, <b>update</b> and <b>delete</b> events will be created to work on the created View as a table.',$table,$create_schema);
echo "</div>"; // table-cell
echo "</div>"; // table-row


echo "<div style='display:table-row'>";
echo "<div style='display:table-cell;padding:10px'>";
echo "<div id='loadEditor'>";
echo "<button class='pure-button button-success' id='loadEditorButton' onclick='toggle_hide(\"loadEditor\");toggle_show(\"hc2\");loadCodeEditor(\"hc2e\",\"sql\",true);'>Load editor</button>";
echo "</div>";
echo "<div id='hc2' class='hidden' style='background-color:#3e3e3e;padding:1px;border-radius:3px'>
            <button class='pure-button button-success' onclick='refreshCodeEditor(\"hc2e\",\"sql\")'>Refresh</button>
            <button class='pure-button button-warning' onclick='saveEditor(\"hc2e\");saveView(\"hc2e\",null)'>".t(str_save)."</button><br>
            <textarea id='hc2e' style='max-width:500px;'>$viewcmd</textarea></div>";

echo "</div>"; // table-cell
echo "</div>"; // table-row
echo "</div>"; // table

# Move base table to its own schema 
$cmd = "BEGIN;";
$cmd = sprintf('CREATE SCHEMA %1$s',$table);
$cmd = sprintf('ALTER TABLE %1$s SET SCHEMA %1$s',$table);
$cmd = sprintf('UPDATE header_names SET f_project_schema=%1$s 
    WHERE f_project_name=%2$s 
      AND f_project_table=%1$s',
        quote($table),PROJECTTABLE);     
    
?>
