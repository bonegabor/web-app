<?php


    echo "<div class='infotitle'>".wikilink('admin_pages.html#upload-forms',t('str_documentation'))."</div>";

    $mtable = $_GET['mtable'] ?? PROJECTTABLE;

    $st_col = st_col($mtable,'array',false);

    $modules->set_main_table($mtable);

    $st_col = st_col($mtable,'array',false);
    if (isset($_GET['editor'])) $editor = $_GET['editor']; # state clikk
    else if (isset($_GET['edit_form'])) $editor = 'edit';
    else $editor = 'edit';

    if ($editor == 'edit')
        echo "<a href='#tab_basic' data-url='includes/project_admin.php?options=upload_forms&editor=new' title='".t(str_new)."  ".str_form."' class='admin_direct_link' style='float:right;margin-right:1em;font-size:1.2em;'><i class='fa fa-gavel'></i> ".t(str_new)." ".str_form."</a>";
    else
        echo "<a href='#tab_basic' data-url='includes/project_admin.php?options=upload_forms&editor=edit' title='".t(str_edit_forms)."' class='admin_direct_link' style='float:right;margin-right:1em;font-size:1.2em'><i class='fa fa-pencil-square-o'></i> ".t(str_edit_forms)."</a>";


    if ($editor == 'edit') {
        // list available forms
        $cmd = sprintf('(SELECT form_id,form_name,form_id,active,last_mod, published_form_id, array_to_string("form_type",\'|\') as form_type 
            FROM project_forms p1 
            WHERE project_table=\'%1$s\' AND draft = true
            ORDER BY form_name)
            UNION
            (
            SELECT t1.form_id,t1.form_name,t1.form_id,t1.active,t1.last_mod, t1.published_form_id, array_to_string("form_type",\'|\') as form_type FROM project_forms t1
            JOIN 
            (
               SELECT form_name,MAX(last_mod) AS MAXDATE
               FROM project_forms
               WHERE project_table=\'%1$s\'
               GROUP BY form_name
            ) t2
            ON t1.form_name = t2.form_name
            AND t1.last_mod = t2.MAXDATE
            WHERE t1.project_table=\'%1$s\')
            ORDER BY form_name',PROJECTTABLE);

        $res = pg_query($BID,$cmd);

        echo "<table class='resultstable' style='margin-bottom:20px'><tr><th>".t(str_form)." ".str_name."</th><th></th><th>".t(str_edit)."</th><th>".t(str_state)."</th><th>".t(str_delete)."</th></tr>";
        while($row = pg_fetch_assoc($res)) {
            if ($row['active']==1) {
                $rd = t(str_ban);
                $fa = 'fa-ban';
            } else {
                $rd = t(str_activate);
                $fa = 'fa-check';
            }
            $label = $row['form_name'];
            if (preg_match('/^str_/',$label))
                if (defined($label))
                    $label = constant($label);

            $type_icons = array('file'=>'fa-file-o','web'=>'fa-table','api'=>'fa-mobile-phone fa-lg');
            $ti = array();
            if ($row['form_type'] == '') {
                $ti[] = "<span><i class='fa fa-lg fa-border fa-warning text-error'></i></span>";
            } else {
                foreach (explode('|',$row['form_type']) as $type) {
                    $ti[$type] = "<span><i class='fa fa-border {$type_icons[$type]}'></i></span>";
                }
            }
            $adcolor = ($row['active']!=1) ? 'background-color:#f5f5f5;color:#9c9c9c' : '';

            echo "<tr style='$adcolor'>
            <td><span title='From id: {$row['form_id']}'>$label</span></td>
            <td>".implode(' ',$ti)."</td>
            <td><button id='editform' data-tag='{$row['form_id']}' data-val='{$row['active']}' class='pure-button button-href'><i class='fa fa-pencil-square-o fa-lg'></i> ".t(str_edit)."</button></td>
            <td><button id='deacform' data-tag='{$row['form_id']}' data-val='{$row['active']}' class='pure-button button-href pure-u-1-1 '><i class='fa $fa fa-lg text-warning'></i> $rd</button></td>
            <td><button id='delform' data-tag='{$row['form_id']}' data-val='{$row['active']}' class='pure-button button-href'><i class='fa fa-trash fa-lg text-error'></i> ".t(str_delete)."</button></td>
            </tr>";
        }
        echo "</table>";
    }

    //form header table
    $h = "";
    $name='';
    $s1_sel = array('','','');
    $s2_sel = array('','','');
    $s3_sel = array('','','');
    $s4_sel = array('','','');
    $g = array();
    $do = array();
    $dg = array();
    $do_upl = '';
    $dg_upl = '';
    $style_mod = "disabled readonly";
    $rclass='button-passive';
    $description = "";
    $dest_table = PROJECTTABLE;
    $project_schema = 'public';
    $srid_value = '';
    $published_form_id = '';
    $form_list_group = '';
    $observationlist_time = '';
    $readonly_obsl_options = '';
    $periodic_notification_time = '';
    $draft = 'f';

    if (isset($_GET['edit_form'])) {
        $s1_values = array(0,1,2);
        $s2_values = array('file','web','api');
        $s3_values = array('false','true','force');
        $e = preg_replace('/[^0-9]/','',$_GET['edit_form']);
        $cmd = "SELECT form_name,array_to_string(\"form_type\",'|') as ft,form_access,array_to_string(\"groups\",'|') as g,description,destination_table,
                    ARRAY_TO_STRING(srid,',') as srid,array_to_string(\"data_groups\",'|') as dg, array_to_string(\"data_owners\",'|') as do, last_mod,observationlist_mode,observationlist_time,
                    tracklog_mode,periodic_notification,published_form_id,draft,f_project_schema
                FROM project_forms 
                LEFT JOIN header_names h ON (f_project_name = project_table and f_project_table=destination_table and f_project_schema=project_schema)
                WHERE form_id='$e' AND project_table='".PROJECTTABLE."'";
        $res2 = pg_query($BID,$cmd);
        $r2 = pg_fetch_assoc($res2);
        $name = $r2['form_name'];
        $published = $r2['last_mod'];
        $published_form_id = $r2['published_form_id'];
        $draft =  $r2['draft'];
        $description = $r2['description'];
        $srid_value = $r2['srid'];
        if ($r2['destination_table']!='')
            $dest_table = $r2['destination_table'];
        $project_schema = $r2['f_project_schema'];
        //valid request
        if ($name!='') {
            $h = $e;
        }
        //access selection
        $n=-1;
        $n=array_search($r2['form_access'],$s1_values);
        if($n!==false) {
            $s1_sel[$n]='selected';
            if($n==2) {
                $style_mod = "";
                $rclass = '';
            }
        }
        //type selection
        foreach(explode('|',$r2['ft']) as $ft) {
            $n=0;
            foreach($s2_values as $t) {
                if($ft==$t) {
                    $s2_sel[$n]='selected';
                }
                $n++;
            }
        }
        //observationlist_mode selection
        $n=-1;
        $n=array_search($r2['observationlist_mode'],$s3_values);
        if($n!==false) {
            if ($n == 0) {
                $readonly_obsl_options = 'disabled';
            }
            $s3_sel[$n]='selected';
        }
        $n=-1;
        $n=array_search($r2['tracklog_mode'],$s3_values);
        if($n!==false) {
            $s4_sel[$n]='selected';
        }

        $observationlist_time = $r2['observationlist_time'];
        $periodic_notification_time = $r2['periodic_notification'];

        //group selection
        $g = explode('|',$r2['g']);
        $do = explode('|',$r2['do']);
        $dg = explode('|',$r2['dg']);

        if (in_array(-2,$do)) $do_upl = 'checked';
        if (in_array(-2,$dg)) $dg_upl = 'checked';

        $cmd = sprintf("SELECT name FROM project_forms_groups WHERE project_table='%s' AND %d=ANY(form_id)",PROJECTTABLE,$e);
        $res3 = pg_query($BID,$cmd);
        if (pg_num_rows($res3)) {
            $row3 = pg_fetch_assoc($res3);
            $form_list_group = $row3['name'];
        }
    }

    //access groups
    $n = array();
    foreach ($csor as $row) {
        //$sel = "";
        //if (in_array($row['role_id'],$g)) $sel = "selected";
        //$groups .= "<option value='{$row['group_id']}' $sel>".$row['description']."</option>";
        $n[] = $row['description']."::".$row['role_id'];
    }
    $access_groups_options = selected_option($n,$g);

    //desnitaion read-access groups
    $n = array();
    foreach ($csor as $row) {
        $n[] = $row['description']."::".$row['role_id'];
    }
    $destination_read_groups_options = selected_option($n,$dg);

    //desnitaion write-access groups
    $n = array();
    foreach ($csor as $row) {
        $n[] = $row['description']."::".$row['role_id'];
    }
    $destination_write_groups_options = selected_option($n,$do);


    /* form definitions */
    # IDE JÓ LENNE VALAMI PLUGIN szerű dolog egyedi kiegészítőknek
    /*$cmd = sprintf('SELECT 1 FROM projects WHERE \'%1$s_projects\'=ANY(main_table) AND project_table=\'%1$s\'',PROJECTTABLE);
    if(pg_query($BID,$cmd)){
        #$cmd = sprintf('SELECT %1$s_projects.id as id,nev,modszer FROM %1$s_projects LEFT JOIN %1$s_methods ON (%1$s_methods.id=ANY(modszerek)) GROUP BY nev,modszer,%1$s_projects.id',PROJECTTABLE);
        $cmd = sprintf('SELECT %1$s_projects.id as id,nev,orszagos,helyi_program FROM %1$s_projects ORDER by nev',PROJECTTABLE);
        $pm_res = PGquery($ID,$cmd);
    }*/


    if ($editor == 'edit' and !isset($_GET['edit_form'])) {
        goto no_form_editor_displayed;
    }

    $form = '';

    if (isset($_GET['edit_form'])) {
        $form = 'edit';
        echo "<h3>".t(str_edit_form)."</h3>";
        $to = "<option>$project_schema.$dest_table</option>";
    } else {
        $form = 'new';
        echo "<h3>".t(str_new)." ".str_form."</h3>";
        $to = '';

        $cmd = "SELECT CONCAT_WS('.',f_project_schema,f_project_table) as m FROM header_names WHERE f_project_name='".PROJECTTABLE."' AND f_project_table LIKE '".PROJECTTABLE."%'";
        $res = pg_query($BID,$cmd);

        while ($row = pg_fetch_assoc($res)) {
            $selected = "";
            if (isset($_GET['new_form_table']) and $_GET['new_form_table'] == $row['m']) $selected = "selected";
            elseif (!isset($_GET['new_form_table']) and $row['m'] == 'public.'.PROJECTTABLE) $selected = "selected"; // Default

            $to .= "<option $selected>{$row['m']}</option>";
        }
    }
    echo "<div id='upl-form'>";
    echo "<input type='hidden' id='edit_form_id' value='$h'>";

    echo "<table class='resultstable pure-form'>";

    echo "<tr><th>".t(str_form)." ".str_name.":<br>".wikilink('upload_forms.html#name-of-the-form',str_what_this,'faq-light')."</th>
                <td><input name='formname' id='form_name' class='pure-u-1' value='$name'></td>
                <td colspan='4'>";

    if (isset($_GET['edit_form'])) {
        if ($draft == 'f')
            echo "<span title='Published form id: $published_form_id'><i class='fa fa-fw fa-lock fa-lg'></i> Published form at ".$published."</span>";
        else
            echo "<span title='Published form id: $published_form_id'><i class='fa fa-fw fa-unlock fa-lg'></i> ".t(str_draft). " ".str_form." ".$published."</span>";
    } else {
        // new form
        echo "<i class='fa fa-fw fa-unlock fa-lg'></i> " .  t(str_new)." ".str_form;
    }

    echo "</td></tr>";

    echo "<tr><th>".t(str_form)." ".str_table.":<br>".wikilink('upload_forms.html#destination-table',str_what_this,'faq-light')."</th>
                <td><select name='formtable' class='text-lg pure-u-1' id='form_table'>$to</select></td>
                <th colspan='4'>".t(str_description).":<br>".wikilink('upload_forms.html#form-description',str_what_this,'faq-light')."</th></tr>";
    
    echo "<tr><th>".t(str_form)." ".str_type."::<br>".wikilink('upload_forms.html#form-type',str_what_this,'faq-light')."</th>
            <td><select name='type' class='text-lg pure-u-1' id='form_type' multiple='multiple' size=3>
                    <option value='web' {$s2_sel[1]}>".t(str_web_form)."</option>
                    <option {$s2_sel[0]} value='file'>".t(str_file_upload)."</option>
                    <option {$s2_sel[2]} value='api'>API (mobile)</option></select></td>
            <td colspan='4' rowspan='2' style='vertical-align:top'>
                <textarea name='description' id='form_description' style='width:50em;height:15em;resize:both'>$description</textarea></td>
            </tr>";

            echo "<tr><th>".t(str_form)." ".str_access.":<br>".wikilink('upload_forms.html#form-access',str_what_this,'faq-light')."
            <td><select name='restriction' id='form_access' class='pure-u-1'>
                    <option value=0 {$s1_sel[0]}>".str_public."</option>
                    <option value=1 {$s1_sel[1]}>all logined users</option>
                    <option value=2 {$s1_sel[2]}>only specified groups</option></select>

                   <select multiple name='group_restriction' class='pure-u-1 $rclass' $style_mod id='form_group_access'>$access_groups_options</select> 
                    
                    </td></tr>";

    echo "<tr><th rowspan=3>".t(str_data_assign).":<br>".wikilink('upload_forms.html#data-access',str_what_this,'faq-light')."</th>
            <td rowspan=3>";

            if ($st_col['USE_RULES']) {
                if (!$st_col['RULES_WORKS'])
                    echo "<div class='warning'>Rules trigger is turned off!</div>";
    echo "  <b>".t('str_read').":</b><br>
                <select style='margin-left:5px' multiple name='data_access_read' id='form_data_access_read'>$destination_read_groups_options</select><div style='margin-left:5px'>".t('str_uploader').": <input style='vertical-align:bottom' type='checkbox' id='form_data_access_read_uploader' $dg_upl></div><hr style='border-top:1px dashed lightgray;border-bottom:none'>
                <b>".t('str_write').":</b><br>
                <select style='margin-left:5px' multiple name='data_access_write' id='form_data_access_write'>$destination_write_groups_options</select><div style='margin-left:5px'>&nbsp;".t('str_uploader').": <input style='vertical-align:bottom' type='checkbox' id='form_data_access_write_uploader' $do_upl></div>";
            }

    echo "  </td>
            <th>form group:<br>".wikilink('upload_forms.html#form-groupping',str_what_this,'faq-light')."</th><td><input id='form-list-group' list='form-groups' class='pure-u-1' value='$form_list_group'></td>
            <th rowspan=2 style='vertical-align:top'>observationlist mode<br>".wikilink('upload_forms.html#observationlists',str_what_this,'faq-light')."<br><br>
                observationlist time:<br>".wikilink('upload_forms.html#observationlists',str_what_this,'faq-light')."<br><br>
                tracklog mode<br>".wikilink('upload_forms.html#tracklogs',str_what_this,'faq-light')."</th>
            <td rowspan=2 style='vertical-align:top'>
                <select class='pure-u-1' name='observationlist_mode' id='form_observationlist_mode'>
                    <option value='false' {$s3_sel[0]}>".str_false."</option>
                    <option value='true' {$s3_sel[1]}>".str_true."</option>
                    <option value='force' {$s3_sel[2]}>".str_force."</option></select><br><br>
                <input name='observationlist-time' id='form_observationlist_time' class='pure-u-1-2' value='$observationlist_time' $readonly_obsl_options>".t('str_minutes')."
                <br><br>
                <select class='pure-u-1-2' name='tracklog_mode' id='form_tracklog_mode' $readonly_obsl_options>
                    <option value=''></option>
                    <option value='force' {$s4_sel[2]}>".str_force."</option>
                    <option value='false' {$s4_sel[0]}>".str_false."</option>
                    </select>
            </td></tr>";

    echo "<tr><th rowspan=2>".t('str_form')." <a href='http://spatialreference.org/' target='_blank'>epsg srid</a>:<br>".wikilink('upload_forms.html#form-srid',str_what_this,'faq-light')."</th>
            <td rowspan=2><input class='pure-u-1' name='srid' id='form_srid' placeholder='4326:WGS84,23700:EOV' value='$srid_value'></td>
            </tr>";
    echo "<tr><th>".t('str_periodic_notification').":<br>".wikilink('upload_forms.html#periodic-notification',str_what_this,'faq-light')."</th>
                <td><input name='periodic-notification' id='form_periodic_notification' class='pure-u-1-2' value='$periodic_notification_time'>".t('str_minutes')."</td></tr>";
    echo "</table><br>";
        
    echo '<datalist id="form-groups">';
    $cmd = sprintf("SELECT name FROM project_forms_groups WHERE project_table='%s' ORDER BY name",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $r = pg_fetch_assoc($res);
        echo "<option value='{$r['name']}'>";
    }
    echo '</datalist>';


    if (isset($_GET['new_form_table'])) $dest_table = $_GET['new_form_table'];

    /* form columns */
    if ($dest_table) {

        echo "<table class='resultstable pure-form ctable' id='table_$dest_table'><tr>
            <th>".t(str_included)."?<br>".wikilink('upload_forms.html#included',str_what_this,'faq-light')."</th>
            <th>".t(str_order)."<br>".wikilink('upload_forms.html#column-order',str_what_this,'faq-light')."</th>
            <th>".t(str_column)."<br>".wikilink('upload_forms.html#column',str_what_this,'faq-light')."</th>
            <th>".t(str_obligatory)."<br>".wikilink('upload_forms.html#obligatory',str_what_this,'faq-light')."</th>
            <th>".t(str_description)."<br>".wikilink('upload_forms.html#column-description',str_what_this,'faq-light')."</th>
            <th>".t(str_type)."<br>".wikilink('upload_forms.html#column-type',str_what_this,'faq-light')."</th>
            <th>".t(str_inputcontrol)."<br>".wikilink('upload_forms.html#input-control',str_what_this,'faq-light')."</th>
            <th>".t(str_list)." ".str_definition."<br>".wikilink('upload_forms.html#list-definition',str_what_this,'faq-light')."</th>
            <th>".t(str_default_values)."<br>".wikilink('upload_forms.html#default-values',str_what_this,'faq-light')."</th>
            <th>".t(str_api_params)."<br>".wikilink('upload_forms.html#api-params',str_what_this,'faq-light')."</th>
            <th>".t(str_relation)."<br>".wikilink('upload_forms.html#column-relations',str_what_this,'faq-light')."</th>
            <th>".t(str_pseudo_column)."<br>".wikilink('upload_forms.html#pseudo-columns',str_what_this,'faq-light')."</th></tr>";

        $type_values=array('text','numeric','list','point','line','polygon','wkt','date','datetime','time','file_id','boolean','autocomplete','timetominutes','tinterval','crings','autocompletelist','array');
        $control_values=array('nocheck','minmax','regexp','spatial','custom_check');

        if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$dest_table,$m)) {
            $project_schema = $m[1]; 
            $dest_table = $m[2];
        }

        $g = dbcolist('array',$dest_table,$project_schema,false);
        $g['obm_id'] = 'Edit ID';

        /* It would be possible to get low level description...
         * $cmd = sprintf('SELECT
                    cols.column_name,
                    (
                        SELECT
                            pg_catalog.col_description(c.oid, cols.ordinal_position::int)
                        FROM
                            pg_catalog.pg_class c
                        WHERE
                            c.oid = (SELECT (\'"\' || cols.table_name || \'"\')::regclass::oid)
                            AND c.relname = cols.table_name
                    ) AS column_comment
                FROM
                    information_schema.columns cols
                WHERE
                    cols.table_catalog    = \'%1$s\'
                    AND cols.table_name   = \'%2$s\'
                    AND cols.table_schema = \'public\'',gisdb_name,$dest_table);

        $res5 = pg_query($ID,$cmd);
        $comment_rows = pg_fetch_all($res5);
        $comments = array_column($comment_rows, 'column_comment', 'column_name');*/

        if (isset($_GET['edit_form'])) {
            $cmd = "SELECT \"column\",position_order FROM project_forms_data WHERE form_id=$e ORDER BY position_order";
            $respo = pg_query($BID,$cmd);
            $column_positions = pg_fetch_all($respo);
            $columns = array_column($column_positions, 'column');
            $positions = array_column($column_positions, 'position_order');

            // [{"column":"adatkozlo","position_order":"1"},{"column":"altema","position_order":"2"},
            // {"adatkozlo":"adatk\u00f6zl\u0151","szamossag":"szamossag","gyujto":"gyujto",
            if (array_filter($positions))
                $g = array_merge(array_flip($columns), $g);
        }


        foreach($g as $k=>$column_label) {
            $cmd = "SELECT column_name, data_type,character_maximum_length FROM information_schema.columns WHERE table_name = '$dest_table' AND table_schema = '$project_schema' AND column_name = '$k'";
            $rest = pg_query($ID,$cmd);
            $db_type = 'not exists';
            if (!pg_num_rows($rest)) {
                log_action("Destination table does not exist: $dest_table", __FILE__, __LINE__);
            }
            else {
                $rowt = pg_fetch_assoc($rest);
                $db_type = $rowt['data_type'];
                if ($rowt['character_maximum_length']!='')
                    $db_type .= " ({$rowt['character_maximum_length']})";
            }


            $cmd = "SELECT description FROM project_metaname WHERE project_schema = '$project_schema' AND project_table='$dest_table' AND column_name='$k'";
            $resm = pg_query($BID,$cmd);
            $row = pg_fetch_assoc($resm);
            if (!isset($row['description']))
                $row['description'] = '';

            $type_sel=array('','','','','','','','','','','','','','','','','','');
            $selected_control = '';
            $fullist_sel=array('','');
            $obl_sel=array('','','');
            $count = '';
            $list='';
            $real_list=$list;
            $checked='';
            $oblcolor1='';
            $oblcolor2='';

            $default_value = '';
            $api_params = array('sticky','hidden','readonly','list_elements_as_buttons','once','unfolding_list');
            $api_params_options = array();
            foreach ($api_params as $t) {
                    $api_params_options[] = "<option>$t</option>";
            }

            $relation = '';
            $pseudo_columns = '';
            $position_order = '';

            //if (isset($comments[$k]) and $comments[$k]!='')
            //    $row['description'] = $comments[$k];

            if (isset($_GET['edit_form'])) {
                $e = preg_replace('/[^0-9]/','',$_GET['edit_form']);
                // az OR csak kompatibilitás miatt van
                $cmd = "SELECT custom_function,\"description\",\"type\",\"control\",array_to_string(\"count\",':') as cn,array_to_string(list,',') AS list,\"obl\",\"fullist\",default_value,\"column\",genlist,
                    api_params,relation,regexp,ST_AsText(spatial) AS spatial,pseudo_columns,list_definition,position_order,column_label
                        FROM project_forms_data
                        WHERE \"form_id\"=$e AND \"column\"='$k'";

                $res2 = pg_query($BID,$cmd);
                $r2 = pg_fetch_assoc($res2);

                if ($r2) {

                    $k = $r2['column'];
                    $row['description'] = $r2['description'];

                    if ($r2['column_label']!='')
                        $column_label = $r2['column_label'];

                    //type
                    $n=-1;
                    $n=array_search($r2['type'],$type_values);
                    if($n>=0) $type_sel[$n]='selected';

                    //control
                    $n=-1;
                    $n=array_search($r2['control'],$control_values);
                    if($n>=0) {
                        $selected_control = $control_values[$n];
                    }

                    //count
                    $count = $r2['cn'];
                    if($r2['regexp']!='')
                        $count = $r2['regexp'];

                    if($r2['spatial']!='')
                        $count = $r2['spatial'];

                    if($r2['custom_function']!='')
                        $count = $r2['custom_function'];

                    //list select option show
                    $list=$r2['list'];
                    $real_list = $list;
                    $glist=$r2['genlist'];
                    if ($glist != '') {
                        $list = $glist;
                    }
                    if ($r2['list_definition']!='') {
                        $l = json_decode($r2['list_definition']);

                        // user friendly list definition
                        $list = [];
                        $real_list = $list;
                        if (isset($l->list)) {

                            foreach ($l->list as $value => $keys) {

                                $ll = '';
                                if (count($keys))
                                    $ll .= implode('#',$keys). ':';

                                $ll .= ($value == '_empty_') ? '' : $value;

                                $list[] = $ll;
                            }
                            $list = implode(', ',$list);
                        }
                        elseif (isset($l->optionsTable)) {
                            //select elements from table
                            $list = "SELECT:".$l->optionsTable.".".$l->valueColumn;
                            if (isset($l->labelColumn) and $l->labelColumn!='')
                                $list .= ":".$l->labelColumn;

                        }
                        else
                            $list = json_encode(json_decode($r2['list_definition']),JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);

                        $real_list = json_encode(json_decode($r2['list_definition']),JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES| JSON_UNESCAPED_UNICODE);
                    }
                    // mandatory check
                    $checked='checked';
                    if ($r2['obl']==1) {
                        $oblcolor2=$oblcolor1="style='background:#FF636E'";
                        $obl_sel[0] = 'checked';
                    }
                    elseif ($r2['obl']==2) {
                        $oblcolor2=$oblcolor1="style='background:#B6B6B6'";
                        $obl_sel[1] = 'checked';
                    }
                    elseif ($r2['obl']==3) {
                        //soft error
                        $oblcolor2=$oblcolor1="style='background:#FFB6BB'";
                        $obl_sel[2] = 'checked';
                    }

                    if ($r2['fullist']==0)
                        $fullist_sel[0] = 'selected"';
                    else
                        $fullist_sel[1] = 'selected';

                    if ($r2['default_value']==NULL) $default_value = '';
                    else $default_value = $r2['default_value'];

                    $ap = array();
                    $api_params_options = array();
                    if ($r2['api_params']!=NULL and $r2['api_params']!='') {
                        $ap = json_decode($r2['api_params'],TRUE);
                    }
                    foreach ($api_params as $t) {
                        $sel_par = '';
                        if (in_array($t,$ap)) {
                            $sel_par = 'selected';
                        }
                        $api_params_options[] = "<option $sel_par>$t</option>";
                    }

                    if ($r2['relation']==NULL) $relation = '';
                    else {
                        $relation = $r2['relation'];
                        $oblcolor1="style='background:#73CBFE'";
                    }

                    if ($r2['pseudo_columns']==NULL) $pseudo_columns = '';
                    else {
                        $pseudo_columns = $r2['pseudo_columns'];
                    }

                    if ($r2['position_order']==NULL) $position_order = '';
                    else {
                        $position_order = $r2['position_order'];
                    }
                }
            }
            $coptions = selected_option(array_merge(array(str_no_check.'::nocheck','min : max::minmax','regexp::regexp',str_spatial.'::spatial',str_custom_check.'::custom_check')),$selected_control);

            $trstyle = '';
            $typeselstyle = '';
            if ($k=='obm_id') $trstyle = "style='background-color:yellow'";
            elseif ($k=='obm_files_id' and $type_sel[10]!='selected' and $checked=='checked') $typeselstyle = "style='background-color:darkorange'";
            elseif ($type_sel[0]=='selected' and $checked=='checked' and !preg_match('/^(character)|(text)/',$db_type)) $typeselstyle = "style='background-color:darkorange'";

            // date-time type checks
            if ($checked!='' and preg_match('/^timestamp with /',$db_type) and $type_sel[8]!='selected') $typeselstyle = "style='background-color:darkorange'";
            elseif ($checked!='' and preg_match('/^time with /',$db_type) and $type_sel[9]!='selected') $typeselstyle = "style='background-color:darkorange'";
            elseif ($checked!='' and $db_type == 'date' and $type_sel[7]!='selected') $typeselstyle = "style='background-color:darkorange'";

            // automatic type selection
            if ($checked=='' and $db_type=='integer') $type_sel[1]='selected';
            elseif ($checked=='' and $db_type=='numeric') $type_sel[1]='selected';
            elseif ($checked=='' and $db_type=='real') $type_sel[1]='selected';
            elseif ($checked=='' and $db_type=='double precision') $type_sel[1]='selected';
            elseif ($checked=='' and $db_type=='smallint') {
                $type_sel[1]='selected';
                $coptions = selected_option(array_merge(array(str_no_check.'::nocheck','min : max::minmax','regexp::regexp',str_spatial.'::spatial',str_custom_check.'::custom_check')),'minmax');
                $count = "-32768:32767";
            }
            elseif ($checked=='' and $db_type=='date') $type_sel[7]='selected';
            elseif ($checked=='' and preg_match('/^timestamp with/',$db_type)) $type_sel[8]='selected';
            elseif ($checked=='' and preg_match('/^time /',$db_type)) $type_sel[9]='selected';
            elseif ($checked=='' and preg_match('/^character varying \((\d+)\)/',$db_type,$m)) {
                $type_sel[0]='selected';
                $coptions = selected_option(array_merge(array(str_no_check.'::nocheck','min : max::minmax','regexp::regexp',str_spatial.'::spatial',str_custom_check.'::custom_check')),'minmax');
                $count = "0:$m[1]";
            }
            elseif ($checked=='' and $k=='obm_files_id') $type_sel[10]='selected'; // automatic type selection

            // UPLOAD FORM-EDITOR TABLE
            echo "<tr $trstyle><td class='uplf_color1-$k' $oblcolor1><input type='checkbox' class='uplf_ckb' name='uplf_ckb[]' value='1' id='uplf_ckb-$k' $checked></td><td class='uplf_color1-$k' $oblcolor1><input id='uplf_order-$k' value='$position_order' list='available_items' class='available_item_trigger uplf_column_order' size=2 style='text-align:center'></td><td class='uplf_color2-$k' $oblcolor1><div contenteditable='true' id='uplf_column_label-$k'>$column_label</div><i>$k</i></td>
 <td class='uplf_color3-$k' $oblcolor2>
    <div style='vertical-align:middle'><input type='radio' name='obl-$k' id='uplf_obl_y-$k' class='uplf_obl' name='obligatory' title='Choose yes, if this field should not be empty' value='1' style='vertical-align:middle' {$obl_sel[0]}> ".str_yes."</div>
    <div style='vertical-align:middle'><input type='radio' name='obl-$k' id='uplf_obl_n-$k' class='uplf_obl' name='obligatory' title='Choose no, if this field can be empty' style='vertical-align:middle' value='2' {$obl_sel[1]}> ".str_no."</div>
    <div style='vertical-align:middle'><input type='radio' name='obl-$k' id='uplf_obl_s-$k' class='uplf_obl' name='obligatory' title='Choose soft, if this field should not be empty, but possible' value='3' style='vertical-align:middle' {$obl_sel[2]}> ".str_soft_error."</div>
</td>
<td><textarea name='uplf_description[]' id='uplf_description-$k' style='resize:both'>{$row['description']}</textarea></td><td $typeselstyle>$db_type<br><select class='uplf_type' name='uplf_tpye[]' id='uplf_type-$k'><option title='any type of characters allowed' {$type_sel[0]} value='text'>".t(str_text)."</option><option title='only numeric input allowed' {$type_sel[1]} value='numeric'>".t(str_numeric)."</option><option title='input from predefined list' {$type_sel[2]} value='list'>".t(str_list)."</option><option title='wkt geometry definition' value='point' {$type_sel[3]}>".t(str_geometry.':',str_point)."</option><option title='wkt geometry definition' value='line' {$type_sel[4]}>".t(str_geometry.':',str_line)."</option><option title='wkt geometry definition' value='polygon' {$type_sel[5]}>".t(str_geometry.':',str_polygon)."</option><option value='wkt' title='wkt geometry definition' {$type_sel[6]}>".t(str_geometry.':',str_any)."</option><option value='date' {$type_sel[7]}>".t(str_date)."</option><option value='datetime' {$type_sel[8]}>".t(str_datetime)."</option><option value='time' {$type_sel[9]}>".t(str_time)."</option>
<option value='timetominutes' {$type_sel[13]}>".t(str_time_to_minutes)."</option>
<option value='tinterval' {$type_sel[14]}>".t(str_time_interval)."</option>
<option {$type_sel[10]} value='file_id'>File connect</option><option value='boolean' {$type_sel[11]}>".t(str_boolen)."</option><option value='autocomplete' {$type_sel[12]}>Autocomplete</option><option value='autocompletelist' {$type_sel[16]}>Autocomplete list</option><option value='crings' {$type_sel[15]}>Colour rings</option><option value='array' {$type_sel[17]}>".t(str_array)."</option></select></td>
<td><select class='uplf_length' id='uplf_length-$k' name='uplf-length' name='textlength'>$coptions</select><br><input id='uplf_count-$k' name='uplf_count' size='24' value='$count'></td>
<td><textarea class='magnify list-editor' id='uplf_list-$k' placeholder='JSON array' data-simplified='$list' style='resize:both'>";
    echo $real_list;
echo "</textarea></td>";
        echo "<td><input id='uplf_defval-$k' value='$default_value' list='default_options' placeholder='click to get options'></td>";
        echo "<td><select multiple id='uplf_api_params-$k'>".implode('',$api_params_options)."</select></td>";
        echo "<td><textarea class='magnify uplf_relation' id='uplf_relation-$k' placeholder='(column_name=condition) {function(parameter);function(parameter)},..' style='resize:both'>$relation</textarea></td>";
        echo "<td><input class='magnify' id='uplf_pseudocol-$k' placeholder='form_name:column1,column2' value='$pseudo_columns'></td>";
        echo "</tr>";

        }
        echo "</table>";
        echo '<datalist id="default_options">
    <option value="_attachment">
    <option value="_autocomplete">
    <option value="_auto_geometry">
    <option value="_boolean">
    <option value="_datum">
    <option value="_email">
    <option value="_geometry">
    <option value="_input">
    <option value="_list">
    <option value="_login_name">
    <option value="_login_email">
    <option value="_time">
    </datalist>';
        echo '<datalist id="available_items"></datalist>';

    }
    echo "<br>";

    if ($form == 'edit') {
        if ($draft == 't') {
            echo "<button class='pure-button button-warning button-xlarge pure-u-1-5' id='form-publish'> ".str_save." </button> " . wikilink('upload_forms.html#form-publish',str_what_this);
        } else
            echo "<button class='button-warning button-xlarge pure-button pure-u-1-5' id='form-update'> ".str_save." </button>";
        echo " &nbsp; &nbsp; <button class='button-success button-xlarge pure-button pure-u-1-5' id='form-draft-submit'> ".str_create_draft." </button> " . wikilink('upload_forms.html#form-test',str_what_this);
    } else {
        // new form
        echo  "<button class='pure-button button-warning button-xlarge pure-u-1-5' id='form-publish'> ".str_save." </button> " . wikilink('upload_forms.html#form-publish',str_what_this);
        echo " &nbsp; &nbsp; <button class='button-success button-xlarge pure-button pure-u-1-5' id='form-draft-submit'> ".str_create_draft." </button> " . wikilink('upload_forms.html#form-test',str_what_this);
    }

    echo "<br><br>";
    echo "</div>";

    $lm_widget = ($modules->is_enabled('list_manager',$dest_table)) ? $modules->_include('list_manager','upload_form_builder_widget') : '' ;

    echo "<div id='list-editor'>
        <input type='hidden' id='list-editor-return'>
<div id='list-editor-help-box'>
    <span class='add-item'>list</span>: {
            option-1: [label-1, label-2]
    },
    <span class='add-item'>optionsSchema</span>: <div class='item-content' data-type='optionsSchema' data-value='' data-schema=''>string</div>,
    <span class='add-item'>optionsTable</span>: <div class='item-content' data-type='optionsTable' data-value='optionsSchema' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>valueColumn</span>: <div class='item-content' data-type='valueColumn' data-value='optionsTable' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>labelColumn</span>: <div class='item-content' data-type='labelColumn' data-value='optionsTable' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>filterColumn</span>: <div class='item-content' data-type='filterColumn' data-value='optionsTable' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>pictures</span>:  <div class='item-content' data-type='pictures' data-value='list'>{ option-1: url-string }</div>,
    <span class='add-item'>triggerTargetColumn</span>: <div class='item-content' data-type='triggerTargetColumn' data-value=''>string</div>,
    <span class='add-item'>Function</span>: <div class='item-content' data-type='Function' data-value=''>string</div>,
    <span class='add-item'>disabled</span>: <div class='item-content' data-type='disabled' data-value='list'>[option-1]</div>,
    <span class='add-item'>preFilterColumn</span>: <div class='item-content' data-type='preFilterColumn' data-value='optionsTable' data-schema='optionsSchema'>string</div>,
    <span class='add-item'>preFilterValue</span>: string,
    <span class='add-item'>multiselect</span>: <div class='item-content' data-type='multiselect' data-value=''>boolean</div>,
    <span class='add-item'>selected</span>: <div class='item-content' data-type='selected' data-value='list'>[option-1]</div>,
    <span class='add-item'>size</span>: integer,
    <span class='add-item'>add_empty_line</span>: <div class='item-content' data-type='add_empty_line' data-value=''>boolean</div>
    <span class='add-item'>extendable</span>: <div class='item-content' data-type='extendable' data-value=''>boolean</div>
</div>

$lm_widget

         <!--<div style='position:relative'>Simple editor: <button class='pure-button button-small button-secondary pure-u-3-1' style='position:absolute;right:2px;top:20px'>".str_update."</button></div>
        <textarea id='list-editor-simple'></textarea><br>-->

        <div style='position:relative'><br><!-- <button class='pure-button button-small button-secondary pure-u-3-1' style='position:absolute;right:2px;top:20px'>".str_update."</button>--></div>
        <textarea id='list-editor-json'></textarea><br><br>

        <button class='pure-button button-secondary pure-u-3-1' id='list-editor-save'>".str_save."</button>
        <button class='pure-button button-gray pure-u-3-1' id='list-editor-cancel'>".str_cancel."</button>
        </div>";

    no_form_editor_displayed:
    echo "";

?>
