<?php
if (isset($_REQUEST['language-administration']) && $_REQUEST['language-administration'] === 'get_translations') {
    $cmd = "WITH consts as (SELECT DISTINCT const as cnst FROM translations WHERE project = '". PROJECTTABLE."'),";
    $cmd .= implode(', ', array_map(function ($lang) {
        return sprintf("%1\$s as (SELECT const, translation as %1\$s FROM translations WHERE scope = 'local' AND project = '%2\$s' AND lang = '%1\$s')", $lang, PROJECTTABLE);
    }, array_keys(LANGUAGES)));
    $cmd .= " SELECT * FROM consts ";
    $cmd .= implode("", array_map(function ($lang) {
        return "LEFT JOIN $lang ON (consts.cnst = $lang.const) "; 
    }, array_keys(LANGUAGES)));
    
    if (!$res = pg_query($BID, $cmd)) {
        log_action('query error', __FILE__, __LINE__);
        echo common_message('error', 'query error!');
        exit;
    }
    $translations = array_map(function ($t) {
        unset($t['const']);
        return $t;
    }, pg_fetch_all($res));
    echo common_message('ok', [
        'languages' => LANGUAGES,
        'translations' => $translations
    ]);
    exit;
}
else if (isset($_REQUEST['language-administration']) && $_REQUEST['language-administration'] === 'save') {
    if (!isset($_POST['language_strings'])) {
        echo common_message('error', 'const not provided');
        exit;
    }
    $cmd = [];
    foreach (LANGUAGES as $lang => $label) {
        $cmd[] = sprintf("INSERT INTO translations (scope, project, lang, const, translation) VALUES ('local', %s, %s, %s, %s) ON CONFLICT ON CONSTRAINT translations_scope_project_lang_const DO UPDATE SET translation = EXCLUDED.translation;", 
            quote(PROJECTTABLE),
            quote($lang),
            quote($_POST['language_strings']['cnst']),
            quote(($_POST['language_strings'][$lang]) ? $_POST['language_strings'][$lang] : "__not_translated__")
        );
    }
    if (!query($BID, $cmd)) {
        log_action('query error', __FILE__, __LINE__);
        echo common_message('error', 'query error!');
        exit;
    }
    echo common_message('ok','ok');
    exit;
}
else if (isset($_REQUEST['language-administration']) && $_REQUEST['language-administration'] === 'delete') {
    if (!isset($_POST['cnst'])) {
        echo common_message('error', 'const not provided');
        exit;
    }
    $cmd = sprintf("DELETE FROM translations WHERE scope = 'local' AND project = %s AND const = %s;", 
        quote(PROJECTTABLE),
        quote($_POST['cnst']),
    );
    if (!pg_query($BID, $cmd)) {
        log_action('query error: ' . $cmd, __FILE__, __LINE__);
        echo common_message('error', 'query error' );
        exit;
    }
    echo common_message('ok','ok');
    exit;
}
else {

    echo "<div class='infotitle'>".wikilink('admin_pages.html#translations',t('str_documentation'))."</div>";
    echo "<div id='language-definitions'></div>";
}
