<?php
    echo "<div class='infotitle'>".wikilink('admin_pages.html#administrative-access',t('str_documentation'))."</div>";

    $admin_pages = get_admin_pages_list();

    $pages  = array_column($admin_pages, 'page');
    $groups = array_column($admin_pages, 'group');
    $labels = array_column($admin_pages, 'label');
    array_multisort($groups, SORT_ASC, $labels, SORT_NATURAL | SORT_FLAG_CASE, $admin_pages);

    $user_classes = array();
    foreach ($csor as $csrow) {
        // skip user roles
        if ($csrow['user_id']!='') continue;
        $user_classes[$csrow['role_id']] = $csrow['description'];
    }

    $privileges = get_privileges();

    echo "<form class='pure-form pure-form-stacked' id='privileges-form'>";
    $tbl = new createTable();
    $tbl->def(['tid'=>'privileges-table','tclass'=>'resultstable']);
    $tbl->addHeader(array_merge([str_function],$user_classes));
    $rows = [];
    foreach ($admin_pages as $ap) {
        $label = $ap['label'];
        $page = $ap['page'];
        $group = $ap['group'];
        $row = [$label];
        foreach ($user_classes as $role_id=>$group_name) {
            $checked = (isset($privileges[$role_id]) && in_array($page,$privileges[$role_id]))
                ? 'checked="checked"'
                : '';
            $row[] = "<input type='checkbox' name='$role_id@$page' $checked>";
        }
        if ($group == 'project_admin' and $page!='privileges' and $page!='groups' and $page!='members')
            $tbl->addRows($row,'style="background-color:orange"');
        elseif ($page == 'privileges' or $page == 'groups' or $page == 'members')
            $tbl->addRows($row,'style="background-color:red;color:white"');
        else
            $tbl->addRows($row);
    }

    echo $tbl->printOut();
    echo "<input type='hidden' name='privileges' value='save'>";
    echo "<button class='button-warning button-xlarge pure-button' id='save_privileges' type='submit'><i id='ficon' class='fa fa-floppy-o'></i> ".str_save."</button></form>";

?>
