<?php

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>OBM Document editor</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
    .draggable { height: auto; width: 250px; margin: 0; font-size:10mm; border: 1px dotted #888; }
    .nwgrip, .negrip, .swgrip, .segrip, .ngrip, .egrip, .sgrip, .wgrip {
        width: 10px;
        height: 10px;
        background-color: #ffffff;
        border: 1px solid #000000;
    }
    .segrip {
        right: -5px;
        bottom: -5px;
    }
    .original { display:none; height:0px; }
    .box-handle { background-color: red; opacity: 0.5; margin-top:-10px; height: 10px; cursor: hand; display: none}
    .box-top { height: 10px; margin-top: -10px }
    .editable { height: auto; width: auto; }
    .handler { border: 1px dotted lightblue;padding:0px 0px 2px 0px;min-height:20px;background-color:aliceblue;
background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/PjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiPjxkZWZzPjxwYXR0ZXJuIGlkPSJwYXR0ZXJuX1p6dTEiIHBhdHRlcm5Vbml0cz0idXNlclNwYWNlT25Vc2UiIHdpZHRoPSI2IiBoZWlnaHQ9IjYiIHBhdHRlcm5UcmFuc2Zvcm09InJvdGF0ZSg0NSkiPjxsaW5lIHgxPSIwIiB5PSIwIiB4Mj0iMCIgeTI9IjYiIHN0cm9rZT0iIzVFQzBFMSIgc3Ryb2tlLXdpZHRoPSIyIi8+PC9wYXR0ZXJuPjwvZGVmcz4gPHJlY3Qgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgZmlsbD0idXJsKCNwYXR0ZXJuX1p6dTEpIiBvcGFjaXR5PSIxIi8+PC9zdmc+')}
    #doccontainer {
        margin-top:10px;
        -webkit-box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.75);
        box-shadow: 0px 0px 11px 2px rgba(0,0,0,0.75);
        border-radius: 2px 2px 2px 2px;
        width:210mm;height:297mm;
        border:1px solid #acacac;
        position:relative;
        color:#f6f6f6;
        font-size:12mm;
    }
    #docarea {
        position:relative;
        color:#dadada;
        background-color:#fcfcfc;
        height:100%;
        margin:0;
        padding:0;
        border:1px solid #dadada;
        font-size:12mm;
    }
    #margin-left {
        width:20mm;
    }
    #margin-right {
        width:20mm;
    }
    #margin-top {
        height:20mm;
    }
    #margin-bottom {
        height:20mm;
    }
    #ctrlarea { border:1px solid gray; padding:10px;}
    .table { display:table }
    .table-row { display:table-row }
    .table-cell { display:table-cell;vertical-align:top }
    .non-selectable {-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none; user-select: none;}
    .ui-widget-content { background-color: transparent }
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
    var focuselement;
    $(document).ready(function() {
        $("body").on("click","#addbox",function(){
            duplicate('box')
        });
        $("body").on("click","#addimage",function(){
            duplicate('image')
        });
        $("body").on("click","#addmap",function(){
            duplicate('map')
        });
        $("body").on("click","#addsql",function(){
            duplicate('sql')
        });


        $("#margin").change(function(){
            $("#margin-left").css({"width": $(this).val() + "mm"}); 
            $("#margin-right").css({"width": $(this).val() + "mm"}); 
            $("#margin-top").css({"height": $(this).val() + "mm"}); 
            $("#margin-bottom").css({"height": $(this).val() + "mm"}); 
        });
        $("#docarea").on("click",".element",function(){
            focuselement = $(this).attr("id");
            $("#ctrl-id").html(focuselement);
            //$(this).resizable( "option", "autoHide", false );
        });
        $(".ctrlopt").change(function(){
            var ctrltype = $(this).data("ctrltype");
            var elementBox = $("#"+focuselement).find('.editable');
            if (ctrltype=="background-color") {
                elementBox.css({"background-color": $(this).val()});
            } else if (ctrltype=="font-size") {
                $("#"+focuselement).css({"font-size": $(this).val()+"px"});
            } else if (ctrltype=="font-family") {
                $("#"+focuselement).css({"font-family": $(this).val()});
            } else if (ctrltype=="color") {
                $("#"+focuselement).css({"color": $(this).val()});
            } else if (ctrltype=="text-align") {
                $("#"+focuselement).css({"text-align": $(this).val()});
            }
        });
        
        $(".ctrlopt-drop").click(function(){
            $("#"+focuselement).remove();
        });

        $("#save").click(function(){
            $("#docarea").find(".element").each(function(){
                if (!$(this).hasClass("original")) {
                    var pos = $(this).position();
                    $("#messagearea").append("<br>");
                    $("#messagearea").append("left: "+pos.left+", top: "+pos.top + "<br>");
                    $("#messagearea").append("font-size:" + $(this).css("font-size") + "<br>");
                    $("#messagearea").append("background:" + $(this).css("background-color") + "<br>");
                    $("#messagearea").append("content: "+$(this).find('.editable').html());
                }
            });
        });
        $("#docarea").on({
            mouseenter: function () {
                $(this).find(".box-handle").show();
                $(this).find(".box-top").hide();
            },
            mouseleave: function () {
                $(this).find(".box-handle").hide();
                $(this).find(".box-top").show();
            }
        }, ".element");
        $()
        $('#docarea').on('focus', '[contenteditable]', function() {
            const $this = $(this);
            $this.data('before', $this.html());
        }).on('blur keyup paste input', '[contenteditable]', function() {
            const $this = $(this);
            if ($this.data('before') !== $this.html()) {
                $this.data('before', $this.html());
                $this.trigger('change');
                $(this).parent().css({'height':$(this).height()});
            }
        });
        $('html').keyup(function(e){
            if(e.keyCode == 46) {
                $("#"+focuselement).remove();
            }
        });
        $('#zuzu').click(function(){
            //focuselement = '';
            //$('#ctrl-id').html('');
        });
    });
var i = 0;
function duplicate(id) {
            i++;
            var res_clone = jQuery("#element_"+id).clone();
            jQuery(res_clone).attr("id", id + i);
            //jQuery(res_clone).find(".ui-resizable-handle").remove();
            jQuery(res_clone).removeClass("original");
            jQuery(res_clone).draggable({
                snap: true,
                containment: "#docarea",
                handle:".box-handle",
                stop: function( event, ui ) {
                    $(this).trigger('click');
                    //$(this).resizable('option','autoHide',false);
                }
            });
            jQuery(res_clone).resizable({
                containment: "#docarea",
                autoHide: true,
                handles: {
                    'ne': '.negrip',
                    'se': '.segrip',
                    'sw': '.swgrip',
                    'nw': '.nwgrip',
                },
                stop: function( event, ui ) {
                    $(this).find('.editable').css({'height':$(this).height()})
                    //$(this).resizable('option','autoHide',false);
                }
            });
            jQuery("#docarea").append(res_clone);
            res_clone.trigger("click");
}
  </script>
</head>
<body>
<div class='table' id='zuzu'>

<div class='table-row'>
    <div class='table-cell'> 
        <div id='buttonsband'> 
            <button id='addbox'> Add TextBox </button> 
            <button id='addimage'> Add ImageBox </button> 
            <button id='addmap'> Add MapBox </button> 
            <button id='addsql'> Add SQLReference </button> 
            Page margin: <input id='margin' type="number" value='20' style='width:40px'  min="0" max="50"> 
            <button id='save'>Save</button>
        </div>
    </div>
    <div class='table-cell'></div> 
</div>

<div class='table-row'>
    <div class='table-cell'> 
        <div id='doccontainer' class='table'><span class='non-selectable' style='position:absolute;top:0;left:0'>MARGIN</span>
        <div class='table-row' id='margin-top'></div>
        <div class='table-row'>
            <div id='margin-left' class='table-cell'></div>
            <div id='docarea' class='table-cell'>
                <span class='non-selectable' style='position:absolute;top:0;left:0'>DOCUMENT AREA</span>
                <div id='element_box' class='element draggable resizeable ui-widget-content original'>
                    <div class='box-handle'></div>
                    <div class='box-top'></div>
                    <div class='editable' contenteditable='true'>I am editable and draggable</div>
                    <div class="ui-resizable-handle ui-resizable-nw nwgrip"></div>
                    <div class="ui-resizable-handle ui-resizable-ne negrip"></div>
                    <div class="ui-resizable-handle ui-resizable-sw swgrip"></div>
                    <div class="ui-resizable-handle ui-resizable-se segrip"></div>
                </div>
                <div id='element_sql' class='element draggable resizeable ui-widget-content original'>
                    <div class='box-handle'></div>
                    <div class='box-top'></div>
                    <div class='editable' contenteditable='true'>%$1:faj%</div> <!--SQL Reference for client -->
                    <div class="ui-resizable-handle ui-resizable-nw nwgrip"></div>
                    <div class="ui-resizable-handle ui-resizable-ne negrip"></div>
                    <div class="ui-resizable-handle ui-resizable-sw swgrip"></div>
                    <div class="ui-resizable-handle ui-resizable-se segrip"></div>
                </div>
                <div id='element_map' class='element draggable resizeable ui-widget-content original'>
                    <div class='box-handle'></div>
                    <div class='box-top'></div>
                    <div class='editable' contenteditable='true'>%map.reference%</div> <!--MAP Reference for client -->
                    <div class="ui-resizable-handle ui-resizable-nw nwgrip"></div>
                    <div class="ui-resizable-handle ui-resizable-ne negrip"></div>
                    <div class="ui-resizable-handle ui-resizable-sw swgrip"></div>
                    <div class="ui-resizable-handle ui-resizable-se segrip"></div>
                </div>
                <div id='element_image' class='element draggable resizeable ui-widget-content original'>
                    <div class='box-handle'></div>
                    <div class='box-top'></div>
                    <div class='editable' contenteditable='true'>%image.reference%</div> <!--IMAGE Reference for client -->
                    <div class="ui-resizable-handle ui-resizable-nw nwgrip"></div>
                    <div class="ui-resizable-handle ui-resizable-ne negrip"></div>
                    <div class="ui-resizable-handle ui-resizable-sw swgrip"></div>
                    <div class="ui-resizable-handle ui-resizable-se segrip"></div>
                </div>
            </div>
            <div id='margin-right' class='table-cell'></div>
        </div>
        <div class='table-row' id='margin-bottom'></div>
        </div> <!--doccontainer-->
    </div>
    <div class='table-cell'>
        <ul style='list-style-type:none;'><li>
        <div id='ctrlarea'>
            <div style='border:3px solid lightgray; padding:6px'><span id='ctrl-id'></span>
            <ul style='list-style-type:none;line-height:200%'>
                <li>font-size:  <input class='ctrlopt' data-ctrltype='font-size' type='number' value='12' style='width:40px'  min='6' max='56'></li>
                <li>font-generic: <select class='ctrlopt' data-ctrltype='font-family'>
                                    <option>arial</option>
                                    <option>helvetica</option>
                                    <option>serif</option>
                                    <option>sans-serif</option>
                                    <option>monospace</option>
                                    </select></li>
                <li>font-family: <select class='ctrlopt' data-ctrltype='font-family'>
                                    <option value='Times'>Times New Roman, Times</option>
                                    <option value='Book Antiqua'>Book Antiqua</option>
                                    <option value='Georgia'>Georgia</option>

                                    <option>Arial</option>
                                    <option>Helvetica</option>
                                    <option>Comic Sans MS</option>
                                    <option>Verdana</option>

                                    <option>Courier New</option>
                                    <option>Lucida Console</option>
                                    </select></li>
                <li>font-color: <input class='ctrlopt' data-ctrltype='color' type='color' value='#000000' style='width:85%;'>
                <li>background-color: <input class='ctrlopt' data-ctrltype='background-color' type='color' value='#ffffff' style='width:85%;'>
                <li>text-align: <select class='ctrlopt' data-ctrltype='text-align'>
                                    <option>center</option>
                                    <option>left</option>
                                    <option>right</option>
                                    <option>justify</option>
                                    </select></li>
                <li><button class='ctrlopt-drop'>drop</button></li>
            </ul>
            </div>
            <div style='border:3px solid lightgray; padding:6px'><span id='ref-ctrl-id'></span>
            <ul style='list-style-type:none;line-height:200%'>
                <li>sql-reference:  <input class='ctrlopt'></li>
                <li>schema: <input class='ctrlopt'></li>
                <li>table: <input class='ctrlopt'></li>
                <li>column: <input class='ctrlopt'>
                <li>filterValue: <input class='ctrlopt' value='$1'>
                <li><button class='ctrlopt-drop'>drop</button></li>
                <li><button class='ctrlopt-drop'>add</button></li>
            </ul>
            </div>

        </div></li><li>
        <div id='messagearea'></div>
        </li></ul>
    </div>
</div>
<div class='table-row'>
    <div class='table-cell'></div>
    <div class='table-cell'></div>
</div>
</div>

</body>
</html>
