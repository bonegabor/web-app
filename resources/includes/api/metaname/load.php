<?php 
/**
 * 
 */
class metaname_load extends iapi {

    public function execute($params) {

        $j = json_decode($params,true);

        if (count($j['functions'])) {
            if (in_array('repositories', $j['functions'])) {
                return self::route($j['options']);
            }
        }
    }
    
    private static function route($options) {
        if (isset($options['metaname']) && method_exists(__CLASS__, $options['metaname'])) {
            $meth = $options['metaname'];
            return self::$meth($options);
        }
    }
    
    public static function get_repositories($request) {
        return Metaname::$repositories;
    }
    
    public static function fetch_remote_data($request) {
        $mn = new Metaname($request);
        return $mn->data;
    }
}

class Metaname {
    
    private $string = '';
    public static $repositories = [
        /*
        'CoL' => [
            'short' => 'CoL',
            'name' => 'Catalogue of Life',
            'url' => 'http://www.catalogueoflife.org/col/webservice?name=',
            'format' => 'xml',
            'icon' => 'col_icon.jpg',
            'wspace_char' => '+',
        ],
        */
        'GNI' => [
            'short' => 'GNI',
            'name' => 'Global Name Index',
            'url' => 'http://gni.globalnames.org/name_strings.xml?search_term=',
            'format' => 'xml',
            'icon' => 'gni_icon.jpg',
            'wspace_char' => '+',
        ],
        'EoL' => [
            'short' => 'EoL',
            'name' => 'Enciclopedia of Life',
            'url' => 'https://eol.org/api/search/1.0.json?page=1&exact=false&filter_by_taxon_concept_id=&filter_by_hierarchy_entry_id=&filter_by_string=&cache_ttl=&q=',
            'format' => 'json',
            'icon' => 'eol_icon.jpg',
            'wspace_char' => '+',
        ],
        'GBiF' => [
            'short' => 'GBiF',
            'name' => 'GBiF',
            'url' => 'https://api.gbif.org/v1/species/match?verbose=false&name=',
            'format' => 'json',
            'icon' => 'gbif_icon.png',
            'wspace_char' => '+',
        ]    
    ];
    private $repository = [];
    
    public $raw = null;
    public $data = [ ];
    
    function __construct($request) {
        
        if (!in_array($request['repository'], array_keys(self::$repositories))) {
            return "Unknown repository!";
        }
        $this->repository = self::$repositories[$request['repository']];
        
        mb_internal_encoding("UTF-8");
        mb_regex_encoding("UTF-8");
        
        $this->string = mb_ereg_replace('\s+', $this->repository['wspace_char'], $request['term']);

        // fetching the data
        $fetch_fun = "fetch_{$this->repository['format']}";
        $this->$fetch_fun();
        
        // processing the data
        $process_fun = "process_{$this->repository['short']}";

        $this->$process_fun();        
    }
    
    private function url() {
        return $this->repository['url'] . $this->string;
    }
    
    private function fetch_json() {
        $this->raw = file_get_contents($this->url());
    }
    
    private function fetch_xml() {
        $this->raw = fetch_remote_xml($this->url());
    }
        
    //Catalogue of Life
    /*
    private function process_CoL() {
        $sa = "";
        if (is_object($data)) {
            if ($data['total_number_of_results']!='0') {
                //http://www.catalogueoflife.org/col/search/all/key/Pica+pica/fossil/0/match/1
                $sa .= $data['total_number_of_results']." records found.<br>";
                if ($data['total_number_of_results'] > 1)
                $sa .= "<a href='http://www.catalogueoflife.org/col/search/all/key/$string/fossil/0/match/1' target='_blank'>Catalogue of Life</a><br>";
                else {
                    $sa .= '<a href="'.$data->result->url.'" target="_blank">Catalogue of Life</a><br>';
                    $sa .='Rank:<b>'.$data->result->rank."</b><br>";
                    $sa .='Name status:<b>'.$data->result->name_status."</b><br>";
                }
            } else {
                $sa .="<a href='$url' target='_blank'>Catalogue of Life</a><br>";
                $sa .= "No results.";
            }
        } else {
            $sa .="<a href='$url' target='_blank'>Catalogue of Life</a><br>";
            $sa .= "URL fetch error.";
        }
        echo $sa;
    }
    */
    
    //Global Name Index
    private function process_GNI() {
        if (is_object($this->raw)) {
            if (!$this->raw->name_strings[0]['nil']) {
                $this->data['number_of_records'] = (string)$this->raw->name_strings_total;
            }
        } else {
            $this->error = true;
        }
        $this->data['url'] = "<a href='" . preg_replace('/\.xml/','.html',$this->url()) . "' target='_blank'>" . $this->repository['name'] . "</a>";
    }
    
    //Enciclopedia of Life
    private function process_EoL() {
        $this->data['url'] = "<a href='https://eol.org/api/search/1.0.json?q={$this->string}' target='_blank'>" . $this->repository['name'] . "</a>";
        if (is_json($this->raw)) {
            $j = json_decode($this->raw);
            if (isset($j->{'totalResults'})) {
                $this->data['number_of_records'] = $j->{'totalResults'};
                $this->data['url'] = "<a href='" . $j->{'results'}[0]->{'link'} . "' target='_blank'>" . $this->repository['name'] . "</a>";
            } 
        } else {
            $this->error = true;
        }
    }
    
    //GBiF
    //https://api.gbif.org/v1/species?name=Puma%20concolor
    //https://api.gbif.org/v1/species/match?verbose=false&name=Oenante%20oenanthe
    private function process_GBiF() {
        if (is_json($this->raw)) {
            $j = json_decode($this->raw);
            if (isset($j->{'scientificName'})) {
                $this->data["Distributions"] = "<a href='https://www.gbif.org/occurrence/map?taxon_key=" . $j->{'usageKey'} . "' target='_blank'>Distributions</a>";
                $this->data['kingdom'] = "<a href='https://www.gbif.org/species/" . $j->{'kingdomKey'}."' target='_blank'>" . $j->{'kingdom'} . "</a>";
                $this->data['phylum'] = "<a href='https://www.gbif.org/species/" . $j->{'phylumKey'}."' target='_blank'>" . $j->{'phylum'} . "</a>";
                $this->data['order'] = "<a href='https://www.gbif.org/species/" . $j->{'orderKey'}."' target='_blank'>" . $j->{'order'} . "</a>";
                $this->data['family'] = "<a href='https://www.gbif.org/species/" . $j->{'familyKey'}."' target='_blank'>" . $j->{'family'} . "</a>";
                $this->data['genus'] = "<a href='https://www.gbif.org/species/" . $j->{'genusKey'}."' target='_blank'>" . $j->{'genus'} . "</a>";
                $this->data['scientificName'] = "<a href='https://www.gbif.org/species/" . $j->{'speciesKey'}. "' target='_blank'>" . $j->{'scientificName'} . "</a>";
            } else {
                $this->data['try fuzzy match?'] = "<a href='https://api.gbif.org/v1/species/match?verbose=true&name={$this->string}' target='_blank'> try fuzzy match? </a>";
            }
        } else {
            $this->error = true;
            $this->data['urls'] = [
                [ "href" => "https://gbif.org", "text" => $this->repository['name'] ],
            ];
        }
    }
}
?>
