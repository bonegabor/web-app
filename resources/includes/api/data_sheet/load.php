<?php
class data_sheet_load extends iapi {

    private static $acc = 0;
    private static $allowed_cols = [];
    private static $sensitivity_values = [
        '0' => 'public',
        'public' => 'public',
        '1' => 'no-geom',
        'no-geom' => 'no-geom',
        '2' => 'sensitive',
        'restricted' => 'sensitive',
        'sensitive' => 'sensitive',
        '3' => 'only-owner',
        'only-owner' => 'only-owner'
    ];
    private static $modules;
    private static $x_modules;
    private static $ds_export = false;
    private static $getid;
    private static $sheet_table;
    private static $PROJECTTABLE;
    
    public function execute($params) {
        try {
            $j = json_decode($params,true);
            self::$PROJECTTABLE = constant('PROJECTTABLE');
            $ret = $params;
            
            if (count($j['functions'])) {

                if (!isset($_SESSION['getid']) || !isset($_SESSION['current_query_table'])) {
                    throw new \Exception("Error displaying data", 1);
                }
                if (isset($_SESSION['current_query_keys'])) {
                    unset($_SESSION['current_query_keys']);
                }

                //getid can be complex id of different included tables separated by _ the first number is the maintable id
                self::$getid = preg_replace('/[^0-9]+/','',$_SESSION['getid']);
                //track data view
                track_data(self::$getid);

                self::$sheet_table = $_SESSION['current_query_table'];
                st_col(self::$sheet_table);
                
                self::$acc = rst('acc', self::$getid, self::$sheet_table, has_access('master')); 

                self::$x_modules = new x_modules();
                self::$modules = new modules(self::$sheet_table);
        
                self::$ds_export = (isset($j['options']['export_data_sheet']));

                if (in_array('prepare_data',$j['functions'])) {
                    return self::prepare_data();
                }
                if (in_array('load_access_control',$j['functions'])) {
                    return self::load_access_control();
                }
            }
        } catch (\Exception $e) {
            return common_message('error', $e->getMessage());
        }
    }
    
    /** MAIN **/
    private static function load_access_control () {
        
        $sheet_table = self::$sheet_table;
        $getid = self::$getid;

        list($raw_record, $COLUMNS, $na) = self::fetch_record();

        $access_control = self::access_control_data($raw_record, $sheet_table);
        $access_control = array_merge(
            $access_control,
            [
                'obm_id' => $getid,
                'sheet_table' => $sheet_table,
                't' => self::translations(),
            ]
        );
        return common_message('ok', $access_control);
    }

    private static function prepare_data() {
        global $protocol;

        $sheet_table = self::$sheet_table;
        $getid = self::$getid;
        
        $data = [
            'title' => t('str_datapage') . ": [$sheet_table] $getid",
            'obm_id' => $getid,
            'docheader' => (self::$ds_export) ? projectProperties() : false,
            'url' => $protocol.'://'.constant('URL'),
            'sheet_table' => $sheet_table,
        ];
        
        // checking if the row exists in the sheet_table
        if (!self::check_if_row_exists($sheet_table, $getid)) {
            $data['not_available'] = t('str_dataid_does_not_exists');
            return common_message('ok', $data);
        }
        
        // ???
        $_SESSION['evaluates'] = array();
        
        list($raw_record, $COLUMNS, $na) = self::fetch_record(); 

        if ($na) {
            $data['not_available'] = $na;
            return common_message('ok', $data);
        }
        
        self::$allowed_cols = self::allowed_columns(array_keys($COLUMNS));
        $MASTER = has_access('master');
        
        if (!$MASTER and self::$allowed_cols === ['obm_id']) {
            $data['not_available'] = t('str_this_record_is_not_available');
            return common_message('ok', $data);
        }
        
        $geom_show = false;
        $joined_data = self::prepare_joined_data($raw_record, $sheet_table);

        $species_info = array();
        if (isset($raw_record[$_SESSION['st_col']['SPECIES_C']]))
            $species_info = array('species'=>$raw_record[$_SESSION['st_col']['SPECIES_C']],'source'=>'GBiF');

        $add_attachments_module = '';
        foreach (self::$x_modules->which_has_method('add_attachments') as $mm) {
            $add_attachments_module = self::$x_modules->_include($mm,'add_attachments');
            break;
        }

        return common_message('ok', array_merge($data, [
            'photo_div' => $add_attachments_module,
            'geom_modal' => self::geom_modal(),
            't' => self::translations(),
            'is_master' => has_access('master'),
            'logged_in' => $_SESSION['Tid'] ?? false,
            'can_edit' => rst('mod', $getid, $sheet_table, has_access('master')),
            'attributes' => self::attributes_data($raw_record, $sheet_table, $COLUMNS, $geom_show),
            'header' => self::header_data($raw_record, $sheet_table),
            'evaluation' => self::evaluation_data($raw_record, $sheet_table),
            'geom_show' => $geom_show,
            'joined_data' => [
                'has_joined_data' => (count($joined_data) > 0),
                'data' => $joined_data
            ],
            'species_info' => [
                'has_species_info' => (count($species_info) > 0),
                'data' => $species_info
            ],
        ]));
    }

    private function fetch_record() {
        global $ID;
        $columns = $record = $na = false;

        require_once(getenv('OB_LIB_DIR').'results_builder.php');
        $rb = new results_builder();
        if ($rb->results_query(self::$getid,true)) {
            $result = pg_query($ID,$rb->rq['query']);
            if (!pg_num_rows($result)) {
                $na = t('str_this_record_is_not_available');
                return [$record, $columns, $na];
            }
        } else {
            throw new \Exception(join(' / ', $rb->error), 1);
        }
        $columns = $rb->rq['columns'];
        $record = pg_fetch_assoc($result);

        return [$record, $columns, $na];
    }
    

    /** COMPONENTS **/ 
    
    private static function prepare_joined_data($raw_record, $sheet_table) {
        $results = self::$modules->_include(
            'join_tables', 
            'load_joined_data', 
            ["mtable" => $sheet_table, "filter" => [['obm_id', $raw_record['obm_id']]]]
        );
        $data = [];
        if (!$results) {
            return $data;
        }
        $view_attachment_module = false;
        foreach (self::$x_modules->which_has_method('view_attachment') as $mm) {
            $view_attachment_module = $mm;
            break;
        }

        foreach ($results as $table => $rows) {
            if (!count($rows)) {
                continue;
            }
            $stable_params = ['obm_id' => 'obm_id'];

	    $joined_table_modules = new modules($table);

            $columns = $joined_table_modules->_include('results_asStable', 'get_params');
            // column names in order:
            $columns = array_values(array_intersect(array_keys($rows[0]), $columns));

            // thumbnails
            $rows = array_map(function($row) use ($view_attachment_module) {
                $keys = array_keys($row);
                $row = array_map(function($val, $key) use ($view_attachment_module) {
                    if ($key === 'obm_files_id' && $val != "" && $view_attachment_module!==false) {
                        $val = self::$x_modules->_include($view_attachment_module, 'get_thumbnails', $val);
                    }
                    return $val;
                }, $row, $keys);
                return array_combine($keys, $row);
            }, $rows);

            $data[] = [
                'joined_table' => defined("str_$table") ? constant("str_$table") : $table,
                'data' => array_map(function ($row) use ($columns) {
                    return ['row' => array_values(array_filter($row, function ($c) use ($columns) {
                        return (in_array($c, $columns));
                    }, ARRAY_FILTER_USE_KEY))];
                }, $rows),
                'columns' => $columns
            ];
        }
        return $data;
    }
    
    private static function attributes_data($raw_record, $sheet_table, $COLUMNS, &$geom_show) {
        $main_cols = dbcolist('columns',$sheet_table);
        $non_repetative = array();
        $BOLD_YELLOW = (self::$modules->is_enabled('bold_yellow')) ? self::$modules->_include('bold_yellow','mark_text',[],true) : array();
        
        $view_attachment_module = false;
        foreach (self::$x_modules->which_has_method('view_attachment') as $mm) {
            $view_attachment_module = $mm;
            break;
        }

        $record = [];
        foreach ($COLUMNS as $cn => $label) {
            if (preg_match('/(\w)+\.(.+)$/',$cn,$m)) {
                $cn = $m[2];
                if (preg_match('/(\w)+\.(.+)$/',$label,$m)) {
                    $label = $m[2];
                }
            }
            
            if (self::skip_column($cn, $raw_record, $non_repetative)) {
                continue;
            }
            
            //mark joined tables
            $jts = (!in_array($cn,$main_cols)) ? "graybg" : ""; //gray style
            
            $row = [
                'class' => (in_array($cn,$BOLD_YELLOW)) ? "lmh $jts" : "lm $jts",
                'label' => t($label),
                'thumbnails' => false
            ];

            if ($cn == 'obm_files_id') {
                $row['icon'] = 'paperclip';
                if ($view_attachment_module!==false) {
                    $row['thumbnails'] = (isset($raw_record[$cn])) ? self::$x_modules->_include($view_attachment_module, 'get_thumbnails', $raw_record[$cn]) : false;
                    $row['value'] = (has_access('master')) ? edit('',"{$cn}b{$raw_record['obm_id']}",$cn,!self::$ds_export,'edbox_send',0,'photo-here',$sheet_table) : "...";
                } else {
                    $row['thumbnails'] = (isset($raw_record[$cn])) ? false : false;
                    $row['value'] = (has_access('master')) ? edit($raw_record[$cn],"{$cn}b{$raw_record['obm_id']}",$cn,!self::$ds_export,'edbox_send',0,'photo-here',$sheet_table) : $raw_record[$cn];
                }
            }
            elseif ($cn == $_SESSION['st_col']['GEOM_C']) {
                $row['icon'] = 'map-marker';
                if (isset($raw_record[$cn])) {
                    $geom_show = self::process_geometry($raw_record[$cn]);
                }
                $row['value'] = (has_access('master')) ? edit($raw_record[$cn],"{$cn}b{$raw_record['obm_id']}",$cn,!self::$ds_export,'edbox_send',0,'geom-down',$sheet_table,'sheet') : $raw_record[$cn];
            }
            else {
                $row['icon'] = 'pencil';
                if (isset($raw_record[$cn])) {
                    $row['value'] = (has_access('master')) ? edit($raw_record[$cn],"{$cn}b{$raw_record['obm_id']}",$cn,!self::$ds_export,'edbox_send',0,'',$sheet_table) : $raw_record[$cn];
                }
                else {
                    // columns with missing values are skipped if user has no master access
                    $row['value'] =  edit("","{$cn}b{$raw_record['obm_id']}",$cn,!self::$ds_export,'edbox_send',0,'',$sheet_table);
                }
            }
            
            $record[] = $row;
        }
        return $record;
    }
    
    private static function access_control_data($raw_record, $sheet_table) {
        if (!isset($_SESSION['Tid'])) {
            return [];
        }
        
        $access_control = [];
        
        # master users
        $masters = new Role('project masters');
        $master_names = $masters->get_member_names();
        unset($masters);
        
        $uploader_id = $raw_record['uploader_id'];
        $uploader_name = $raw_record['uploader_name'];

        $ACC_LEVEL = constant('ACC_LEVEL');
        $MOD_LEVEL = constant('MOD_LEVEL');

        if ($_SESSION['st_col']['USE_RULES']) {
            
            $access_control['controller'] = 'Data access controlled by rules.';
            
            if (!is_null($raw_record['obm_sensitivity'])) {
                if (in_array($raw_record['obm_sensitivity'], ['public', '0'])) {
                    $access_control['readers'] = [
                        'label' => 'This record readable for',
                        'list' => 'everybody'
                    ];
                    $access_control['modifiers'] = [
                        'label' => 'The following people can modify this record',
                        'list' => implode(', ', array_unique(array_merge($master_names,array($uploader_name))))
                    ];
                } 
                else {
                    if (in_array($raw_record['obm_sensitivity'], ['no-geom', '1'])) {
                        $access_control['readers']['label'] = 'This record does not appear on the map, except for the following users';
                    }
                    elseif (in_array($raw_record['obm_sensitivity'], ['restricted', '2', 'sensitive'])) {
                        $access_control['readers']['label'] = 'This is sensitive record, with limited access, except for the following users';
                    }
                    elseif (in_array($raw_record['obm_sensitivity'], ['3', 'only-owner'])) { 
                        $access_control['readers']['label'] = 'This is a hidden record and can only be retrieved by the following users';
                    }
                    
                    $read_roles = $master_names;
                    foreach (explode(',',$raw_record['obm_read']) as $role_id) {
                        $r = new Role($role_id);
                        $read_roles = array_unique(array_merge($read_roles, $r->get_member_names()));
                    }
                    $access_control['readers']['list'] = implode(', ', $read_roles);
                    
                    $read_roles = $master_names;
                    foreach (explode(',',$raw_record['obm_write']) as $role_id) {
                        $r = new Role($role_id);
                        $read_roles = array_unique(array_merge($read_roles, $r->get_member_names()));
                    }
                    $access_control['modifiers'] = [
                        'label' => 'The following people can modify this record',
                        'list' => implode(', ', $read_roles)
                    ];
                    $access_control['allowed_columns'] = self::query_allowed_columns($raw_record, $sheet_table);
                }
            } 
            else {
                $access_control['no_rules_defined'] = "No rules defined for this data.";
                if ( in_array($ACC_LEVEL, ['1', 'login']) ) {
                    $access_control['readers']['label'] = 'This record readable for all logged-in users.';
                } 
                else if (in_array($ACC_LEVEL, ['0', 'public'])) {
                    $access_control['readers']['label'] = 'This record readable for everybody.';
                } 
                else if (in_array($ACC_LEVEL, ['2', 'group'])) {
                    $access_control['readers']['label'] = 'This record readable for the following people:';
                    $access_control['readers']['list'] = implode(', ',array_unique(array_merge($master_names,array($uploader_name))));
                }
                
                $access_control['allowed_columns'] = self::query_allowed_columns($raw_record, $sheet_table);
            }
            
        } 
        else {
            $access_control['controller'] = 'Access to data is general, defined at the project level.';
            
            if ( $ACC_LEVEL == '1' or $ACC_LEVEL == 'login' ) {
                $access_control['readers']['label'] = 'This record is readable for all logged-in users.';
            }
            else if ($ACC_LEVEL == '0' or $ACC_LEVEL == 'public') {
                $access_control['readers']['label'] = 'This record is readable for everybody.';
            } 
            else if ($ACC_LEVEL == '2' or $ACC_LEVEL == 'group') {
                $access_control['readers']['label'] = 'This record is readable for the following people:';
                $access_control['readers']['list'] = implode(', ', array_unique(array_merge($master_names,array($uploader_name))));    
            }
            
            if ( $MOD_LEVEL == '0' or $MOD_LEVEL == 'public' ) {
                $access_control['modifiers']['label'] = 'This record can be changed for everybody.';
            } elseif ( $MOD_LEVEL == '1' or $MOD_LEVEL == 'login' ) {
                $access_control['modifiers']['label'] = 'This record can be changed for all logged-in users.';
            } elseif ( $MOD_LEVEL == '2' or $MOD_LEVEL == 'group' or $MOD_LEVEL == '3' or $MOD_LEVEL == 'only-owner' ) {
                $access_control['modifiers']['label'] = 'This record can be modified for the following people:';
                $access_control['modifiers']['list'] = implode(', ', array_unique(array_merge($master_names,array($uploader_name))));
            }
        }
        
        # update rules
        if (has_access('master') and $_SESSION['st_col']['USE_RULES']) {
            $pm = new Role('project members');
            $roleoptions = array_map(function ($m) {
                $u = new userdata($m,'role_id');
                $description = $u->get_role_description();
                return "$description::$m";
            }, json_decode($pm->role['members']));
            
            $typeoptions = array_unique(self::$sensitivity_values);
            
            $read = explode(',',$raw_record['obm_read']);
            $write = explode(',',$raw_record['obm_write']);
            $sensitivity = self::$sensitivity_values[$raw_record['obm_sensitivity']] ?? "";
            
            $options_r = selected_option($roleoptions,$read);
            $options_w = selected_option($roleoptions,$write);
            $options_p = selected_option($typeoptions,$sensitivity);
            
            $access_control['update_rules'] = compact('options_r', 'options_w', 'options_p');
        }
        
        return $access_control;
    }
    
    private static function evaluation_data($raw_record, $sheet_table) {
        global $GID;
        
        $cmd = sprintf("SELECT SUM(valuation) AS score FROM system.evaluations WHERE row=%d AND \"table\"=%s",$raw_record['obm_id'], quote($sheet_table));
        if (!$sres = pg_query($GID,$cmd)) {
            throw new \Exception("Error Processing Query", 1);
        }
        
        $srow = pg_fetch_assoc($sres);
        
        $comments = array();
        $data_evaluation_score = $srow['score'];
        $summary_evaluation_score = validation($raw_record['obm_id'], $sheet_table);
        
        if (isset($_SESSION['Tid'])) {
            $comments = comments($raw_record['obm_id'], $sheet_table, 'data');
        }
        
        return compact('data_evaluation_score', 'summary_evaluation_score', 'comments');
    }
    
    /** HELPER METHODS **/
    
    private static function translations() {
        $strings = [
            'str_edit_record', 'str_header_information', 'str_uploader', 
            'str_changes', 'str_last_modify', 'str_views', 'str_downloads',
            'str_citations', 'str_data_access', 'str_data_access', 'str_read',
            'str_write', 'str_update', 'str_evaluation', 'str_data_evaluation_score',
            'str_summary_evaluation_score', 'str_comments', 'str_your_opinion',
            'str_thanks_for', 'str_opinion_post_1', 'str_avoid', 
            'str_opinion_post_2', 'str_dnlike', 'str_like', 'str_joined_data','str_species_info'
        ];
        return array_combine($strings, array_map(function ($str) {
            return t($str);
        }, $strings));
    }

    private static function check_if_row_exists($sheet_table, $getid) {
        global $ID;
        $res = pg_query($ID, sprintf("SELECT 1 FROM %s WHERE obm_id=%d", $sheet_table,$getid));
        return (pg_num_rows($res));
    }

    private static function skip_column($cn, $record, $non_repetative)
    {
        $u = (isset($_SESSION['Tuser'])) ? unserialize($_SESSION['Tuser']) : new User('non-logined-user');
        $tgroups = $u->roles ?? 0;
        extract(data_access_check(self::$acc, $_SESSION['st_col']['USE_RULES'], self::$allowed_cols, $tgroups, false, $record));

        return (
            in_array($cn, ['', 'hist_time']) ||                             // We do not print the history time column if it passed, as it is a special column and there is no meaning to edit it ever!
            ($column_restriction && !in_array($cn, self::$allowed_cols)) ||   // High level restrictions handling by allowed_columns module allow some columns for those who has no access right
            in_array($cn, $u->excluded_taxon_columns) ||                      // When multilingual taxon names are set up then we exclude other languages than the scientific and the selected
            ($geometry_restriction and $cn == 'obm_geometry') ||
            in_array($cn, $non_repetative) ||
            ($record[$cn] == '' && (!has_access('master') || self::$ds_export))                      // Skipping column if empty and the user is not master 
        );
    }
    
    private static function allowed_columns($allowed_cols) {
        
        extract(allowed_columns(self::$modules, self::$sheet_table));
        return $allowed_cols;
    }
    
    private static function geom_modal() {
        global $ID, $protocol;
        if (isset($_SESSION['Tid'])) {
            $cmd = sprintf(
                "SELECT id,name,to_char(timestamp,'YYYY.MM.DD:HH24:MI:SS') as datetime 
                FROM system.shared_polygons LEFT JOIN system.polygon_users p ON polygon_id=id 
                WHERE select_view IN ('2','3','upload','select-upload') AND p.user_id=%d 
                ORDER BY name,datetime",
                $_SESSION['Tid']
            );
        }
        else {
            $cmd = sprintf(
                "SELECT id,name,to_char(timestamp,'YYYY.MM.DD:HH24:MI:SS') as datetime 
                FROM system.shared_polygons 
                WHERE access IN ('4','public') OR (project_table='%s' AND access IN ('3','project')) 
                ORDER BY name,datetime",
                constant('PROJECTTABLE')
            );
        }
        
        $res = pg_query($ID,$cmd);
        $gopt = "<option>".t('str_get_geometry_from_named_list')."</option>";
        while($row=pg_fetch_assoc($res)) {
            $gopt .= sprintf("<option value='%d'>%s - %s</option>",$row['id'],$row['name'],$row['datetime']);
        }
        
        $u = "$protocol://".constant('URL')."/upload/geomtest/?addgeom";
        return "<div class='fix-modal' id='gpm' style='border:1px solid #666;border-bottom:4px solid #666'>
        <h2 style='color:white !important;background-color:#666 !important;padding:6px;margin:-10px -10px 10px -10px !important'>".t('str_geometry')."</h2><button style='margin: -11px' class='pure-button button-passive fix-modal-close' id='gpm-close'><i class='fa fa-close'></i></button>
        <br>
        <ul style='list-style-type:none;padding:10px'>
        <li style='font-size:150%;padding-bottom:1em'><a href='$u' id='map_' class='mapclick'><i class='fa fa-lg fa-map-marker fa-fw'></i> ".t('str_get_coordinates_from_map')."</a></li>
        <li style='font-size:150%'><i class='fa fa-lg fa-map fa-fw'></i> <select id='gopt_' class='goptchoice'>$gopt</select></li></ul>
        </div>";
    }
    
    private static function process_geometry($geom_show) {
        global $BID, $protocol;
        
        if (preg_match('/^point|line|polygon/i',$geom_show))
            $cmd = "SELECT st_AsGeoJSON('$geom_show'::text,15,0) as gjson, st_AseWKT('$geom_show'::text) as wkt";
        else
            $cmd = "SELECT st_AsGeoJSON('$geom_show'::geometry,15,0) as gjson, st_AseWKT('$geom_show'::geometry) as wkt";

        if (!$res = pg_query($BID,$cmd)) {
            throw new \Exception("Error Processing Query", 1);
        }
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);  
            //{"type":"Point","coordinates":[18.767587411656972,47.785560590815244]}
            //$geojson = '{"type":"Feature","geometry": {"type": "Point","coordinates": [125.6, 10.1]},"properties": {"name": "Dinagat Islands"}}';
            $wkt = preg_replace('/SRID=\d+;/','',$row['wkt']);
            return [
                'geom' => addslashes($row['gjson']), 
                'stylesheets' => [
                    ['file' => '/css/openlayers_v6.14.1_css_ol.css', 'rev' => rev(getenv('PROJECT_DIR') . '/css/openlayers_v6.14.1_css_ol.css') ],
                    ['file' => '/css/ol-layerswitcher.css', 'rev' => rev(getenv('PROJECT_DIR') . '/css/ol-layerswitcher.css') ],
                ],
                'scripts' => [
                    ['file' => '/js/openlayers_v6.14.1_build_ol.js', 'rev' => rev(getenv('PROJECT_DIR') . '/js/openlayers_v6.14.1_build_ol.js') ],
                    ['file' => '/js/ol-layerswitcher.js', 'rev' => rev(getenv('PROJECT_DIR') . '/js/ol-layerswitcher.js') ],
                    ['file' => '/js/map_init.js', 'rev' => rev(getenv('PROJECT_DIR') . '/js/map_init.js') ],
                    ['file' => '/js/map_functions.js', 'rev' => rev(getenv('PROJECT_DIR') . '/js/map_functions.js') ],
                    
                ]
            ];
            
        }
        return false;
    }
    
    private static function header_data($raw_record, $sheet_table) {
        // header
        $header = [];

        // adatfeltöltés adatai
        $header['uploader_name'] = ($ulink=fetch_user($raw_record['uploader_id'],'roleid','link')) ? $ulink : $raw_record['uploader_name'];

        // create readable links instead of long get urls
        $header['uploading_id'] = $raw_record['uploading_id'];
        $header['uploading_date'] = preg_replace('/:\d{2}$/','',preg_replace('/\.\d+/','',$raw_record['uploading_date']));

        // History
        $genid = (preg_match('/([0-9]+)_([0-9]+)/',$raw_record['obm_id'],$m)) ? $m[1] : $raw_record['obm_id'];
        
        $qtable_history = sprintf("%s_history",preg_replace("/_history$/","",constant('PROJECTTABLE')));
        $header['last_mod'] = data_changes($qtable_history,$sheet_table,$genid);
        
        // Metrics
        $header['metrics'] = [
            'views' => metrics($raw_record['obm_id'],'data_views'),
            'citations' => json_decode(cite_metrics($raw_record['obm_id'],'citations',$sheet_table)),
            'downloads' => metrics($raw_record['obm_id'],'downloads'),
        ];
        
        return $header;
    }
    
    private static function query_allowed_columns($raw_record, $sheet_table) {

        # Ez nem jó, mert csak a bejelentkezett admin szintjén értelmezett modulokat látja, azaz csak egy allowed_columns-ot kérdez le, nem az összeset!
        #$main_cols = array_merge(dbcolist('columns', $sheet_table), array('obm_id','uploader_name','uploading_date','uploading_id'));
        #$allowed_cols = self::$modules->_include('allowed_columns','return_columns',$main_cols,true); // for_sensitive_data
        #$allowed_gcols = self::$modules->_include('allowed_columns','return_gcolumns',$main_cols,true); // for_no-geom_data
        #$a = array('for_sensitive_data'=>$allowed_cols,'for_no-geom_data'=>$allowed_gcols);
        #$d = self::$modules->get_modules();
        #$key = array_search('allowed_columns', array_column($d, 'module_name'));
        #debug($d[$key]);
        
        global $BID;
        $cmd = sprintf(
            "SELECT new_params as params, module_access, array_to_string(group_access,',') as groups 
            FROM modules 
            WHERE module_name='allowed_columns' AND main_table='%s' AND enabled='t'",
            $sheet_table
        );
        
        $res = pg_query($BID,$cmd);
        $allowed_columns = false;

        
        while ($row = pg_fetch_assoc($res)) {
            $ac = [];
            $p = json_decode($row['params'], true);
            
            if (in_array($raw_record['obm_sensitivity'], ['restricted', 'sensitive', '2'])) {
                $lst = $p['for_sensitive_data'];
            }
            elseif (in_array($raw_record['obm_sensitivity'],  ['1', 'no-geom'])) {
                $lst = $p['for_no-geom_data'];
            }
            else {
                continue;
            }
            // column list
            $ac['list'] = implode(', ', $lst);
            
            $roles = array();
            foreach(explode(',',$row['groups']) as $role_id) {
                $r = new Role($role_id);
                $roles = array_merge($roles,$r->get_member_names());
            }
            $role_list = implode(', ', array_unique($roles));


            if ($row['module_access'] == 0) {
                $ac['label'] = 'The following columns are accessible for everybody (based on allowed_columns settings)';
            } 
            else {
                if ($row['groups'] == '0') {
                    $row['groups'] = 'logged-in users';
                }
                $ac['label'] = "The following columns are accessible for $role_list";
            }
            $allowed_columns[] = $ac;
        }
        return $allowed_columns;
    }
    
    
}

?>
