<?php
    header('Content-Type: text/javascript', true);
    require(getenv('OB_LIB_DIR').'db_funcs.php');
    if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS databases.");
    if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
        die("Unsuccessful connect to GIS database with biomaps.");
    if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
        die("Unsuccesful connect to UI database.");
    
    require(getenv('OB_LIB_DIR').'modules_class.php');
    require(getenv('OB_LIB_DIR').'common_pg_funcs.php');
    require(getenv('OB_LIB_DIR').'auth.php');
    require(getenv('OB_LIB_DIR').'prepare_auth.php');
    require(getenv('OB_LIB_DIR').'prepare_session.php');
    require(getenv('OB_LIB_DIR').'languages.php');
    #if (isset($_SESSION['Tid']))
    #    require_once(getenv('OB_LIB_DIR').'languages-admin.php');
    setExpires(30);

    #debug('include modules.js.php',__FILE__,__LINE__);
    // list of modules which contains print_js functions - should be automatically loaded 
    $mj = new modules($_SESSION['current_query_table']);
    $mjx = new x_modules();

    $js = "";

    
    #$mtable = isset($_REQUEST['mtable']) ? $_REQUEST['mtable'] : (isset($_SESSION['current_query_table']) ? $_SESSION['current_query_table'] : PROJECTTABLE);
    #debug($mtable,__FILE__,__LINE__);

    // Module hook: print_js
    // print javascripts
    if (!isset($_GET['no_table_modules'])) { // ez mi?
        $m_c = array();
        foreach ($mj->which_has_method('print_js',true) as $m) {
            $m_c[] = $m;
        }
        $m_c = array_unique($m_c); // preventing double include
        foreach ($m_c as $m) {
            echo $mj->_include($m,'print_js');
            # A results_buttons-ra azt mondja, hogy not enabled, amikor az echo megtörténik. Ha csak debug-ba megy a kimenet, nem mondja, hogy nincs engedélyezve... Miért???
        }
    }
    foreach ($mjx->which_has_method('print_js') as $m) { 
        echo $mjx->_include($m,'print_js');
    }
    
    # BAD....
    #require_once('vendor/autoload.php');
    #$output = \Garfix\JsMinify\Minifier::minify($js);
    //echo $js;
    #echo $output;
    
?>
