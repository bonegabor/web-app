<?php

if ($scope == 'database') {

    $lang = $_SESSION['LANG']; //default english in DOI

    $cmd = "SELECT short,long,language,email FROM project_descriptions LEFT JOIN projects ON (project_table=projecttable) WHERE projecttable='".PROJECTTABLE."'";
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_all($res);
    $k = array_search($_SESSION['LANG'], array_column($row, 'language'));
    if(!$k) $k = 0;
    $OB_project_title = $row[$k]['short'];
    $OB_project_description = $row[$k]['long'];
    $OB_contact_email = $row[$k]['email'];

    $cmd = 'SELECT "Creator" as founder,to_char(creation_date,\'YYYY mon. DD.\') as fdate,creation_date,
        to_char(running_date,\'YYYY mon. DD.\') as rdate,doi,licence,rum,licence_uri,array_to_string(alternate_id,\';\') as aid,lower(collection_dates) as cdate1,upper(collection_dates) as cdate2,project_hash,ARRAY_TO_STRING(subjects,\';;\') AS subjects,geolocation
FROM projects WHERE project_table=\''.PROJECTTABLE.'\'';
    $result = pg_query($BID,$cmd);
    $project_row=pg_fetch_assoc($result);

    $out = sprintf("<h2 style='font-size:2em !important'>DataCite DOI metadata page for \"<i>%s</i>\" database<br></h2>",PROJECTTABLE);
    $json = array();

    # DOI
    $project_hash = substr(base_convert(md5($project_row['creation_date'].$project_row['founder'].PROJECTTABLE), 16,32), 0, 12);
    if ($project_row['doi']!='') {
        $doi = '<label class="unselectable">doi: </label>'.$project_row['doi'];
        $json['Identifier'] = 'DOI: '.$project_row['doi'];
    } else if ($project_row['project_hash']!='') {
        $doi = "<span style='color:red'>DOI: NOT REGISTERED YET</span> <i>10.18426/obm.".$project_row['project_hash']."</i>";
        $json['Identifier'] = 'DOI: NOT REGISTERED YET 10.18426/obm'.$project_row['project_hash'];
    } else {
        $out .= "<ul><li>Fill the project_hash: ".$project_hash."</li></ul>";
        return $out;
    }
    $out .= "<a name='Identifier'></a><h2>Identifier</h2>";
    $out .= sprintf("<ul style='padding:0 0 30px 30px;list-style-type:none'><li style='font-size:2.0em'>%s</li></ul>", $doi);

    # CREATOR
    $out .= "<a name='Creators'></a><h2>Creators</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>%s</li>",$project_row['founder']);
    $out .= "</ul>";
    $json['Creator'] = $project_row['founder'];

    # TITLE
    $out .= "<a name='Titles'></a><h2>Titles</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $cmd = "SELECT short FROM project_descriptions WHERE language = 'en' AND projecttable='".PROJECTTABLE."'";
    if ($res = pg_query($BID,$cmd)) {
        $row = pg_fetch_assoc($res);
        $OB_project_title = $row['short'];
    }

    $out .= sprintf("<li>%s</li>",$OB_project_title);
    $out .= "</ul>";
    $json['Titles'] = $OB_project_title;

    # PUBLISHER
    $out .= "<a name='Publisher'></a><h2>Publisher</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>%s</li>","The OpenBioMaps Consortium");
    $out .= "</ul>";
    $json['Publisher'] = "The OpenBioMaps Consortium";

    # PUBLICATION YEAR
    $out .= "<a name='PublicationYear'></a><h2>Publication Year</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>%s</li>",$project_row['fdate']);
    $out .= "</ul>";
    $json['PublicationYear'] = $project_row['fdate'];

    # SUBJECT
    $out .= "<a name='Subject'></a><h2>Subject</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    foreach (preg_split('/;;/',$project_row['subjects']) as $s) {
        $out .= "<li>$s</li>";
    }
    $out .= "</ul>";
    $json['Subject'] = $project_row['subjects'];

    # CONTRIBUTORS
    $out .= "<a name='contributors'></a><h2>Contributors</h2>";
    $cmd = 'SELECT id,username,institute,email,orcid
    FROM users u 
    LEFT JOIN project_users pu ON (pu.user_id=u.id)
    WHERE pu.project_table=\''.PROJECTTABLE.'\' AND user_status::varchar IN (\'2\',\'master\')';
    $result = pg_query($BID,$cmd);
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    //$out .= sprintf("<li>Type: DataCurator</li><ul><li>Name: %s</li><li>Affiliation: %s &nbsp; %s</li>",$row['username'],$row['institute'],$row['email']);
    while ($row=pg_fetch_assoc($result)) {
        $out .= sprintf("<li>Type: DataManager</li><ul><li>Name: %s</li><li>Affiliation: %s &nbsp; %s</li>",$row['username'],$row['institute'],$row['email']);
        if ($row['orcid']!='') {
            $out .= sprintf("<li>Identifier: <ul><li>Identifier Scheme: ORCID</li><li>Scheme URI: http://orcid.org/</li><li>Identifier: %s</li></ul></li>",$row['orcid']);
        }
        $out .= "</ul>";
    }
    $out .= "</ul>";
    $json['Contributors'] = array(
        'Type'=>array(
            'Data Manager'=>array(
                'Name'=>$row['username'],'Affiliation'=>$row['institute'].' '.$row['email'],'Identifier'=>array(
                'Identifier Scheme'=>'ORCID','Scheme URI'=>'http://orcid.org/','Identifier'=>$row['orcid']))
    ));

    # DATES
    $out .= "<a name='Dates'></a><h2>Dates</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>Accepted: %s</li>",$project_row['rdate']);
    if ($project_row['cdate1']!='' and $project_row['cdate2']!='')
        $out .= sprintf("<li>Collected: %s - %s</li>",$project_row['cdate1'],$project_row['cdate2']);
    $out .= "</ul>";
    $json['Dates'] = array('Accepted'=>$project_row['rdate'],'Collected'=>"{$project_row['cdate1']},{$project_row['cdate2']}");

    ## LANGUAGE
    $out .= "<a name='Languages'></a><h2>Languages</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";

    $l = array('en'=>'English','hu'=>'Hungarian','ro'=>'Romanian');
    $cmd = "SELECT language FROM project_descriptions WHERE projecttable='".PROJECTTABLE."'";
    $result = pg_query($BID,$cmd);
    $jl = array();
    while ($row=pg_fetch_assoc($result)) {
        if (array_key_exists($row['language'],$l)) {
            $out .= sprintf("<li>%s</li>",$l[$row['language']]);
            $jl[] = $l[$row['language']];
        } else {
            $out .= sprintf("<li>%s</li>",$row['language']);
            $jl[] = $row['language'];
        }
    }
    $out .= "</ul>";
    $json['Languages'] = $jl;

    # RESOURCE TYPE
    $out .= "<a name='ResourceType'></a><h2>Resource Type</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>Service</li>");
    $out .= "</ul>";
    $json['ResourceType'] = 'Service';

    # ALTERNATE IDENTIFIER
    $out .= "<a name='AlternateIdentifier'></a><h2>Alternate Identifier</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>URL: http://%s</li>",URL);
    if ($project_row['aid']!='')
        $out .= sprintf("<li><i>(type)</i>: %s</li>",$project_row['aid']);
    $out .= "</ul>";
    $json['AlternateIdentifier'] = $project_row['aid'];

    # SIZES
    $out .= "<a name='Sizes'></a><h2>Sizes</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";

    $cmd = "SELECT f_project_table FROM header_names WHERE f_project_name='".PROJECTTABLE."' AND f_project_table LIKE '".PROJECTTABLE."%'";
    $result = pg_query($BID,$cmd);
    $approx = 0;
    while($row = pg_fetch_assoc($result)) {
        $cmd = "SELECT reltuples AS approximate_row_count FROM pg_class WHERE relname = '{$row['f_project_table']}'";
        $result2 = pg_query($ID,$cmd);
        $rowe = pg_fetch_assoc($result2);
        $approx += $rowe['approximate_row_count'];
    }

    $out .= sprintf("<li>%s tables</li>",pg_num_rows($result));
    $out .= sprintf("<li>%d rows</li>",$approx);
    $out .= "</ul>";
    $json['Sizes'] = $row['array_length'].'/'.$approx;

    # RIGHTS
    $out .= "<h2>Rights</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>Rights: %s <ul><li>URI: %s</li></ul></li>",$project_row['licence'],$project_row['licence_uri']);
    $out .= "</ul>";
    $json['Rights'] = Array($project_row['licence'],Array('URI'=>$project_row['licence_uri']));

    # DESCRIPTION / ABSTRACT
    $out .= "<h2>Description</h2>";

    $cmd = "SELECT long FROM project_descriptions WHERE language = 'en' AND projecttable='".PROJECTTABLE."'";
    if ($res = pg_query($BID,$cmd)) {
        $row = pg_fetch_assoc($res);
        $OB_project_description = $row['long'];
    }
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= "<li>Abstract: <ul><li style='width:900px;text-align:justify'>$OB_project_description</li></ul></li>";
    $json['Description'] = $OB_project_description;

    # DESCRIPTION / OTHER
    $out .= "<li>Other: <ul>";
    $cmd = "SELECT srid,type FROM geometry_columns WHERE f_project_name='".PROJECTTABLE."'";
    if ($res = pg_query($ID,$cmd)) {
        $row = pg_fetch_assoc($res);
        $out .= sprintf('<li>GEO Projection (epsg): %1$s</li>',$row['srid']);
        $out .= sprintf("<li>PostGIS geometry type: %s</li>",strtolower($row['type']));
    }

    if (defined('PUBLIC_MAPFILE')) {
        $url = "http://".URL."/public/proxy.php?MAP=PMAP&SERVICE=WFS&VERSION=1.1.0&REQUEST=GetCapabilities";
        $xml = simpleXML_load_file($url,"SimpleXMLElement",LIBXML_NOCDATA);
        if ($xml !== FALSE) {
            $out .= "<li>Public <i>WFS</i> service: $url</li>";
        }
        $url = "http://".URL."/public/proxy.php?MAP=PMAP&SERVICE=WMS&VERSION=1.1.0&REQUEST=GetCapabilities";
        $xml = simpleXML_load_file($url,"SimpleXMLElement",LIBXML_NOCDATA);
        if ($xml !== FALSE) {
            $out .= "<li>Public <i>WMS</i> service: $url</li>";
        }
    }
    $out .= sprintf("<li>OBM openness class: %s <ul><li>URI: %s</li></ul></li>",$project_row['rum'],'http://openbiomaps.org/documents/en/faq.html#what-is-the-rum');
    $out .= "</ul></li></ul>";

    # GeoLocation
    $out .= "<h2>GeoLocation</h2>";
    $out .= "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
    $out .= sprintf("<li>geoLocationPlace: %s</li>",$project_row['geolocation']);
    $out .= "</ul>";
    $json['GeoLocation'] = Array($project_row['geolocation']);



    $out .= "</ul>";

    $_SESSION['LANG'] = $lang;
} 
elseif ($scope == 'query') {

    if ($META_ID == '__DOI__') {
        $w = sprintf("q.doi ILIKE %s",quote('%'.$DOI));
    
    } else {
        list($id,$s) = preg_split('/@/',$META_ID);
        $w = "q.id='$id' AND sessionid='$s'";
    }
    
    $cmd = sprintf("SELECT name,datetime,query,to_char(datetime,'YYYY-MM-DD HH:MI') as date,\"type\",abstract,authors,q.licence,q.licence_uri,q.id,
                            sessionid,\"domain\",username,q.doi,query,length(result) as size,ARRAY_TO_STRING(q.subjects,';;') as subjects,formats,data_collectors,
                            lower(date_range) as date_range_from, upper(date_range) AS date_range_to,publisher,version, q.geolocation,fundingreference,language,computational_package
                        FROM project_repository q LEFT JOIN header_names ON (f_project_table=q.project) LEFT JOIN \"projects\" ON (f_project_name=project_table) LEFT JOIN users ON (user_id=users.id) 
                        WHERE %s",$w); 

    $result = pg_query($BID,$cmd);
    $em = '';
    if (pg_num_rows($result)) 
    {    
        $row = pg_fetch_assoc($result);

        $project_hash = substr(base_convert(md5($row['date'].$row['username'].PROJECTTABLE), 16,32), 0, 12);
        $doi = ($row['doi']!='') ? $row['doi'] : "DOI: <i>10.18426/obm.$project_hash</i><br><span style='color:red'>This DOI not registered yet!</span>";
        $link = ($row['domain']!='') ? sprintf("%s",$row['domain']) : URL;

        $em = "<h2 style='font-size:2em !important'>Metadata sheet for the permanently stored dataset with the identifier: <i>DOI:$load_loadmeta</i><br></h2>";

        $em .= '<ul style="list-style-type:none">';

        $em .= "<li><h2>Identifier</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>$doi</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";
            
        $em .= "<li><h2>Creators</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>".$row['authors']."</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";
            
        $em .= "<li><h2>Titles</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>".$row['name']."</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";
            
        $em .= "<li><h2>Publisher</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>{$row['publisher']}</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";
            
        $em .= "<li><h2>PublicationYear</h2>"; // Year
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>".$row['date']."</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2>ResourceType</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>{$row['type']}</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2>Subjects</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            //$subem .= sprintf("<li>Data subset based on query from ".PROJECTTABLE." database:<br> %s</li>",preg_replace("/\\\/","",str_replace(array("\\r\\n", "\\r", "\\n"), "<br />",$row['query'])));
            foreach (preg_split('/;;/',$row['subjects']) as $s) {
                $subem .= "<li>$s</li>";
            }
            $subem .= "</ul>";
            $em .= $subem."</li>";
            
        $em .= "<li><h2>Contributors</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>Type: DataCollector</li><ul>";
            $subem .= "<li>ContributorName: {$row['data_collectors']}</li>";
            $subem .= "</ul></ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2>Dates</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>From {$row['date_range_from']} to  {$row['date_range_to']}</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2><i>Language</i></h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>{$row['language']}</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2><i>AlternateIdentifier</i></h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>AlternateIdentifierType: OBM-PermanentIdentifier<ul><li><a href='$protocol://$link/Share/{$row['id']}@{$row['sessionid']}/'>$protocol://$link/Share/{$row['id']}@{$row['sessionid']}/</a>"."</li></ul></li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2>Related Identifier</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>{$row['relatedidentifier']}</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2><i>Size</i></h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>{$row['size']} bytes</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2><i>Formats</i></h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>{$row['formats']}</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2><i>Version</i></h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>{$row['version']}</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2><i>Rights</i></h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>Licence:<ul><li>{$row['licence']}</li></ul></li>";
            $subem .= "<li>Licence-URI:<ul><li>{$row['licence_uri']}</li></ul></li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2>Description</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>Abstract:<ul><li>{$row['abstract']}</li></ul></li>";
            $subem .= "<li>Other:"; 
            $subem .= "<ul>";

            if ($row['computational_package']!='') {
                $package = $row['computational_package'];

                $ciphertext = sslEncrypt($package,MyHASH);
                $comp = sprintf("<a href='$protocol://$link/boat/?viewcomputation=$ciphertext&params'>".t('str_view_computations')."</a>");
                $subem .= "<li>Reproduce this analisis: $comp </li>";
                $files = glob("computational_packages/$package/.git");
                if (count($files)) {
                    $res = exec("git config --get remote.origin.url");
                    $subem .= "<li>GIT repository of analisis: $res </li>";
                }
            }

            $subem .= "<li>Raw data (saved state): <a href='$protocol://$link/Share/{$row['id']}@{$row['sessionid']}/data/'>$protocol://$link/Share/{$row['id']}@{$row['sessionid']}/data/</a></li>";
            $subem .= "<li>Map (saved state): <a href='$protocol://$link/Share/{$row['id']}@{$row['sessionid']}/map/'>$protocol://$link/Share/{$row['id']}@{$row['sessionid']}/map/</a></li>";
            $subem .= "<li>Data table (current state): <a href='$protocol://$link/Share/{$row['id']}@{$row['sessionid']}/table/'>$protocol://$link/Share/{$row['id']}@{$row['sessionid']}/table/</a></li>";

            $subem .= "</ul></li>";
            $subem .= "</ul></li>";
            $em .= $subem."</li>";

        $em .= "<li><h2>GeoLocation</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>{$row['geolocation']}</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";

        $em .= "<li><h2><i>FundingReference</i></h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>{$row['fundingreference']}</li>";
            $subem .= "</ul>";
            $em .= $subem."</li>";


        $em .= '</ul>';
    }
    $out = $em;

    // DataCite Citation
    $out .= "<h3><a href='https://doi.org/10.14454/7xq3-zf69'>DataCite Metadata Schema 4.3</a></h3>";
}

/*
$e = new schemaem('Contributors');
$e->subadd(null,'contributor');
$e->attradd('contributor','contributorType','DataCollector');
$e->subadd('contributor','contributorName','Bán Miklós');
$e->subadd('contributor','givenName','Miklós');
$e->subadd('contributor','familyName',null,'Bán');
$e->subadd(null,'contributor');
$e->attradd('contributor','contributorType','ProjectLeader');
$e->jsonout();

$e = new schemaem('Language','en-US');
$e->jsonout();

$e = new schemaem('resourceType','XML');
$e->attradd(null,'resourceTypeGeneral','Software');
$e->jsonout();
 */

class schemaem {
    public $em;
    public $subem;
    private $p;
    public $subcounter = 0;

    function __construct($name,$value=null) {
        $this->p = $name;
        if ($value===null)
            $this->em = array($name => array());
        else
            $this->em = array($name => array('value'=>$value));
    }

    function attradd($path=null,$key=null,$value=null) {
        if ($path===null) {
            if (isset($this->em[$this->p]['attr']))
                $this->em[$this->p]['attr'][$key] = $value;
            else {
                $this->subcounter--;
                $this->em[$this->p]['attr'] = array($key=>$value);
            }
        } else {
            if (isset($this->em[$this->p][$this->subcounter][$path]['attr']))
                $this->em[$this->p][$this->subcounter][$path]['attr'][$key] = $value;
            else
                $this->em[$this->p][$this->subcounter][$path]['attr'] = array($key=>$value);
        }
    }

    function subadd($path=null,$name,$value=null) {
        if ($path===null) {
            $this->em[$this->p][] = array($name => array());
            $this->subcounter++;
        } else {
            if ($value===null)
                $this->em[$this->p][$this->subcounter][$path][$name] = array();
            else
                $this->em[$this->p][$this->subcounter][$path][$name] = $value;
        }
    }

    function jsonout () {
        echo "<pre>".json_encode($this->em, JSON_PRETTY_PRINT)."</pre>";
    }
    function yamlout () {
    }
    function xmlout () {
    }
    function htmlout () {
            $em .= "<li><h2>Contributors</h2>";
            $subem  = "<ul style='padding:0 0 30px 30px;list-style-type:none'>";
            $subem .= "<li>Type: DataCollector</li><ul>";
            $subem .= "<li>ContributorName: {$row['data_collectors']}</li>";
            $subem .= "</ul></ul>";
            $em .= $subem."</li>";


    } 
}
?>
