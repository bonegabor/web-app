$(document).ready(function() {
    $(document).on('click','#export_project', async function(ev) {
        ev.preventDefault();
        try {
            const include_visitors = $('#include-visitors').prop('checked');
            const include_shared = [];
            $('[name="include-shared"]:checked').each(function () {
              include_shared.push($(this).val())
            });
            const new_project_name = $('#new-project-name').val();
            const parameters = {
                m: 'move_project',
                include_visitors: include_visitors,
                include_shared: include_shared,
                new_project_name: new_project_name,
                action: 'export_project',
            }
            let retval = await $.post('ajax', parameters);
            retval = JSON.parse(retval);

            const msgdiv = $('#message-div');
            if ( retval.status == 'success' ) {
                msgdiv.removeClass().addClass('message message-success');
                msgdiv.html(retval.data);
            }
            else if (retval.status == 'warning') {
                msgdiv.removeClass().addClass('message message-warning');
                msgdiv.html(retval.message);
            }
            else if (retval.status == 'error') {
                msgdiv.removeClass().addClass('message message-error');
                msgdiv.html(retval.message);
            }
            msgdiv.css('height','auto');
            msgdiv.show();
        }
        catch (error) {
            console.log(error);
        }
    });
});
