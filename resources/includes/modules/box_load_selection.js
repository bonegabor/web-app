// box load selection module

/* modules/box_load_selection.php
 * FILTERBOX
 * Szelekció betöltése
 * can be multipolygon width several repeated WFS query, wich resulting various output
 *
 * */
$(document).ready(function () {
  $("#mapholder").on("click", "#shared-geom-settings-btn", function () {    
    $("#shared-geom-settings").toggle();
  });
  const getWktGeometry = () => {
    if ($("#shared_geom_id").val()) {
      vectorLayer.getSource().clear();
      geometry_query_selection = 0;
      $.post(
        "ajax",
        {
          getWktGeometry: $("#shared_geom_id").val(),
          buffer: $("#spatial_buffer_input").val(),
        },
        function (data) {
          // draw polygon on map
          drawPolygonFromWKT(data, 1);
          geometry_query_selection = 1;
        }
      );
    }
  };

  $("#mapfilters").on("change", "#shared_geom_id", function () {
    getWktGeometry();
  });

  $("#spatial_buffer_input").on("input", (e) => {
    $("#spatial_buffer_slider").slider("value", e.target.valueAsNumber);
  });

  $("#spatial_buffer_slider").slider({
    min: 0,
    max: 1000,
    value: 0,
    slide: function (event, ui) {
      $("#spatial_buffer_input").val(ui.value);
    },
  });

  $("#set_buffer").click(() => getWktGeometry());
});
