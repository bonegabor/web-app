-- create_terms_table
CREATE SEQUENCE %1$s_term_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
CREATE TABLE "public"."%1$s_terms" ( 
    term_id integer DEFAULT nextval('%1$s_term_id_seq') NOT NULL, 
    wid SERIAL, 
    data_table character varying(64) NOT NULL, 
    subject character varying(128) NOT NULL, 
    term character varying NOT NULL, 
    description character varying, 
    status character varying(128), 
    taxon_db integer DEFAULT 0,
    role_id integer,
    parent_id integer,
    CONSTRAINT %1$s_data_table_subject_term_key UNIQUE ("data_table","subject","term")
) WITH (oids = false); 

-- create_terms_table_index
CREATE INDEX CONCURRENTLY index_%1$s_terms_on_term_trigram ON %1$s_terms USING gin (term gin_trgm_ops);
