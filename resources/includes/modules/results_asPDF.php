<?php
#' ---
#' Module:
#'   results_asPDF
#' Files:
#'   [results_asPDF.php]
#' Description: >
#'   Query result export as PDF
#' Methods:
#'   [print_data, export_as]
#' Module-type:
#'   table
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   0.1
class results_asPDF extends module {
    
    var $error = '';
    var $retval;
    var $strings = ['str_reports'];
    
    function __construct($action = null, $params = null,$pa = array()) {

        $this->params = $params;
        
        if ($action) {
            $this->retval = $this->$action($params,$pa);
        }
    }

    protected function moduleName() {
        return __CLASS__;
    }

    public function print_data($params,$pa) {

        try {
            $de = new DataExport((int)$pa['de_id']);
            if (!$this->templateExists($pa['report'])) {
                throw new Exception('Template file missing');
            }

            $custom_methods_file = getenv('PROJECT_DIR') . "local/includes/modules/results_asPDF.php";
            $data = [];
            $m = "prepare_report_{$pa['report']}";
            $cm_conf_m = "config_{$pa['report']}";

            // including the custom methods for preparing the report data of additional reports or overriding the default reports
            if (file_exists($custom_methods_file)) {
                require_once($custom_methods_file);
            }
            $config = [];
            if (class_exists('results_asPDF_CM') && method_exists('results_asPDF_CM', $m)) {
                $data = results_asPDF_CM::$m($de);
                if (method_exists('results_asPDF_CM', $cm_conf_m)) {
                    $cm_conf = results_asPDF_CM::$cm_conf_m();
                    if (is_array($cm_conf)) {
                        $config = array_merge($config, $cm_conf);
                    }
                }
            }
            elseif (method_exists(__CLASS__, $m)) {
                $data = self::$m($de);
            }
            
            require_once(getenv('OB_LIB_DIR') . 'vendor/autoload.php');
            $mpdf = new \Mpdf\Mpdf($config);
            $mpdf->WriteHTML(file_get_contents(getenv('PROJECT_DIR') . 'css/pure/pure-min.css'), \Mpdf\HTMLParserMode::HEADER_CSS);
            $mpdf->WriteHTML(render_view("documents/report_{$pa['report']}", [
                'docheader' => projectProperties(),
                'data' => $data,
                'docfooter' => []
            ], false));
            $mpdf->Output($de->filename . "_report_{$pa['report']}.pdf", "I");

        } catch (Exception $e) {
            $this->error = $e->getMessage();
            return;
        }
    }

    private function templateExists($report) {
        $p = getenv('PROJECT_DIR') . '%s' . STYLE_PATH . "/views/documents/report_$report.mustache";
        return (file_exists(sprintf($p, 'local')) || file_exists(sprintf($p,"")));
    }

    private static function prepare_report_observations($de) {
        global $modules;

        $observations = [];
        $colnames = array_keys($de->colnames);
        $collabels = $de->colnames;

        $indexes = ['obm_id'];
        if ($modules->is_enabled('results_asStable')) {
            $indexes = array_merge($indexes, array_map(function ($col) {
                return ($col === 'obm_taxon') ? $_SESSION['st_col']['SPECIES_C'] : $col;
            }, $modules->_include('results_asStable', 'get_params')));
        }

        if ($handle = fopen($de->tmpfile(), "r")) {
            while (($row = fgetcsv($handle, $de->maxlength)) !== FALSE) {
                $row = array_combine($colnames, $row);
                $r = array_map(function ($idx) use ($row) {
                    global $modules;
                    if ($idx === 'obm_files_id' && $row[$idx] !== '') {
                        $val = ['photo' => true, 'val' => $modules->_include('photos', 'get_thumbnails', $row[$idx])];
                    }
                    elseif ($idx === 'obm_geometry' && $row[$idx] !== '') {
                        $val = ['geom' => true, 'val' => $row[$idx]];
                    }
                    elseif ($idx === 'obm_id') {
                        $val = ['id' => true, 'val' => $row[$idx]];
                    }
                    else {
                        $val = ['simple' => true, 'val' => $row[$idx]];
                    }
                    return $val;
                }, $indexes);
                $observations[] = ['row' => $r];
            }
            fclose($handle);
        }
        
        return [
            'mtable' => $_SESSION['current_query_table'],
            'collabels' => array_map(function($v) use ($collabels) {
                return $collabels[$v];
            }, $indexes),
            'observations' => $observations
        ];
    }
    
}
?>
