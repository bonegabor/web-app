Vue.component('rules_date_between', {
    template:`
        <div>
            <input type="text" name="from" v-model='from'/>
            <br>
            <input type="text" name="until" v-model='until'/>
        </div>
    `,
    props: {
        value: {
            type: String,
            required: true
        }
    },
    data () {
        return {
            from: this.value.split(',')[0],
            until: this.value.split(',')[1]
        }
    },
    computed: {
        interval_edited () {
            return `${this.from},${this.until}`
        }
    },
    mounted() {
        var vm = this
        $('input[name=from]').datepicker({
          changeMonth: true,
          dateFormat:'mm-dd',
          firstDay:1,
          beforeShow: function (input, inst) {
            inst.dpDiv.addClass('NoYearDatePicker');
          },
          onClose: function(dateText, inst){
            vm.from = dateText
            vm.$emit('input', vm.interval_edited)
            inst.dpDiv.removeClass('NoYearDatePicker');
          }
        })
        $('input[name=until]').datepicker({
          changeMonth: true,
          dateFormat:'mm-dd',
          firstDay:1,
          beforeShow: function (input, inst) {
            inst.dpDiv.addClass('NoYearDatePicker');
          },
          onClose: function(dateText, inst){
            vm.until = dateText
            vm.$emit('input', vm.interval_edited)
            inst.dpDiv.removeClass('NoYearDatePicker');
          }
        })
    }
})

Vue.component('rules_in_grid', {
    props: {
        value: {
            type: String,
            required: true,
        }
    },
    data () {
        return {
            showMap: false,
            featuresSelected: this.value.split(',').filter((el) => el),
            map: {},
            geojsonstr: null,
            geojson: {},
            stylelayer: {
                defecto: {
                    color: "red",
                    opacity: 1,
                    fillcolor: "red",
                    fillOpacity: 0.1,
                    weight: 0.5
                },
                selected: {
                    weight: 5,
                    color: '#0D8BE7',
                    dashArray: '',
                    fillOpacity: 0.7
                },
                hover: {
                    weight: 5,
                    color: 'orange',
                    dashArray: '',
                    fillOpacity: 0.7
                }
            }
        }
    },
    computed: {
        list_str: {
            get () { 
                return this.featuresSelected.join(',') 
            },
            set (value) {
                this.featuresSelected = value.split(',')
            }
        }
    },
    template: `
    <div>
        <input type='text' v-model='list_str' @change="$emit('input', list_str)">
        <button 
            type='button'
            class='pure-button button-secondary' 
            title='Open map'
            @click="openMap"
        > 
            <i class='fa fa-map'></i>
        </button>
        <div v-show="showMap" class="modal2-mask">
          <div class="modal2-wrapper">
            <div class="modal2-container">
              <div class="modal2-header">
                asdfasdf
              </div>
              <div class="modal2-body">
                <div id='rules_map' style="width: 100%; height: 800px;"></div>
              </div>
              <div class="modal2-footer">
                <button type="button" class="pure-button" @click="closeMap"> Cancel </button> 
                <button type="button" class="pure-button button-success" @click="saveMap"> Save </button>
              </div>
            </div>
          </div>
        </div>
    </div>
    `,
    methods: {
        openMap () {
            this.mapLoading = true
            this.map = L.map('rules_map').setView([46, 25], 7);
            
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(this.map);
            
            $.getJSON("https://openbirdmaps.ro/downloads/etrs10.json")
            .then((data) => {
                this.geojson = L.geoJSON(data, {
                    style: this.style_cell,
                    onEachFeature: this.onEachFeature
                }).addTo(this.map);
                this.mapLoading = false;
                this.showMap = true;
                this.map.invalidateSize()
            })
            .fail(function(err){
                console.log(err.responseText)
            });
            
            var drawnItems = new L.FeatureGroup();
            this.map.addLayer(drawnItems);
            this.map.addControl(new L.Control.Draw({
                draw: {
                    marker: false,
                    polyline: false,
                    circlemarker: false,
                    circle: false,
                    polygon: {
                        allowIntersection: false,
                        showArea: true
                    }
                }
            }));
            this.map.on(L.Draw.Event.CREATED, (event) => {
                var layer = event.layer;
                this.geojson.eachLayer((cell) => { 
                    if (turf.booleanIntersects(cell.toGeoJSON(), layer.toGeoJSON())) { 
                        this.selectCell(cell, cell.feature) 
                    } 
                });
            });
            
        },
        saveMap () {
            this.$emit('input', this.list_str);
            this.closeMap()
        },
        closeMap () {
            this.geojson.clearLayers();
            this.map.remove();
            this.showMap = false;
        },
        style_cell (feature) {
            return this.checkExistsLayers(feature) ? this.stylelayer.selected : this.stylelayer.defecto
        },
        onEachFeature(feature, layer) {
            layer.on({
                mouseover: this.selectedFeature,
                mouseout: this.resetHighlight,
                click: this.selectCellByClick
            });
        },
        selectedFeature(e) {
            var layer = e.target;
            layer.setStyle(this.stylelayer.hover);
        },
        resetHighlight(e) {
            var layer = e.target;
            var feature = e.target.feature;
            this.setStyleLayer(layer, this.style_cell(feature))
        },
        selectCellByClick(e) {
            var layer = e.target;
            var feature = e.target.feature;
            this.selectCell(layer, feature);
            this.$emit('input', this.featuresSelected)
        },
        selectCell(layer, feature) {
            if (this.checkExistsLayers(feature)) {
                this.removerlayers(feature, this.setStyleLayer, layer, this.stylelayer.defecto)
                
            } else {
                this.addLayers(feature, this.setStyleLayer, layer, this.stylelayer.selected)
            }
        },
        setStyleLayer(layer, styleSelected) {
            layer.setStyle(styleSelected)
        },
        removerlayers(feature, callback) {
            this.featuresSelected = this.featuresSelected.filter(obj => obj != feature.properties.name)
            callback(arguments[2], arguments[3])
        },
        addLayers(feature, callback) {
            this.featuresSelected.push(feature.properties.name)
            callback(arguments[2], arguments[3])
        },
        checkExistsLayers(feature) {
            return this.featuresSelected.indexOf(feature.properties.name) > -1
        }
    },
})

Vue.component('rules_table_row', {
    data () {
        return {
            rule_btns: {
                save: {
                    title: 'Save',
                    label: 'Save',
                    fa: 'floppy-o',
                    method: this.saveRule,
                    classes: 'button-success'
                },
                delete: {
                    title: 'Delete',
                    label: 'Delete',
                    fa: 'trash-o',
                    method: this.deleteRule,
                    classes: 'button-error'
                },
            }
        }
    },
    props: {
        rule: Object,
        vData: Object
    },
    template: `
        <tr >
            <td> 
                <select v-model='rule.target'>
                    <option value='' disabled> Please select </option>
                    <option v-for="target in vData.params.targets" :value="target" :key="target"> {{ target }} </option>
                </select>
            </td>
            <td> 
                <select v-model='rule.col'>
                    <option value='' disabled> Please select </option>
                    <option v-for="column in vData.columns" :value="column" :key="column"> {{ column }} </option>
                </select>
            </td>
            <td> 
                <select v-model='rule.relation'>
                    <option value='' disabled> Please select </option>
                    <option v-for="relation in vData.relations" :value="relation" :key="relation"> {{ relation }} </option>
                </select>
            </td>
            <td> 
                <rules_date_between v-if="rule.relation === 'DATE_BETWEEN'" v-model="rule.val"/>
                <rules_in_grid v-else-if="rule.relation === 'IN_GRID'" v-model="rule.val" />
                <input v-else v-model="rule.val" type="text" />
            </td>
            <td> <input type="text" v-model="rule.match_value" /> </td>
            <td> <input type="text" v-model="rule.else_value" /> </td>
            <td>
                <v-toolbar :buttons='rule_btns' />
            </td>
        </tr>
    `,
    methods: {
        onValueChanged (val) {
            this.rule.val = val;
        },
        saveRule () {
            this.modifyRule('save');
        },
        deleteRule () {
            this.modifyRule('delete');
        },
        async modifyRule (action) {
            try {
                if (action != 'save' && action !== 'delete') {
                    throw new Error('unknown rule action')
                }
                let resp = await $.post('ajax', {
                    m: 'validation',
                    action: `${action}_rule`,
                    rule: this.rule
                })
                resp = JSON.parse(resp)
                if (resp.status !== "success") {
                    throw new Error(resp.message)
                }
            } catch (e) {
                console.log(e);
            }
        }
    }
})

Vue.component('rules_table', {
    data () {
        return {
            bottom_toolbar: {
                new_rule: {
                    title: 'Add new rule',
                    label: 'New rule',
                    fa: 'plus-square-o',
                    method: this.newRule
                }
            },
        }
    },
    props: {
        taxon_id: Number,
        vData: Object
    },
    asyncComputed: {
        async rules () {
            const resp = await $.getJSON('ajax', {
                m: 'validation', 
                action: 'load_rules',
                taxon_id: this.taxon_id
            })
            if (resp.status != 'success') {
                throw new Error(resp.message)
            }
            return resp.data;
        }
    },
    template: `
        <form class="pure-form">
            <table class="pure-table pure-table-horizontal">
                <thead>
                </thead>
                <tbody>
                    <rules_table_row v-for='rule in rules' :key='rule.id' :rule="rule" :vData='vData'/>
                    <tr>
                        <td colspan="8">
                            <v-toolbar :buttons="bottom_toolbar" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    `,
    methods: {
        newRule () {
            this.rules.push({
                taxon_id: this.taxon_id,
                target: "", relation: "", col: "", val: "",
                match_value: "", else_value: ""
            })
        }
    }
})
