<?php
#' ---
#' Module:
#'   notify
#' Files:
#'   [notify.php]
#' Description: >
#'   very special module
#'   postgres listen functinality and email alerts
#'   It is not used ever, just an idea!
#' Methods:
#'   [unlisten, listen, notify, email]
#' Module-type:
#'   project
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   0.1
class notify extends module {
    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function listen($param) {
        global $GID;
        pg_query($GID, "LISTEN $param");
        return;
    }

    public function unlisten($param) {
        global $GID;
        pg_query($GID, "UNLISTEN $param");
        return;
    }

    // it is a good place to create custom trigger function like sending mails after uploading data
    // using postgres LISTEN/NOTIFY
    public function notify($param) {
        global $GID;
        $w = 0;
        while (1) {
            if ($w == 5) break;

            $notify = pg_get_notify($GID);
            if ($notify) {
                $payload = $notify['payload'];
                
                break;

            } else {
                //log_action('No notify message yet.');
            }

            $w++;
            sleep(1);
        }
        $this->unlisten($param);
        return;
    }

    public function email($param) {
        return; 
    }

}

?>
