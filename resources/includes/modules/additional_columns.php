<?php
#' ---
#' Module:
#'   additional_columns
#' Files:
#'   [additional_columns.php]
#' Description: >
#'   additional columns
#'   use it together with the join_tables module
#'   return with an array:
#'   co [0] columns array
#'   c  [1] column name assoc array
#' Methods:
#'   [return_columns]
#' Examples: >
#'   [
#'   "meta_1",
#'   "dmi.egyedszam"
#'   ]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class additional_columns extends module {

    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        $this->params = $this->split_params($static_params);
        if ($action)
            $this->retval = $this->$action($this->params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function return_columns($params, $dp) {
        $c = array();
        $co = array();
        if (count($params)) {
            foreach ($params as $pm) {
                $j = preg_split('/:/',$pm);
                if (isset($j[0]) and isset($j[1])) {
                    if(preg_match("/([a-z0-9._]+) as ([a-z0-9_]+)/i",$j[0],$m)) {
                        $c[$m[2]] = $j[1];
                        $co[] = "$m[1] as $m[2]";
                    } else {
                        $c[$j[0]] = $j[1];
                        $co[] = $j[0];
                    }
                } else {
                    $c[$j[0]] = $j[0];
                    $co[] = $j[0];
                }
            }
        }
        // column => visible name
        return(array($co,$c));
   
    }
}
?>
