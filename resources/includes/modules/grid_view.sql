-- init_grid_tbl_exists
SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%1$s_qgrids');

-- init_create_grid_table
CREATE TABLE public.%1$s_qgrids (
    "row_id" integer NOT NULL,
    "original" public.geometry,
    "snap" public.geometry,
    "kef_5" public.geometry,
    "kef_10" public.geometry,
    "utm_2.5" public.geometry,
    "utm_10" public.geometry
);
ALTER TABLE public.%1$s_qgrids OWNER TO %1$s_admin;
COMMENT ON COLUMN public.%1$s_qgrids.original IS 'original';
COMMENT ON COLUMN public.%1$s_qgrids.kef_5 IS 'KEF 5x5';
CREATE TRIGGER update_grid_geoms BEFORE INSERT OR UPDATE ON public.%1$s_qgrids FOR EACH ROW EXECUTE PROCEDURE public.update_qgrid_geoms_arg('0', '0', 't', 'f', 'f', 'f', 'f', 'f');
