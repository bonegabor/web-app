<?php
    function saveURL($url,$package) {
        $path = getenv('PROJECT_DIR')."computational_packages/".$package;
        #$cmd = sprintf("UPDATE computation...");
        $y = yaml_emit_file("$path/computation_state.yml",array("working-server"=>$url));
    }

    $dir = __DIR__;
    $dir_path = explode('/',$dir);
    array_pop($dir_path);
    $local_lib_dir = implode("/",$dir_path);
    $local_lib_dir_path = $dir_path;

    array_pop($local_lib_dir_path);
    $local_lib_dir_path = implode('/',$local_lib_dir_path);

    putenv("PROJECT_DIR=$local_lib_dir_path/");
    putenv("OB_LIB_DIR=$local_lib_dir/");

    require_once(getenv('OB_LIB_DIR').'db_funcs.php');
    require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
    require_once(getenv('OB_LIB_DIR').'cache_functions.php');

    if ( isset($argv) ) {
        $package = $argv[1];
        $url = $argv[2];
        $user = $argv[3];
    }
    elseif (isset($_GET) and isset($_GET['package'])) $package = $_GET['package'];


    $path = getenv('PROJECT_DIR')."computational_packages/".$package;
    $y = yaml_parse_file("$path/computation_conf.yml");

    $content = ""; 

    $t = obm_cache('get',"computational-upload-".$package,'','',FALSE);

    if ($t !== false )
    {
        if ($t[0] == 'done')
            echo common_message('error',$t[1]);
        else
            echo common_message('ok',$t[1]);
        exit;
    } else if (!isset($url)) {
        echo common_message('error','expired');
        exit;
    }


    if (!$y) {

        $content .= "Syntax error in config file";
        obm_cache('set',"computational-upload-".$package,array('done',$content),30,FALSE);

    } else  {

        $project = $y['project'];

        //$files = is_array($y['includes']) ? $y['includes'] : array();

        $j = json_encode($y);

        // if software->type=='R'
        // create_init_r()
        $ciphertext = sslEncrypt(COMPUTATIONAL_CLIENT_SEC, COMPUTATIONAL_CLIENT_KEY);
        $cmd = array('project'=>$project,'user'=>$user,
            'package'=>$package,'config-file'=>$j,'method'=>'create');
        
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array('Accept: text/json',"client-key: $ciphertext",'client-name: '.COMPUTATIONAL_CLIENT_NAME),
            CURLOPT_POST => 1,
            )
        );

        $phar_path = getenv('PROJECT_DIR')."computational_packages/$project"."_$package.tar";
        if (file_exists($phar_path.'.gz'))
            unlink(realpath($phar_path.".gz"));
        
        $pharPath = '';

        $dir = "$path/";
        $iterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dir,
                FilesystemIterator::SKIP_DOTS)
        );

        $filterIterator = new CallbackFilterIterator($iterator , function ($file) {
            return (in_array($file,array('.git')) === false);
        });

        try {
            $phar = new PharData($phar_path);
            $phar->buildFromIterator($filterIterator, $dir);
            $phar->compress(Phar::GZ);
            unlink(realpath($phar_path));
        } catch (Exception $e) {
            obm_cache('set',"computational-upload-".$package,array("done","Compress content failed: ".$e->getMessage()),30,FALSE);
            debug('Compress failed',__FILE__,__LINE__);
        }
        
        $cmd["files[0]"] = new CurlFile($phar_path.".gz", mime_content_type($phar_path.".gz"));
        $cmd["files[1]"] = new CurlFile("$path/computation_conf.yml", mime_content_type("$path/computation_conf.yml"));

        #$i = 0;
        #foreach ($files as $f) {
        #    $cmd["files[$i]"] = new CurlFile("$path/$f", mime_content_type("$path/$f"));
        #    $i++;
        #}

        // 10 minutes cache for waiting for uploading
        obm_cache('set',"computational-upload-".$package,array("progress","Uploading package..."),600,FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $cmd);
        $res = curl_exec($ch);

        $state = 0;
        if (!curl_errno($ch)) {
            $info = curl_getinfo($ch);
            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case 200:  # OK
                    $content .= 'Took '. $info['total_time']. ' seconds to send a request to '. $info['url']. "<br>";
                    $content .= $res;
                    saveURL($url,$package);
                    $state = 1;
                    break;
                default:
                    $content .= 'Unexpected HTTP code: '. $http_code. "<br>";
            }
        } else {
            $content .= 'Curl error: ' . curl_error($ch);
        }
        obm_cache('set',"computational-upload-".$package,array("progress",$content),30,FALSE);

        // Close handle
        curl_close($ch);
        unlink(realpath($phar_path.".gz"));
        if ($state) {
            obm_cache('set',"computational-upload-".$package,array("done",$content),30,FALSE);
        }
    }

    obm_cache('set',"computational-upload-".$package,array("done",$content),30,FALSE);

    return; 

?>
