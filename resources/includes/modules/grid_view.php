<?php
#' ---
#' Module:
#'   grid_view
#' Files:
#'   [grid_view.php]
#' Description: >
#'   Grid type chooser for restricted data displaying
#'   This module is an iterface for users to choose a grid (e.g. UTM )for displaying data
#' Methods:
#'   [init, print_box, default_grid_geom, get_grid_layer, filter_query]
#' Examples: >
#'   { 
#'    "layer_options":[
#'        "kef_5 (dinpi_grid)", "utm_2.5 (dinpi_grid)", "utm_10 (dinpi_grid)", "original (dinpi_points,dinpi_grid)""
#'    ]
#'   }
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class grid_view extends module {

    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    function print_box ($params) {
        global $ID;
        #$p = preg_split('/;/',$params);
        $p = $this->split_params($params,'pmi');
        $available_grids = array();
        foreach ($p as $pkey=>$pm) {
            if ($pkey == 'layer_options') {
                if (!is_array($pm))
                    if (preg_match('/\*\*/',$pm))
                        $grids = preg_split('/\*\*/',$pm);
                    else
                        $grids = preg_split('/,/',$pm);
                else
                    $grids = $pm;
                //utm_10 (dinpi_grid),utm_100 (dinpi_grid)
                $available_grids = array();
                foreach($grids as $gr) {
                    if (preg_match('/(\w+)/',trim($gr),$m)){
                        $available_grids[] = $m[1];
                    }
                }
            }
        }

        $js = "<script language='javascript'>$(document).ready(function() { ";
        $js .= "
        $(\"body\").on('click','#qgrids_chooser_send',function(){

            $( '#dialog' ).text('Waiting for the WMS response...');
            var isOpen = $( '#dialog' ).dialog( 'isOpen' );
            if (!isOpen) $( '#dialog' ).dialog( 'open' );

            $.post('ajax', {choose_display_grid_for_map:$('#qgrids_chooser').val()},
            function(data){
                var retval = jsendp(data);
                if (retval['status']=='error') {
                    $( '#dialog' ).text(retval['message']);
                } else if (retval['status']=='fail') {
                    $( '#dialog' ).text('Invalid response received.');
                } else if (retval['status']=='success') {


                    for(var i=0;i<dataLayers.length;i++) {
                        dataLayers[i].visibility = true
                        dataLayers[i].refresh();
                    }
                    $( '#dialog' ).dialog( 'close' );
                    /*$.post('ajax',{'check_layer':dataLayers_ns},function(data) {
                        var retval = jsendp(data);
                        if (retval['status']=='success') {
                            var v = retval['data'];
                            var layers_state = JSON.parse(v);
                            for(var i=0;i<layers_state.length;i++) {
                                if (layers_state[i]=='drop') {
                                    //set visibility false prevent send query to this layer!
                                    dataLayers[i].visibility = false
                                } else {
                                    dataLayers[i].visibility = true
                                }
                            }
                        }
                        for(var i=0;i<dataLayers.length;i++) {
                            dataLayers[i].redraw(true);
                        }
                    });*/
                }
            });

        });";
        $js .= "});</script>";

        $t = new table_row_template();
    
        $cmd = sprintf("SELECT c.column_name, pgd.description FROM pg_catalog.pg_statio_all_tables as st inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid) 
            INNER JOIN information_schema.columns c on 
                (pgd.objsubid=c.ordinal_position and c.table_schema=st.schemaname and c.table_name=st.relname and c.table_name = '%s_qgrids' and c.table_schema = 'public')",$_SESSION['current_query_table']);
     
        $res = pg_query($ID,$cmd);
        $options = "<option value=''></option>";
        while($row=pg_fetch_assoc($res)) {
            $selected = '';
            if (in_array($row['column_name'],$available_grids)) {
                $label = $row['description'];
                if (isset($_SESSION['display_grid_for_map']) and $row['column_name'] == $_SESSION['display_grid_for_map'])
                    $selected = 'selected';
                $grid_text = str_grid;
                if ($row['column_name'] == 'original') {
                    $label = str_original;
                    $grid_text = "";
                }
                $options .= sprintf("<option value='%s' $selected>%s %s</option>",$row['column_name'],$label,$grid_text);
            }
        }
        
        $t->cell(mb_convert_case(str_display_data, MB_CASE_UPPER, "UTF-8"),2,'title center');
        $t->row();
        $t->cell("<select id='qgrids_chooser' style='width:100%'>$options</select>",2,'content');
        $t->row();
        $t->cell("<button id='qgrids_chooser_send' class='button-gray button-xlarge pure-button'><i class='fa-refresh fa'></i> ".str_update."</button>",2,'title');
        $t->row();

        return sprintf("%s<table class='mapfb'>%s</table>",$js,$t->printOut());
    }
    // first element of the list is the default!
    function default_grid_geom($params) {
        $p = $this->split_params($params,'pmi');
        foreach ($p as $pkey=>$pm) {
            if ($pkey == 'layer_options') {
                if (!is_array($pm))
                    if (preg_match('/\*\*/',$pm))
                        $gr = preg_split('/\*\*/',$pm); # lefelé kompatibilitás
                    else
                        $gr = preg_split('/,/',$pm); # lefelé kompatibilitás
                else
                    $gr = $pm;
                $default_grid = '';
                if (preg_match('/([a-z0-9._-]+)/i',trim($gr[0]),$m)){
                    $default_grid = $m[1];
                }

                return $default_grid;
            }
        }
    }
    //overlay a layer
    function get_grid_layer($params) {
        $p = $this->split_params($params,'pmi');
        foreach ($p as $pkey=>$pm) {
            if ($pkey == 'layer_options') {
                //utm_10 (dinpi_grid),utm_100 (dinpi_grid)
                if (!is_array($pm))
                    if (preg_match('/\*\*/',$pm))
                        $lg = preg_split('/\*\*/',$pm);
                    else
                        $lg = preg_split('/,/',$pm);
                else
                    $lg = $pm;
                $layers = array();
                foreach($lg as $l) {
                    if (preg_match('/([\w.]+)\s*\((\w+)\)/',trim($l),$m)) {
                        $layers[$m[1]] = $m[2];
                    }
                }
                return $layers;
            }
        }
    }
    function filter_query($params) {
        $p = $this->split_params($params,'pmi');
        foreach ($p as $pkey=>$pm) {
            //"AND layer_cname='layer_data_biotika'";
            if ($pkey == 'filter_query' and $pm!='') {
                if (!is_array($pm))
                    if (preg_match('/\*\*/',$pm))
                        $lg = preg_split('/\*\*/',$pm);
                    else
                        $lg = preg_split('/,/',$pm);
                else
                    $lg = $pm;

                return sprintf('AND layer_cname IN (%s)',implode(',',array_map('quote',$lg)) );
            }
        }
    }

    public function init($params, $pa) {
        global $ID;
        $st_col = st_col($pa['mtable'], 'array');
        // checking if the grid table exists
        $cmd = sprintf(getSQL('init_grid_tbl_exists', __CLASS__), PROJECTTABLE);
        if (!$res = pg_query($ID, $cmd)) {
            $this->error = "init query error!";
            log_action(pg_last_error($ID),__FILE__,__LINE__);
            return false;
        }
        $result = pg_fetch_assoc($res);
        if ($result['exists'] == 't') {
            return true;
        }
        
        // creating the grid table
        $cmd = sprintf(getSQL('init_create_grid_table', __CLASS__), PROJECTTABLE);
        if (!$res = pg_query($ID, $cmd)) {
            $this->error = "init query error!";
            log_action(pg_last_error($ID),__FILE__,__LINE__);
            return false;
        }
        return true;
    }

}
?>
