<?php
#' ---
#' Module:
#'   results_asSHP
#' Files:
#'   [results_asSHP.php]
#' Description: >
#'   Export results as SHP
#' Methods:
#'   [print_data, export_as]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class results_asSHP extends module {
    
    var $error = '';
    var $retval;
    
    function __construct($action = null, $params = null,$pa = array()) {

        $params = $this->split_params($params);
        
        if ($action) {
            $this->retval = $this->$action($params,$pa);
        }
    }

    protected function moduleName() {
        return __CLASS__;
    }

    public function print_data($params,$pa) {
        
        $st_col = st_col($_SESSION['current_query_table'],'array');
        
        if (!isset($st_col['GEOM_C']) or $st_col['GEOM_C']=='') {
            echo "No geometry column defined.";
            return;
        }
        
        $de = new DataExport((int)$pa['de_id']);

        $shp_header = $this->prepare_shp_header($de->colnames, $st_col['GEOM_C']);
        
        // geometry column index
        $gix = array_search($st_col['GEOM_C'], array_keys($de->colnames));
        
        //return if there is no geometry column 
        if ( !$gix ) {
            log_action('No geometry column',__FILE__,__LINE__);
            return;
        }
        
        if (file_exists($de->tmpfile())) {
            
            if (($handle = fopen($de->tmpfile(), "r")) === false) {
                return;
            }
                
            $geomtypes = [];
            // splitting up data into three different files based on their geoemtry type
            while (($row = fgetcsv($handle, $de->maxlength)) !== FALSE) {
                if (substr($row[$gix],0,5) === 'POINT') {
                    $geomtype = 'POINT';
                }
                elseif (substr($row[$gix],0,10) === 'LINESTRING') {
                    $geomtype = 'LINESTRING';
                }
                elseif (substr($row[$gix],0,7) === 'POLYGON') {
                    $geomtype = 'POLYGON';
                }
                else {
                    log_action('Unsupported geometry type', __FILE__, __LINE__);
                    continue;
                }
                $file_var = "{$geomtype}_file";
                $handle_var = "{$geomtype}_handle";
                if (!in_array($geomtype, $geomtypes)) {
                    
                    $$file_var = OB_TMP . $de->filename . "_" . $geomtype . ".shp.csv";
                    $$handle_var = fopen($$file_var, 'w');
                    fputcsv($$handle_var, $shp_header);
                    $geomtypes[] = $geomtype;
                }
                fputcsv($$handle_var, $row);
            }
            
            fclose($handle);
            foreach ($geomtypes as $geomtype) {
                $handle_var = "{$geomtype}_handle";
                fclose($$handle_var);
            }
            
            
            //writing tmp csv files and converting them into shp files with ogr2ogr
            $zip_file = $de->filename . '_SHP.zip';
            $zip_path = OB_TMP . $zip_file;
            
            $zip = new ZipArchive;
            
            if (!$zip->open($zip_path,  ZipArchive::CREATE)) {
                log_action('zip could not be opened', __FILE__,__LINE__);
                return;
            }
            
            foreach ($geomtypes as $geomtype) {
                $tmp_file = OB_TMP . $de->filename . "_" . $geomtype . ".shp.csv";
                    
                $shp_name = $de->filename . "_$geomtype";
                $shp_path = OB_TMP . $shp_name;
                
                $GDAL_PATH = defined("GDAL_PATH") ? constant("GDAL_PATH") : "";
                $res = exec("{$GDAL_PATH}ogr2ogr -a_srs EPSG:4326 -f \"ESRI Shapefile\" $shp_path.shp $tmp_file -lco ENCODING=UTF-8");
                
                unlink($tmp_file);
                
                foreach (['shp','shx','dbf','cpg','prj'] as $ext) {
                    $zip->addFile("$shp_path.$ext", "$shp_name.$ext" );
                }
            }
            
            $zip->close();
            
            if (file_exists($zip_path)) {
                //    track_download($track_id);
                # header definíció!!!
                header("Content-type: application/zip"); 
                header("Content-Disposition: attachment; filename=$zip_file");
                header("Content-length: " . filesize($zip_path));
                header("Pragma: no-cache"); 
                header("Expires: 0"); 
                $handle = fopen($zip_path, "rb");
                while (!feof($handle)){
                    echo fread($handle, 8192);
                }
                fclose($handle);
                
                $de->increment_dl_count();
                
                foreach ($geomtypes as $geomtype) {
                    $shp_path = OB_TMP . $de->filename . "_$geomtype";
                    foreach (['shp','shx','dbf','cpg','prj'] as $ext) {
                        unlink("$shp_path.$ext");
                    }
                }
                unlink($zip_path);
                return;
            }
            
            echo "Error. See the system log.";
            return;
            
        } else {
            echo "Error. See the system log.";
            return;
        }

    }

    private function prepare_shp_header($header, $GEOM_C) {
        $header = array_keys($header);
        $remove_accents_file = getenv('OB_LIB_DIR') . 'remove_accents.function.php';
        if (!file_exists($remove_accents_file)) {
            log_action("$remove_accents_file does not exist");
            return false;
        }
        require_once($remove_accents_file);
            
        // cutting header names at 10 char and renaming duplicates
        $col_names = [];
        for ($i = 0; $i < count($header); $i++) {
            $cell = $header[$i];
            if ($cell === $GEOM_C) {
                $cell = 'wkt';
            }
            $shp_name = $this->shp_attribute_name($cell);
            //renaming identical column names
            $j = 1;
            while (in_array($shp_name,$col_names)) {
                $suffix = "_$j";
                $shp_name = substr($shp_name, 0, strlen($suffix) * -1) . $suffix;
                $j++;
            }
            $col_names[] = $shp_name;
        }
        return $col_names;
    }
    
    // this function removes the accents from the column names and truncates them at 10 char length
    private function shp_attribute_name($str) {
        return substr(preg_replace('/\W+/', '', strtolower(remove_accents($str))),0,10);
    }
}
?>
