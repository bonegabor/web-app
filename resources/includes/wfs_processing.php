<?php
/* Ennek semmi mást nem kéne csinálni csak a GML feldolgozását, amit egy json tömbbként ad vissza a $_SESSION['wfs_response'] 
 * változóban. Az echo-s sorok miatt ez kissé zavaros működésű és nehezen felhasználható többféle célra.
 * A merge részben nem működik a sok sor korlátozás
 * A korlátozott sorokat egyben is vissza kéne tudni adni, amikor fájl kimenet a kérés!
 * */

require_once(getenv('OB_LIB_DIR').'db_funcs.php');

session_start();

#xdebug_start_trace('/tmp/xdebug');

$post_id = array();
$post_data = array();
$post_geom = array();

if (isset($_POST['skip_processing']) and $_POST['skip_processing']=='yes') {
    // Ezt azt hiszem nem használja már semmi...

    // Read previously processed XML data
    #syslog(LOG_WARNING,"wfa-get-count:".count($_SESSION['wfs_full_array']));
    if (isset($_SESSION['wfs_full_array'])) {
        $post_id = $_SESSION['wfs_full_array']['id'];
        $post_data = $_SESSION['wfs_full_array']['data'];
        $post_geom = $_SESSION['wfs_full_array']['geom'];
    }

} elseif (isset($_POST['skip_processing']) and $_POST['skip_processing']=='no') {
    if(getenv("PROJECT_DIR") !== false) {
        #require_once(getenv('OB_LIB_DIR').'system_vars.php.inc');
        require_once('/etc/openbiomaps/system_vars.php.inc');
        if (!is_dir(getenv('PROJECT_DIR'))) {
            echo '{"status":"fail","data":"No such project."}';
            exit;
        }
        require_once(getenv('PROJECT_DIR').'local_vars.php.inc');
        require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
        require_once(getenv('OB_LIB_DIR').'base_functions.php');
        if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
            die("Unsuccessful connect to GIS database.");
        #if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
        #    die("Unsuccesful connect to UI database.");
        if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))
            die("Unsuccessful connect to GIS database.");

    } else {
        exit;
    }

    // Processing XML
    //
    $ses = session_id();
    $no_input = 1;
    $xml = false;
    $json = false;

    //nincs használva jelenleg
    //$list = glob(OB_TMP."conts_$ses*.xml");

    /*if (isset($_SESSION['loaded_xml_content'])) {
        
        //from load_queries
        $xml = simplexml_load_string($_SESSION['loaded_xml_content']); 
        unset($_SESSION['loaded_xml_content']);
        $no_input = 0;

    } else */
    
    /* if (count($list)) {

        // xml file processing
        natsort($list);
        $file = array_pop($list);
        $xml = simplexml_load_file($file);
        // A merge fájlkat nem lehet lementeni!!!
        // a mentés csak egy xml-t tárol egyszerre!!
        //if ($merge) unlink($file);
        $no_input = 0;

    } else */

    // Share processing
    // This session variable created in load_queries.php
    if (isset($_SESSION['loaded_geojson_content'])) {

        $json = json_decode($_SESSION['loaded_geojson_content']);
        unset($_SESSION['loaded_geojson_content']);
        $no_input = 0;

    }
    
    if ($no_input) {
        echo json_encode(array("res"=>'',"geom"=>'',"error"=>'Proxy error: no xml/geojson files to read'));
        exit;
    }

    if($xml ===  FALSE and $json === FALSE) {
        echo json_encode(array("res"=>'',"geom"=>'',"error"=>'Proxy error: failed to read xml/geojson contents'));
        exit;
    }
    
    if ($xml !== FALSE) {
        $namespaces = $xml->getDocNamespaces();
        //$xml->registerXPathNamespace("ms", $namespaces['ms']);
        //$xml->registerXPathNamespace("gml", $namespaces['gml']);
        //var_dump($namespaces);
        //var_dump($xml->asXML());
    
        /*Mapserver errors*/
        if (isset($namespaces['ows'])) {
            $e = "";
            foreach($xml->children($namespaces['ows']) as $child) {
                foreach($child->children($namespaces['ows']) as $data){
                    if ($data->getName() == 'ExceptionText') {
                        $e .= (string) $data;
                    }   
                }
            }
            $_SESSION['wfs_array'] = array("id"=>'',"geom"=>'',"data"=>'');
            echo json_encode(array("geom"=>'',"error"=>"mapserver exception: $e"));
            exit;
        }

        list($post_data,$post_geom,$post_id) = gml_processing($namespaces);

    } elseif ($json !== FALSE) {
        // JSON processing

        list($post_data,$post_geom,$post_id) = geojson_parsing($json);
        //$post_geom = $json;
        $geojson = array(
           'type'      => 'FeatureCollection',
           'features'  => array()
        );
        // GeoJson reprocessing
        for ($i=0;$i<count($post_geom);$i++) {
            if (isset($post_geom[$i]['point'])) {
                $feature = array(
                    'id' => $post_id[$i],
                    'type' => 'Feature', 
                    'geometry' => array(
                        'type' => 'Point',
                        # Pass Longitude and Latitude Columns here
                        'coordinates' => array($post_geom[$i]['point'][0],$post_geom[$i]['point'][1])
                    ),
                    # Pass other attribute columns here
                    'properties' => array()
                );
            } else if (isset($post_geom[$i]['line'])) {
                $feature = array(
                    'id' => $post_id[$i],
                    'type' => 'Feature', 
                    'geometry' => array(
                        'type' => 'LineString',
                        # Pass Longitude and Latitude Columns here
                        'coordinates' => $post_geom[$i]['line']
                    ),
                    # Pass other attribute columns here
                    'properties' => array()
                );
            } else {
                $feature = array(
                    'id' => $post_id[$i],
                    'type' => 'Feature', 
                    'geometry' => array(
                        'type' => 'Polygon',
                        # Pass Longitude and Latitude Columns here
                        'coordinates' => array($post_geom[$i]['polygon'])
                    ),
                    # Pass other attribute columns here
                    'properties' => array()
                );
            }
            # Add feature arrays to feature collection array
            array_push($geojson['features'], $feature);
        }
        $post_geom = $geojson;

        $columns = json_decode(json_encode($json->{'features'}[0]->{'properties'}),true);
        $c = array_keys($columns);
        
        $index = array_search('obm_id',$c);
        unset($c[$index]);

        list($cols,$col_types) = get_type_of_columnslist($c,'public',PROJECTTABLE);

        if ( create_query_tmp_table_from_geojson($json,$cols,$col_types) ) {
            $_SESSION['do-not-create-query'] = 'it is already done by wfs_processing';
        } else {
            // failed to create tmp table
            echo json_encode(array("geom"=>'',"error"=>'Failed to create temp table'));
            exit;
        }
    }

    if (!count($post_id)) {
        //No matching records found
        echo json_encode(array("geom"=>'',"error"=>'0'));
        exit;
    }
}

if (isset($_SESSION['wfs_rows'])) {
    unset($_SESSION['wfs_rows']);
    unset($_SESSION['wfs_rows_counter']);
}

if (isset($_SESSION['wfs_full_array'])) {
    unset($_SESSION['wfs_full_array']);
}

$_SESSION['wfs_array'] = array("id"=>$post_id,"geom"=>$post_geom,"data"=>$post_data);

if (isset($_POST['return']) and $_POST['return']=='id')

    echo json_encode(array("id"=>$post_id,"error"=>''));

elseif (isset($_POST['return']) and $_POST['return']=='none')

    echo json_encode(array("error"=>''));

else
    if (!isset($no_echo))
        echo json_encode(array("geom"=>$post_geom,"error"=>''));


#xdebug_stop_trace();
?>
