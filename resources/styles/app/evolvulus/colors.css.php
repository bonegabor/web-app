<?php
header ("Content-Type:text/css");
require_once(getenv('PROJECT_DIR') . 'local_vars.php.inc');

$color = defined('PRIMARY_COLOR') ? PRIMARY_COLOR : "#83781b";
?>

body {
    --primary-color: <?= $color ?>;
}
