<?php
# Running jobs from jobs directory as background process
# Add this crontab entry as root to www-data
#
# crontab -u www-data -e
# 
# */5 * * * * /usr/bin/php path/to/jobs.php &> /dev/null
# 
# in this case jobs can't be run more frequently than 5 minutes
# This Scheduler can handle weekly schedules
#
# DOCKER notes
#
# Add this line to host machine's cron to pass through the job, because there is no cron in the container
# */5 * * * * docker exec -u www-data **CONTAINER_NAME** php /var/www/html/biomaps/root-site/projects/**PROJECT_NAME**/jobs.php
#

$path = __DIR__.'/';
$error_log = $path.'jobs/error.log';
$event_log = $path.'jobs/event.log';

if (file_exists($path.'jobs/job_settings.php.inc'))
    require_once($path.'jobs/job_settings.php.inc');

require_once($path.'includes/job_functions.php');
require_once('/etc/openbiomaps/system_vars.php.inc');
require_once('local_vars.php.inc');

$minute = roundUp(date('i'));
$hour = date('H');
$day = date('N');

# simplified cron like scheduler
# minute[0-59], hour[0-23], day[1-7]
# * * * job-command
$table = file($path.'jobs/jobs.table'); 
$jobs = array();
foreach($table as $t) {
    $te = preg_split('/ /',$t);
    $jobs[trim($te[3])] = array($te[0],$te[1],$te[2]);
}
foreach (glob($path."jobs/run/{*.php,*.sh,*.perl,*.R}",GLOB_BRACE ) as $filename) {

        $path_parts = pathinfo($filename);
        $ext = $path_parts['extension'];
        $arg = $dirname = $path_parts['dirname'];
        
        $run = 0;
        if (isset($jobs[basename($filename)])) {
            if (in_array(roundUp($jobs[basename($filename)][0]), array( '*', $minute)) and 
                     in_array($jobs[basename($filename)][1], array( '*', $hour)) and 
                          in_array($jobs[basename($filename)][2], array( '*', $day))) $run = 1;
        }
        
        if (!$run) continue;

        # long running test?
        # grep ps ...
        if ($ext == 'php') {
            $php = (defined('PHP_PATH')) ? constant('PHP_PATH') : '/usr/bin/php';
            $pid = exec("nohup $php $filename $path $arg >>$event_log 2>>$error_log & echo $!; ");
        }
        elseif ($ext == 'R') {
            $r = (defined('R_PATH')) ? constant('R_PATH') : '/usr/bin/Rscript';
            $pid = exec("nohup $r --vanilla $filename $path $arg >>$event_log 2>>$error_log & echo $!; ");
        }
        else
            $pid = exec("nohup $filename $dirname >>$event_log 2>>$error_log & echo $!; ");

        $pidfile = fopen($path."jobs/".basename($filename).".pid", "w") or die("Unable to open file!");
        fwrite($pidfile, "$pid\n");
        fclose($pidfile);


}

?>
