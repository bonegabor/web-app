<?php
// drop all inputs...
//$_GET = array();
//$_POST = array();

if (isset($_GET['api_version'])) {
    
    $API_VERSION = $_GET['api_version'];

} else {

    // no api version information provided - old api requests
    $API_VERSION = '1.1';

}

// OB_LIB_DIR defined in /etc/openbiomaps/local_vars.php.inc
// it is included in oauth/server.php should be called in
// projects/---/pds.php
if (getenv('OB_LIB_DIR')===false) {
    echo "OB_LIB_DIR should be defined";
    return;
}

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'db_funcs.php');

if (session_status() === PHP_SESSION_NONE) {
    session_cache_limiter('nocache');
    session_cache_expire(0);
    session_start();
}
else {
    session_regenerate_id(true);
}

#require_once __DIR__.'/constans.php.inc';
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require(getenv('PROJECT_DIR').'pds/pds_funcs.php');
require(getenv('PROJECT_DIR').'pds/pds_class.php');

header('Content-type:application/json;charset=utf-8');

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host)) {

    http_response_code(503);
    echo common_message('fail','Unsuccessful connect to UI database...');
    session_destroy();
    exit;

}

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) {

    http_response_code(503);
    echo common_message('fail','Unsuccessful connect to GIS database...');
    session_destroy();
    exit;

}
// PRS metaname call, or some other new features...
if (!isset($API_PARAMS)) {

    http_response_code(401);
    echo common_message('error','Direct access not allowed, use pds.php.');
    session_destroy();
    exit;

}

$pds = new pds ($API_PARAMS,$API_VERSION);

if ($pds->request_error != '') {
    
    header('Content-type:application/json;charset=utf-8');
    echo common_message('error',$pds->request_error);
    $pds = null;
    session_destroy();
    exit;

} elseif ( count($pds->request_warnings) )  {
    
    header('Content-type:application/json;charset=utf-8');
    echo common_message('warning',$pds->request_warnings);
    $pds = null;
    session_destroy();
    exit;

}


if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) {
    
    http_response_code(503);
    echo common_message('fail','Unsuccessful connect to UI database...');
    $pds = null;
    session_destroy();
    exit;

}
# inspiration...
#SERVICE=WFS&VERSION=1.0.0&MAP=/home/banm/maps/cuckoo_2011.map&REQUEST=getfeature&typename=dinpi&Filter=(<Filter><Intersects><PropertyName>Geometry</PropertyName><gml:Polygon><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>19.24179504075156 47.24999264177116,19.241760518998973 47.24975471798812,19.241658280392798 47.24952593647466,19.241492253905346 47.249315089349246,19.24126881984141 47.24913027960786,19.240996564646768 47.24897860969101,19.240685950935866 47.24886590849429,19.24034891541942 47.24879650732071,19.239998410183322 47.24877307339167,19.239647904947216 47.24879650732071,19.239310869430774 47.24886590849429,19.239000255719873 47.24897860969101,19.238728000525228 47.24913027960786,19.238504566461295 47.249315089349246,19.23833853997384 47.24952593647466,19.23823630136767 47.24975471798812,19.238201779615082 47.24999264177116,19.23823630136767 47.25023056448539,19.23833853997384 47.25045934295517,19.238504566461295 47.2506701855254,19.238728000525228 47.250854989893575,19.239000255719873 47.25100665443723,19.239310869430774 47.251119351078756,19.239647904947216 47.25118874920865,19.239998410183322 47.2512121820689,19.24034891541942 47.25118874920865,19.240685950935866 47.251119351078756,19.240996564646768 47.25100665443723,19.24126881984141 47.250854989893575,19.241492253905346 47.2506701855254,19.241658280392798 47.25045934295517,19.241760518998973 47.25023056448539,19.24179504075156 47.24999264177116</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></Intersects></Filter>)
#


# Project Data Row Service
# data,data_history
if (isset($pds->method) and $pds->method=='PRS') {

    // multi requerst processing
    $pds->getData();
    if ($pds->request_error != '') {
        
        header('Content-type:application/json;charset=utf-8');
        echo common_message('error',$pds->request_error);

    } elseif ( count($pds->request_warnings) )  {
        
        header('Content-type:application/json;charset=utf-8');
        echo common_message('warning',$pds->request_warnings);

    }
    $pds = null;
    session_destroy();
    exit;

} 

# Project Function Data Service
# history,profile
if (isset($pds->method) and $pds->method=='PFS') {

    if ($pds->pfs_put_data) {

        // multi requerst processing
        $pds->putFunction($API_PARAMS);
        if ($pds->request_error != '') {
            
            header('Content-type:application/json;charset=utf-8');
            echo common_message('error',$pds->request_error);

        } elseif ( count($pds->request_warnings) )  {
            
            header('Content-type:application/json;charset=utf-8');
            echo common_message('warning',$pds->request_warnings);

        }

    } else {

        // prevent multi requerst processing
        foreach($pds->request as $key=>$val)
        {
            $pds->getFunction($key,$val);

            if ($pds->request_error != '') {
                
                header('Content-type:application/json;charset=utf-8');
                echo common_message('error',$pds->request_error);

            } elseif ( count($pds->request_warnings) )  {
                
                header('Content-type:application/json;charset=utf-8');
                echo common_message('warning',$pds->request_warnings);
            }

            // else getFunction should send common_message() 
        }
    }

    $pds = null;
    session_destroy();
    exit;

}

# Default response
http_response_code(400);
echo common_message('error','No service defined');
session_destroy();
?>
