--
-- OpenBioMaps gisdata database default structure for web application.
-- Version: 5.7.9

-- Changes:
-- 5.5.15: files


SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

----CREATE DATABASE gisdata WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
SELECT 'CREATE DATABASE gisdata WITH TEMPLATE = template0 ENCODING = ''UTF8'' LC_COLLATE = ''en_US.UTF-8'' LC_CTYPE = ''en_US.UTF-8''' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'gisdata')\gexec

--\c gisdata;

--
-- Name: shared; Type: SCHEMA; Schema: -; Owner: gisadmin
--

CREATE SCHEMA shared;


ALTER SCHEMA shared OWNER TO gisadmin;

--
-- Name: SCHEMA shared; Type: COMMENT; Schema: -; Owner: gisadmin
--

COMMENT ON SCHEMA shared IS 'shared data';


--
-- Name: system; Type: SCHEMA; Schema: -; Owner: gisadmin
--

CREATE SCHEMA system;


ALTER SCHEMA system OWNER TO gisadmin;

--
-- Name: SCHEMA system; Type: COMMENT; Schema: -; Owner: gisadmin
--

COMMENT ON SCHEMA system IS 'system tables';


--
-- Name: temporary_tables; Type: SCHEMA; Schema: -; Owner: gisadmin
--

CREATE SCHEMA temporary_tables;


ALTER SCHEMA temporary_tables OWNER TO gisadmin;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: intarray; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS intarray WITH SCHEMA public;


--
-- Name: EXTENSION intarray; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION intarray IS 'functions, operators, and index support for 1-D arrays of integers';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';

--
-- Name: unaccent; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA public;


--
-- Name: EXTENSION unaccent; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION unaccent IS 'text search dictionary that removes accents';


--
-- Name: drop_temp_table(); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION drop_temp_table() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
        IF (TG_OP = 'DELETE') THEN
            execute format('DROP TABLE IF EXISTS temporary_tables.%I',OLD.table_name);
            RETURN OLD;
        END IF; 
END$$;


ALTER FUNCTION public.drop_temp_table() OWNER TO gisadmin;

--
-- Name: file_connect_status_update(); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION file_connect_status_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
     IF (new.obm_files_id IS NOT NULL) THEN
         UPDATE file_connect SET temporal=false WHERE temporal=true AND conid=new.obm_files_id;
     END IF;
     RETURN new;
  END IF;
END$$;


ALTER FUNCTION public.file_connect_status_update() OWNER TO gisadmin;

--
-- Name: geom_accuracy_reduce(geometry, integer); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION geom_accuracy_reduce(geom geometry, precisity integer) RETURNS geometry
    LANGUAGE plpgsql
    AS $$DECLARE
  cgeom geometry;
BEGIN
  SELECT ST_Point(
    regexp_replace(st_x(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '}).+',E'\\1')::float,
    regexp_replace(st_y(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '}).+',E'\\1')::float) INTO cgeom;
  RETURN cgeom;
END;
$$;


ALTER FUNCTION public.geom_accuracy_reduce(geom geometry, precisity integer) OWNER TO gisadmin;

--
-- Name: geom_noise(geometry, integer); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION geom_noise(geom geometry, precisity integer) RETURNS geometry
    LANGUAGE plpgsql
    AS $$DECLARE
  cgeom geometry;
BEGIN
  SELECT ST_Point(
(regexp_replace(st_x(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '}).+',E'\\1') ||
round(regexp_replace(st_x(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '})(.+)',E'\\2')::float*random()))::float,
(regexp_replace(st_y(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '}).+',E'\\1') ||
round(regexp_replace(st_y(st_centroid(geom))::character varying,'([0-9]+\.[0-9]{' || precisity || '})(.+)',E'\\2')::float*random()))::float

) INTO cgeom;
  RETURN cgeom;
END;$$;


ALTER FUNCTION public.geom_noise(geom geometry, precisity integer) OWNER TO gisadmin;

--
-- Name: history_sablon(); Type: FUNCTION; Schema: public; Owner: sablon_admin
--

CREATE FUNCTION history_sablon() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        INSERT INTO sablon_history (operation,hist_time,userid,query,row_id,modifier_id,data_table) 
            SELECT 'D', now(), user, CONCAT(OLD.*), OLD.obm_id, OLD.obm_modifier_id, 'sablon';
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN
        INSERT INTO sablon_history (operation,hist_time,userid,query,row_id,modifier_id,data_table) 
            SELECT 'U', now(), user, CONCAT(NEW.*), OLD.obm_id, OLD.obm_modifier_id, 'sablon';
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
        INSERT INTO sablon_history (operation,hist_time,userid,query,row_id,modifier_id,data_table) 
            SELECT 'I', now(), user, CONCAT(NEW.*), OLD.obm_id, OLD.obm_modifier_id, 'sablon';
        RETURN NEW;
    END IF; 
END $$;


ALTER FUNCTION public.history_sablon() OWNER TO sablon_admin;

--
-- Name: rules_sablon(); Type: FUNCTION; Schema: public; Owner: sablon_admin
--

CREATE FUNCTION rules_sablon() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        DELETE FROM sablon_rules WHERE row_id=OLD.obm_id ;
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
        INSERT INTO sablon_rules ("data_table",row_id,read,write) 
            SELECT 'sablon',NEW.obm_id,ARRAY(SELECT uploader_id 
            FROM system.uploadings WHERE system.uploadings.id=NEW.obm_uploading_id),ARRAY(SELECT uploader_id FROM system.uploadings 
            WHERE system.uploadings.id=NEW.obm_uploading_id);
        RETURN NEW;
    END IF; 
END $$;

ALTER FUNCTION public.rules_sablon() OWNER TO sablon_admin;

CREATE FUNCTION tracklog_line() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    new.tracklog_line_geom = (
      SELECT 
        st_setSrid(
          ST_MakeLine(
            ST_GeomFromGeoJSON(A -> 'geometry')
          ), 
          4326
        ) 
      FROM 
      (
        SELECT 
          json_array_elements(
            new.tracklog_geom -> 'features'
          ) as A
      ) foo
    ); 
    RETURN new;
END $$;

ALTER FUNCTION public.tracklog_line() OWNER TO gisadmin;

--
-- Name: update_uploadings_vote(); Type: FUNCTION; Schema: public; Owner: gisadmin
--

CREATE FUNCTION update_uploadings_vote() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
	IF NEW."table" = 'uploadings' AND NEW.valuation!=0 THEN
        UPDATE system.uploadings SET validation=(SELECT avg(valuation) FROM evaluations 
        WHERE "table"='uploadings' AND row=NEW.row AND valuation!=0) WHERE id=NEW.row;
      	END IF;
      	RETURN NEW;
   END;$$;


ALTER FUNCTION public.update_uploadings_vote() OWNER TO gisadmin;

--
-- Name: update_vote(); Type: FUNCTION; Schema: public; Owner: biomapsadmin
--

CREATE FUNCTION update_vote() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
par character varying(32);
gid character varying(8);
val character varying(16);
BEGIN
      IF NEW.valuation!=0 THEN
         par = NEW."table";
         gid = 'obm_id';
         val = 'obm_validation';
         IF par='uploadings' THEN
             gid = 'id';
             val = 'validation';
         END IF;
         EXECUTE 'UPDATE ' || quote_ident(par) || ' SET ' || val || '=(SELECT avg(valuation) FROM evaluations WHERE "table"=' || quote_literal(par) || ' AND row=' || NEW.row || ' AND valuation!=0) WHERE ' || gid || '=' || NEW.row;
      END IF;
      RETURN NEW;
END;$$;


ALTER FUNCTION public.update_vote() OWNER TO biomapsadmin;

SET search_path = temporary_tables, pg_catalog;

--
-- Name: json_array_append(json, text); Type: FUNCTION; Schema: temporary_tables; Owner: gisadmin
--

CREATE FUNCTION json_array_append(j json, e text) RETURNS json
    LANGUAGE sql IMMUTABLE
    AS $$
    select array_to_json(array_append(array(select * from json_array_elements_text(j)), e))::json
$$;


ALTER FUNCTION temporary_tables.json_array_append(j json, e text) OWNER TO gisadmin;

SET search_path = hrsz_terkepek, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

SET search_path = public, pg_catalog;

--
-- Name: sablon; Type: TABLE; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE TABLE public.sablon (
    obm_id integer NOT NULL,
    obm_geometry geometry,
    obm_uploading_id integer,
    obm_validation numeric,
    obm_comments text[],
    obm_modifier_id integer,
    obm_datum timestamp with time zone DEFAULT now(),
    obm_files_id character varying(32),
    species_name_field character varying(255),
    species_name_auto character varying(255),
    national_name_auto character varying(255),
    location character varying(255),
    abundance character varying(255),
    "size" character varying(255),
    no_of_individuals double precision,
    male double precision,
    female double precision,
    occurrence_status character varying(255),
    collector character varying(255),
    identifier character varying(255),
    data_provider character varying(255),
    method character varying(255),
    habitat character varying(255),
    "comment" text,
    "natura2000" boolean,
    date_from timestamp without time zone,
    date_to timestamp without time zone,
    "time" time without time zone,
    CONSTRAINT enforce_dims_the_geom CHECK ((st_ndims(obm_geometry) = 2)),
    CONSTRAINT enforce_srid_the_geom CHECK ((st_srid(obm_geometry) = 4326))
);


ALTER TABLE public.sablon OWNER TO sablon_admin;

--
-- Name: TABLE sablon; Type: COMMENT; Schema: public; Owner: sablon_admin
--

COMMENT ON TABLE public.sablon IS 'Template project - main table';


--
-- Name: sablon_obm_id_seq; Type: SEQUENCE; Schema: public; Owner: sablon_admin
--

CREATE SEQUENCE public.sablon_obm_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_obm_id_seq OWNER TO sablon_admin;

--
-- Name: sablon_obm_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sablon_admin
--

ALTER SEQUENCE sablon_obm_id_seq OWNED BY sablon.obm_id;


--
-- Name: sablon_history; Type: TABLE; Schema: public; Owner: gisadmin; Tablespace: 
--

CREATE TABLE public.sablon_history (
    operation character(1) NOT NULL,
    hist_time timestamp without time zone NOT NULL,
    userid text NOT NULL,
    query text,
    row_id integer NOT NULL,
    hist_id integer NOT NULL,
    modifier_id integer,
    data_table character varying(64)
);


ALTER TABLE public.sablon_history OWNER TO gisadmin;

--
-- Name: sablon_history_hist_id_seq; Type: SEQUENCE; Schema: public; Owner: gisadmin
--

CREATE SEQUENCE public.sablon_history_hist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_history_hist_id_seq OWNER TO gisadmin;

--
-- Name: sablon_history_hist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gisadmin
--

ALTER SEQUENCE sablon_history_hist_id_seq OWNED BY sablon_history.hist_id;

--
-- Name: sablon_rules; Type: TABLE; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE TABLE public.sablon_rules (
    data_table character varying(128) NOT NULL,
    row_id integer NOT NULL,
    owners character varying,
    read integer[],
    write integer[],
    sensitivity character varying(16) DEFAULT 'public'::text,
    download integer[]
);


ALTER TABLE public.sablon_rules OWNER TO sablon_admin;

--
-- Name: sablon_taxon; Type: TABLE; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE TABLE public.sablon_taxon (
    meta character varying(254) NOT NULL,
    word character varying(254) NOT NULL,
    lang character varying(16) NOT NULL,
    taxon_id integer NOT NULL,
    status character varying(16) NOT NULL DEFAULT 'undefined',
    modifier_id integer DEFAULT 0,
    frequency integer DEFAULT 0,
    taxon_db integer DEFAULT 0,
    wid integer NOT NULL
);


ALTER TABLE public.sablon_taxon OWNER TO sablon_admin;

--
-- Name: COLUMN sablon_taxon.frequency; Type: COMMENT; Schema: public; Owner: sablon_admin
--

COMMENT ON COLUMN sablon_taxon.frequency IS 'query counter';


--
-- Name: COLUMN sablon_taxon.taxon_db; Type: COMMENT; Schema: public; Owner: sablon_admin
--

COMMENT ON COLUMN sablon_taxon.taxon_db IS 'number of related records';


--
-- Name: sablon_taxon_taxon_id_seq; Type: SEQUENCE; Schema: public; Owner: sablon_admin
--

CREATE SEQUENCE public.sablon_taxon_taxon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_taxon_taxon_id_seq OWNER TO sablon_admin;

--
-- Name: sablon_taxon_taxon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sablon_admin
--

ALTER SEQUENCE public.sablon_taxon_taxon_id_seq OWNED BY sablon_taxon.taxon_id;

--
-- Name: sablon_taxon_wid_seq; Type: SEQUENCE; Schema: public; Owner: sablon_admin
--

CREATE SEQUENCE public.sablon_taxon_wid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sablon_taxon_wid_seq OWNER TO sablon_admin;

--
-- Name: sablon_taxon_wid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: sablon_admin
--

ALTER SEQUENCE sablon_taxon_wid_seq OWNED BY sablon_taxon.wid;


SET search_path = shared, pg_catalog;

--
-- Name: weather_data; Type: TABLE; Schema: shared; Owner: gisadmin; Tablespace: 
--

CREATE TABLE weather_data (
    id integer NOT NULL,
    the_geom public.geometry NOT NULL,
    rain numeric,
    wind numeric,
    moisture numeric,
    blast numeric,
    temperature numeric,
    datetime timestamp without time zone NOT NULL
);


ALTER TABLE weather_data OWNER TO gisadmin;

--
-- Name: TABLE weather_data; Type: COMMENT; Schema: shared; Owner: gisadmin
--

COMMENT ON TABLE weather_data IS 'project independent weather data';


--
-- Name: weather_data_id_seq; Type: SEQUENCE; Schema: shared; Owner: gisadmin
--

CREATE SEQUENCE weather_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE weather_data_id_seq OWNER TO gisadmin;

--
-- Name: weather_data_id_seq; Type: SEQUENCE OWNED BY; Schema: shared; Owner: gisadmin
--

ALTER SEQUENCE weather_data_id_seq OWNED BY weather_data.id;


SET search_path = system, pg_catalog;

--
-- Name: evaluations; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE evaluations (
    "table" character varying(64) NOT NULL,
    "row" integer NOT NULL,
    user_name character varying(128) NOT NULL,
    valuation integer,
    id integer NOT NULL,
    datum timestamp without time zone DEFAULT now(),
    user_id integer,
    comments text,
    related_id integer
);


ALTER TABLE evaluations OWNER TO gisadmin;

--
-- Name: evaluations_id_seq; Type: SEQUENCE; Schema: system; Owner: gisadmin
--

CREATE SEQUENCE evaluations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE evaluations_id_seq OWNER TO gisadmin;

--
-- Name: evaluations_id_seq; Type: SEQUENCE OWNED BY; Schema: system; Owner: gisadmin
--

ALTER SEQUENCE evaluations_id_seq OWNED BY evaluations.id;


--
-- Name: file_connect; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE file_connect (
    file_id integer NOT NULL,
    conid character varying(32) NOT NULL,
    temporal boolean DEFAULT true,
    sessionid character varying(64),
    rownum smallint
);


ALTER TABLE file_connect OWNER TO gisadmin;

--
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: biomapsadmin
--

CREATE SEQUENCE files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE files_id_seq OWNER TO biomapsadmin;


--
-- Name: files; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE files (
    id integer DEFAULT nextval('files_id_seq'::regclass) NOT NULL,
    project_table character varying(32) NOT NULL,
    reference text NOT NULL,
    comment text,
    datum timestamp with time zone DEFAULT now(),
    access integer,
    user_id integer DEFAULT 0,
    status character varying(16) DEFAULT 'valid'::character varying,
    sessionid character varying(64),
    sum character varying(64),
    mimetype character varying(128),
    data_table character varying(256),
    exif json,
    slideshow boolean DEFAULT false,
    project_schema text
);


ALTER TABLE files OWNER TO gisadmin;

--
-- Name: files_id_seq; Type: ACL; Schema: public; Owner: biomapsadmin
--

REVOKE ALL ON SEQUENCE files_id_seq FROM PUBLIC;
GRANT ALL ON SEQUENCE files_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE files_id_seq TO biomapsadmin;
GRANT ALL ON SEQUENCE files_id_seq TO project_admin;

--
-- Name: imports; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE imports (
    project_table character varying(32) NOT NULL,
    user_id numeric NOT NULL,
    ref character varying(32) NOT NULL,
    datum timestamp without time zone,
    header text NOT NULL,
    data text NOT NULL,
    form_type character varying(6) NOT NULL,
    form_id numeric,
    file text,
    template_name character varying(128),
    massive_edit json
);


ALTER TABLE imports OWNER TO gisadmin;

--
-- Name: TABLE imports; Type: COMMENT; Schema: system; Owner: gisadmin
--

COMMENT ON TABLE imports IS 'saved import forms with data';


--
-- Name: COLUMN imports.template_name; Type: COMMENT; Schema: system; Owner: gisadmin
--

COMMENT ON COLUMN imports.template_name IS 'save as sablon - name';


--
-- Name: polygon_users; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE polygon_users (
    user_id integer NOT NULL,
    polygon_id integer NOT NULL,
    select_view character varying(13)
);


ALTER TABLE polygon_users OWNER TO gisadmin;

--
-- Name: query_buff; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE query_buff (
    user_id integer NOT NULL,
    datetime timestamp with time zone DEFAULT now(),
    "table" character varying(64),
    id integer NOT NULL,
    name character varying(100),
    access numeric DEFAULT 1,
    geometry public.geometry
);


ALTER TABLE query_buff OWNER TO gisadmin;

--
-- Name: query_buff_id_seq; Type: SEQUENCE; Schema: system; Owner: gisadmin
--

CREATE SEQUENCE query_buff_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE query_buff_id_seq OWNER TO gisadmin;

--
-- Name: query_buff_id_seq; Type: SEQUENCE OWNED BY; Schema: system; Owner: gisadmin
--

ALTER SEQUENCE query_buff_id_seq OWNED BY query_buff.id;


--
-- Name: shared_polygons; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE shared_polygons (
    id integer NOT NULL,
    "timestamp" timestamp with time zone DEFAULT now(),
    project_table character varying(64),
    geometry public.geometry NOT NULL,
    name character varying(128) NOT NULL,
    access character varying(7) DEFAULT 'public'::character varying NOT NULL,
    user_id integer,
    original_name character varying
);


ALTER TABLE shared_polygons OWNER TO gisadmin;

--
-- Name: shared_polygons_id_seq; Type: SEQUENCE; Schema: system; Owner: gisadmin
--

CREATE SEQUENCE shared_polygons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE shared_polygons_id_seq OWNER TO gisadmin;

--
-- Name: shared_polygons_id_seq; Type: SEQUENCE OWNED BY; Schema: system; Owner: gisadmin
--

ALTER SEQUENCE shared_polygons_id_seq OWNED BY shared_polygons.id;


--
-- Name: temp_index; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE temp_index (
    table_name character varying(128) NOT NULL,
    datum timestamp without time zone DEFAULT now() NOT NULL,
    interconnect boolean DEFAULT false
);


ALTER TABLE temp_index OWNER TO gisadmin;

--
-- Name: TABLE temp_index; Type: COMMENT; Schema: system; Owner: gisadmin
--

COMMENT ON TABLE temp_index IS 'unlogged temporary table indexes';

--
-- Name: tracklogs; Type: TABLE; Schema: system; Owner: gisadmin
--

CREATE TABLE tracklogs (
    project text,
    user_id integer,
    start_time timestamp with time zone NOT NULL,
    end_time timestamp with time zone NOT NULL,
    trackname text,
    tracklog_id text NOT NULL,
    observation_list_id text,
    tracklog_geom json,
    tracklog_line_geom public.geometry(LineString,4326),
    CONSTRAINT enforce_geotype_tracklog_line_geom CHECK ((public.geometrytype(tracklog_line_geom) = 'LINESTRING'::text)),
    CONSTRAINT enforce_srid_tracklog_line_geom CHECK ((public.st_srid(tracklog_line_geom) = 4326))
);


ALTER TABLE tracklogs OWNER TO gisadmin;

--
-- Name: tracklogs tracklogs_id_key; Type: CONSTRAINT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY tracklogs
    ADD CONSTRAINT tracklogs_id_key UNIQUE (tracklog_id);


--
-- Name: tracklogs tracklog_line; Type: TRIGGER; Schema: system; Owner: gisadmin
--

CREATE TRIGGER tracklog_line BEFORE INSERT ON system.tracklogs FOR EACH ROW EXECUTE PROCEDURE public.tracklog_line();


--
-- Name: uploadings; Type: TABLE; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE TABLE uploadings (
    id integer NOT NULL,
    uploading_date timestamp without time zone NOT NULL,
    uploader_id integer NOT NULL,
    uploader_name character varying(100),
    validation numeric,
    collectors text,
    description text,
    access numeric DEFAULT 0 NOT NULL,
    "group" integer[],
    project_table character varying(64),
    owner integer[],
    project character varying(32),
    form_id integer,
    metadata json
);


ALTER TABLE uploadings OWNER TO gisadmin;

--
-- Name: COLUMN uploadings.access; Type: COMMENT; Schema: system; Owner: gisadmin
--

COMMENT ON COLUMN uploadings.access IS 'Access level 0-public,1-logined users,2-specified';


--
-- Name: uploadings_id_seq; Type: SEQUENCE; Schema: system; Owner: gisadmin
--

CREATE SEQUENCE uploadings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE uploadings_id_seq OWNER TO gisadmin;

--
-- Name: uploadings_id_seq; Type: SEQUENCE OWNED BY; Schema: system; Owner: gisadmin
--

ALTER SEQUENCE uploadings_id_seq OWNED BY uploadings.id;


SET search_path = temporary_tables, pg_catalog;

SET search_path = public, pg_catalog;

--
-- Name: obm_id; Type: DEFAULT; Schema: public; Owner: sablon_admin
--

ALTER TABLE ONLY sablon ALTER COLUMN obm_id SET DEFAULT nextval('sablon_obm_id_seq'::regclass);


--
-- Name: hist_id; Type: DEFAULT; Schema: public; Owner: gisadmin
--

ALTER TABLE ONLY sablon_history ALTER COLUMN hist_id SET DEFAULT nextval('sablon_history_hist_id_seq'::regclass);

--
-- Name: taxon_id; Type: DEFAULT; Schema: public; Owner: sablon_admin
--

ALTER TABLE ONLY sablon_taxon ALTER COLUMN taxon_id SET DEFAULT nextval('sablon_taxon_taxon_id_seq'::regclass);

--
-- Name: wid; Type: DEFAULT; Schema: public; Owner: sablon_admin
--

ALTER TABLE ONLY sablon_taxon ALTER COLUMN wid SET DEFAULT nextval('sablon_taxon_wid_seq'::regclass);


SET search_path = shared, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: shared; Owner: gisadmin
--

ALTER TABLE ONLY weather_data ALTER COLUMN id SET DEFAULT nextval('weather_data_id_seq'::regclass);


SET search_path = system, pg_catalog;

--
-- Name: id; Type: DEFAULT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY evaluations ALTER COLUMN id SET DEFAULT nextval('evaluations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY query_buff ALTER COLUMN id SET DEFAULT nextval('query_buff_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY shared_polygons ALTER COLUMN id SET DEFAULT nextval('shared_polygons_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY system.uploadings ALTER COLUMN id SET DEFAULT nextval('uploadings_id_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- Name: sablon_history_key; Type: CONSTRAINT; Schema: public; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY sablon_history
    ADD CONSTRAINT sablon_history_key PRIMARY KEY (hist_id);

--
-- Name: sablon_pkey; Type: CONSTRAINT; Schema: public; Owner: sablon_admin; Tablespace: 
--

ALTER TABLE ONLY sablon
    ADD CONSTRAINT sablon_pkey PRIMARY KEY (obm_id);


--
-- Name: sablon_rules_ukey; Type: CONSTRAINT; Schema: public; Owner: sablon_admin; Tablespace: 
--

ALTER TABLE ONLY sablon_rules
    ADD CONSTRAINT sablon_rules_ukey UNIQUE (data_table, row_id);


--
-- Name: sablon_taxon_metaword_key; Type: CONSTRAINT; Schema: public; Owner: sablon_admin; Tablespace: 
--

ALTER TABLE ONLY sablon_taxon
    ADD CONSTRAINT sablon_taxon_metaword_key UNIQUE (meta, word);

SET search_path = shared, pg_catalog;

--
-- Name: weather_data_pkey; Type: CONSTRAINT; Schema: shared; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY weather_data
    ADD CONSTRAINT weather_data_pkey PRIMARY KEY (id);


SET search_path = system, pg_catalog;

--
-- Name: file_connect_ukey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY file_connect
    ADD CONSTRAINT file_connect_ukey UNIQUE (file_id, conid);


--
-- Name: files_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_pkey PRIMARY KEY (id);


--
-- Name: files_project_table_reference_key; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_project_table_reference_key UNIQUE (project_table, reference);


--
-- Name: imports_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY imports
    ADD CONSTRAINT imports_pkey PRIMARY KEY (ref);


--
-- Name: shared_polygons_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY shared_polygons
    ADD CONSTRAINT shared_polygons_pkey PRIMARY KEY (id);


--
-- Name: temp_index_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY temp_index
    ADD CONSTRAINT temp_index_pkey PRIMARY KEY (table_name);


--
-- Name: uploadings_pkey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY system.uploadings
    ADD CONSTRAINT uploadings_pkey PRIMARY KEY (id);


--
-- Name: user_polygon_ukey; Type: CONSTRAINT; Schema: system; Owner: gisadmin; Tablespace: 
--

ALTER TABLE ONLY polygon_users
    ADD CONSTRAINT user_polygon_ukey UNIQUE (user_id, polygon_id);


SET search_path = public, pg_catalog;

--
-- Name: sablon_geom; Type: INDEX; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE INDEX sablon_geom ON sablon USING gist (obm_geometry);

--
-- Name: sablon_obm_uploading_id_idx; Type: INDEX; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE INDEX sablon_obm_uploading_id_idx ON sablon USING btree (obm_uploading_id);


--
-- Name: fa_gin; Type: INDEX; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE INDEX species_name_gin ON sablon USING gin (species_name_field gin_trgm_ops);

--
-- Name: trgm_idx; Type: INDEX; Schema: public; Owner: sablon_admin; Tablespace: 
--

CREATE INDEX trgm_idx ON sablon_taxon USING gist (meta gist_trgm_ops);



SET search_path = system, pg_catalog;

--
-- Name: buff_id; Type: INDEX; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE UNIQUE INDEX buff_id ON query_buff USING btree (id);


--
-- Name: evaluations_key; Type: INDEX; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE UNIQUE INDEX evaluations_key ON evaluations USING btree (id);


--
-- Name: uploadings_date_idx; Type: INDEX; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE INDEX uploadings_date_idx ON system.uploadings USING btree (uploading_date);


--
-- Name: uploadings_project_idx; Type: INDEX; Schema: system; Owner: gisadmin; Tablespace: 
--

CREATE INDEX uploadings_project_idx ON system.uploadings USING btree (project);


SET search_path = public, pg_catalog;


--
-- Name: file_connection; Type: TRIGGER; Schema: public; Owner: sablon_admin
--

CREATE TRIGGER file_connection AFTER INSERT ON sablon FOR EACH ROW EXECUTE PROCEDURE file_connect_status_update();

--
-- Name: history_update_sablon; Type: TRIGGER; Schema: public; Owner: sablon_admin
--

CREATE TRIGGER history_update_sablon AFTER DELETE OR UPDATE ON sablon FOR EACH ROW EXECUTE PROCEDURE history_sablon();

--
-- Name: rules_sablon; Type: TRIGGER; Schema: public; Owner: sablon_admin
--

CREATE TRIGGER rules_sablon AFTER INSERT OR DELETE OR UPDATE ON sablon FOR EACH ROW EXECUTE PROCEDURE rules_sablon();


SET search_path = system, pg_catalog;

--
-- Name: drop_temporay_table; Type: TRIGGER; Schema: system; Owner: gisadmin
--

CREATE TRIGGER drop_temporay_table AFTER DELETE ON temp_index FOR EACH ROW EXECUTE PROCEDURE public.drop_temp_table();


--
-- Name: vote; Type: TRIGGER; Schema: system; Owner: gisadmin
--

CREATE TRIGGER vote AFTER INSERT OR UPDATE ON evaluations FOR EACH ROW EXECUTE PROCEDURE public.update_vote();


SET search_path = public, pg_catalog;

--
-- Name: uploading_id; Type: FK CONSTRAINT; Schema: public; Owner: sablon_admin
--

ALTER TABLE ONLY sablon
    ADD CONSTRAINT uploading_id FOREIGN KEY (obm_uploading_id) REFERENCES system.uploadings(id);


SET search_path = system, pg_catalog;

--
-- Name: files_fkey; Type: FK CONSTRAINT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY file_connect
    ADD CONSTRAINT files_fkey FOREIGN KEY (file_id) REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: shared_polygons_id; Type: FK CONSTRAINT; Schema: system; Owner: gisadmin
--

ALTER TABLE ONLY polygon_users
    ADD CONSTRAINT shared_polygons_id FOREIGN KEY (polygon_id) REFERENCES shared_polygons(id) ON UPDATE CASCADE ON DELETE CASCADE;

--
-- Name: public; Type: ACL; Schema: -; Owner: biomapsadmin
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO biomapsadmin;
GRANT ALL ON SCHEMA public TO project_admin;

--
-- Name: system; Type: ACL; Schema: -; Owner: gisadmin
--

REVOKE ALL ON SCHEMA system FROM PUBLIC;
REVOKE ALL ON SCHEMA system FROM gisadmin;
GRANT ALL ON SCHEMA system TO gisadmin;
GRANT USAGE ON SCHEMA system TO project_admin;

--
-- Name: temporary_tables; Type: ACL; Schema: -; Owner: gisadmin
--

REVOKE ALL ON SCHEMA temporary_tables FROM PUBLIC;
REVOKE ALL ON SCHEMA temporary_tables FROM gisadmin;
GRANT ALL ON SCHEMA temporary_tables TO gisadmin;
GRANT ALL ON SCHEMA temporary_tables TO project_admin;
GRANT ALL ON SCHEMA temporary_tables TO biomapsadmin;


SET search_path = public, pg_catalog;

--
-- Name: sablon; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON TABLE sablon FROM PUBLIC;
REVOKE ALL ON TABLE sablon FROM sablon_admin;
GRANT ALL ON TABLE sablon TO sablon_admin;
GRANT SELECT ON TABLE sablon TO sablon_user;
GRANT ALL ON TABLE sablon TO biomapsadmin;


--
-- Name: sablon_obm_id_seq; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON SEQUENCE sablon_obm_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE sablon_obm_id_seq FROM sablon_admin;
GRANT ALL ON SEQUENCE sablon_obm_id_seq TO sablon_admin;
GRANT ALL ON SEQUENCE sablon_obm_id_seq TO biomapsadmin;


--
-- Name: sablon_history; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON TABLE sablon_history FROM PUBLIC;
REVOKE ALL ON TABLE sablon_history FROM gisadmin;
GRANT ALL ON TABLE sablon_history TO gisadmin;
GRANT ALL ON TABLE sablon_history TO sablon_admin;
GRANT ALL ON TABLE sablon_history TO biomapsadmin;


--
-- Name: sablon_history_hist_id_seq; Type: ACL; Schema: public; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE sablon_history_hist_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE sablon_history_hist_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE sablon_history_hist_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE sablon_history_hist_id_seq TO sablon_admin;
GRANT ALL ON SEQUENCE sablon_history_hist_id_seq TO biomapsadmin;


--
-- Name: sablon_rules; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON TABLE sablon_rules FROM PUBLIC;
REVOKE ALL ON TABLE sablon_rules FROM sablon_admin;
GRANT ALL ON TABLE sablon_rules TO sablon_admin;
GRANT ALL ON TABLE sablon_rules TO biomapsadmin;


--
-- Name: sablon_taxon; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON TABLE sablon_taxon FROM PUBLIC;
REVOKE ALL ON TABLE sablon_taxon FROM sablon_admin;
GRANT ALL ON TABLE sablon_taxon TO sablon_admin;
GRANT ALL ON TABLE sablon_taxon TO biomapsadmin;


--
-- Name: sablon_taxon_taxon_id_seq; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON SEQUENCE sablon_taxon_taxon_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE sablon_taxon_taxon_id_seq FROM sablon_admin;
GRANT ALL ON SEQUENCE sablon_taxon_taxon_id_seq TO sablon_admin;
GRANT ALL ON SEQUENCE sablon_taxon_taxon_id_seq TO biomapsadmin;

--
-- Name: sablon_taxon_wid_seq; Type: ACL; Schema: public; Owner: sablon_admin
--

REVOKE ALL ON SEQUENCE sablon_taxon_wid_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE sablon_taxon_wid_seq FROM sablon_admin;
GRANT ALL ON SEQUENCE sablon_taxon_wid_seq TO sablon_admin;
GRANT ALL ON SEQUENCE sablon_taxon_wid_seq TO biomapsadmin;


SET search_path = shared, pg_catalog;

--
-- Name: weather_data; Type: ACL; Schema: shared; Owner: gisadmin
--

REVOKE ALL ON TABLE weather_data FROM PUBLIC;
REVOKE ALL ON TABLE weather_data FROM gisadmin;
GRANT ALL ON TABLE weather_data TO gisadmin;
GRANT ALL ON TABLE weather_data TO sablon_admin;


SET search_path = system, pg_catalog;

--
-- Name: evaluations; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE evaluations FROM PUBLIC;
REVOKE ALL ON TABLE evaluations FROM gisadmin;
GRANT ALL ON TABLE evaluations TO gisadmin;
GRANT SELECT ON TABLE evaluations TO project_admin;
GRANT ALL ON TABLE evaluations TO biomapsadmin;


--
-- Name: evaluations_id_seq; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE evaluations_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE evaluations_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE evaluations_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE evaluations_id_seq TO biomapsadmin;


--
-- Name: file_connect; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE file_connect FROM PUBLIC;
REVOKE ALL ON TABLE file_connect FROM gisadmin;
GRANT ALL ON TABLE file_connect TO gisadmin;
GRANT ALL ON TABLE file_connect TO project_admin;
GRANT ALL ON TABLE file_connect TO biomapsadmin;


--
-- Name: files; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE files FROM PUBLIC;
REVOKE ALL ON TABLE files FROM gisadmin;
GRANT ALL ON TABLE files TO gisadmin;
GRANT SELECT ON TABLE files TO project_admin;
GRANT ALL ON TABLE files TO biomapsadmin;


--
-- Name: imports; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE imports FROM PUBLIC;
REVOKE ALL ON TABLE imports FROM gisadmin;
GRANT ALL ON TABLE imports TO gisadmin;
GRANT SELECT ON TABLE imports TO project_admin;


--
-- Name: polygon_users; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE polygon_users FROM PUBLIC;
REVOKE ALL ON TABLE polygon_users FROM gisadmin;
GRANT ALL ON TABLE polygon_users TO gisadmin;
GRANT SELECT ON TABLE polygon_users TO project_admin;
GRANT ALL ON TABLE polygon_users TO biomapsadmin;


--
-- Name: query_buff; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE query_buff FROM PUBLIC;
REVOKE ALL ON TABLE query_buff FROM gisadmin;
GRANT ALL ON TABLE query_buff TO gisadmin;
GRANT SELECT ON TABLE query_buff TO project_admin;
GRANT ALL ON TABLE query_buff TO biomapsadmin;


--
-- Name: query_buff_id_seq; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE query_buff_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE query_buff_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE query_buff_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE query_buff_id_seq TO biomapsadmin;


--
-- Name: shared_polygons; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE shared_polygons FROM PUBLIC;
REVOKE ALL ON TABLE shared_polygons FROM gisadmin;
GRANT ALL ON TABLE shared_polygons TO gisadmin;
GRANT SELECT ON TABLE shared_polygons TO project_admin;
GRANT ALL ON TABLE shared_polygons TO biomapsadmin;


--
-- Name: shared_polygons_id_seq; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE shared_polygons_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE shared_polygons_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE shared_polygons_id_seq TO gisadmin;
GRANT ALL ON SEQUENCE shared_polygons_id_seq TO biomapsadmin;


--
-- Name: temp_index; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE temp_index FROM PUBLIC;
REVOKE ALL ON TABLE temp_index FROM gisadmin;
GRANT ALL ON TABLE temp_index TO gisadmin;
GRANT ALL ON TABLE temp_index TO project_admin;


--
-- Name: uploadings; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON TABLE system.uploadings FROM PUBLIC;
REVOKE ALL ON TABLE system.uploadings FROM gisadmin;
GRANT ALL ON TABLE system.uploadings TO gisadmin;
GRANT SELECT ON TABLE system.uploadings TO project_admin;
GRANT ALL ON TABLE system.uploadings TO biomapsadmin;

--
-- Name: uploadings_id_seq; Type: ACL; Schema: system; Owner: gisadmin
--

REVOKE ALL ON SEQUENCE uploadings_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE uploadings_id_seq FROM gisadmin;
GRANT ALL ON SEQUENCE uploadings_id_seq TO gisadmin;
GRANT SELECT ON SEQUENCE uploadings_id_seq TO project_admin;
GRANT ALL ON SEQUENCE uploadings_id_seq TO biomapsadmin;

--
-- Name: sablon_terms; Schema: public; Owner: sablon_admin
--

CREATE SEQUENCE public.sablon_terms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE public.sablon_terms_wid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE TABLE public.sablon_terms (
    term_id integer DEFAULT nextval('public.sablon_terms_id_seq'::regclass) NOT NULL,
    wid integer DEFAULT nextval('public.sablon_terms_wid_seq'::regclass) NOT NULL,
    data_table character varying(64) NOT NULL,
    subject character varying(128) NOT NULL,
    term character varying NOT NULL,
    description character varying,
    status character varying(128),
    taxon_db integer DEFAULT 0,
    role_id integer,
    parent_id integer
);


ALTER TABLE public.sablon_terms OWNER TO sablon_admin;

ALTER TABLE public.sablon_terms_id_seq OWNER TO sablon_admin;

ALTER TABLE public.sablon_terms_wid_seq OWNER TO sablon_admin;

ALTER SEQUENCE public.sablon_terms_wid_seq OWNED BY public.sablon_terms.wid;

ALTER SEQUENCE public.sablon_terms_id_seq OWNED BY public.sablon_terms.wid;

--
-- Name: sablon_terms sablon_data_table_subject_term_key; Type: CONSTRAINT; Schema: public; Owner: sablon_admin
--

ALTER TABLE ONLY public.sablon_terms
    ADD CONSTRAINT sablon_data_table_subject_term_key UNIQUE (data_table, subject, term);


--
-- Name: index_sablon_terms_on_term_trigram; Type: INDEX; Schema: public; Owner: sablon_admin
--

CREATE INDEX index_sablon_terms_on_term_trigram ON public.sablon_terms USING gin (term public.gin_trgm_ops);

--
-- This should be the last action in this file
--

CREATE TABLE temporary_tables.installation_complete ();
