--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE biomapsadmin;
ALTER ROLE biomapsadmin WITH SUPERUSER NOINHERIT CREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md5fudohpef4iyaijoopoJ5oohaa7dooxie' VALID UNTIL 'infinity';
ALTER ROLE biomapsadmin SET search_path TO public, system, shared, pg_catalog;

CREATE ROLE gisadmin;
ALTER ROLE gisadmin WITH SUPERUSER NOINHERIT CREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md5fudohpef4iyaijoopoJ5oohaa7dooxie' VALID UNTIL 'infinity';
ALTER ROLE gisadmin SET search_path TO public, system, shared, pg_catalog;

CREATE ROLE sablon_admin;
ALTER ROLE sablon_admin WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD '12345' VALID UNTIL 'infinity';
ALTER ROLE sablon_admin SET search_path TO public, system, shared, pg_catalog;

CREATE ROLE sablon_user;
ALTER ROLE sablon_user WITH NOSUPERUSER NOINHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
ALTER ROLE sablon_admin SET search_path TO public, shared;

CREATE ROLE project_admin;
ALTER ROLE project_admin WITH NOSUPERUSER NOINHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION PASSWORD 'md59ccc066cf2a8ca0b564e1f6f40f0cab8' VALID UNTIL 'infinity';
ALTER ROLE project_admin SET search_path TO public, system, shared, pg_catalog;



--
-- Role memberships
--

GRANT sablon_admin TO gisadmin WITH ADMIN OPTION;
GRANT project_admin TO gisadmin WITH ADMIN OPTION;
GRANT sablon_admin TO biomapsadmin WITH ADMIN OPTION GRANTED BY gisadmin;
GRANT project_admin TO sablon_admin GRANTED BY gisadmin;


--
-- PostgreSQL database cluster dump complete
--

