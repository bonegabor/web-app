<?php
// include our OAuth2 Server object
require_once __DIR__.'/server.php';

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();

// validate the authorize request
if (!$server->validateAuthorizeRequest($request, $response)) {
    $response->send();
    die;
}
// display an authorization form
if (empty($_POST)) {
    exit('
<style>
body {
    font-family: Arial,Helvetica,"Nimbus Sans L",sans-serif;
}
#content {
    background: white none repeat scroll 0 0;
    box-shadow: 0 0 1px #999;
    float: left;
    margin-bottom: 10px;
    margin-left: auto;
    margin-right: auto;
    padding: 10px 20px;
    position: relative;
    width: 974px;
}
.authorize_options {
    margin: 0;
    padding: 0;
}
#content .authorize_options li {
    float: left;
    font-style: normal;
    list-style-type: none;
}
#content ul li {
    color: #d85700;
    font-size: 14px;
    font-style: italic;
}
.authorize_options li.cancel {
    margin-left: 15px;
    padding-top: 8px;
}
.button {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    -moz-user-select: none;
    background: rgba(0, 0, 0, 0) linear-gradient(#fafafa, #eaeaea) repeat scroll 0 0;
    border-color: #d4d4d4 #d4d4d4 #bcbcbc;
    border-image: none;
    border-radius: 3px;
    border-style: solid;
    border-width: 1px;
    color: #333;
    cursor: pointer;
    display: inline-block;
    font-family: Helvetica,arial,freesans,clean,sans-serif;
    font-size: 13px;
    font-weight: bold;
    line-height: 24px;
    padding: 0 10px;
    position: relative;
    text-decoration: none;
    text-shadow: 0 1px 0 white;
    white-space: nowrap;
}
a {
    color: #069;
    outline: medium none;
    text-decoration: none;
}
.button:hover {
    background: rgba(0, 0, 0, 0) linear-gradient(#599bdc, #3072b3) repeat scroll 0 0;
    border-color: #518cc6 #518cc6 #2a65a0;
    color: white;
    text-decoration: none;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.3);
}
</style>
<div id="container">
    <article class="home" role="main">
        <div id="content" role="main">
            <h3>Welcome to the OpenBioMaps Authenticator!</h3>
            <p>You have been sent here by <strong>'.$_GET['client_id'].'</strong> client would like to access the following data:</p>
            <ul>
                <li>accessible forms</li>
                <li>form details</li>
            </ul>
            <p>It will use this data to:</p>
            <ul>
                <li>upload data</li>
            </ul>
            <p>Click the button below to complete the authorize request and grant an <code>Authoriation Code</code> to <strong>'.$_GET['client_id'].'</strong> client.
            <ul class="authorize_options">
            <li>
                <form action="" method="post">
                    <input type="submit" class="button authorize" value="Yes, I Authorize This Request" />
                    <input type="hidden" name="authorized" value="1" />
                </form>
            </li>
            <li class="cancel">
                <form id="cancel" action="" method="post">
                    <a href="#" onclick="document.getElementById("cancel").submit()">cancel</a>
                    <input type="hidden" name="authorized" value="0" />
                </form>
            </li>
            </ul>
        </div>
    </article>
</div>');
}

// print the authorization code if the user has authorized your client
$is_authorized = ($_POST['authorized'] === '1');
$server->handleAuthorizeRequest($request, $response, $is_authorized);
if ($is_authorized) {
    // this is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
    #$code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
    #exit("SUCCESS! Authorization Code: $code");
}
$response->send();
?>
