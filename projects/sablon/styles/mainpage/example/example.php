<?php



?>

<div class="wrapper">

  <div class="bx box1"><h3>One</h3>

    <p>Grid based mainpage example</p>
</div>
  <div class="bx box2"><h4>Two</h4></div>
  <div class="bx box3">Three</div>
  <div class="bx box4">Four</div>
  <div class="bx box5"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget scelerisque mi. Aliquam hendrerit mattis nunc, sit amet varius justo cursus eget. Donec ut cursus purus. Curabitur malesuada tellus erat, vitae facilisis ipsum tristique a. Nulla ac lorem non lacus volutpat bibendum tempor ut sem. Integer in tellus sapien. Donec nec tempus augue, egestas porta justo. </div>
</div>

<style>
body {
    background-color:black;
}
#holder {
 /* override the holder background */
  background-color: black;
}
#bheader {
 /* override the bheader background */
  background-color: #1a1a1a;
}
#bheader .htitle p {
 /* override the bheader htitle background */
  background: unset;
}
.wrapper {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-rows: 8vW;
  background: url('https://media.istockphoto.com/photos/crested-gecko-isolated-on-black-picture-id153516614?k=20&m=153516614&s=612x612&w=0&h=msLiF6FC0qoqNLJFoIqePi4UwSJ1cKSJInbSsEOcetA=');
  background-color: rgba(0, 0, 0, 0);
  background-repeat: no-repeat;
  background-color: black;
  background-position-x: right;
}
.bx {
    border: 1px solid gray;
    border-radius:10px;
    margin:2px;
    padding:2px;
}
.box1 {
  grid-column-start: 1;
  grid-column-end: 4;
  grid-row-start: 1;
  grid-row-end: 3;
}

.box2 {
  grid-column-start: 1;
  grid-row-start: 3;
  grid-row-end: 5;
  background: url('https://media.istockphoto.com/photos/crested-gecko-on-black-background-picture-id1318363571?k=20&m=1318363571&s=612x612&w=0&h=_srkzTVUgHXCQjtkWDby_LtK9gWnuD5_k6DGJEb4ubg=');
  background-repeat: no-repeat;
}
</style>
