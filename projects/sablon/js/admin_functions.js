/* functions for logined users
 *
 * */
// invitation sending from profile
function sendInvitation() {
    var to_val = $('#name').val();
    var to_mail = $('#email').val();
    var to_comm = $('#invcomment').val();
    var lang = $('#invlang').find(':selected').val();
    if (to_mail === "" || lang === "") {
        alert('the email address and the language selection are required');
        return;
    }
    $.post("ajax", {name:to_val,mail:to_mail,lang:lang,comment:to_comm,invitesend:1 },
    function(data) {
        var retval = jsendp(data);
        if (retval['status']=='error') {
            alert(retval['message']);
        } else if (retval['status']=='fail') {
            alert(retval['message']);
        } else if (retval['status']=='success') {
            var url = $("#invites").attr("data-url");
            if (typeof url !== "undefined") {
                var pane = $("#invites"), href = $('#invites').attr('href');
                // ajax load from data-url
                //$(href).load(url,function(result){ //pane.tab('show'); });
                $(href).load(projecturl+url,function(result){
                });
            }
        }
    });
}

function toggle_hide(el) {
    var x = document.getElementById(el);
    x.classList.add("hidden");
}
function toggle_show(el) {
    var x = document.getElementById(el);
    x.classList.remove("hidden");
}
function toggle_visible(el) {
    var x = document.getElementById(el);
    x.classList.toggle("hidden");
}

var myviewCodeMirror = {};
var myviewCodeText = {};
function refreshCodeEditor(el,mode) {
    var x = document.getElementById(el);

    //$('#selectJoinedTable')
    
    let text = myviewCodeMirror[el].getValue();

    $.post("ajax", {'refreshViewCode':text,
                    'joined1Table':$('#selectJoinedTable').val(),
                    'joined1Columns':$('#selectJoinedColumn').val(),
                    'mainTable':$('#mainTable').val(),
                    'mainTableColumns':$('#selectBaseColumn').val(),
                    'mainTableJoinColumn':$('#mainTableJoinColumn').val(),
                    'joined1TableJoinColumn':$('#joined1TableJoinColumn').val(),
                    'view_name':$('#view_name').val(),
                    'schema':$('#schema').val(),
                   },
        function(data){
            var resp = JSON.parse(data);
            
            if (resp.status == 'success' || resp.status == 'OK') {
                myviewCodeMirror[el].getDoc().setValue(resp.data);

            } else {
                alert(' :( ');
            }
    });

    /*myviewCodeMirror = CodeMirror.fromTextArea(x,{
        mode: mode,
        styleActiveLine: true,
        theme : 'monokai',
    });*/
}
function loadCodeEditor(el,mode,on = false) {
    var x = document.getElementById(el);
    
    if (on && typeof myviewCodeMirror[el] !== 'undefined') {
        myviewCodeMirror[el].toTextArea();
    }
    if ($("#" + el).is(":visible") || on) {
        myviewCodeMirror[el] = CodeMirror.fromTextArea(x,{
            mode: mode,
            styleActiveLine: true,
            theme : 'monokai',
        });
    } else {
        myviewCodeMirror[el].toTextArea();
    }
}
function saveEditor(el) {
    myviewCodeText[el] = myviewCodeMirror[el].getValue();
    //console.log(myviewCodeText);
}
function saveView(id,el=null) {
    let moveTable = 'f';
    if ($("#moveTable").is(":checked")) { moveTable = 't'; }
    $.post("ajax", { 
        save_view:get_name_of_view(el),
        content:myviewCodeText[id],
        table:$('#mainTable').val(),
        move_table:moveTable
    },function(data){ 
            var resp = JSON.parse(data);
            
            if (resp.status == 'success' || resp.status == 'OK') {
                alert('OK');

            } else {
                alert(' :( ' + resp.message);
            }
    });
}
function refreshView(el=null) {

    $.post("ajax", { 
        refresh_view:get_name_of_view(el),
    },function(data){ 
            var resp = JSON.parse(data);
            
            if (resp.status == 'success' || resp.status == 'OK') {
                alert('OK');
            } else {
                alert(' :( ' + resp.message);
            }
    });
}

function dropView(el=null) {

    $.post("ajax", { 
        drop_view:get_name_of_view(el),
    },function(data){ 
            var resp = JSON.parse(data);
            
            if (resp.status == 'success' || resp.status == 'OK') {
                alert('OK');
            } else {
                alert(' :( ' + resp.message);
            }
    });
}
function editViewName(el=null) {

    $.post("ajax", { 
        edit_view_name:get_name_of_view(el),
        edit_view_newname:$('#' + el + '-newname').val()
    },function(data){ 
            var resp = JSON.parse(data);
            
            if (resp.status == 'success' || resp.status == 'OK') {
                alert('OK');
            } else {
                alert(' :( ' + resp.message);
            }
    });
}
function get_name_of_view(el) {
    let name = null;
    if (el !== null) {
        name = $('#'+el).attr("data-name");
    }
    return name;
}
/* Change group
 * */
$('body').on('click','.change_group',function(e){
    e.stopPropagation();
    e.preventDefault();
    var id=$(this).attr('id');
    var widgetId=id.substring(id.indexOf('-')+1,id.length);
    $.post("ajax", { change_group:widgetId },function(data){ 
        $('#tab_basic').load(projecturl+'includes/profile.php?options=profile',function(result){
            //$(".customScroll").mCustomScrollbar({theme:'dark',scrollInertia:100,scrollButtons:{ enable: true }});
        });
    });
});
/* profile update: interface.php -> profile.php
 * */
function sendInputProfile() {
    $('body').on('click','.edbox_send_profile',function(e){
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        var inputId='dc-'+widgetId;
        var fieldtext=$('#'+inputId).val();
        var name=$('#'+inputId).attr('name');
        
        $.post(projecturl+'includes/update_profile.php', {name:name,text:fieldtext,id:widgetId },
            function(data){
                $('#'+inputId).removeClass('edbox-active');
                var pt = 0;
                var pt1=data.indexOf("Invalid request:");
                if (pt1>-1) pt = pt1+15;
                var pt1=data.indexOf("false");
                if (pt1>-1) pt = pt1+5;
                if(pt>0) {
                    var dt=data.substring(pt,data.length);
                    $('#'+inputId).addClass('edbox-error');
                    //$('#message').html(dt);
                    //$('#message').show();
                    $( "#dialog" ).text(dt);
                    var isOpen = $( "#dialog" ).dialog( "isOpen" );
                    if(!isOpen) $( "#dialog" ).dialog( "open" );
                }
                else if(data.match('OK')) {
                    $('#'+inputId).addClass('edbox-done');
                    if (name=='password') {
                        $('#'+inputId).val('*****');
                        $('#pwnotice').hide();
                    }
                }
                else if(data=='check_your_mailbox') {
                    $('#'+inputId).addClass('edbox-wait');
                    $('#cer').text('Check your mailbox!');
                }
                $('#'+id).hide();
            });

    });
}
// taxon admin function, 
function setTaxon( message ) {
        var a = message.split("#");
        species_plus = species_plus.concat(a[0] + "#" + a[1]); 
        $("<button class='pure-button button-href remove-species' value='" + a[0] + "#" + a[1] + "'>" + a[2] + " &nbsp; <i class='fa fa-close'></i></button>").prependTo( "#tsellist" );
        $("#taxon_sim").val("");
        $("#taxon_sim").html("");
        $("#taxon_sim").text("");
        $("#taxon_sim").focus();
}

function clear_list() {
    const list_editor_json = $("#list-editor-json");
    list_editor_json.val("");
}

function get_list(key = null) {
    const list_editor_json = $("#list-editor-json");
    const editor_string = list_editor_json.val(); 

    try {
        j = JSON.parse(editor_string);
    } catch(e) {
        j = {};
    }

    if (key == null)
        return j;
    else if (Object.keys(j).indexOf(key) >= 0)
        return j[key];
    else 
        return [];
}

function update_list(key, value) {
    const list_editor_json = $("#list-editor-json");

    j = get_list();

    try {
        value = JSON.parse(value);
    } catch(e) {
        value = value;
    }

    j[key] = value;

    if (['triggerTargetColumn','preFilterColumn'].indexOf(key) >= 0 && !Array.isArray(j[key])) { 
        j[key] = [ j[key] ];
    }
    if (key == 'list' && Array.isArray(j[key])) {
        let obj = {};
        for (let i = 0; i < j[key].length; i++) {
            const el = j[key][i];
            obj[el] = [el];
        }
        j[key] = obj;
    }

    list_editor_json.val(JSON.stringify(j,null,2));
}
function update_json(widgetId, value) {

    const je = myCM.widgetId.getValue();
    var text;

    try {
        j = JSON.parse(je);
    } catch(e) {
        j = je;
    }

    if (j == '') j = [];

    if (Array.isArray(j)) {
        j.push(value);
        text = JSON.stringify(j,null,2)
    } else if (typeof(j) == 'object') {
        text = JSON.stringify(j,null,2)
    } else {
        text = je;
    }
    myCM.widgetId.setValue(text);
}
function upload_form_save(method) {
    var template = {
        method:method,
        name:{},access:{},type:{},description:{},type:{},length:{},count:{},file:{},list:{},obl:{},id:{},fullist:{},
        form_name:{},form_type:{},form_access:{},edit_form_id:{},form_group_access:{},form_description:{},form_table:{},form_srid:{},form_data_access_read:{},
        position_order:{},column_label:{},published:{},form_list_group:{},form_data_access_write:{},observationlist_mode:{},observationlist_time:{},tracklog_mode:{},
        periodic_notification:{}
    };
    var a = new Array(),b = new Array(),c = new Array(),d = new Array(),e = new Array(),f = new Array(),g = new Array(),h = new Array(),i = '',j = new Array(),k = '',m = '',
        n = new Array(),p = new Array(),q = new Array(),o='',r='',t = new Array(),s = new Array(),ps = new Array(),sr = '',no = new Array(),po = new Array(),cl = new Array(),
        nw = new Array(),olm = '',olt = '',tm = '',pn = '';

    i = $("#form_name").val();
    j = $("#form_type").val();
    k = $("#form_access").val();
    m = $("#edit_form_id").val();
    n = $("#form_group_access").val();
    olm = $("#form_observationlist_mode").val();
    olt = $("#form_observationlist_time").val();
    tm = $("#form_tracklog_mode").val();
    pn = $("#form_periodic_notification").val();

    if ($("#form_data_access_read").val() !== null) {
        no = $("#form_data_access_read").val();
    }
    if ($("#form_data_access_write").val() !== null) {
        nw = $("#form_data_access_write").val();
    } 

    if ($("#form_data_access_read_uploader").is(":checked")) {
        no.push(-2);
    }
    if ($("#form_data_access_write_uploader").is(":checked")) {
        nw.push(-2);
    }
    o = $("#form_description").val();
    r = $("#form_table").val();
    sr = $("#form_srid").val();
    fp = $("#form-published-at").val();
    flg = $("#form-list-group").val();

    $(".uplf_ckb").each(function() {
        var id=$(this).attr('id');
        var widgetId=id.substring(id.indexOf('-')+1,id.length);
        if ($("#uplf_ckb-"+widgetId).is(":checked")) {
            a.push($("#uplf_description-"+widgetId).val());
            b.push($("#uplf_type-"+widgetId).val());
            c.push($("#uplf_length-"+widgetId).val());
            d.push($("#uplf_count-"+widgetId).val());
            e.push($("#uplf_file-"+widgetId).val());
            f.push($("#uplf_list-"+widgetId).val());
            g.push($("input[name='obl-"+widgetId+"']:checked","#upl-form").val());
            h.push(widgetId);
            p.push($("#uplf_fullist-"+widgetId).val());
            q.push($("#uplf_defval-"+widgetId).val());
            t.push($("#uplf_api_params-"+widgetId).val());
            s.push($("#uplf_relation-"+widgetId).val());
            ps.push($("#uplf_pseudocol-"+widgetId).val());
            po.push($("#uplf_order-"+widgetId).val());
            cl.push($("#uplf_column_label-"+widgetId).html());
        }
    });
    template['description']=a;template['type']=b;template['length']=c;template['count']=d;template['file']=e;template['list']=f;template['obl']=g;template['id']=h;
    template['form_name']=i;template['form_type']=j;template['form_access']=k;template['edit_form_id']=m;template['form_group_access']=n;template['fullist']=p;
    template['default_value']=q;template['form_description']=o;template['form_table']=r;
    template['api_params']=t;template['relation']=s;template['pseudo_columns']=ps;template['form_srid']=sr;template['form_data_access_read']=no;
    template['position_order']=po;template['column_label']=cl;template['published']=fp;template['form_list_group']=flg;template['form_data_access_write']=nw;
    template['observationlist_mode']=olm;template['observationlist_time']=olt;template['tracklog_mode']=tm;
    template['periodic_notification']=pn;

    $.post(projecturl+"includes/mod_project.php", {upload_form:JSON.stringify(template)},
    function(data){
        var retval = jsendp(data);
        if (retval['status']=='error') {
            alert(retval['message']);
        } else if (retval['status']=='fail') {
            console.log(retval['data']);
            alert(retval['message']);
        } else if (retval['status']=='success') 
            $('#tab_basic').load(projecturl+'includes/project_admin.php?options=upload_forms');
    });
}
