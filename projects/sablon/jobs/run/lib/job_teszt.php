<?php
class job_teszt extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('job_teszt initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        job_log("running job_teszt module");

        $params = parent::getJobParams(__CLASS__);
        $sleep = 120;

        if (!$params) {
            job_log('use parameters: {\n"sleep": "60",\n"parameter2": "value"\n}');
        } else {

            extract((array)$params);
            job_log("sleep: ".$sleep);
            job_log("parameter2: ".$parameter2);

        }
        
        job_log('Sleep job for '.$sleep.' seconds');
        exec("sleep $sleep");
        job_log('Done');

        /* R Job execute */
        #$r = (defined('R_PATH')) ? constant('R_PATH') : '/usr/bin/Rscript';
        #exec("nohup $r --vanilla $filename $dirname >>$event_log 2>>$error_log &");
        
        /* Executable job file, e.g .pl, .sh, .. */
        #exec("nohup $filename $dirname >>$event_log 2>>$error_log &");
    }
}
?>
