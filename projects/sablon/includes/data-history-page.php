<?php

    st_col($_SESSION['current_query_table']);
    $qtable=constant('PROJECTTABLE')."_history";

    $ACC_LEVEL = constant('ACC_LEVEL');
    if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
        return;
    }

    $getid = preg_replace('/(\d+)[_]?(\d+)?/','$1',$_SESSION['getid']);
    $has_master_access = has_access('master');

    $acc = rst('acc',$getid,$_SESSION['current_query_table'], $has_master_access); 

    $e = array();

    $sheet_table = $_SESSION['current_query_table'];

    require_once(getenv('OB_LIB_DIR').'results_builder.php');
    $rb = new results_builder();
    if ($rb->results_query($getid)) {
        $res = pg_query($ID,$rb->rq['query']);
        if (pg_num_rows($res))
            $e = pg_fetch_assoc($res);
        else {
            $cmd = sprintf("SELECT 1 FROM %s WHERE obm_id=%d", $sheet_table,$getid);
            $res = pg_query($ID,$cmd);
            if (pg_num_rows($res)) {
                echo "<div style='margin-left:50px;font-size:150%'>".t('str_this_record_is_not_available')."</div>";
            } else {         
                echo "<div style='margin-left:50px;font-size:150%'>".t('str_dataid_does_not_exists')."</div>";
            }
            return;
        }
    } else {
        echo $rb->error;
        return;
    }

    $t = array_keys($e);
    $t[] = 'obm_validation';

    // a sorrend rendezett sorrend esetén gond!!
    // nem megengedett adatok is kijöhetnek miatta
    // vissza kell rendezni az oszlopkat az column_num alapján!!

    $cmd = "SELECT column_name FROM information_schema.columns WHERE table_name='".$sheet_table."' ORDER BY ordinal_position";
    $res = pg_query($ID,$cmd);
    $columns = pg_fetch_all($res);
    $column_names = array();
    foreach ($columns as $j) {
        $column_names[] = $j['column_name'];
    }

    $allowed_cols = $column_names;

    //a subset of the main cols, depending on user's access rights
    if ($modules->is_enabled('allowed_columns')) {
        $allowed_cols = $modules->_include('allowed_columns','return_columns',$column_names,true);
    } else {
        if (!isset($_SESSION['Tid']) and ( "$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' )) {
            // drop all column for non logined users if access level is higher
            $allowed_cols = array('obm_id');
        }
        if (!$acc) $allowed_cols = array('obm_id');
    }

    // _rules JOIN and sensitive check
    if (!isset($_SESSION['Tid']))
        $tid = 0;
    else
        $tid = $_SESSION['Tid'];
    if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
        $tgroups = 0;
    else
        $tgroups = $_SESSION['Tgroups'];


    if (!$has_master_access and $_SESSION['st_col']['USE_RULES']) {
        $rules_filter = " AND ( (sensitivity::varchar IN('3','only-owner') AND ARRAY[$tgroups] && read) ";
        if (isset($_SESSION['current_query_keys']) and in_array('obm_geometry',$_SESSION['current_query_keys'])) {
            $rules_filter .= " OR (sensitivity::varchar IN ('1','no-geom') AND ARRAY[$tgroups] && read) ";
        }
        $rules_filter .= " OR sensitivity::varchar IN ('0','2','public','restricted','sensitive') ) ";
        $rules_join = sprintf("LEFT JOIN %s ON (h.row_id=obm_id) %s",$sheet_table, join_table('rules', ['qtable' => $sheet_table, 'id_col' => 'obm_id']));
    } else {
        $rules_filter = '';
        $rules_join = '';
    }

    $cmd = sprintf('SELECT * FROM %1$s h %2$s WHERE h.row_id=%3$s %4$s',$qtable,$rules_join,quote($getid),$rules_filter);
    $res = pg_query($ID,$cmd);

    if (!pg_num_rows($res)) {
        echo "<div style='margin-left:50px;font-size:150%'>".t('str_this_record_is_not_available')."</div>";
        return;
    }

    $table = new createTable();
    $table->def(['tid'=>'mytable','tclass'=>'resultstable']);
    $h = array();
    $z = array();
    

    while ($row = pg_fetch_assoc($res)) {
        $h = array();
        //restore has no effect
        //It is not exists yet!!!


        if(rst('mod',$getid,$sheet_table, $has_master_access)) {
            $z = array('<button class="pure-button button-passive" id="restore" val="'.$getid.'"><i class="fa fa-hand-o-left fa-lg"></i></button>');
        }
        else {
            $z = array('');
        }
        $z[] = $row['operation'];
        $z[] = $row['hist_time'];
        $z[] = $row['userid'];
        $z[] = $row['hist_id'];
        $z[] = $row['modifier_id'];

        $a = preg_replace("/^\(/","",$row['query']);
        $a = preg_replace("/\)$/","",$a);
        $l = str_getcsv($a,",",'"');

        // drop geometry column if it is restricted
        $geometry_restriction = 0;
        if (!has_access('master') and $_SESSION['st_col']['USE_RULES'] and array_search('obm_geometry',$allowed_cols)!==false) {

            $cmd = sprintf("SELECT CASE WHEN sensitivity::varchar NOT IN ('1','no-geom') OR (sensitivity::varchar IN ('1','no-geom') AND ($tid=ANY(read))) THEN 0 ELSE 1 END AS geometry_restriction
                FROM %s_rules WHERE row_id=%d",constant('PROJECTTABLE'),$getid);

            $res2 = pg_query($ID,$cmd);
            if (pg_num_rows($res2)) {
                $row2 = pg_fetch_assoc($res2);
                if ($row2['geometry_restriction']) {
                    $geometry_restriction = 1;
                }
            }
        }

        $acc = rst('acc',$getid,$sheet_table, $has_master_access); 

        foreach($column_names as $cn)
        {
            $k = array_shift($l);
            if (!array_search($cn,$t)) continue;

            /* High level restrictions handling
            /* we can query data but drop details if it is out of allowed */
            if (!$acc) {
                if ( !in_array($cn,$allowed_cols) ) {
                    $k = '';
                }
            }

            if ($cn == 'obm_geometry') {
                if ($geometry_restriction) $k = '';
                else {
                    if ($k!='') {
                        $cmd = sprintf("SELECT st_asText('%s'::geometry)",$k);
                        $res3 = pg_query($BID,$cmd);
                        if(pg_num_rows($res3)) {
                            $mas = pg_fetch_row($res3);
                            $k = $mas[0];
                        }
                    }
                }
            }

            $h[] = $cn;
            $z[] = $k;
        }
        $table->addRows($z);
    }

    $table->addHeader(array_merge(array('restore','action','when','admin','histid','OBM user'),$h));
    echo $table->printOut();

?>
