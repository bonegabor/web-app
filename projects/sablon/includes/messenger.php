<?php
/**
 */

class Messenger {
    public $project_email;
    public $reply_email;
    public $non_news_types = ['personal message', 'comment notification', 'system message', 'only email'];
    public $news_types = ['system news','project news'];
    private $unread;
    public $error;

    function __construct($action = null, $params = null,$pa = array()) {
        $this->query_project_email();
        $this->query_number_of_unread_messages();
    }

	/**
    * Send a message
    * 
    * This method can send a personal message to a user or broadcast a system or project news
    *
	* @param object\string $from an instance of User who is sending the message or any of ['project','system'] in case of system or project messages
	* @param object|string $to an instance of User for which the message is meant or 'all' if it is a news broadcasted to the news wall
	* @param string $subject subject of a message
    * @param string $message message that is sent to a user
	* @param integer $respond ID of conversation to which this message is a part of
    * @param boolean $send_email If true a notification email is sent to the recipient
    *
    * @return boolean 
    *
	**/
    private function send_message($message_type, $from, $to, $subject, $message, $respond = null, $send_email = false) {
        global $BID;
        
        // 1. case: system news to everybody
        // 2. case: project news to everybody
        if (in_array($message_type,['system news','project news'])) {
            $from_quoted = 'NULL';
            $to_quoted = 'NULL';
            $receive_mail = false;
        }
        // 3. case: system message to user
        // 4. case: project message to a user
        elseif (in_array($message_type,['system message']) && ($to instanceof User)) {
            $from_quoted = 'NULL';
            $to_quoted = quote($to->user_id);
            $receive_mail = ($send_email === 'force') ? true : $to->options->receive_system_message_mails;
        }
        // 5. case: personal message from user to user
        elseif (in_array($message_type,['personal message','comment notification']) && ($from instanceof User) && ($to instanceof User)) {
            $from_quoted = quote($from->user_id);
            $to_quoted = quote($to->user_id);
            $receive_mail = ($message_type == 'personal message')
                ? $to->options->receive_personal_message_mails
                : $to->options->receive_comment_notification_mails;
        }
        elseif ($message_type == 'only email' && ($from instanceof User)) {
            $to_email = $to;
            $to = new User('non-logined-user');
            $to->email = $to_email;
            $this->reply_email = $from->email;
            
            $from_quoted = quote($from->user_id);
            $to_quoted = 'NULL';
            $receive_mail = true;
        }
        else {
            log_action('incorrect usage of send_message method', __FILE__, __LINE__);
            log_action($message_type,__FILE__,__LINE__);
            log_action($from,__FILE__,__LINE__);
            log_action($to,__FILE__,__LINE__);
            return false;
        }
        
        $respond = ($respond) ? quote($respond) : 'NULL';
        
        $cmd = sprintf("INSERT INTO project_messages (user_to, user_from, subject, message, respond, project_table, message_type) 
            VALUES (%s,%s,%s,%s,%s,'%s','%s') RETURNING id;",$to_quoted, $from_quoted, quote($subject), quote($message), $respond, PROJECTTABLE, $message_type);
        if (!$res = query($BID,$cmd)) {
            $this->error = 'message saving query error';
            return false;
        }
        if ($send_email && $receive_mail) {
            $results = pg_fetch_assoc($res[0]);
            /*
            if ($from->is_master() && $message_type === 'announcement') {
                $this->send_email_to_project_users();
            }
            */
            $email_sent = mail_to($to->email,$subject,$this->project_email,$this->reply_email,$message,'multipart');
            if ($email_sent !== 2) {
                log_action("Sending notification email of message #{$results['id']} failed ($email_sent).", __FILE__,__LINE__);
                $this->error = 'Message emailing failed';
            }
            $cmd = sprintf("UPDATE project_messages SET email_notification = '%s' WHERE id = %d;",
                ($email_sent==2) ? 'sent' : 'failed',
                $results['id']
            );
            query($BID,$cmd);
        }
        return true;
    } // END send_message
    
    
    public function send_system_message($to, $template_name, $words = [], $send_email = false) {
        $mt = new MessageTemplate($template_name);
        if (!$mt->exists) {
            log_action('system messages must have a template',__FILE__,__LINE__);
            return false;
        }
        if (in_array($template_name, ['lostpw',  'dropmyaccount', 'change_email_address'] )) {
            $send_email = 'force';
        }
        $lang = $to->options->language ?? array_keys(LANGUAGES)[0];
        $subject = $mt->get('subject', $lang, $words);
        $message = $mt->get('message', $lang, $words);
        
        return $this->send_message('system message',null, $to, $subject, $message, null, $send_email);
    }
    
    public function send_project_news($template_name, $words = []) {
        $mt = new MessageTemplate($template_name);
        if (!$mt->exists) {
            log_action('project news must have a template',__FILE__,__LINE__);
            return false;
        }
        $lang = array_keys(LANGUAGES)[0];
        $subject = $mt->get('subject', $lang, $words);
        $message = $mt->get('message', $lang, $words);
        return $this->send_message('project news', null, null, $subject, $message);
    }
    
    public function send_system_news($template_name, $words = []) {
        $mt = new MessageTemplate($template_name);
        if (!$mt->exists) {
            log_action('system news must have a template',__FILE__,__LINE__);
            return false;
        }
        
        $lang = array_keys(LANGUAGES)[0];
        $subject = $mt->get('subject', $lang, $words);
        $message = $mt->get('message', $lang, $words);

        return $this->send_message('system news', null, null, $subject, $message);
    }
    
    public function send_pm($from, $to, $subject, $message, $response, $send_email = false) {
        return $this->send_message('personal message', $from, $to, $subject, $message, $response, $send_email);
    }
    
    public function send_comment_notification($from, $to, $template_name, $words) {
        
        $lang = $to->options->language ?? array_keys(LANGUAGES)[0];
        $mt = new MessageTemplate($template_name);
        if (!$mt->exists) {
            log_action('comment notifications must have a template',__FILE__,__LINE__);
            return false;
        }
        
        $subject = $mt->get('subject', $lang, $words);
        $message = $mt->get('message', $lang, $words);
        
        if (defined('DATA_COMMENTS_ARE_PUBLIC'))
            return (
                $this->send_project_news($subject, $message) &&
                $this->send_message('comment notification', $from, $to, $subject, $message, null, true)
            ); 
        else
            return (
                $this->send_message('comment notification', $from, $to, $subject, $message, null, true)
            );
    }
    
    /** 
     * this function is used for sending messages outside the project (invitation, bug_report)
     * the reason using this is, to have a copy of the message in the sent messages
     */
    public function send_only_email($from, $to, $subject, $message) {
        return ($this->send_message('only email', $from, $to, $subject, $message, null, true));
    }
    
    public function get_messages($message_type, $options = []) {
        $defaults = [
            'from' => false,
            'to' => false,
            'limit' => 100,
            'since' => false,
            'until' => false,
            'subject_contains' => false,
            'message_contains' => false,
            'only_this_project' => true,
            'orderBy' => 'sender_open',
            'order' => 'DESC'
        ];
        foreach ($defaults as $key => $value) {
            if (!isset($options[$key])) {
                $options[$key] = $value;
            }
        }
        
        global $BID;

        if (!is_array($message_type)) $message_type = array($message_type);
        
        foreach ($message_type as $mt) {
            if (!in_array($mt, array_merge($this->non_news_types, $this->news_types))) {
                return false;
            }
        }
        $where = sprintf("message_type = ANY ('{%s}')", implode(",",$message_type));
        
        $where_project = ($options['only_this_project'] === true ) 
            ? sprintf("project_table = '%s' AND ", PROJECTTABLE)
            : "";
        
        if (is_int($options['from'])) {
            $where .= sprintf(" AND user_from = %d", $options['from']);
        }
        if (is_int($options['to'])) {
            $where .= sprintf(" AND user_to = %d", $options['to']);
        }
        if ($options['since'] instanceof DateTime) {
            $where .= sprintf(" AND sender_open > '%s'", $options['since']->format('Y-m-d H:i:s'));
        }
        if ($options['until'] instanceof DateTime) {
            $where .= sprintf(" AND sender_open < '%s'", $options['until']->format('Y-m-d H:i:s'));
        }
        if ($options['subject_contains']) {
            $where .= sprintf(" AND subject LIKE %s", quote("%{$options['subject_contains']}%"));
        }
        if ($options['message_contains']) {
            $where .= sprintf(" AND message LIKE %s", quote("%{$options['message_contains']}%"));
        }
        
        $cmd = sprintf("SELECT pm.*, s.username as from_name, r.username as to_name FROM project_messages pm LEFT JOIN users s ON s.id = pm.user_from LEFT JOIN users r ON r.id = pm.user_to WHERE %s %s ORDER BY %s %s LIMIT %d;" ,
            $where_project, 
            $where, 
            $options['orderBy'],
            $options['order'],
            $options['limit']
        );
        if ( !$res = pg_query($BID, $cmd)) {
            $this->error = 'get_sent_messages query error';
            return false;
        }

        if (!pg_num_rows($res)) return array();
        else return pg_fetch_all($res);
    }
    
    
    public function get_system_messages($options) {
        return $this->get_messages('system message',$options);
    }
    
    public function get_my_messages($message_type) {
        return $this->get_messages($message_type, ['to' => (int)$_SESSION['Tid']]);
    }
    
    public function get_my_sent_messages() {
        return $this->get_messages(array('comment notification','personal message','only email'), ['from' => (int)$_SESSION['Tid']]);
    }
    
    public function get_news($limit = null) {
        $news = array_merge(
            $this->get_messages('project news',['only_this_project' => true]),
            $this->get_messages('system news')
        );
        $date = array_column($news, 'sender_open');
        array_multisort($date, SORT_DESC, $news);
        
        return ($limit && is_numeric($limit)) ? array_slice($news, 0, (int)$limit) : $news;
    }
    
    private function query_number_of_unread_messages() {
        global $BID;
        if (! isset($_SESSION['Tid']) ) {
            return array();
        }
        $cmd = sprintf("SELECT count(*) as c, message_type FROM project_messages WHERE user_to = %d AND receiver_open IS NULL AND message_type IN (%s) AND project_table = '%s' GROUP BY message_type;", 
            (int)$_SESSION['Tid'], 
            implode(',',array_map('quote',$this->non_news_types)), 
            PROJECTTABLE);
        if (! $res = query($BID,$cmd)) {
            log_action('get_number_of_unread_messages query error',__FILE__,__LINE__);
            log_action($cmd,__FILE__,__LINE__);
            $this->unread = false;
        }
        $counts = [];
        while ($row = pg_fetch_assoc($res[0])) {
            $counts[preg_replace("/\s/","_",$row['message_type'])] = $row['c'];
        }
        foreach ($this->non_news_types as $mt) {
            if (!isset($counts[preg_replace("/\s/","_",$mt)])) {
                $counts[preg_replace("/\s/","_",$mt)] = 0;
            }
        }
        
        $this->unread = $counts;
    }
    
    public function get_unread_numbers($requery = false) {
        if ($requery === true) {
            $this->query_number_of_unread_messages();
        }
        
        return $this->unread;
    }
    
    public function get_unread_badge($message_type = null, $requery = false) {
        if ($message_type === null) {
            $nr = array_reduce($this->unread, function($sum, $item) {
                return $sum += $item;
            });
        }
        else {
            $nr = $this->get_unread_numbers($requery)[$message_type];
        }
        return ($nr > 0) ? "<span class='unread_messages_badge'>$nr</span>" : "";
    }
    
    public function get_total_unread_number($requery = false) {
        return array_reduce($this->get_unread_numbers($requery), function ($sum, $item) {
            return $sum += $item;
        });
    }
    
    
    
    
    
    private function query_project_email() {
        global $BID;
        
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        
        if (defined("OB_PROJECT_DOMAIN")) {
            $domain = constant("OB_PROJECT_DOMAIN");
            $this->project_email = PROJECTTABLE."@".parse_url("$protocol://".$domain,PHP_URL_HOST);
            $this->reply_email = "noreply@".parse_url("$protocol://".$domain,PHP_URL_HOST);
        } elseif (isset($_SERVER["SERVER_NAME"])) {
            $domain = $_SERVER["SERVER_NAME"];
            $this->project_email = PROJECTTABLE."@".$domain;
            $this->reply_email = "noreply@".$domain;
        }
        else {
            $res = pg_query($BID,"SELECT email FROM projects WHERE project_table='".PROJECTTABLE."'");
            $row = pg_fetch_assoc($res);
            $this->project_email = $row['email'];
            $this->reply_email = (preg_match('/^.*@([^\/]+)/i', $this->project_email, $em)) ? "norepy@{$em[1]}" : "noreply@openbiomaps.org";
        }
    }
    
    public function inbox() {
        ob_start();
        
        if (isset($_SESSION['role_processed'])) {
            unset($_SESSION['role_processed']);
        }
        $project_members = new Role('project members');
        $active_members = array_values(array_filter($project_members->get_members(), fn ($m) => $m->status != '0'));
        $groups_of_members = $project_members->get_groups();

        $strings = [
            'str_messages', 'str_personal_messages', 'str_sent_messages', 'str_system_messages', 'str_comment_notifications', 'str_news_stream', 'str_write_message', 'str_send', 'str_sender', 'str_recipient', 'str_date', 'str_subject', 'str_recipient', 'str_message_as_email'
        ];
        $inbox_data = [
            't' => array_combine($strings, array_map('t', $strings)),
            'cog' => isset($_GET['messages']),
            'protocol' => protocol(),
            'url' => constant('URL'),
            'tcrypt' => $_SESSION['Tcrypt'],
            'members' => array_map(function ($m) {
                return [
                    'role_id' => $m->role_id,
                    'name' => $m->name
                ];
            }, $active_members),
            'pm_unread_badge' => $this->get_unread_badge('personal message'),
            'sm_unread_badge' => $this->get_unread_badge('system message'),
            'cn_unread_badge' => $this->get_unread_badge('comment notification'),
        ];

        if (has_access('master')) {
            $inbox_data['is_master'] = true;
            $inbox_data['groups'] = array_map(fn ($m) => [
                'role_id' => $m->role_id,
                'description' => $m->description
            ], $groups_of_members);
        }
        
        return render_view('admin_pages/inbox', $inbox_data, false);
    }
}

/**
 * 
 */
class TemplateEditor
{
    private $templates = [];
    
    function __construct()
    {
        $this->request_templates();
    }
    
    public function get_templates_list() {
        return $this->templates;
    }
    
    private function request_templates() {
        global $BID;
        
        $cmd = sprintf("SELECT DISTINCT name FROM message_templates WHERE project_table IS NULL OR project_table = '%s' ORDER BY name ASC;", PROJECTTABLE);
        if (! $res = pg_query($BID, $cmd)) {
            log_action(__CLASS__ . ' query error');
            return;
        }
        while ($row = pg_fetch_assoc($res) ) {
            $this->templates[] = $row['name'];
        }
        
    }
    
    function interface() {
        
        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
        
        
        ob_start();
        ?>
        <div class="template_manager">
            <div class="templates">
                <ul>
                <?php foreach ($this->templates as $template): ?>
                    <li><a href="" class="template"><?= $template ?></a></li>
                <?php endforeach; ?>
                </ul>
            </div>
            <div class="editor">
                <div class="template_manager_help">
                    <?= t('str_template_manager_help') ?>
                </div>
                
                <form id="template-editor-form" class="pure-form pure-form-aligned" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="save_message_template">    
                    <input id="template_name" type="text" name="template_name" placeholder="<?= t('str_template_name') ?>">
                    <?php foreach (LANGUAGES as $lang => $label): ?>
                        
                        <h3> <span style="background-image: url(<?= $protocol . '://' . URL . '/images/' . $lang . '.svg' ?>);"></span> <?= $label ?></h3>
                        <input id="template_subject_<?= $lang ?>" class="template_subjects" type="text" placeholder="<?= t('str_subject') ?>">
                        <div id="template_editor_<?= $lang ?>" class="template_editor"></div>
                    <?php endforeach; ?>
                    <div class="buttons">
                        <button type="submit" class="pure-button button-success"> <i class="fa fa-floppy-o"></i> <?= str_save ?></button>
                        <button type="button" class="pure-button" name="clear_editor"> <i class="fa fa-refresh"></i>  <?= str_clear ?></button>
                        <button type="button" class="pure-button button-danger" name="delete_template"> <i class="fa fa-trash"></i>  <?= str_delete ?></button>
                    </div>
                </form>
            </div>
        </div>
        
        <?php 
        return ob_get_clean();
    }
}

/**
 * 
 */
class Message
{
    public $message;
    function __construct($id)
    {
        global $BID;
        if (!is_numeric($id)) {
            return;
        }
        $cmd = sprintf("SELECT * FROM project_messages WHERE id = %s;", quote($id));
        if (!$res = query($BID, $cmd)) {
            log_action('Message constructor query failed',__FILE__,__LINE__);
            log_action($cmd,__FILE__,__LINE__);
            return;
        }
        $this->message = pg_fetch_assoc($res[0]);
    }
    
    public function set_read() {
        global $BID;
        if (!isset($_SESSION['Tid'])) {
            return false;
        }
        
        $cmd = sprintf("UPDATE project_messages SET receiver_open = NOW() WHERE id = %d AND user_to = %d;", $this->message['id'], $_SESSION['Tid']);
        return (query($BID,$cmd));
    }
}

/**
 * Message template
 */
class MessageTemplate 
{
    public $name = "";
    public $subject = [];
    private $message = [];
    private $project_title = [];
    private $project_email = "";
    private $admin_emails = "";
    //private $frame;
    public $exists = false;
    
    function __construct($name = null) {
        global $BID;
        
        $this->request_project_title();
        
        if (is_null($name) || ! preg_match('/^\w+$/', $name )) {
            return;
        };
        $languages = sprintf("(%s)", implode(',', array_map(function($l) {
            return "'$l'";
        }, array_keys(LANGUAGES) )));
        // project templates overriding default templates
        $cmd = sprintf("WITH 
        pr as ( 
            SELECT * FROM message_templates WHERE name = %1\$s AND project_table = '%2\$s' AND lang IN %3\$s
        ),
        def as ( 
            SELECT mt.* FROM message_templates mt LEFT JOIN pr ON mt.name = pr.name AND mt.lang = pr.lang  WHERE mt.name = %1\$s AND mt.project_table IS NULL AND pr.name IS NULL AND mt.lang IN %3\$s )
        SELECT * FROM pr UNION SELECT * FROM def;", 
            quote($name), 
            PROJECTTABLE,
            $languages
        );
        //debug($cmd,__FILE__,__LINE__);
        
        if (! $res = pg_query($BID, $cmd)) {
            log_action(__CLASS__ . " query error!");
            return;
        };
        $this->name = $name;
        
        if (pg_num_rows($res) != 0) {
            $this->exists = true;
            $templates = pg_fetch_all($res);
            foreach ($templates as $t) {
                $this->subject[$t['lang']] = $t['subject'];
                $this->message[$t['lang']] = $t['message'];
            }
        }
        
    }
    
    public function set($what, $lang, $content) {
        if (!in_array($what, ['message','subject'])) {
            return false;
        }
        if (!in_array($lang, array_keys(LANGUAGES))) {
            return false;
        }
        $this->$what[$lang] = $content;
    }
    
    public function save() {
        global $BID;
        
        $cmd = [];
        foreach (array_keys(LANGUAGES) as $lang) {
            $cmd[] = sprintf('INSERT INTO message_templates (name, lang, subject, message, project_table) VALUES (%1$s, %2$s, %3$s, %4$s, %5$s)
            ON CONFLICT (name, lang, project_table) DO UPDATE SET subject = %3$s, message = %4$s;', 
                quote($this->name), 
                quote($lang), 
                quote($this->subject[$lang]), 
                quote($this->message[$lang]), 
                quote(PROJECTTABLE)
            );
        }
        return ($res = query($BID, $cmd));
        
    }
    
    public function delete() {
        global $BID;
        
        $cmd = sprintf('DELETE FROM message_templates WHERE name = %s AND project_table = \'%s\';', 
            quote($this->name), 
            PROJECTTABLE
        );
        return ($res = pg_query($BID, $cmd));
        
    }
    
    public function get($what, $lang, $words = []) {
        if (!in_array($what, ['message','subject'])) {
            log_action("What else do you want than subject or message?");
            return false;
        }
        
        if (! isset($this->$what[$lang])) {
            log_action("Template does not exist on $lang language.");
            return "";
        }
        
        $text = $this->include_templates($what, $lang);
        
        $words = array_merge($words, [
            'PROJECT_TABLE' => PROJECTTABLE,
            'USER_NAME' => (isset($_SESSION['Tname'])) ? $_SESSION['Tname'] : 'unlogged user',
            'URL' => URL,
            'OB_DOMAIN' => OB_DOMAIN,
            'DOMAIN' => $this->domain,
            'PROTOCOL' => $this->protocol,
            'PROJECT_TITLE' => $this->project_title[$lang] ?? PROJECTTABLE,
            'PROJECT_DESCRIPTION' => $this->project_description[$lang] ?? PROJECTTABLE,
            'PROJECT_EMAIL' => $this->project_email,
            'ADMIN_EMAIL' => implode(', ', $this->admin_emails),
        ]);
        
        return $this->substitute($text,$words);
        
    }
    
    
    public function get_raw($what, $lang = null) {
        if (!in_array($what, ['message','subject'])) {
            return false;
        }
        if (is_null($lang)) {
            return $this->$what;
        }
        else {
            return (isset($this->$what[$lang])) ? $this->$what[$lang] : '';
        }
    }
    
    
    private function include_templates($what, $lang) {
        if (!in_array($what, ['message','subject'])) {
            return false;
        }
        
        if (! isset($this->$what[$lang])) {
            return "";
        }
        
        $text = $this->$what[$lang];
        if ( preg_match_all('/@(\w+)@/', $text, $templates) ) {
            foreach ($templates[1] as $t) {
                $mt = new MessageTemplate($t);
                $text = str_replace("@$t@", $mt->get_raw($what, $lang), $text);
            }
        }
        return $text;
    }
    
    
    private function substitute($string, $words) {
        
        $patterns = array_map(function($word) {
            return "/%" . $word ."%/";
        }, array_keys($words));
        $replacements = array_values($words);
        
        return preg_replace($patterns, $replacements, $string);
    }
    
    private function request_project_title() {
        global $BID;
        
        $cmd = [
            "SELECT short, long, language FROM project_descriptions WHERE projecttable='" . PROJECTTABLE . "';",
            "SELECT domain, protocol, email FROM projects WHERE project_table = '".PROJECTTABLE."';",
        ];
        
        if (! $res = query($BID,$cmd) ) {
            log_action(__CLASS__ . ' query error', __FILE__,__LINE__);
            return false;
        }
        
        $results = pg_fetch_all($res[0]);
        foreach ($results as $row) {
            $this->project_title[$row['language']] = $row['short'];
            $this->project_description[$row['language']] = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/","<br><br>",$row['long']);
        }
        $results = pg_fetch_assoc($res[1]);
        $this->domain = $results['domain'];
        $this->protocol = $results['protocol'];
        $this->project_email = $results['email'];
        
        $masters = new Role('project masters');
        $this->admin_emails = $masters->get_member_emails();
        
        return true;
    }
}

?>
