<?php
class species_list_load extends iapi {

    public static $main_taxon_field;

    public function execute($params) {

        $j = json_decode($params,true);

        $ret = $params;

        if (count($j['functions'])) {
            if (in_array('taxon_table',$j['functions'])) {
                return self::get_taxon_table();
            }
            elseif (in_array('taxon_list',$j['functions'])) {
                return self::get_taxon_list();
            }
        }

        if (count($j['options'])) {
            // ...
        }
        

    }

    private function get_taxon_list() {
        global $ID;
        $species_array = self::get_species_list_columns();
        parent::$species_fields = array_reverse($species_array);
        $cmd = self::species_query($species_array);
        
        if (!$res = pg_query($ID,$cmd)) {
            log_action('query error', __FILE__, __LINE__);
            return common_message('error','Query error');
        }
        $results = pg_fetch_all($res);

        $main = self::$main_taxon_field;

        $x_modules = new x_modules();
        if ($x_modules->is_enabled('taxon_meta') && $x_modules->_include('taxon_meta', 'is_submodule_enabled', ['TaxonOrder'])) {
          $results = array_map(function ($r) use ($main) {
              $r['taxon_id'] = $r["taxon_id_$main"];
              $r['word'] = $r[$main];
              return $r;
          }, $results);
          $orderby = false;
          if (isset($_SESSION['Tid'])) {
            $User = new User($_SESSION['Tid']);
            $orderby = $User->options->taxon_order;
          }
          $results = $x_modules->_include('taxon_meta', 'include_submodule', ['TaxonOrder', 'reorder_list', ['list' => $results, 'orderby' => $orderby]] );
        }
        
        return common_message('ok',$results);
    }
    
    private function get_taxon_table() {
        # legegyszerűbb fajlista lekérdezése
        global $ID;
        $st_col = st_col($table, 'array');
        $species_array = $st_col['ALTERN_C'];
        $sp = implode(",",$species_array);
        $cmd = "SELECT * FROM \"".PROJECTTABLE."_taxon\" ORDER BY taxon_id";
        $result = pg_query($ID,$cmd);
        
        if (!$result) {
            return common_message('error','Failed to get specieslist for the project');
        }

        if (pg_num_rows($result)) {
            return common_message('ok',pg_fetch_all($result));
        }
    }

    private function get_species_list_columns() {
        $species_array = array_filter(
            array_unique(
                array_merge(
                    array_diff($_SESSION['st_col']['ALTERN_C'], array_values($_SESSION['st_col']['NATIONAL_C'])),
                    array_values($_SESSION['st_col']['NATIONAL_C']),
                    [$_SESSION['st_col']['SPECIES_C']],
                    [$_SESSION['st_col']['SPECIES_C_SCI']]
                )
            )
        );
        if ($specieslist_columns = get_option('specieslist_columns')) {
            $species_array = explode(',', $specieslist_columns);
        }
        return $species_array;
    }

    private function species_query($species_array) {
    
        $only_accepted = (get_option('specieslist_onlyaccepted') !== 'false');
    
        $main = array_pop($species_array);
        self::$main_taxon_field = $main;
        $join = array(sprintf("%s_taxon $main",PROJECTTABLE));
        $c = array(sprintf('%1$s.word as %1$s,%1$s.taxon_id taxon_id_%1$s,%1$s.lang as lang_%1$s',$main));
        $where = array("$main.lang='$main'");

        foreach($species_array as $column) {
            $oa = ($only_accepted) ? "AND ($column.status = 'accepted' OR $main.status = 'accepted')" : "";
            $c[] = sprintf('%1$s.word as %1$s,%1$s.taxon_id as taxon_id_%1$s,%1$s.lang as lang_%1$s',$column);
            $join[] = sprintf("LEFT JOIN %s_taxon $column ON ($column.taxon_id=$main.taxon_id AND $main.lang='$main' AND $column.lang='$column' $oa)",PROJECTTABLE);
            $where[] = "$column.lang='$column'";
        }
        $cmd = sprintf("SELECT %s FROM %s WHERE %s ORDER BY %s",implode(',',$c),implode(' ',$join),implode(' OR ',$where),"$main.word");
        
        return $cmd;
    }
}

?>
