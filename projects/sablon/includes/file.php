<?php 
/**
 * 
 */
class File extends Entity {
    protected $tableSchema = 'system';
    protected $tableName = 'files';
    protected $db = 'gisadmin';
    
    public $id;
    public $project_table;
    public $reference;
    public $comment;
    public $datum;
    public $access;
    public $user_id;
    public $status;
    public $sessionid;
    public $sum;
    public $mimetype;
    public $data_table;
    public $exif;
    public $slideshow;
    
    function __construct() {
        parent::__construct();
    }
    
    /**
     * Removes files if they belong to other project
     **/
    public static function filter_results($results) {
        return array_values(array_filter($results, function ($file) {
            return $file->project_table === PROJECTTABLE;
        }));
    }
    
    public function before_save() {
        $this->check_access();
    }
    
    public function before_delete() {
        
        $this->check_access();
        
        $root_dir = getenv('PROJECT_DIR') . 'local/attached_files';
        if (!unlink($root_dir . '/' . basename($this->reference))) {
            throw new \Exception("File can not be delted!", 1);
        }
        
        #search for thumbnails - delete them all
        chdir("$root_dir/thumbnails/");
        foreach(glob("*{".basename($this->reference)."}", GLOB_BRACE) as $s) {
            unlink($s);
        }
        return true;
    }
    
    public function check_access() {
        if (!has_access('master')) {
            if (!isset($_SESSION['Tid'])) {
                throw new \Exception(t('str_access_denied'), 1);
            }
            if ($file->user_id !== $_SESSION['Tid']) {
                throw new \Exception(t('str_access_denied'), 1);
            }
        }
    }
    
    public function getConnections() {
        if (!$this->id) {
            return [];
        }
        
        $connections = FileConnection::find(['file_id', $this->id]);
        // getting the obm_ids 
        if (count($connections)) {
            $obm_ids = $this->getObmIDs(array_column($connections, 'conid'));
            for ($i=0; $i < count($connections); $i++) {
                // delete orpfan connections (conid not found in data table)
                if (!isset($obm_ids[$connections[$i]->conid])) {
                    log_action("orphan connection: {$connections[$i]->conid} deleted.");
                    $connections[$i]->delete();
                    unset($connections[$i]);
                    continue;
                }
                $connections[$i]->obm_id = $obm_ids[$connections[$i]->conid];
            }
            $connections = array_values($connections);
        }
        
        return $connections;
    }
    
    public function addConnection($row_id) {
        //global $GID;
        
        $conid = $this->get_obm_files_id($row_id);
        
        $con = FileConnection::morph([
            'file_id' => $this->id,
            'conid' => $conid,
            'temporal' => false,
            'sessionid' => session_id(),
            'rownum' => null,
        ]);
        $con->save();

        if (!$conid) {
            $this->set_obm_files_id($row_id, $con->conid);
        }
        return true;
    }
    
    public function deleteConnection($row_id) {
        $this->check_access();
        foreach ($this->getConnections() as $connection) {
            if (is_numeric($row_id) && $connection->obm_id === (int)$row_id) {
                $connection->delete();
                break;
            }
            else if ($row_id === 'all') {
                $connection->delete();
            }
            else {
                throw new \Exception("Invalid row_id provided", 1);
            }
        }
        return true;
    }
    
    private function get_obm_files_id($row_id) {
        global $ID;
        // getting the connection id from the data_table
        $cmd = sprintf("SELECT obm_files_id FROM %s WHERE obm_id=%d",$this->data_table, $row_id);
        $res = pg_query($ID,$cmd);
        if (!pg_num_rows($res)) {
            throw new \Exception("This id does not exist!: $row_id");
        }
        $row = pg_fetch_assoc($res);
        $conid = $row['obm_files_id'];
        return $conid;
    }
    
    private function set_obm_files_id($row_id, $conid) {
        global $ID;
        
        $cmd = sprintf("UPDATE %s SET obm_files_id = %s WHERE obm_id=%d", $this->data_table, quote($conid), (int)$row_id);
        $res = pg_query($ID,$cmd);
        if (pg_last_error($ID)) {
            $errorID = uniqid();
            log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
            throw new \Exception("Error. ErrorID#$errorID#");
        }
    }
    
    private function getObmIDs($conids) {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT obm_id, obm_files_id FROM %s WHERE obm_files_id IN (%s);", $this->data_table, implode(',', $this->quArr($conids, 'safe')));
        $res = pg_query($ID,$cmd);
        if (pg_last_error($ID)) {
            $errorID = uniqid();
            log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
            throw new \Exception("Error. ErrorID#$errorID#");
        }
        if (!pg_num_rows($res)) {
            throw new \Exception("No rows found", 1);
        }
        $results = [];
        while ($row = pg_fetch_assoc($res)) {
            $results[$row['obm_files_id']] = (int)$row['obm_id'];
        }
        return $results;
    }
}

/**
 * 
 */
class FileConnection extends Entity {
    protected $db = 'gisadmin';
    protected $tableSchema = 'system';
    protected $tableName = 'file_connect';
    protected $pk_col = null;
    protected $unique_cols = ['file_id', 'conid'];
    
    public $file_id;
    public $conid;
    public $temporal;
    public $sessionid;
    public $rownum;
    
    function __construct() {
        parent::__construct();
    }
    
    public function initialize($options) {
        if (!$this->conid) {
            $this->conid = uniqid($_SESSION['Tid']."$",true);
        }
    }
    
    /** 
     * clears the data_table obm_files_id if no more connections left
     **/
    public function after_delete() {
        global $ID;
        
        // I think data_table should be in the file_connect table
        $file = File::first(['id', $this->file_id]);
        $connections = FileConnection::find(['conid', $this->conid]);
        
        if (!count($connections)) {
            $cmd = sprintf("UPDATE %s SET obm_files_id = NULL WHERE obm_files_id=%s", $file->data_table, quote($this->conid));
            $res = pg_query($ID,$cmd);
            if (pg_last_error($ID)) {
                $errorID = uniqid();
                log_action("ErrorID#$errorID# ".pg_last_error($ID),__FILE__,__LINE__);
                throw new \Exception("Error. ErrorID#$errorID#");
            }
            
        }
    }
}

 ?>
