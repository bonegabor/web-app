<?php
    include_once(getenv('OB_LIB_DIR').'interface.php');

    $source = 'syslog';
    $filter = '';
    $search = '';
    $output = '';
    if (isset($_GET['source'])) $source = $_GET['source'];
    if (isset($_GET['search'])) $search = $_GET['search'];
    if (isset($_GET['filter'])) $filter = preg_replace('/[^a-z0-9-_: ]/i',"",base64_decode($_GET['filter']));
    $filter = substr($filter,0,24);

    if (!isset($_GET['update'])) {
        if (isset($_GET['showserverlogs'])) {
            $output .= "<a href='$protocol://".URL."/profile/{$_SESSION['Tcrypt']}' class='pure-button button-href' style='float:right;margin-top:-10px'><i class='fa fa-lg fa-cogs'></i></a>";
        }
        $output .= "<div class='shortdesc'><!--Description of logs--></div>";

        $output .= str_choose_log.": <select id='syslog_source'>".selected_option(array('system log::syslog','mapserver log::mapserv','Job events::jobevents','Job errors::joberrors'),$source)."</select> <button id='syslog_refresh'><i class='fa fa-refresh'></i></button>";
        $output .= " filter: <input id='syslog_filter' value='$filter'><br>";
    }

    if (!has_access($_GET['options']) or !isset($_SESSION['Tid'])) {
        echo "<h1>Sorry, you cannot read the logs!</h1>";
        return;
    }
    
    $log = server_logs($source, $search, $filter);

    if (isset($_GET['update']))
        echo $log;
    else {
        echo "<div class='infotitle'>".wikilink('admin_pages.html#server-logs',t('str_documentation'))."</div>";
        echo $output."<div id='logconsole'>$log</div>";
    }

?>
