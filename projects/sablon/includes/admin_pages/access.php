<?php

    echo "<div class='infotitle'>".wikilink('admin_pages.html#data-access',t('str_documentation'))."</div>";

    $acc = array(0=>'everybody',1=>'logined users',2=>'specified group members');
    $mod = array(0=>'everybody',1=>'logined_users',2=>'specified group members');
    $levels = array('0'=>0,'1'=>1,'2'=>2,'public'=>0,'login'=>1,'group'=>2);

    //access levels
    echo "<div>".str_data_read_acc.": ";
    $n = array();
    foreach ($acc as $k=>$v) {
        if ($levels[ACC_LEVEL] == $k)
            $n[] = "<b>$v</b>";
        else
            $n[] = "$v";
    }
    echo "[".implode('] &nbsp; [',$n)."].</div>";


    //mod leveles
    echo "<div>".str_data_mod_acc.": ";
    $n = array();
    foreach ($mod as $k=>$v) {
        if ($levels[MOD_LEVEL] == $k)
            $n[] = "<b>$v</b>";
        else
            $n[] = "$v";
    }
    echo "[".implode('] &nbsp; [',$n)."].</div>";

    echo "<br><i>You can change these options in the local_vars.php.inc file.</i><br><br>";

    echo "<div class='infotitle'>Access rules per table and per line</div>";

    $cmd = "SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '".PROJECTTABLE."_rules') AS exists";
    $result = pg_query($ID,$cmd);
    $rules_table = pg_fetch_assoc($result);


    $schema = 'public';
    $cmd = sprintf("SELECT f_project_table,f_geom_column, f_species_column, f_quantity_column, f_id_column,
        coalesce(f_x_column,'') as f_x_column,coalesce(f_y_column,'') as f_y_column,
        f_srid, f_order_columns,f_use_rules,
        ARRAY_TO_STRING(f_cite_person_columns,',') AS f_cite_person_columns,
        ARRAY_TO_STRING(f_date_columns,',') AS f_date_columns,
        ARRAY_TO_STRING(f_file_id_columns,',') AS f_file_id_columns,
        ARRAY_TO_STRING(f_alter_speciesname_columns,',') AS f_alter_speciesname_columns
    FROM header_names
    WHERE f_project_schema=%s AND f_project_name='%s'",quote($schema),PROJECTTABLE);
    $result = pg_query($BID,$cmd);

    $acclevel = ACC_LEVEL;
    $modlevel = MOD_LEVEL;

    while( $row = pg_fetch_assoc($result)) {

        echo sprintf("<div style='font-weight:bold;padding-top:8px'>%s</div>",$row['f_project_table']);
        $restrict_prestate = $row['f_use_rules'];
        $restriction = "<span title='If you need it, go to `database columns` menu and add `use_rules:1` comment to `obm_id` column.'>not set</span>";
        $restriction_enabled = false;

        $acm = $modules->is_enabled('allowed_columns',$row['f_project_table']);

        if ($rules_table['exists']=='t') {
            if ("$acclevel" == "2" or "$acclevel" == "group") {
                if ($restrict_prestate=='0' or $restrict_prestate=='false' or $restrict_prestate=='-1' or $restrict_prestate===false or $restrict_prestate=='f'){
                    $restriction = "<span title='If you need it, go to `database columns` menu to enable it.'>disabled</span>";
                } elseif ($restrict_prestate!='' or $restrict_prestate!=NULL) {
                    $restriction = "<span title=\"If you don't need it, go to `database columns` menu and set `use_rules:0` comment on `obm_id` column.\">enabled</span>";
                    $restriction_enabled = true;
                }
            } elseif ("$acclevel" == "1" or "$acclevel" == "login") {
                if (!$restrict_prestate){
                    $restriction = "enabled but not applicable due to public access level";
                } else {
                    $restriction = "enabled";
                }
            } elseif ("$acclevel" == "0" or "$acclevel" == "public") {
                if ($restrict_prestate){
                    $restriction = "enabled but not applicable due to public access level";
                }
            }
        } else {
            if ("$acclevel" == "2" or "$acclevel" == "group") {
                echo "<div style='warning'>The global access level is `group` but ".PROJECTTABLE."_rules table does not exists!</div>";
            }
        }
        echo sprintf("<div class=''>Restriction by rules (usage of public.%s_rules) is <span style='border-bottom:1px dotted gray;font-family:monospace'>%s</span> in biomaps.header_names for {$row['f_project_table']}</div>",PROJECTTABLE,$restriction);

        if ($restriction_enabled) {
            $cmd = sprintf("SELECT tgenabled FROM pg_trigger WHERE tgname='rules_%s'",$row['f_project_table']);
            $res2 = pg_query($ID,$cmd);
            if (pg_num_rows($res2)) {
                $row2 = pg_fetch_assoc($res2);
                if ($row2['tgenabled'] == 'D') {
                    echo sprintf("<div class='warning'> - BUT rules table can't operate due to <i>rules_%s</i> trigger is not enabled!</div>",$row['f_project_table']);
                }
            } else {
                echo sprintf("<div class='warning'> - BUT rules table can't operate due to <i>rules_%s</i> trigger is not enabled!</div>",$row['f_project_table']);
            }
        }

        if (("$acclevel" == "1" or "$acclevel" == "login" or "$acclevel" == "2" or "$acclevel" == "group") and !$acm) {

            echo "<div style='warning'><i>Allowed columns</i> module is not enabled for ".$row['f_project_table'].",
                    therfore all columns disabled for non logined users, and all accessible for logined users!</div>";

        }
        elseif (("$acclevel" == "1" or "$acclevel" == "login" or "$acclevel" == "2" or "$acclevel" == "group") and $acm) {

            $params = $modules->get_params('allowed_columns',$row['f_project_table']);
            $li = "";
            foreach ($params as $key=>$val) {
                if ($key == 'for_sensitive_data')
                    $li .= "<li>for sensitive data: ".implode($val,",")."</li>";
                elseif ($key == 'for_no-geom_data')
                    $li .= "<li>for no-geom data: ".implode($val,",")."</li>";
                elseif ($key == 'for_general') {
                    if ("$acclevel" == "2" or "$acclevel" == "group")
                        $li .= "<li>in general: ".implode($val,",")."</li>";
                    else
                        $li .= "<li class='warning'>There are general column restrictions but does not applicable. Group level access setting is needed in local_vars.php.inc!</li>";
                } else {
                    $li .= "<li class='warning'>Not valid column settings in module parameter! (Either of the following rules should be defined before the list of the columns:
                            <br>for_sensitive_data, for_no-geom_data, for_general</li>";
                }
            }

            echo "<div>Accessible columns<ul>$li</ul></div>";

        } elseif ($acm) {
            echo "<div class='warning'><i>Allowed columns</i> module is enabled but not applicable due to public access level.</div>";

        }
    }


    // other local_vars variables??

?>
