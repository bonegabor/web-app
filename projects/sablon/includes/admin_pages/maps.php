<?php

    echo "<div class='infotitle'>".wikilink('admin_pages.html#map-settings',t('str_documentation'))."</div>";

    $cmd = "SELECT f_project_schema, f_project_table, f_geom_column, f_srid FROM header_names WHERE f_project_name='".PROJECTTABLE."' AND f_project_table LIKE '".PROJECTTABLE."%' AND f_geom_column IS NOT NULL";
    $res = pg_query($BID,$cmd);
    $tables = array();
    $geom_columns = array();
    $srids = array();
    $schemas = array();
    while ($row = pg_fetch_assoc($res)) {
        $tables[] = $row['f_project_table'];
        $geom_columns[] = $row['f_geom_column'];
        $srids[] = $row['f_srid'];
        $schemas[] = $row['f_project_schema'];
    }

    echo "<h3>".t(str_proj_geom)."</h3>";
    echo "<div>";
    #if ($_SESSION['st_col']['GEOM_C'] == '') {
    #    echo "Geometry column is not set in project tables.";
    #}
    for ($i=0;$i<count($tables);$i++) {
        if ($geom_columns[$i] != '') {
            $cmd = "SELECT Find_SRID('{$schemas[$i]}', '{$tables[$i]}', '{$geom_columns[$i]}')";
            $res = pg_query($ID,$cmd);

            if ($res) {
                $row = pg_fetch_assoc($res);
                echo "<div style='font-size:125%'> <a href='#tab_basic' class='profilelink' data-url='includes/project_admin.php?options=dbcols'>{$tables[$i]}</a></div>";
                echo "<b>Spatial reference system</b>:
                    <div style='padding-left:2em'>".str_data_srid." <a href='https://spatialreference.org/ref/epsg/{$row['find_srid']}/'>EPSG:{$row['find_srid']}</a> </div>
                    <div style='padding-left:2em'>".str_project_srid.": <a href='https://spatialreference.org/ref/epsg/{$srids[$i]}/'>EPSG:".$srids[$i].'</a></div>';

                $res = pg_query($ID,"SELECT ST_Extent({$geom_columns[$i]}) AS extent FROM \"{$schemas[$i]}\".\"{$tables[$i]}\"");
                $row = pg_fetch_assoc($res);
                $e = preg_replace("/[^0-9., ]/",'',$row['extent']);
                $e = preg_replace("/,/"," ",$e);
                echo str_map_extent_text."<br>";
                echo '<b>'.str_data_extent." (lower left and upper right corners):</b><div style='padding-left:2em'>$e</div>";
            } else {
                echo "<div style='font-size:125%'> <a href='#tab_basic' class='profilelink' data-url='includes/project_admin.php?options=dbcols'>{$tables[$i]}</a></div>";
                echo "<b>Spatial reference system</b>:
                    <div style='padding-left:2em'>".str_data_srid.": ".str_no_srid." </div>
                    <div style='padding-left:2em'>".str_project_srid.": <a href='https://spatialreference.org/ref/epsg/{$srids[$i]}/'>EPSG:".$srids[$i].'</a></div>';

                $res = pg_query($ID,"SELECT ST_Extent({$geom_columns[$i]}) AS extent FROM ".$tables[$i]);
                $row = pg_fetch_assoc($res);
                $e = preg_replace("/[^0-9., ]/",'',$row['extent']);
                $e = preg_replace("/,/"," ",$e);
                echo str_map_extent_text."<br>";
                echo '<b>'.str_data_extent." (lower left and upper right corners):</b><div style='padding-left:2em'>$e</div>";
            }
        }
    }
    echo "</div><br><br>";

    echo "<h3>Mapserver ".str_definitions."</h3>";

    echo "<div class='infotitle'>".str_mapserver_help_text."</div><br>";

    extract(parse_layers_from_msmap());

    // echo mapserver layers
    echo '<h5>'.t(str_mapserver_layer)."</h5>The following layers are detected in this mapfile:";
    echo "<form id='mapfile-form'><table class='pure-form' style='margin-left:30px'><tr><td>layer</td><td>geometry type</td></tr>";
    for ($i=0;$i<count($map_layers);$i++) {
        $gp = $geom_types[$i];
        if (strtoupper(trim($gp)) == 'LINE') $gp = 'LINESTRING';
        $cmd = sprintf("SELECT 1 FROM project_mapserver_layers WHERE mapserv_layer_name=%s AND project_table=%s AND geometry_type=%s",quote($map_layers[$i]),quote(PROJECTTABLE),quote(strtoupper(trim($gp))));
        $res = pg_query($BID,$cmd);
        $bgcolor = '';
        if (!pg_num_rows($res)) {
            $bgcolor = 'background-color:red';
        }

        echo "<tr><td><input name='mp[]' id='mp_$i' class='mapserver_layer' value='{$map_layers[$i]}' readonly style='$bgcolor'></td><td><input class='mapserver_layer_geometry' style='$bgcolor' name=mg[] id='mg_$i' value='{$geom_types[$i]}'></td></tr>";
    }
    echo '</table></form><br>';


    $private_map = file(getenv('PROJECT_DIR').'private/private.map');
    echo '<h5>OpenBioMaps '.str_private.' mapfile</h5>';
    if(!is_writable(getenv('PROJECT_DIR').'private/private.map')) {
        echo '<span class="err">'.str_file_not_writable.'</span>';
    } else {
        echo "<textarea style='width:1100px;height:600px;font-family:monospace' id='private_map'>";
        $template_vars = array();
        foreach ($private_map as $line) {
            $m = array();
            if (preg_match_all('/(@@\w+@@)/',$line,$m)) {
                foreach($m[1] as $q) $template_vars[]=$q;
            }
            echo $line;
        }
        echo "</textarea><br>";
        if (count(array_unique($template_vars))) {
            echo "<br><div style='color:red'>".str_replacvar.":<br>";
            foreach(array_unique($template_vars) as $var){
                echo $var."<br>";
            }
            echo "</div>";
        }

        echo "<br><button id='private' class='button-success button-large pure-button map_update'><i id='priv-icon' class='fa fa-cog'></i> ".str_save."</button> &nbsp; ";
        echo "<button id='getmaptest-private' class='button-secondary button-large pure-button maptest'><i class='fa fa-eye'></i> ".str_view."</button><br>";
    }
    /* AJAX check
     * */
    echo "Map check result:<div class='infobox' id='maptest-private'></div>";

    $public_map = file(getenv('PROJECT_DIR').'public/public.map');
    echo '<br><h5>'.t(str_public).' mapfile</h5>';
    echo '<div style="color:red;max-width:700px">This mapfile is not used by OpenBioMaps! If it is set properly, these layers will be accessible through OpenBioMaps proxy service without any restrictions to any mapserver client applications like QGIS or web map clients.</div>';
    if(!is_writable(getenv('PROJECT_DIR').'public/public.map')) {
        echo '<span class="err">'.str_file_not_writable.'</span>';
    } else {
        echo "<textarea style='width:1100px;height:300px;font-family:monospace;background-color:#efeafa' id='public_map'>";
        foreach ($public_map as $line) {
            echo $line;
        }
        echo "</textarea><br><button id='public' class='button-success button-large pure-button map_update'><i id='pub-icon' class='fa fa-cog'></i> ".str_save."</button> &nbsp; ";
        // AJAX check
        echo "<button id='getmaptest-public' class='button-secondary button-large pure-button maptest'><i class='fa fa-eye'></i> ".str_view."</button>";
    }
    echo "<br>".str_map_check_result.":<div class='infobox' id='maptest-public'></div>";


    /* OpenLayers definitions */
    echo "<br><br><h3>Online map ".str_settings."</h3>";
    echo "<div class='infotitle'>".str_openlayers_help_text."</div><br>";


    echo "<h5>Online map centre/zoom</h5>";
    $cmd = "SELECT ST_AsText(map_center) as mapcentre,map_zoom FROM project_variables WHERE project_table='".PROJECTTABLE."'";
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_assoc($res);
    echo "<div class='pure-form'><input id='map-centre' class='pure-u-1-4' value='".$row['mapcentre']."' placeholder='WKT Point'><input id='map-zoom' class='pure-u-1-4' value='{$row['map_zoom']}' placeholder='Zoom level: 1-12'>
            <br><br>\n
            <button class='pure-button button-warning button-large' id='map-centre-set'>".t(str_set)."</button></div><br>";


    echo "<h5>Layers</h5>";
    echo "<div class='infotitle'>".str_layers_help."</div>";

    $map = getenv('PROJECT_DIR').'private/private.map';
    ob_start();
    passthru("grep LAYER $map -A 1|grep -i NAME");
    $var = ob_get_contents();
    ob_end_clean();
    $var = preg_replace('/NAME/i','<li>',$var);
    $var = preg_replace('/\'/i','',$var);
    $var = preg_replace('/"/i','',$var);

    $map_layers = array_filter(preg_split('/\n|\r/',$var));
    $map_layers = preg_replace('/\s+<li>\s+/','',$map_layers);

    $layer = preg_replace('/\n|\r/','',$var);
    $layers = explode('<li>',$layer);
    $layers = array_map('trim',array_filter($layers, function($a) { return trim($a) !== ""; }));

    $tbl = new createTable();
    $tbl->def(['tid'=>'mytable','tclass'=>'admintable pure-form']);

    $cmd = "SELECT layer_cname,enabled FROM project_queries WHERE project_table='".PROJECTTABLE."'";
    $layers_cname = pg_query($BID,$cmd);
    $cname_options = array('');
    $x = array();
    while ($row = pg_fetch_assoc($layers_cname)) {
        $m = array();
        if (preg_match('/^layer_data_(.+)/',$row['layer_cname'],$m)) {

            $cname_options[] = "$m[1]::".$row['layer_cname'];
        }
        if ($row['enabled'] == 'f')
            $lcx[] = $row['layer_cname'];
    }

    $cmd = sprintf("SELECT l.id,l.project_table,layer_name,layer_def,tipus,url,map,name,l.enabled,layer_order,ms_layer,singletile,legend,
	layer_query,q.enabled as qena,layer_type,rst,layer_cname,main_table FROM project_layers l
                    LEFT JOIN project_queries q ON layer_cname=layer_name AND l.project_table=q.project_table
                    WHERE l.project_table='%s' ORDER BY l.enabled,layer_name",PROJECTTABLE);
    $layers_res = pg_query($BID,$cmd);
    while ($layers_row = pg_fetch_assoc($layers_res)) {

        $r = array();

        if(in_array($layers_row['layer_name'],$layers)) {
            $k = array_search($layers_row['layer_name'],$layers);
            unset($layers[$k]);
        }
        // Mapserver type
        array_push($r,"<select id='opltype-{$layers_row['id']}' style='width:100px' name='tipus'>".
            selected_option(array('LocalWMS','ImageWMS','TileWMS','WFS','TomTom','Bing','XYZ','OSM'),$layers_row['tipus'])."</select>");


        // SQL Layer name
        array_push($r,sprintf("<select id='oplnam-{$layers_row['id']}'>%s</select>",
            selected_option( $cname_options, $layers_row['layer_name'], $lcx)));

        // SQL State queries
        if ($layers_row['qena']=='t') {
                array_push($r,"<span style='background-color:rgb(159, 198, 81);display:block'>OK!</span>");
        }
        elseif ($layers_row['qena']=='f') {
                array_push($r,"<span style='background-color:rgb(223, 117, 20);display:block'>Disabled!</span>");
        }
        else {
            if ($layers_row['tipus'] == 'WMS' or $layers_row['tipus'] == 'LocalWMS')
                array_push($r,sprintf("<span style='background-color:rgb(223, 117, 20);display:block'>NO QUERY!</span>"));
            else
                array_push($r,'');
        }
        // Mapserver layer
        array_push($r,sprintf("<input id='oplmslayer-{$layers_row['id']}' value='%s'>",$layers_row['ms_layer']));

        // OpenLayers definition
        if (preg_match("/^{/",$layers_row['layer_def'])) {
            $ldef = json_encode(json_decode($layers_row['layer_def']),JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        } else {

            $ldef = preg_split("/,/",$layers_row['layer_def']);
            $u = array();
            foreach ($ldef as $x) {
                list($k,$v) = preg_split("/:/",$x);
                $u[trim($k)] = str_replace("'", "", trim($v));
            }
            $ldef = json_encode($u, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        }
        array_push($r,sprintf("<textarea class='my_codemirror' id='opldef-{$layers_row['id']}' style='min-width:20em;resize:both'>%s</textarea>",$ldef));


        array_push($r,sprintf("<input id='oplurl-{$layers_row['id']}' value='%s'>",$layers_row['url']));
        array_push($r,sprintf("<input id='oplmap-{$layers_row['id']}' size=8 value='%s'>",$layers_row['map']));
        array_push($r,sprintf("<input id='opldesc-{$layers_row['id']}' value='%s'>",$layers_row['name']));
        array_push($r,sprintf("<input id='oplorder-{$layers_row['id']}' size=2 value='%s'>",$layers_row['layer_order']));
        array_push($r,sprintf("<select id='oplena-{$layers_row['id']}' name='enabled'>".selected_option(array(str_true.'::t',str_false.'::f'),$layers_row['enabled'])."</select>"));
        array_push($r,sprintf("<select id='oplsingletile-{$layers_row['id']}' name='singletile'>".selected_option(array(str_true.'::t',str_false.'::f'),$layers_row['singletile'])."</select>"));
        array_push($r,sprintf("<select id='opllegend-{$layers_row['id']}' name='legend'>".selected_option(array(str_true.'::t',str_false.'::f'),$layers_row['legend'])."</select>"));
        array_push($r,sprintf("<button name='copld' class='button-warning button-xlarge pure-button opl_update' id='copld_".$layers_row['id']."'>".str_modifyit."</button>"));

        $tbl->addRows($r);
    }

    $lk = '';
    $def_def = '';
    $si = '';
    $def = '';
    $de = '';
    if (count($layers)) {
        $lk = trim(array_pop($layers));
        $def_def = json_encode(json_decode(preg_replace("/map-file-layer-name/",
                        $lk,
                        '{"layers":"map-file-layer-name", "isBaseLayer":"false", "visibility":"true", "opacity":"1.0", "format":"image/png", "transparent":"true","numZoomLevels":"20"}')),JSON_PRETTY_PRINT);
        $de = 'Data layer';
        if (preg_match('/_query/',$lk)) {
            $si = 'selected';
            $de = 'Query layer';
        }
        $def = 'default';
    }
    array_unshift($map_layers,'');

    // new line at the end of the table
    $tbl->addRows(array(
        "<select style='width:100px' id='opltype-new'>
            <option>LocalWMS</option>
            <option>ImageWMS</option>
            <option>TileWMS</option>
            <option>WFS</option>
            <option>TomTom</option>
            <option>Bing</option>
            <option>XYZ</option>
            <option>OSM</option>
        </select>",
        "<select  id='oplnam-new'>".selected_option($cname_options,' ',$lcx)."</select>",
        "",
        "<select id='oplmslayer-new' style='width:100px' name='tipus'>".selected_option($map_layers,'')."</select>",
        "<textarea style='min-width:20em' class='my_codemirror' id='opldef-new'>$def_def</textarea>",
        "<input id='oplurl-new' placeholder='proxy' value='$def'>",
        "<input id='oplmap-new' size=8 placeholder='default' value='$def'>",
        "<input id='opldesc-new' value='$de'>","<input id='oplorder-new' size=2>",
        "<select id='oplena-new'><option>TRUE</option><option>FALSE</option></select>",
        "<select id='oplsingletile-new'><option>TRUE</option><option>FALSE</option></select>",
        "<select id='opllegend-new'><option>TRUE</option><option>FALSE</option></select>",
        "<input type='button' name='aopld' class='button-success button-xlarge pure-button opl_update' id='aopld_new' value='".str_add."'>"));
    $tbl->addHeader(
        array(
            "Mapserver ".t(str_type),
            t(str_sql_reference),
            "SQL ".str_state,
            t(str_mapserver_layer),
            "OpenLayers ".str_layer_def,
            "Mapserver ".t(str_url),
            "MAP file",
            "OpenLayers ".str_name,t(str_order),
            t(str_enabled),
            "Singletile",
            "Legend",
            t(str_actions)
        )
    );
    echo $tbl->printOut();
    echo "</div>";


?>
