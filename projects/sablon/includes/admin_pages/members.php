<?php

    echo "<div class='infotitle'>".wikilink('admin_pages.html#members',t('str_documentation'))."</div>";

    $table = new createTable();
    $table->def(['tid'=>'tagok','tclass'=>'resultstable pure-form']);

    $eurl = sprintf($protocol.'://'.URL.'/profile/COL-8/');
    $table->tformat(['format_col'=>'1','format_string'=>'<a href=\''.$eurl.'\'>COL-1</a>']);

    // query all members
    $cmd = "SELECT id,username,institute,address,email,round(validation,2) as validation,user_status,'groups' AS groups,\"user\"
            FROM users u
            LEFT JOIN project_users pu ON (pu.user_id=u.id)
            WHERE pu.project_table='".PROJECTTABLE."'
            ORDER BY familyname,givenname,username";

    $res = pg_query($BID,$cmd);
    while ( $user_row = pg_fetch_assoc($res) ) {

        # groups
        #$options = array();
        #foreach ($csor as $cs) {
        #    if ($cs['role_id']==$user_row['id']) continue;
        #    $options[] = "{$cs['description']}::{$cs['role_id']}";
        #}

        $role_id = -1;
        $cmd = sprintf("SELECT role_id FROM project_roles WHERE user_id=%s AND project_table='%s'",quote($user_row['id']),PROJECTTABLE);
        $role_res = pg_query($BID,$cmd);
        if (pg_num_rows($role_res)) {
            $role_row = pg_fetch_assoc($role_res);
            $role_id = $role_row['role_id'];
        }

        $options = array();
        $cmd = sprintf("SELECT DISTINCT description FROM project_roles WHERE %d=ANY(container) AND project_table='%s'",$role_id,PROJECTTABLE);
        $rres = pg_query($BID,$cmd);
        while ($row = pg_fetch_assoc($rres))
            $options[] = $row['description'];
        //$options = array_unique($options);

        $user_row['groups'] = implode("<br>",$options);

        $status = array('0'=>'banned','1'=>'normal','2'=>'master','banned'=>'banned','normal'=>'normal','master'=>'master');

        $user_row['user_status'] = $status[$user_row['user_status']];

        $options = selected_option(array(sprintf('%s::normal',str_normal),sprintf('%s::master::%s',str_master,str_maintainer_access_explanation),sprintf('%s::banned::%s',str_parked,str_zero_access_explanation)),$user_row['user_status']);
        $user_row['user_status'] = "<select name='edbox' id='sdc-{$user_row['id']}'>$options</select>";


        if ($user_row['id']==$_SESSION['Tid']) $col = "button-warning";
        else $col = "button-success";
        array_push($user_row,"<button class='$col button-large pure-button mrb_send' id='ed-{$user_row['id']}'><i class='fa fa-cog'></i> ".str_save."</button>");

        $table->addRows($user_row);
    }

    $table->addHeader(array(-1,str_nickname,str_institute,str_paddress,str_email,str_validation,str_status,str_group,-1,str_save));
    echo $table->printOut();

?>
