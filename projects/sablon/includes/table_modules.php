<?php
/* Do not add profileItem method for table mosules, but
 * add module to project_modules as well!
 *
 * */


class tableModules {

    private $module;
    private $default_module;
    private $private_module;
    private $private_path;
    public $available_methods;
    public $main_table;

    function __construct($module) {

        $this->default_module = getenv('OB_LIB_DIR').'modules/' . $module . '.php';
        $this->private_module = getenv('PROJECT_DIR').'local/includes/modules' . $module . '.php';
        $this->private_path = getenv('PROJECT_DIR').'local/includes/modules/';

        $wp = '';
        if (file_exists($this->private_module)) {
            $wp = 'private';
            include_once($this->private_module);
        }
        elseif (file_exists($this->default_module)) {
            $wp = 'default';
            include_once($this->default_module);
        }
        else {
            log_action("$module module not found",__FILE__,__LINE__);
            return;
        }

        if (class_exists($module)) {
            $this->available_methods = get_class_methods($module);
            $this->module = $module;
        }
        else {
            log_action("class $module does not exists on $wp path!",__FILE__,__LINE__);
            return;
        }
    }
    public function has_method($method) {
        return (!empty($this->available_methods) && in_array($method,$this->available_methods));
    }

    public function init() {
        $module_name = $this->module;
        $module_name::init();
        // $this->module::init(); # php 7
    }

    function results_specieslist($action,$params,$request=array()) {

        $mf = new results_specieslist($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function results_summary($action,$params,$request=array()) {

        $mf = new results_summary($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        return $mf->retval;
    }

    function results_asHtmlTable($action,$params,$request=array()) {

        $mf = new results_asHtmlTable($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        return $mf->retval;
    }

    function results_asTable($action,$params,$request=array()) {

        $mf = new results_asTable($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        return $mf->retval;
    }

    function results_asGPX($action,$params,$request=array()) {

        $mf = new results_asGPX($action, $params, $request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        return $mf->retval;
    }

    function results_asCSV($action,$params,$request=array()) {

        $mf = new results_asCSV($action, $params, $request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function results_asJSON($action,$params,$request=array()) {

        $mf = new results_asJSON($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function results_asSHP($action,$params,$request=array()) {

        $mf = new results_asSHP($action, $params, $request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function results_asKML($action,$params,$request=array()) {

        $mf = new results_asKML($action, $params, $request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function results_buttons ($action,$params,$request=array()) {

        $mf = new results_buttons($action, $params, $request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function box_load_selection($action,$params,$request=array()) {

        $mf = new box_load_selection($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function box_load_coord($action,$params,$request=array()) {

        $mf = new box_load_coord($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function box_load_last_data($action,$params,$request=array()) {

        $mf = new box_load_last_data($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function box_custom($action,$params,$request=array()) {

        $mf = new box_custom('set_path',$this->private_path);
        if ($action == "print_box")
            $mf->print_box($params,$request);
        elseif ($action == "print_js")
            $mf->print_js($params,$request);

        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function text_filter($action,$params,$request=array()) {
        if ($action == 'assemble_where') {
            $mf = new query_builder();
            return $mf->assemble_where($params,$request[0],$request[1],$request[2]);
        }
        else {
            $mf = new text_filter($action,$params,$request);
            if ($mf->error) {
                $mf->displayError();
                return $mf->retval;
            }
            else
                return $mf->retval;
        }
    }

    function transform_data ($action,$params,$request=array()) {

        $mf = new transform_data($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
        }
        return $mf->retval;
    }

    function results_asStable($action,$params,$request=array()) {

        $mf = new results_asStable($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function results_asList($action,$params,$request=array()) {
        /*if ($action=='print_roll_list') {
            $mf = new results_asList($action,$params,$request);
            if ($mf->error) {
                $mf->displayError();
                return $mf->retval;
            }
            return $mf->retval;
        }
        elseif ($action=='print_slide') {
            $mf = new slide();
            return $mf->print_slide($params,$request[0],$request[1],$request[2]);
        }*/
        $mf = new results_asList($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;

    }

    function allowed_columns($action,$params,$request=array()) {

        $mf = new allowed_columns($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function bold_yellow($action,$params,$request=array()) {

        $mf = new bold_yellow($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;

    }

    function additional_columns($action,$params,$request=array()) {

        $mf = new additional_columns($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function join_tables($action,$params,$request=array()) {

        $mf = new join_tables($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function snap_to_grid($action,$params,$request=array()) {

        $mf = new snap_to_grid($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function restricted_data ($action,$params,$request=array()) {

        $mf = new restricted_data($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function identify_point ($action,$params,$request=array()) {

        $mf = new identify_point($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function custom_notify ($action,$params,$request=array()) {

        $mf = new notify($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function custom_data_check ($action,$params,$request=array()) {

        $mf = new custom_data_check($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function grid_view($action,$params,$request=array()) {

        $mf = new grid_view($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function text_filter2($action,$params,$request=array()) {
        if ($action == 'assemble_where') {
            $tf = new text_filter2("get_params", $params, $request);
            
            $mf = new query_builder();
            return $mf->assemble_where($tf->retval,$request[0],$request[1],$request[2]);
        }
        else {
            $mf = new text_filter2($action,$params,$request);
            if ($mf->error) {
                $mf->displayError();
                return $mf->retval;
            }
            else
                return $mf->retval;
        }
    }

    function read_table ($action,$params,$request=array()) {

        $mf = new read_table($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function massive_edit ($action,$params,$request=array()) {

        $mf = new massive_edit($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }

    function list_manager ($action,$params,$request=array()) {

        $mf = new list_manager($action,$params,$request, $this->main_table);
        #$mf = new list_manager($action,$params,$request);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }
    
    function ioc_bird_list ($action,$params,$request=array()) {
        $mf = new ioc_bird_list($action,$params,$request);

        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }
    
    function results_asPDF ($action,$params,$request=array()) {

        $mf = new results_asPDF($action,$params,$request, $this->main_table);
        if ($mf->error) {
            $mf->displayError();
            return $mf->retval;
        }
        else
            return $mf->retval;
    }
}
?>
