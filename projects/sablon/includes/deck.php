<?php
/*  The deck of the boat
 *
 *  It is an entry point for settings, aka profile and administrative pages
 *
 *
 * */


/* maintenance scheduler code */

$schedule_day = "2011/12/13";
$from = "23:00";
$length = "2 hours";
$address = "127.0.0.1";
$local_mconf = 0;
$system_mconf = 0;
$mconf = "../maintenance.conf";
$maintenance_message = "";

if (file_exists(getenv('PROJECT_DIR').'maintenance.conf'))
    $local_mconf = filemtime(getenv('PROJECT_DIR').'maintenance.conf');

if (file_exists(getenv('PROJECT_DIR').'../maintenance.conf'))
    $system_mconf = filemtime(getenv('PROJECT_DIR').'../maintenance.conf');

if ($local_mconf>$system_mconf)
    $mconf = getenv('PROJECT_DIR')."maintenance.conf";

if (file_exists($mconf)) {
    $contents = file($mconf); 
    $capture = array("day"=>0,"from"=>0,"length"=>0,"address"=>"0");

    foreach ($contents as $line) {

        if ($capture['day'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $schedule_day = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['from'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $from = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['length'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $length = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }
        elseif ($capture['address'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
            $address = trim($line);
            $capture = array_fill_keys(array_keys($capture), 0);
        }

        if (preg_match('/^## start day/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['day'] = 1;
        }
        elseif (preg_match('/^## from/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['from'] = 1;
        }
        elseif (preg_match('/^## length/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['length'] = 1;
        }
        elseif (preg_match('/^## maintenance address/',$line)) {
            $capture = array_fill_keys(array_keys($capture), 0);
            $capture['address'] = 1;
        }
    }
    $date = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
    $date_start = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
    $date_start_format = $date->format('Y-m-d H:i');
    $length = preg_replace("/hour.?/","H",$length);
    $length = preg_replace("/minute.?/","M",$length);
    $date_end = $date->add(new DateInterval('PT'.str_replace(" ","",$length)));
    $date_end_format = $date_end->format('Y-m-d H:i');

    $date_now = new DateTime();
    $interval = $date_now->diff($date_start);
    $diff = $interval->format('%r%d %h %i');

    #var_dump($interval);
    #echo 

    $maintenance_message_span_style = '';
    if ($_SERVER['REMOTE_ADDR']!=$address) {
        if ($interval->invert==0 and $interval->y==0 and $interval->m==0 and  $interval->d==0 and $interval->h<24) {
            $maintenance_message = "Database maintenance will be scheduled between $date_start_format and $date_end_format!";
            $maintenance_message_span_style = 'padding:10px;display:inline-block;';
        }
        elseif ($interval->invert==1 and ($date_end > $date_now and $date_start < $date_now)) {
            $maintenance_message = "Database maintenance!";
            $maintenance_message_span_style = 'padding:10px;display:inline-block;';
            include(getenv('PROJECT_DIR')."maintenance.php");
        }
    } else {
        if ($interval->invert==0 and $interval->y==0 and $interval->m==0 and  $interval->d==0 and $interval->h<24) {
            $maintenance_message = "Database maintenance will be scheduled between $date_start_format and $date_end_format!";
            $maintenance_message_span_style = 'padding:10px;display:inline-block;background-color:pink;width:100%';
        }
        elseif ($interval->invert==1 and ($date_end > $date_now and $date_start < $date_now)) {
            $maintenance_message = "Database maintenance!";
            $maintenance_message_span_style = 'padding:10px;display:inline-block;background-color:pink;width:100%';
        }
    }
}
// **************************************************************************************************** \\

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

if (!isset($_COOKIE['LoadCookie'])) {
    $domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
    setcookie("LoadCookie", 1, time()+3600,'/',$domain,false,true); 
}
#Header("Cache-Control: no-cache, must-revalidate");
#$offset = 60 * 60 * 24 * 3;
#$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
#Header($ExpStr);
if (session_status() === PHP_SESSION_NONE) {
    session_cache_limiter('nocache');
    session_cache_expire(0);
    session_start();
}
require(getenv('OB_LIB_DIR').'db_funcs.php');

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS databases.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host)) 
    die("Unsuccessful connect to GIS database with biomaps.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

// scheduled or topical updates
#require_once(getenv('OB_LIB_DIR').'db_updates.php');
#new db_updates('language files to db'); # 2018-08-
#new db_updates('project user status to varchar');   # 2018-09-08
#new db_updates('access & select_view to varchar');  # 2018-09-10

require(getenv('OB_LIB_DIR').'modules_class.php');
require(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require(getenv('OB_LIB_DIR').'prepare_vars.php');
require(getenv('OB_LIB_DIR').'languages.php');


if (isset($_SESSION['Tid']))
    require(getenv('OB_LIB_DIR').'languages-admin.php');

if (!defined('str_cookies')) {
    echo "<h2>System updates needed!</h2>
        <h3>No languages/translations, go to <a href='/supervisor.php'>supervisor</a> and use the 'Update global translations from GitLab' function and maybe you need to update your system as well.</h3>";
    exit;
}

if (!isset($_SESSION['private_key']))
    $_SESSION['private_key'] = genhash();

if (!isset($_SESSION['openssl_ivs'])) $_SESSION['openssl_ivs'] = array();

#testing memcached
#$mcache = new Memcached();
#$mcache->addServer('localhost', 11211);
#$mcache->set('key', 'hello openbiomaps');
#log_action($mcache->getAllKeys());

/* Visitor counting
 * 
 * */

$M = new Messenger();

track_visitors('controls');

if (!isset($_SESSION['st_col'])) {
    st_col('default_table', 'session', 0);
}

$cmd = "SELECT short,long,language,email FROM project_descriptions LEFT JOIN projects ON (project_table=projecttable) WHERE projecttable='".PROJECTTABLE."'";
$res = pg_query($BID,$cmd);
$row = pg_fetch_all($res);
$k = array_search($_SESSION['LANG'], array_column($row, 'language'));
if(!$k) $k = 0;
$OB_project_title = $row[$k]['short'];
//should be changed to markdown process !!!
//https://github.com/michelf/php-markdown/
//https://allinthehead.com/retro/364/dont-parse-markdown-at-runtime
$OB_project_description = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/","<br><br>",$row[$k]['long']);
$OB_contact_email = $row[$k]['email'];

#debug('load deck.php',__FILE__,__LINE__);
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta http-equiv="cache-control" content="private, max-age=604800, must-revalidate" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OBM - <?php echo $OB_project_title ?></title>
    <link type="image/png" rel="apple-touch-icon" sizes="180x180" href="<?php echo $protocol ?>://<?php echo URL ?>/images/apple-touch-icon.png">
    <link type="image/png" rel="icon" sizes="32x32" href="<?php echo $protocol ?>://<?php echo URL ?>/images/favicon-32x32.png">
    <link type="image/png" rel="icon" sizes="16x16" href="<?php echo $protocol ?>://<?php echo URL ?>/images/favicon-16x16.png">

    <!-- CSS icons -->    
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/academicons/css/academicons.min.css?rev=<?php echo rev('css/academicons/css/academicons.min.css'); ?>" media="print" onload="this.media='all'; this.onload=null;" />
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/css/fork-awesome/css/fork-awesome.min.css?rev=<?php echo rev('css/fork-awesome/css/fork-awesome.min.css'); ?>"  media="print" onload="this.media='all'; this.onload=null;" />

    <!-- pure -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pure/2.0.6/pure-min.css" integrity="sha512-/VectwrK3cVMbl6IFsHj/O+fbxfLQm5rved2LR8HnaNuIL6fs8yBXNSi25hhQCB2713sc3Cbh5a4wLRSsZvDxg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pure/2.0.6/grids-responsive.min.css" integrity="sha512-hL/zOmkRziE9y4nnqXhBXXJtTYZaAkxplhKC2xzS9vmia4emkDtgASBqW2InAiX2ljAqU53qRT1xyCixrX5H8A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- jquery / ui -->
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.css?rev=<?php echo rev('js/ui/jquery-ui.min.css'); ?>"  media="print" onload="this.media='all'; this.onload=null;" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.min.js?rev=<?php echo rev('js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.js?rev=<?php echo rev('js/ui/jquery-ui.min.js'); ?>"></script>

    <!-- obm -->
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/vote.css?rev=<?php echo rev('.'.STYLE_PATH.'/vote.css'); ?>" media="print" onload="this.media='all'; this.onload=null;" />
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/style.css?rev=<?php echo rev('.'.STYLE_PATH.'/style.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/colors.css.php?rev=<?php echo rev('.'.STYLE_PATH.'/colors.css.php'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo  $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/page_style.css?rev=<?php echo rev('.'.STYLE_PATH.'/page_style.css'); ?>" />
    <script type="text/javascript"><?php include(getenv('OB_LIB_DIR').'main.js.php'); ?></script>
    
    <!-- text editor for messages and template edotor -->
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/js/quill/quill.snow.css?rev=<?php echo rev('js/quill/quill.snow.css'); ?>" media="print" onload="this.media='all'; this.onload=null;" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/quill/quill.min.js?rev=<?php echo rev('js/quill/quill.min.js'); ?>"></script>
    
    <!-- obm -->
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/functions.js?rev=<?php echo rev('js/functions.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/roller.js?rev=<?php echo rev('js/roller.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/main.js?rev=<?php echo rev('js/main.js'); ?>"></script>
    
    <!-- a multiselect dropdown lib used in upload & message window / Should be replaced.... -->
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/fSelect.css?rev=<?php echo rev('css/fSelect.css'); ?>"  media="print" onload="this.media='all'; this.onload=null;" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/fSelect.js?rev=<?php echo rev('js/fSelect.js'); ?>"></script>
    
    <!-- vue components -->
    <link type="text/css" rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css" media="print" onload="this.media='all'; this.onload=null;" >
    <?php if (defined('DEV_MODE')) : ?>
        <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/vue.js?rev=<?php echo rev('js/vue.js'); ?>"></script>
    <?php else: ?>
        <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/vue.min.js?rev=<?php echo rev('js/vue.min.js'); ?>"></script>
    <?php endif; ?>
    <script type="module" src="<?php echo $protocol ?>://<?php echo URL ?>/js/vue-components.js?rev=<?php echo rev('js/vue-components.js'); ?>"></script>
    <script src="<?php echo $protocol ?>://<?php echo URL ?>/js/vue-async-computed.js?rev=<?php echo rev('js/vue-async-computed.js'); ?>"></script>
    <script async defer type="text/javascript" src="//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js"></script>
    <script async defer type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js"></script>
    <script async defer type="text/javascript" src="https://unpkg.com/vue-multiselect@2.1.0"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/linnaeus.js?rev=<?php echo rev('js/linnaeus.js'); ?>"></script>

    <!-- codemirror for code editing -->
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/codemirror.css?rev=<?php echo rev('css/codemirror.css'); ?>" type="text/css"  media="print" onload="this.media='all'; this.onload=null;" />
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/monokai.css?rev=<?php echo rev('css/monokai.css'); ?>" type="text/css"  media="print" onload="this.media='all'; this.onload=null;" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/lib/codemirror.js?rev=<?php echo rev('js/lib/codemirror.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/mode/xml/xml.js?rev=<?php echo rev('js/mode/xml/xml.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/mode/clike/clike.js?rev=<?php echo rev('js/mode/clike/clike.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/mode/javascript/javascript.js?rev=<?php echo rev('js/mode/javascript/javascript.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/mode/css/css.js?rev=<?php echo rev('js/mode/css/css.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/mode/php/php.js?rev=<?php echo rev('js/mode/php/php.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/mode/sql/sql.js?rev=<?php echo rev('js/mode/sql/sql.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/mode/htmlmixed/htmlmixed.js?rev=<?php echo rev('js/mode/htmlmixed/htmlmixed.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/mode/r/r.js?rev=<?php echo rev('js/mode/r/r.js'); ?>"></script>
    <!-- codemirror addons -->
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/keymap/vim.js?rev=<?php echo rev('js/keymap/vim.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/addon/dialog/dialog.js?rev=<?php echo rev('js/addon/dialog/dialog.js'); ?>"></script>
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/js/addon/dialog/dialog.css?rev=<?php echo rev('js/addon/dialog/dialog.css'); ?>" media="print" onload="this.media='all'; this.onload=null;" >
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/addon/search/search.js?rev=<?php echo rev('js/addon/search/search.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/addon/search/searchcursor.js?rev=<?php echo rev('js/addon/search/searchcursor.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/addon/search/jump-to-line.js?rev=<?php echo rev('js/addon/search/jump-to-line.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/addon/mode/loadmode.js?rev=<?php echo rev('js/addon/mode/loadmode.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/addon/edit/matchbrackets.js?rev=<?php echo rev('js/addon/edit/matchbrackets.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/addon/selection/active-line.js?rev=<?php echo rev('js/addon/selection/active-line.js'); ?>"></script>

    <!-- Json rendering -->
    <!--<link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/css/renderjson.scss">-->
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/renderjson.js"></script>

<?php 
// Module hook: external_dependencies
// Include external script dependencies <script></script> tags
echo "<!-- Module js -->\n";

$ed = [];
foreach ($x_modules->which_has_method('external_dependencies') as $m) { 
    $ed = array_merge($ed, $x_modules->_include($m,'external_dependencies'));
}
echo implode("\n", array_unique($ed));
unset($ed);

if (function_exists('custom_script_tags')) {
    custom_script_tags();
}
?>
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/profile.css?rev=<?php echo rev('css/profile.css'); ?>" />

<?php
    if ($load_profile or $load_showapikeys or has_access('master')) {
?>
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/admin.css?rev=<?php echo rev('css/admin.css'); ?>" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/admin_functions.js?rev=<?php echo rev('js/admin_functions.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/admin.js?rev=<?php echo rev('js/admin.js'); ?>"></script>

<?php
    }
    if ($load_profile) {
?>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/profile.js?rev=<?php echo rev('js/profile.js'); ?>"></script> 
<?php
    }
?>
    <link type="text/css" rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/includes/modules.css.php?rev=<?php echo rev('includes/modules.css.php'); ?>" />
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/includes/modules.js.php?rev=<?php echo rev('includes/modules.js.php'); ?>"></script>
<?php
//Include custom css/js libraries
$custom_css_directory = getenv('PROJECT_DIR').STYLE_PATH.'/custom_css';
$custom_js_directory = getenv('PROJECT_DIR').STYLE_PATH.'/custom_js';

if (is_dir($custom_css_directory)) {
    $files = scandir($custom_css_directory);
    echo "<!-- Custom css -->\n";

    foreach ($files as $file) {
        // Only css files
        if (pathinfo($file, PATHINFO_EXTENSION) === 'css') {
            echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$protocol://". URL ."". STYLE_PATH ."/{$file}?rev=". rev('.'.STYLE_PATH."/{$file}") ."\"  />\n";
        }
    }
}
if (is_dir($custom_js_directory)) {
    $files = scandir($custom_js_directory);
    echo "<!-- Custom js -->\n";

    foreach ($files as $file) {
        // Only js files
        if (pathinfo($file, PATHINFO_EXTENSION) === 'js') {
            echo "<script type=\"text/javascript\" src=\"$protocol://". URL ."/js/{$file}?rev=". rev("js/$file") ."\"></script>\n";
        }
    }
}
?>
</head>

<?php 
// ez mi?
// css class nem lehet a page...
$page = (!empty($_SESSION['current_page'])) ? array_keys($_SESSION['current_page'])[0] : 'mainpage'; ?>
<body id='index' data-page="<?= $page ?>">

<div id="holder">
<?php

include(getenv('PROJECT_DIR').STYLE_PATH."/header.php.inc");

agree_new_terms();


// Custom pages includes        
if (isset($_GET['includes']) and file_exists(getenv('OB_LIB_DIR').'custom_pages.php')) {
    $custom_load_page = $_GET['includes'];
    if (isset($_GET['subpage']))
        $clp_subpage = $_GET['subpage'];
    else
        $clp_subpage = '';
    include(getenv('OB_LIB_DIR').'custom_pages.php');
}
// Module hook: modal_dialog
// Modal dialogs and other hidden elements
foreach ($x_modules->which_has_method('modal_dialog',) as $m) { 
    echo $x_modules->_include($m,'modal_dialog',['index']);
}
if (isset($_GET['feedback'])) {
    echo "<script>$(document).ready(function() { $('#bugreport-box').show() });</script>";
}

// Body div
?>
<div id='body'>
<div id='message'><?php echo $load_message; ?></div>
<div id="dialog" title="<?php echo t(str_dialog) ?>"><p id='Dt'></p></div>
<div id="dialog-message" title="<?php echo t(str_message) ?>"><p id='Dm'></p></div>
<div id="dialog-confirm" title="<?php echo t(str_confirm) ?>"><p id='Dtc'></p></div>
<?php
// Messages
if (isset($em)) {
    echo $em;    
}

// Module hook: deck_bodypage
// Standalone body pages might be controlled by _GET params
foreach ($x_modules->which_has_method('deck_bodypage') as $m) { 
    echo $x_modules->_include($m,'deck_bodypage',$_GET);
}
// If any landpage loaded set skip all the following $load_ somehow...

// doi-metadata-page can call this function
// A standalone landing page
#if (isset($_GET['viewcomputation'])) {
#    echo $x_modules->_include('computation','viewPackage',array($_GET['viewcomputation'],$_GET['params']),true,false);
    // set skip all the following $load_ somehow...
#}

/* Profile page
 *
 * */
if ($load_profile !== 0) {

    include_once(getenv('OB_LIB_DIR').'interface.php');
    if (isset($_SESSION['Tcrypt'])) {
        echo profile($load_profile,$load_orcid_profile_data);
    }
    else
        $load_message = "Login error!";
}
/* Show saved queries
 * */
if ($load_showbookmarks) {
    if (isset($_SESSION['Tid'])) {
        include(getenv('OB_LIB_DIR').'interface.php');
        //echo tabs($_SESSION['Tcrypt']);
        //echo  "<div style='padding:30px 0px 0px 20px' id='tab_basic'>";
        ShowBookmarks();
        //echo "</div>";
    }
}
/* Show interrupted imports
 * */
if ($load_showiups) {
    if (isset($_SESSION['Tid'])) {
        include(getenv('OB_LIB_DIR').'interface.php');
        ShowIUPS($_SESSION['Tid']);
    }
}
/* Show own API keys 
 * */
if ($load_showapikeys) {
    if (isset($_SESSION['Tid'])) {
        include(getenv('OB_LIB_DIR').'interface.php');
        ShowAPIKeys($_SESSION['Tid']);
    }
}
/* Show user's uploads
 * */
if ($load_showuploads) {
    if (isset($_SESSION['Tid'])) {
        include(getenv('OB_LIB_DIR').'interface.php');
        ShowUploads($_SESSION['getuid']);
    }
}
/* Show own/shared geoms
 * */
if ($load_showowngeoms) {
    if (isset($_SESSION['Tid'])) {
        include(getenv('OB_LIB_DIR').'interface.php');
        ShowSharedGeoms($_SESSION['Tid'],1);
    }
}
if ($load_showsharedgeoms) {
    if (isset($_SESSION['Tid'])) {
        include(getenv('OB_LIB_DIR').'interface.php');
        ShowSharedGeoms($_SESSION['Tid'],2);
    }
}
/* Admin url links */
/* Show server logs 
 * */
if ($load_showserverlogs) {
    include(getenv('OB_LIB_DIR').'interface.php');
    if (!has_access('server_logs') or !isset($_SESSION['Tid'])) {
        return;
    }
    echo "<h2>".t(str_server_logs)."</h2>";
    echo sprintf("<div id='logconsole'>%s</div>",server_logs($_GET['source'],'',$_GET['filter']));
}
/* Show message-box
 * */
if ($load_messages) {
    if (!isset($_SESSION['Tid']))
        echo login_box();
    else {
        include_once(getenv('OB_LIB_DIR').'interface.php');
        echo $M->inbox();
    }
}

/* Own species list
 *
 * */
if ($load_userspecieslist) { 
    include(getenv('OB_LIB_DIR').'interface.php');
    echo usertaxonlist($load_userspecieslist);
}

/* Species sheet: view and edit
 *
 * */
if ($load_metaname) {
    include(getenv('OB_LIB_DIR').'metaname.php');
?>
    <h2><?php echo t(str_taxon_names); ?></h2>
    <div style='padding-top:30px'><?php echo metaname(); ?></div>
<?php
}

// no function calls below
/* Login Box
 *
 * */
if ($load_login and !isset($_SESSION['Tid'])) {
    echo login_box();
} 
/* Registration process
 *
 * */
if ($load_register) {
    echo request_invitation();
}

/* Data usage agreement */
if ($load_useagreement) { ?>
    <br><br> 
    <h2><?php echo t(str_data_usage); ?></h2>
    <div class='preformatted_text'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/datauseandshare.php'))
            include(getenv('PROJECT_DIR').'local/datauseandshare.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'at_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'DataUseAndSharingAgreement_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'DataUseAndSharingAgreement.html');
        ?>    
    </div>
<?php
}
/* Privacy policy */
elseif ($load_privacy) { ?>
    <br><br> 
    <h2><?php echo t(str_privacy_policy) ?></h2>
    <div class='preformatted_text'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/privacy.php'))
            include(getenv('PROJECT_DIR').'local/privacy.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'at_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'at_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'at.html');
        ?>    
    </div>
<?php 
}
/* Terms and conditions */
elseif ($load_terms) { ?>
    <br><br> 
    <h2><?php echo str_terms_and_conditions ?></h2>
    <div class='preformatted_text'>
    <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/terms.php'))
            include(getenv('PROJECT_DIR').'local/terms.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'aszf_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'aszf_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'aszf.html');
    ?>  
    </div>
<?php 
}
/* Server technical info */
elseif ($load_technical) { ?>
    <br><br> 
    <h2><?php echo str_technical_info ?></h2>
    <div class='preformatted_text'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/technical.php'))
            include(getenv('PROJECT_DIR').'local/technical.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'server_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'server_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'server.html');
        ?>    
    </div>
<?php 
}
/* Using Cookies */
elseif ($load_cookies) { ?>
    <h2><?php echo str_cookies ?></h2>
    <div class='preformatted_text'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'local/cookies.php'))
            include(getenv('PROJECT_DIR').'local/cookies.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'cookies_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'cookies_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'cookies.html');
        ?>
    </div>
<?php
}
/* What's new? */
elseif ($load_whatsnew) { ?>
    <h2><?php echo str_whatsnew ?></h2>
    <div class='preformatted_text'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'whatsnew.php'))
            include(getenv('PROJECT_DIR').'whatsnew.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'whatsnew_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'whatsnew_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'whatsnew.html');
        ?>
    </div>
<?php
}
/* Usage */
elseif ($load_usage) { ?>
    <h2><?php echo str_usage ?></h2>
    <div class='preformatted_text'>
        <?php 
        if (file_exists(getenv('PROJECT_DIR').'usage.php'))
            include(getenv('PROJECT_DIR').'usage.php');
        elseif (file_exists(constant('OB_ROOT_SITE').'usage_'.$_SESSION['LANG'].'.html'))
            include(constant('OB_ROOT_SITE').'usage_'.$_SESSION['LANG'].'.html');
        else
            include(constant('OB_ROOT_SITE').'usage.html');
        ?>
    </div>
<?php
}
?>
</div><!--/body-->

    <?php
if (!isset($_SESSION['Tterms_agree']) || !$_SESSION['Tterms_agree']) {
    if (defined('OB_PROJECT_DOMAIN')) {
        $domain = OB_PROJECT_DOMAIN;
    } elseif (defined('OB_DOMAIN')) {
        $domain = OB_DOMAIN;
    } else 
        $domain = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['SERVER_NAME'];

    if (!isset($_SESSION['cookies_accepted'])) {
        echo "<div id='cookie_div' style='text-align:center;position:fixed;z-index:9999;bottom:0;padding:20px 50px 20px 50px;background-color:black;color:#acacac'>";
        if ($_SESSION['LANG']!='hu') {
            echo "
        This site uses cookies to deliver our services and to offer you a better browsing experience. By using our site, you acknowledge that you have read and understand our <a href='$protocol://$domain/cookies/'>Cookie Policy</a>, <a href='$protocol://$domain/privacy/'>Privacy Policy</a>, and our <a href='$protocol//$domain/terms/'>Terms of Services</a>. Your use of OpenBioMaps’s Products and Services, including the OpenBioMaps Network, is subject to these policies and terms. <button class='pure-button' id='cookie-ok'>Accept</button>";
        } else {
            echo "
        Ez a weboldal Sütiket használ az oldal működésének biztosításához és a jobb felhasználói élmény biztosítása érdekében. A Sütik weboldalon történő használatával kapcsolatos részletes tájékoztatás az <a href='$protocol://$domain/privacy/'>Adatkezelési Tájékoztatóban</a> és a <a href='$protocol://$domain/cookies/'>Süti Tájékoztatóban</a> és a <a href='$protocol://$domain/terms/'>Felhasználói szabályzatban</a> található. <button class='pure-button' id='cookie-ok'>Rendben</button>";
        }
        echo "</div>";
    }
}
    ?>

<?php

    echo "<div id='bugreport-box'>
    <h2 style='color:white !important;background-color:#666 !important;padding:6px;margin:0px'>".str_report_an_error."</h2>
    <div style='padding:5px;text-align:center'>
    <button id='bugreport-cancel' style='position:absolute;top:0;right:0' class='pure-button button-large button-passive'><i class='fa fa-close'></i></button>
    <form class='pure-form pure-u-1'>
        <input type='hidden' id='page' value='profile.php'>

        <fieldset class='pure-group'>
            <input type='text' id='issue-title' maxlength='32' class='pure-u-1' placeholder='".str_subject."'>
            <textarea id='issue-body' style='min-height:10em' class='pure-u-1' placeholder='".str_bug_description."'></textarea>
        </fieldset>
        <button id='bugreport-submit' class='pure-button button-large button-warning pure-u-2-3'>".str_send." <i class='fa fa-lg fa-bug'></i></button>
        
    </form>
    </div>
</div>";


require_once(getenv('PROJECT_DIR').STYLE_PATH."/footer.php.inc");

//pg_close($ID);
//pg_close($GID);
//pg_close($BID);
?>
</div><!--/holder-->
</body>
</html>
