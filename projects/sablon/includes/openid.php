<?php

//require_once(getenv('PROJECT_DIR') . 'oauth/oauth2-server-php/src/OAuth2/Autoloader.php');
require_once(getenv('PROJECT_DIR') . 'local_vars.php.inc');
require_once(getenv('OB_LIB_DIR') . 'db_funcs.php');
require_once(getenv('OB_LIB_DIR') . 'auth.php');
require_once(getenv('OB_LIB_DIR') . 'common_pg_funcs.php');


require_once('vendor/autoload.php');

session_start();
if (isset($_REQUEST['provider'])) {
    $remember_me = (isset($_REQUEST['remember_me']) && $_REQUEST['remember_me'] == 'on') ? true : false;
}

if (isset($_REQUEST['callback_address'])) {
    $_SESSION['login_callback_address'] = $_REQUEST['callback_address'];
}

OidcAuth::openid_authentication($_REQUEST['provider'] ?? null, $remember_me ?? null);

$callback_address = $_SESSION['login_callback_address'] ?? protocol() . '://' . constant('URL');
unset($_SESSION['login_callback_address']);

header('Location: ' . $callback_address);


