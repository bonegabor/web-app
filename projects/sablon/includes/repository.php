<?php
class repository {
    /*  */

    public $repo_type;
    public $server_url;
    public $api_token;
    public $D;

    function __construct($D) {
        if (!defined('REPOSITORIES')) {
            $this->repo_type = false;
            return;
        }
        $this->D = $D;

        $rc = constant('REPOSITORIES');

        $this->repo_type = (isset($rc[$D]["TYPE"])) ? $rc[$D]["TYPE"] : false;
        $this->server_url = (isset($rc[$D]["SERVER_URL"])) ? $rc[$D]["SERVER_URL"] : false;
        $this->api_token = (isset($rc[$D]["API_TOKEN"])) ? $rc[$D]["API_TOKEN"] : false;
    }

    public function set($D) {
        $rc = constant('REPOSITORIES');

        $this->D = $D;

        $this->repo_type = (isset($rc[$D]["TYPE"])) ? $rc[$D]["TYPE"] : false;
        $this->server_url = (isset($rc[$D]["SERVER_URL"])) ? $rc[$D]["SERVER_URL"] : false;
        $this->api_token = (isset($rc[$D]["API_TOKEN"])) ? $rc[$D]["API_TOKEN"] : false;
    }
}

?>
