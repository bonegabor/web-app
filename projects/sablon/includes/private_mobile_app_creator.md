# background and icons
## change files
android/app/src/main/res/
    drawable/screen.jpg
    drawable-hdpi/icon.png
    drawable-ldpi/icon.png
    drawable-mdpi/icon.png
    drawable-xhdpi/icon.png
    drawable-xxhdpi/icon.png
    drawable-xxxhdpi/icon.png

# app name
android/app/src/main/res/values/settings.xml
## diff
3c3
<     <string name="app_name">OpenBioMaps</string>
---
>     <string name="app_name">Hóvirágok</string>

# Update sources
## insert contents
### src/Constants.js
#### custom_app_version
>>>
export const CUSTOM_APP_VERSION = 'duna-drava-hovirag_v1';
<<<

### src/api/Client.js
#### auth
>>>
    userName = 'valaki@valahol.hu';
    password = '12345';
<<<
#### get_server_list
>>>
const static_server_string = '[{"obm_id": "4","uploader_name": "Ban Miklos","uploading_date": "2016-04-11","upid": "172","hist_time": "","available": "up","comment": "physical","computer_parameters": "24 Core Intel(R) Xeon(R) CPU E5-2420 v2 @ 2.20GHz; 32Gb RAM; 3.7Tb Hdd","country": "Hungary","domain": "http://dinpi.openbiomaps.org","founding_date": "2015-01-15","institute": "Duna-Dráva Nemzeti Park Igazgatóság","machine": "consortium","obm_geometry": "POINT(18.994067 47.498216)","people": "Berces Sandor, Baranyai Zsolt","place": "Budapest","server": "kormoran" }]';
return JSON.parse(static_server_string);
<<<
#### get_form_list
>>>
return JSON.parse('[{"id":"201","visibility":"Hóvirág adatgyűjtő","form_id":"201","form_name":"Hóvirág adatgyűjtő","last_mod":"1580682825"}]');
<<<
#### get_project_list
>>>
const static_project_list = '{"status":"success","message":"","data":[{"project_table":"hovirag","creation_date":"2020-02-02","Creator":"Duna-Dr\u00e1va Nemzeti Park Igazgat\u00f3s\u00e1g","email":"gaborik@indamail.hu","stage":"stable","doi":null,"running_date":"2020-02-02","licence":null,"rum":null,"collection_dates":null,"subjects":null,"project_hash":"3j9gseulm5e0","project_url":"http://dinpi.openbiomaps.org/projects/hovirag/","project_description":"Hóvirág adatgyűjtés","public_mapserv":"-","training":"f","rserver":"f","language":"hu","game":"off","rserver_port":0}]}';
    const json = JSON.parse(static_project_list);
    const projects = json.data.map((item) => {
        return { ...item };
    });
    return projects;
<<<

### src/navigators/MainNavigator/index.js
#### imports_in_MainNavigator
>>>
import CustomDescriptionScreen from '../../screens/CustomDescriptionScreen';
import CustomReloadPage from '../../screens/CustomReloadPage';
<<<
#### custom_stackNavigators
>>>
    CustomDescription: {
      screen: CustomDescriptionScreen,
      navigationOptions: () => ({
        headerTitle: (
          <View style={[headerTitleStyle.container, { height: 56 }]}>
            <Text style={[headerTitleStyle.title, { marginBottom: 0 }]} numberOfLines={1}>Projekt információ</Text>
          </View>
        ),
        headerStyle: [headerStyle, { height: 56 }],
        headerTitleStyle: titleStyle,
      }), 
    },
    CustomReload: {
      screen: CustomReloadPage,
      navigationOptions: () => ({
        headerTitle: (
          <View style={[headerTitleStyle.container, { height: 56 }]}>
            <Text style={[headerTitleStyle.title, { marginBottom: 0 }]} numberOfLines={1}>Projekt adatok betöltése</Text>
          </View>
        ),
        headerStyle: [headerStyle, { height: 56 }],
        headerTitleStyle: titleStyle,
      }), 
    },
<<<

### src/actions/measurement.js
#### imports_in_measurment.js
>>>
import { CUSTOM_APP_VERSION } from '../Constants';
import DeviceInfo from 'react-native-device-info';
<<<
#### custom_meta_params
>>>
uniqClientId: DeviceInfo.getUniqueId(),
app_cversion: CUSTOM_APP_VERSION,
<<<
    
### src/screens/SettingsScreen/index.js
#### imports_in_SettingsScreen
>>>
import { CUSTOM_APP_VERSION } from '../../Constants';
import DeviceInfo from 'react-native-device-info';
<<<
#### consts_in_SettingsScreen_component
>>>
custom_app_version = {};
device_uniq_id = {};
<<<

### src/screens/HomeScreen/index.js
#### imports_in_homeScreen
>>>
import { Text, Linking } from 'react-native';
<<<
#### consts_in_HomeScreen_render
>>>
const { selectServer, loadServers } = this.props;
<<<
#### menu_config
>>>
I18n.locale = 'hu';
displayMenu.menu_prev2 = false;
displayMenu.menu_collectData = false;
displayMenu.menu_export = false;
displayMenu.menu_type1_style = {
    backgroundColor: 'rgba(102, 160, 83, 0.6)',
};
displayMenu.menu_type2_style = {
    backgroundColor: Colors.opacMantisGreen,
};
displayMenu.header.display = true;
displayMenu.header.title = 'Használati információk';
displayMenu.header.description = 'Hóvirág adatgyűjtés';
displayMenu.header.action = ()=>{NavigationHandler.navigate('CustomDescription')};
displayMenu.header.style = {
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    flex:0,
};
displayMenu.footer.display = true;
displayMenu.footer.title = 'Szerver adatok';
displayMenu.footer.description = 'klikk a betöltéshez...';
//displayMenu.footer.action = ()=>{ Linking.openURL('https://dinpi.openbiomaps.org/projects/ddnpi/')};
displayMenu.footer.action = ()=>{NavigationHandler.navigate('CustomReload')};
displayMenu.footer.style = {
    backgroundColor: 'rgba(0, 0, 0, 0.2)',
    flex:0,
};
loadServers();
setTimeout(() => {
    selectServer('http://dinpi.openbiomaps.org');
}, 200);
<<<
#### consts_mapDispatchToProps
>>>
loadServers,
<<<
#### bind_actions
>>>
loadServers,
<<<

# Add custom pages
## bash
mkdir src/screens/CustomDescriptionScreen
touch index.js
touch styles.js
mkdir src/screen/CustomReloadPage/
touch index.js
touch styles.js
