<?php
#' ---
#' Module:
#'   bold_yellow
#' Files:
#'   [bold_yellow.php]
#' Description: >
#'   bold yellow labels for important variables in data sheet pages and highlighted varaibales in mobile app, recorded data sheet
#' Methods:
#'   [mark_text]
#' Module-type:
#'   table
#' Examples: >
#'   [
#'    "species_name"
#'    "number_of_individuals"
#'    "date"
#'   ]
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class bold_yellow extends module {

    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    function mark_text ($params) {
        $c = array();

        $c = $this->split_params($params);
        /* nem működik mióta benn vannak a tábla nevek!
         * if (!count($c)) {
            //no module params, just set default values
            $c = array_merge($_SESSION['st_col']['DATE_C'],array($_SESSION['st_col']['SPECIES_C'],$_SESSION['st_col']['NUM_IND_C']));
        }*/

        return($c);

    }
}
?>
