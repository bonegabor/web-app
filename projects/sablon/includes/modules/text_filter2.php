<?php
#' ---
#' Module:
#'   text_filter2
#' Files:
#'   [text_filter2.php, text_filter2.js, text_filter2.css]
#' Description: >
#'   Taxon and other text filters
#'   create boxes
#'   assemble WHERE part of query string
#' Methods:
#'   [print_js, print_css, module_params_interface, print_box, ajax, assemble_where, query_button]
#' Module-type:
#'   table
#' Example:
#'   {
#'        "obm_taxon": {
#'            "search_algoritm": "like",
#'            "preFilterColumn": [
#'                "status"
#'            ],
#'            "preFilterValue": [
#'                [
#'                    "accepted",
#'                    "synonym",
#'                    "undefined",
#'                    "taxon"
#'                ]
#'            ],
#'            "preFilterRelation": [
#'                "IN"
#'            ]
#'        },
#'        "obm_datum": {},
#'        "gyujto1": {
#'            "autocomplete": true
#'        },
#'        "obm_terms": {
#'            "search_algoritm": "like",
#'            "subjects": [
#'                "gyujto1"
#'            ],
#'            "preFilterColumn": [
#'                "status"
#'            ],
#'            "preFilterValue": [
#'                [
#'                    "accepted",
#'                    "undefined"
#'                ]
#'            ],
#'            "preFilterRelation": [
#'                "IN"
#'            ],
#'            "fieldLabel": "gyujto1 teszt"
#'        },
#'        "proj_code": {
#'            "optionsTable": "hnpi_terms",
#'            "valueColumn": "term",
#'            "preFilterColumn": [
#'                "data_table",
#'                "subject"
#'            ],
#'            "preFilterValue": [
#'                "hnpi",
#'                "proj_code"
#'            ]
#'        },
#'        "tajegyseg": {
#'            "autocomplete": true
#'        },
#'        "telepules": {
#'            "autocomplete": true
#'        },
#'        "feszekazonosito": {
#'            "autocomplete": true
#'        },
#'        "obm_uploading_id": {
#'            "autocomplete": true
#'        }
#'    }
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   3.0
class text_filter2 extends module {
    var $error = '';
    var $retval;
    var $params = null;
    public $ajax_deps = ['remove_accents.function'];
    public $strings = ['str_from','str_to'];

    function __construct($action = null, $params = null, $pa = array()) {
        
        $this->params = $this->split_params($params);

        if ($action) {
            $this->retval = $this->$action($params,$pa);
        }
    }

    protected function moduleName() {
        return __CLASS__;
    }

    public function module_params_interface($params, $row) {
        $json = json_encode($this->params);
        $j = $this->params;

        $edit = "<button class='edit-text-filter-param pure-button button-small button-warning'><i class='fa fa-edit'> </i> </button>";
        $delete = "<button class='delete-text-filter-param pure-button button-small button-danger'><i class='fa fa-trash-o'> </i> </button>";
        $html = "<div id='text-filter-params' data-json='$json'>";

        foreach ($j as $param => $options) {
            $html .= "<div class='text-filter-param'> $edit $delete <span>$param</span> </div>";
        }

        $html .= "</div>";

        $q = new data_query([
            'database' => 'biomaps',
            'table' => 'project_metaname',
            'columns' => ['value' => 'column_name'],
            'filter' => ['column' => 'project_table', 'value' => $row['mtable']],
            'orderby' => 'order',
        ]);
        $specials = [
            [ 'value' => 'obm_taxon'],
            [ 'value' => 'obm_terms'],
            [ 'value' => 'obm_datum'],
            [ 'value' => 'obm_uploader_user'],
            [ 'value' => 'obm_uploading_date'],
        ];

        $data = array_merge($specials, $q->getData());
        $s = new input_field([
            'element' => 'select',
            'data' => $data,
            'id' => 'target_column',
            'class' => 'tfp',
            'placeholder' => 'Please select column ...',
        ]);

        $html .= "<div class='text-filter-interactions'>";
        $html .= "<div class='basic-settings hidden'> Select target column: " . $s->getElement() . "</div>";

        $q = new data_query([
            'database' => 'biomaps',
            'table' => 'header_names',
            'columns' => ['value' => 'f_project_table'],
            'filter' => ['column' => 'f_project_name', 'value' => PROJECTTABLE],
            'distinct' => true,
        ]);
        $s = new input_field(['element'=>'select','data'=>$q->getData(),'id'=>'optionsTable','class'=>'tfp', 'placeholder' => 'Please select table ...', ]);
        $html .= "<div class='advanced-settings hidden'>";
        $html .= "Values from table:" . $s->getElement();
        $s = new input_field(['element'=>'select','data'=>[],'id'=>'valueColumn','class'=>'tfp']);
        $html .= "Values from column:" . $s->getElement();
        $s = new input_field(['element'=>'select','data'=>[],'id'=>'preFilterColumn','class'=>'tfp']);
        $html .= "Pre-filter column:" . $s->getElement();
        $s = new input_field(['element'=>'select','data'=>[],'id'=>'preFilterValue','class'=>'tfp', 'multiple'=>true]);
        $html .= "Pre-filter values:" . $s->getElement();

        $s = new input_field(['type'=>'checkbox', 'data'=>[],'id'=>'autocomplete','class'=>'tfp']);
        $html .= "Autocomplete:" . $s->getElement();

        $html .= "</div>";
        $html .= "</div>";
        $html .= "<div class='text-filter-buttons'><button class='pure-button button-small button-success add-text-filter-param'><i class='fa fa-plus'></i> Add new </button></div>";

        // nem működik
        //if (preg_match('/(<textarea.*>)(.*)(<\/textarea>)/', $row['textarea'], $ta)) {
        //    $html .= $ta[1] . json_encode(json_decode($ta[2]),JSON_PRETTY_PRINT) . $ta[3];
        //}
        //else
        $html .= $row['textarea'];

        //$html = "<pre>".json_encode($j,JSON_PRETTY_PRINT)."</pre>";
        return $html;
    }

    public function print_box ($p) {
        global $ID,$BID,$modules;
        $t = new table_row_template();

        $qtable = $_SESSION['current_query_table'];

        $j = $this->params;
        $pm_labels = array();

        if (isset($_SESSION['private_key'])) {
            $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
            $iv = openssl_random_pseudo_bytes($ivlen);
            $_SESSION['openssl_ivs']['text_filter_table'] = base64_encode($iv);
        }
        else {
            log_action('ERROR: _SESSION[private_key] not defined',__FILE__,__LINE__);
        }
        
        if (count($j)) {
            $t->cell(mb_convert_case(str_text_filters, MB_CASE_UPPER, "UTF-8"),2,'title center');
            $t->row();

            $opts = array();
            $disableNext = '';
            $dbcolist = dbcolist('array', $qtable);
            // create check list of available filter options
            foreach ($j as $pm => $options) {
                if ($pm === array_keys($j)[0]) {
                    continue;
                }
                
                if (isset($options['fieldLabel'])) {
                    $label = (defined($options['fieldLabel'])) ? constant($options['fieldLabel']) : $options['fieldLabel'];
                }
                elseif (in_array($pm, array_keys($dbcolist))) {
                    $label = $dbcolist[$pm];
                }
                else {
                    $label = (defined($pm)) ? constant($pm) : $pm;
                    if ($pm == 'obm_datum') {
                        $label = str_datum_filter;
                    }
                }

                $disabled = ($pm === $disableNext) ? "disabled='true'" : "";
                if (isset($options['targetElement']) && $options['targetElement'] !== '') {
                    $group = "data-group='{$options['targetElement']}'";
                    $disableNext = $options['targetElement'];
                }
                else {
                    $group = "";
                    $disableNext = "";
                }

                $opts[] = "<input type='checkbox' class='filter-sh-advopt' value='$pm' $group $disabled> $label<br>";
                $pm_labels[$pm] = $label;
            }
        }


        $_SESSION['qform'] = array();
        $first = 0;
        foreach($j as $pm => $options) {

            $def_hidden = ($first==0) ? '' : 'def-hidden';
            $first++;

            if ($pm =='obm_taxon') {

                $t->cell(str_taxon_name.":",1,'title');
                $t->cell("<input type='text' id='taxon_sim' value='' autocomplete='off'>",1,'content');
                $t->row("text-options_obm_taxon $def_hidden");
                $t->cell("<div id='tsellist'>
                <span id='taxon-search-settings' class='pure-button button-href' style='margin-left:5px'><i class='fa fa-cog fa-lg'></i></span>
                <div style='position:relative'><div id='taxon-search-settings-box' style='display:none;border:1px solid #acacac;padding:2px 20px 2px 2px;margin:2px;position:absolute;left:2px;top:0px;background-color:white'>
                    <input type='radio' id='onematch' name='match' style='vertical-align:bottom'> ".str_one_match."<br>
                    <input type='radio' id='allmatch' checked name='match' style='vertical-align:bottom'> ".str_all_match."</div>
                </div>",2,'title');
                $t->row("text-options_obm_taxon $def_hidden");
            } 
            elseif ($pm == 'obm_datum') {

                $t->cell(str_datum_filter,2,'title center');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(str_exact_date,1,'title');
                $t->cell("<input type='text' id='exact_date' class='qf' value='' autocomplete='off'>",1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(str_date_period,2,'title');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(t(str_from),1,'content');
                $t->cell(t(str_to),1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell("<input type='text' id='exact_from' class='qf' value='' autocomplete='off'>",1,'content');
                $t->cell("<input type='text' id='exact_to' class='qf' value='' autocomplete='off'>",1,'content');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell(str_range_period,1,'title');
                $t->cell("<span id='period-of-year' style='font-weight: normal'></span><input type='text' id='exact_mfrom' class='qf period_filter' value='' hidden autocomplete='off'><input type='text' id='exact_mto' class='qf period_filter' value='' hidden autocomplete='off'>", 1, 'title');
                $t->row("text-options_obm_datum $def_hidden");
                $t->cell("<div id='period-of-year-slider'></div>",2,'content');
                $t->row("text-options_obm_datum $def_hidden");
            } 
            elseif ($pm == 'obm_uploading_date') {

                $t->cell(str_upload_date,1,'title');
                $t->cell("<input type='text' id='obm_uploading_date' class='qf' value='' autocomplete='off'>",1,'content');
                $t->row("text-options_obm_uploading_date $def_hidden");
            } 
            elseif ($pm == 'obm_uploader_user') {

                $upl_user_box = "<select id='obm_uploader_user' class='qf'><option></option>";
                $cmd = sprintf("SELECT DISTINCT uploader_id,uploader_name AS un FROM system.uploadings WHERE project_table = '%s' ORDER BY uploader_name;", $qtable);
                $res = pg_query($ID,$cmd);
                $uid = array();
                while($row = pg_fetch_assoc($res)) {
                    if (in_array($row['uploader_id'],$uid)) continue;
                    $uid[] = $row['uploader_id'];
                    $upl_user_box .= "<option value='{$row['uploader_id']}'>{$row['un']}</option>";
                }
                $upl_user_box .="</select>";

                //array_splice($params,$i,1);
                $t->cell(str_uploader,1,'title');
                $t->cell($upl_user_box,1,'content');
                $t->row("text-options_obm_uploader_user $def_hidden");
            } 
            elseif ($pm == 'obm_files_id') {
                // Photo filter

                $t->cell($pm_labels[$pm],1,'title');

                $t->cell('<button class="qf pure-button button-passive button-small" type="button" id="hasphoto"><i class="fa fa-lg fa-toggle-off"></i></button>',1,'content');

                $t->row("text-options_$pm $def_hidden");
            }
            elseif ($pm == 'obm_terms') {
                
                $t->cell(t('str_multisearch_filter'),2,'title center','background-color:azure');
                $t->row("text-options_obm_terms $def_hidden");
                
                if (!$modules->is_enabled('list_manager', $qtable)) {
                    $content = 'ERROR: list_manager module not enabled';
                    $subjects = [];
                }
                else {
                    $subjects = iapi::load("/api/linnaeus/get_linnaeus_options/load.php?table={$qtable}");
                    $subjects = $subjects[$qtable]->managed_columns;
                    if (count($subjects) === 0) {
                        $content = 'ERROR: LINNAEUS_MANAGED_COLUMNS not defined';
                    }
                    else {
                        $content = "<input style='width:100%;font-size:150%' type='text' id='search' class='multi-autocomplete' value='' data-ids='' autocomplete='off'>";
                        $subjects = array_column($subjects, 'name');
                    }
                }
                
                $t->cell($content,2,'content');
                $t->row("text-options_obm_terms $def_hidden");
            
        
                foreach ($subjects as $subject) {
                    $t->cell("<div id='obm_terms_{$subject}' class='qf divInput'><button id='{$subject}_relation' class='search-relation pure-button pure-button-primary hidden'>OR</button></div>",2,'content');
                    $t->row("text-options_obm_terms $def_hidden");
                }
                
                $t->row("text-options_obm_terms $def_hidden");
            }
            else {
                
                $options = $this->prepareOptions($pm, $options);

                //betöltött select mező cimkéje
                $t->cell($pm_labels[$pm],1,'title');
                
                if (isset($options['error'])) {
                    $content = $options['error'];
                }
                else {
                    $s = new filter_input($options);
                    $content = $s->getElement();
                }

                //betöltött szelkció tartalma
                $t->cell($content,1,'content');

                //a teljes sor létrehozása
                $t->row("text-options_$pm $def_hidden");

            }
        }
        if (count($opts)) {
            $opts = implode('',$opts);
            $t->cell("<div>".str_options.":<br><div class='pure-u-1' style='max-height:100px;overflow-y:scroll;'>$opts</div></div>",2,'shadow');
        }

        $t->row();

        $html = sprintf("<input type='hidden' id='taxon_id'>
            <table class='mapfb'>
            %s
            </table>
            <div style='font-size:0.8em' id='responseText'></div>",$t->printOut());
        return $html;
    }

    private function prepareOptions($pm, $options) {

        global $modules, $x_modules;

        $defaults = [
            'allOptions' => false,
            'multiple' => '',
            'valueColumn' => $pm,
            'optionsTable' => $_SESSION['current_query_table'],
            'optionsSchema' => 'public',
            'filterValue' => '',
            'preFilterColumn' => '',
            'preFilterValue' => '',
            'labelAsValue' => false,
            'autocomplete' => false,
            'nested_element' => '',
            'targetElement' => '',
            'tdata' => [],
            'size' => '',
        ];

        foreach ($defaults as $key => $value) {

            $options[$key] = $options[$key] ?? $defaults[$key];

            if ( in_array($key, ['allOptions', 'labelAsValue']) ) {
                $options[$key] = filter_var( $options[$key], FILTER_VALIDATE_BOOLEAN);
            }
        }
        
        $options['labelColumn'] = $options['labelColumn'] ?? $options['valueColumn'];
        $options['filterColumn'] = $options['filterColumn'] ?? $options['preFilterColumn'];
        $options['filterValue'] = ($options['filterValue'] === '') ? $options['preFilterValue'] : $options['filterValue'];

        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        if (isset($options['etr']) and isset($_SESSION['private_key'])) {
            $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
            $table_reference = openssl_decrypt(base64_decode($options['etr']), $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA, $iv);
            list($options['optionsSchema'], $options['optionsTable']) = preg_split('/\./',$table_reference);
            unset($options['etr']);
        }
        else {
            if (!isset($_SESSION['openssl_ivs']['text_filter_table'])) {
                $iv = openssl_random_pseudo_bytes($ivlen);
                $_SESSION['openssl_ivs']['text_filter_table'] = base64_encode($iv);
            } 
            else {
                $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
            }
            $options['etr'] = base64_encode(openssl_encrypt("{$options['optionsSchema']}.{$options['optionsTable']}", $cipher, $_SESSION['private_key'], OPENSSL_RAW_DATA, $iv));
        }

        // parent element
        if ($options['targetElement'] !== '') {
            $options['access_class'] = 'nested';
            $options['tdata'] = array_merge($options['tdata'],[
                'nested_element' => preg_replace('/\./','-',$options['targetElement']),
                'alloptions' => $options['allOptions']
            ]);
        }
        //child element
        if (isset($options['filterColumn']) && ($options['filterValue'] === '')) {
            $options['tdata'] = array_merge($options['tdata'],[
                'etr'=>(isset($options['etr'])) ? $options['etr'] : '',
                'filterColumn' => $options['filterColumn'],
                'valueColumn' => $options['valueColumn'],
                'labelColumn' => $options['labelColumn'],
                'alloptions' => $options['allOptions']
            ]);
        }

        $options['html_id'] = ($options['nested_element'] !== '') ? preg_replace('/\./','-',$options['nested_element']) : $pm;
        
        if ($options['optionsTable'] === 'taxonmeta') {
            
            if (!$x_modules->is_enabled('taxon_meta') ) {
                $this->error = 'taxon_meta module is not enabled';
                return ['error' => $this->error];
            }
            if (!$x_modules->_include('taxon_meta', 'is_submodule_enabled', ['TaxonGroups'])) {
                $this->error = 'TaxonGroups submodules of the taxon_meta module is not enabled';
                return ['error' => $this->error];
            }
            
            $taxonmeta_options = $x_modules->_include('taxon_meta', 'include_submodule', [
                'TaxonGroups',
                'text_filter_input_arguments',
                $pm
            ]);
            
            if (!$taxonmeta_options) {
                $this->error = "\"$pm\" column does not exist";
                return ['error' => $this->error];
            }
            
            foreach ($options as $key => $value) {
                $options[$key] = $taxonmeta_options[$key] ?? $options[$key];
            }
            return $options;
        }

        return $options;
    }


    /* AJAX FUNCTIONS */
    /**
    * getList - returns an autocomplete list for the taxon filter box
    **/
    public function get_list($params, $request) {
        global $modules;
        
        if (!isset($request['table']) || !isset($request['filter'])) {
            $this->error = 'request parametes not complete!';
            return;
        }
        $table = $request['table'];
        if ($table === 'terms' && !$modules->is_enabled('list_manager')) {
            $this->error = 'list_manager module not enabled!';
            return;
        }
        
        $request['subject'] = ($table === 'taxon') ? $_SESSION['st_col']['SPECIES_C'] : null;
        
        $request['limit'] = 20;
        
        $request['search_algoritm'] = $this->params["obm_$table"]['search_algoritm'] ?? 'trgm';
        $request['bilingual_search'] = $this->params["obm_$table"]['bilingual_search'] ?? true;
        
        if (isset($this->params["obm_$table"]['preFilterColumn']) && isset($this->params["obm_$table"]['preFilterValue'])) {
            $request['preFilter'] = [
                "preFilterColumn" => $this->params["obm_$table"]['preFilterColumn'],
                "preFilterValue" => $this->params["obm_$table"]['preFilterValue'],
                "preFilterRelation" => $this->params["obm_$table"]['preFilterRelation'] ?? null,
            ];
        }
        
        $request['only_managed_columns'] = true;
        if ($table == 'terms') {
            $request['orderby'] = $this->params["obm_$table"]['orderby'] ?? 'subject';
            $request['order'] = $this->params["obm_$table"]['order'] ?? 'ASC';
        }
        
        $list = iapi::load("/api/linnaeus/get_list/load.php?" . http_build_query($request));
        $this->ajax_echo_common_message = !($table == 'terms');
        return array_map(function ($el) {
            return [ 
                'meta' => $el->meta,
                'label' => $el->word,
                'value' => $el->word,
                'id' => $el->taxon_id,
                'category' => $el->subject,
                'taxon_db' => $el->taxon_db,
            ];
        }, $list->list);
    }
    
    /*
     * Clear filters
     * */
    public function clear_filters($params, $request) {
        global $ID;
        $res = pg_query($ID,
            sprintf("SELECT EXISTS (
                        SELECT 1
                        FROM   information_schema.tables
                        WHERE  table_schema = 'temporary_tables'
                        AND    table_name = 'temp_%s_%s');",PROJECTTABLE,session_id()));
        $row = pg_fetch_assoc($res);
        if ($row['exists']=='t') {
            $res = pg_query($ID,sprintf('DROP TABLE temporary_tables.temp_%1$s_%2$s',PROJECTTABLE,session_id()));
            return 'ok';
        }
        return;
    }
    
    /* mapview, filterbox
    * get dynamic list of filtered elements from a table
    * return JSON array of filtered elements
    * */
    public function qflist($params, $request) {
        global $ID;
        $term = preg_replace('/;/','',$request['term']);
        $id = preg_replace('/;/','',$request['id']);
        $schema = 'public';
        $column = '';
        $table = '';

        if (isset($request['etr']) and $request['etr']!='' and isset($_SESSION['private_key'])) {
            $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
            $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
            $table_reference = openssl_decrypt($request['etr'], $cipher, $_SESSION['private_key'],$options = 0, $iv);
            if ($table_reference!='') {
                list($schema,$table) = preg_split('/\./',$table_reference);
                $column = $id;
            }
            else {    
                // hogyan lehet elkapni a lejárt lapot már javascripttel?
                // be kellene tenni egy encryptált stringet a main.js.php-val a lap tetejére, mint page_code és session
                // változóként is benn tartani, ha nem egyezik, akkor kötelezően reloadolni a lapot??
                $this->error = "Page expired";
                return;
            }
        } 
        else {
            //tképp ez is jobb lenne encryptálva...
            $sqla = sql_aliases(1);
            $qta = $sqla['qtable_alias'];
            $qt = $sqla['qtable'];
            list($table,$column) = preg_split('/-/',$id);
            // így van egy warning ha nincs - a névben
            if ($column=='') {
                $column = $table;
                $table = $_SESSION['current_query_table'];
            }
            if ($table!=$qta) {
                log_action('table mismatch...',__FILE__,__LINE__);
            }
            else {
                $table = $qt;
            }
        }

        $speedup_filter = '';
        // It can be slow...
        // much more faster with and indexed first letter filter!!!!!
        // ALTER TABLE .... ADD COLUMN obm_first_letter_filter character(1);
        // UPDATE ... SET obm_first_letter_filter=lower(substring(---- from 1 for 1))
        // EXPLAIN ANALYZE SELECT DISTINCT "telepules" "telepules" FROM hrsz_terkepek."kuvet_bevet_2018" WHERE obm_first_letter_filter='g' AND telepules ILIKE 'gárd%' ORDER BY telepules LIMIT 20
        if (isset($request['speedup_filter']) and $request['speedup_filter']=='on') {
            $speedup_filter = sprintf('obm_first_letter_filter=%s AND ',quote(mb_strtolower(mb_substr($term,0,1))));
        }
        $cmd = sprintf('SELECT DISTINCT "%1$s" "%1$s" FROM %5$s."%2$s" WHERE %6$s CAST("%1$s" AS TEXT) ILIKE \'%3$s%4$s\' ORDER BY %1$s LIMIT 20',
            $column,
            $table,
            $term,
            '%',
            $schema,
            $speedup_filter
        );
        $res = pg_query($ID,$cmd);
        if (pg_num_rows($res)) {
            $x = pg_fetch_all($res);
            return json_encode(array_column($x,$column));
        } 
        else {
            $this->error = "No matching string could be found.";
            return;
        }
    }


    
    /* general dynamic select/autocomplete menu
    * etr: Encrypted source table reference
    * id: filtered column name
    * term: filter term
    *
    * return a complete menu
    * */
    public function create_dynamic_menu($params, $request) {
        $nested_element = $request['nested_element'];
        
        $request['optionsSchema'] = $this->params[$nested_element]['optionsSchema'] ?? 'public';
        
        $options = $this->prepareOptions($request['nested_element'], $request);
        
        $options['onlyoptions'] = true;
        
        $s = new filter_input($options);
        return $s->getElement();
    }


    /** Admin functions
     *
     * */
    public function getColumnList($params, $request) {

        $qParams = [
            'database' => 'biomaps',
            'table' => 'project_metaname',
            'columns' => ['value' => 'column_name'],
            'filter' => ['column' => 'project_table', 'value' => $request['mtable']],
            'orderby' => 'order',
        ];
        $q = new data_query($qParams);

        $sel = new input_field([
            'element' => 'select',
            'data' => $q->getData(),
            'id' => $request['id'],
            'class' => 'tfp',
            'placeholder' => 'Please select column ...',
            'onlyoptions' => isset($request['oo']),
        ]);

        return $sel->getElement();
    }

    public function getDistinctValues($params, $request) {
        
        $table = preg_replace('/[^a-zA-Z0-9_]/','',$request['mtable']);
        $column = preg_replace('/[^a-zA-Z0-9_]/','',$request['column']);

        $qParams = [
            'table' => $table,
            'distinct' => true,
            'columns' => ['value' => $column],
            'orderby' => $column,
        ];
        $q = new data_query($qParams);

        $sel = new input_field([
            'element' => 'select',
            'data' => $q->getData(),
            'id' => $request['id'],
            'class' => 'tfp',
            'placeholder' => 'Please select column ...',
            'onlyoptions' => $request['oo'],
        ]);

        return $sel->getElement();
    }
}

class query_builder {
    /*       params:
     *           mc: matched column, the column name, which will be used in the query string
     *     post_val:
     * custom_table:
     *
     * */
    function assemble_where($params,$mc,$post_val,$custom_table) {
        global $ID, $modules, $x_modules;

        $w = '';
        $specials = array('exact_from','exact_mfrom','exact_mto','exact_to','exact_date','obm_uploading_date','obm_uploader_user','obm_id','obm_uploading_id','hasphoto','obm_uploading_id_mine');

        $qtable = $_SESSION['current_query_table'];
        
        if (in_array('obm_terms',array_keys($params))) {
            $subjects = iapi::load("/api/linnaeus/get_linnaeus_options/load.php?table={$qtable}");
            $subjects = $subjects[$qtable]->managed_columns ?? [];
            foreach ($subjects as $s) {
                $specials[] = "obm_terms_{$s->name}";
            }
        }

        $sqla = sql_aliases(1);

        if ($custom_table =='')
            $query_table = $_SESSION['current_query_table'];
        else
            $query_table = $custom_table;

        $st_col = st_col($query_table,'array');

        //deleting specials from dbcolist
        $dbcolist = dbcolist('array',$query_table);
        foreach ( $specials as $del ) {
            unset($dbcolist[$del]);
        }
        $txm_module = ($x_modules->_include('taxon_meta', 'is_submodule_enabled', ['TaxonGroups']));
        if ($txm_module) {
            $gr_cols = $x_modules->_include('taxon_meta', 'include_submodule', ['TaxonGroups','columns']);
            foreach ($gr_cols as $col) {
                $dbcolist[$col] = $col;
            }
        }

        
        $prefix = "";
        if (isset($params[$mc]['qtable_alias']) && $modules->is_enabled('additional_columns')) {
            $additional_columns = $modules->_include('additional_columns', 'return_columns');
            foreach ($additional_columns[0] as $ac) {
                $spl = explode('.',$ac);
                if ($spl[1] === $mc) {
                    $dbcolist[$mc] = $additional_columns[1][$ac];
                    $prefix = "{$spl[0]}.";
                    break;
                }
            }
        }
        if (array_key_exists($mc,$dbcolist)) {
            if ($txm_module && in_array($mc, $gr_cols)) {
                $_SESSION['taxon_join_filter'] = 1;
            }
            $ja = json_decode($post_val,true);
            // if post_val not json, just single value
            if (!count($ja))
                $ja = $post_val;

            if (is_array( $ja ))
                $v = implode(',',array_map('quote',$ja));
            else
                $v = quote($ja);

            if($v=='NULL') return;

            $w = $prefix."\"$mc\" IN ($v)";

            return $w;
        }

        // special search for date ranges
        if (in_array($mc,$specials)) {
            /*qids_exact_date	["2016-01-13"]
              qids_exact_from	["2016-01-13"]
              qids_exact_mfrom ["01-19"]
              qids_exact_mto	 ["01-28"]
              qids_exact_to	["2016-01-22"] */

            //ez megint egy erőltett dolog és nincs dokumentálva, hogy csak az első oszlopra működik!!!
            //MEG KELL VÁLTOZTATNI!!!!
            $DATE_C = $st_col['DATE_C'][0];

            //if (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$DATE_C,$m))
            //    $DATE_C = $m[2];


            if (is_object($post_val)) {
                $ja = json_decode($post_val,true);
            } 
            else {
                if (preg_match('/^\[["\d]/',$post_val))
                    $ja = json_decode($post_val,true);
                else
                    $ja = $post_val;
            }

            if (is_array( $ja )) {
                $v = implode(',',array_map('quote',$ja));
                $test_string = $v;
            } 
            else {
                $v = quote($ja);
                if ($ja == '') $test_string = 'NULL';
                else
                    $test_string = $ja;
            }


            if($v==NULL) {
                //??
                if (isset($_SESSION['exact_date_from']) and $mc == 'exact_to') $v = sprintf("'%s'",date("Y-m-d"));
                elseif (isset($_SESSION['exact_mdate_from']) and $mc == 'exact_mto') $v = sprintf("'%s'",date("m-d"));
                else
                    return;
            }
            if ($mc == 'exact_date' and $test_string!='NULL' and $DATE_C!='') {
                $w = "$v=date_trunc('day', \"$DATE_C\")";
            }
            elseif ($mc == 'exact_from' and $test_string!='NULL' and $DATE_C!='') {
                $w = "($DATE_C >= $v)";
            }
            elseif ($mc == 'exact_to' and $test_string!='NULL' and $DATE_C!='') {
                $w = "($DATE_C <= $v)";
            }
            elseif ($mc == 'exact_mfrom' and $test_string!='NULL') {
                $_SESSION['exact_mdate_from'] = $v;
                return;
            }
            elseif ($mc == 'exact_mto' and $test_string!='NULL' and $DATE_C!='') {
                $start = $_SESSION['exact_mdate_from'];

                unset($_SESSION['exact_mdate_from']);
                $m = array();
                if(preg_match('/([0-9]{2})-([0-9]{2})/',$start,$m)) {
                    $start_month = $m[1];
                    $start_day = $m[2];
                    $m = array();
                    if (preg_match('/([0-9]{2})-([0-9]{2})/',$v,$m)){
                        $end_month = $m[1];
                        $end_day = $m[2];
                        $month = array();
                        if ($start_month<=$end_month)
                            $range = range($start_month,$end_month);
                        else {
                            $range = array_merge(range($start_month,12),range(1,$end_month));
                        }

                        $nc = count($range);
                        $n = 0;
                        foreach($range as $i) {
                            $day_filter = '';
                            /*(extract(month from "datum_tol")=3 AND extract(day from "datum_tol")>2) OR
                            (extract(month from "datum_tol")=4) OR
                            (extract(month from "datum_tol")=5 AND extract(day from "datum_tol")<5)*/
                            if ($i==$start_month and $nc>1 and !$n) {
                                $day_filter = " AND extract(day from \"$DATE_C\")>=$start_day";
                            } 
                            elseif($i==$end_month and $nc>1 and $n==$nc-1) {
                                $day_filter = " AND extract(day from \"$DATE_C\")<=$end_day";
                            } 
                            elseif ($nc == 1 ) {
                                $day_filter = " AND extract(day from \"$DATE_C\")>=$start_day AND extract(day from \"$DATE_C\")<=$end_day";
                            }
                            $month[] = "(extract(month from \"$DATE_C\")=$i $day_filter)";
                            $n++;
                        }
                        $w = "(".implode(" OR ",$month).")";
                    }
                }
            } 
            elseif ($mc == 'obm_uploading_date' and $test_string!='NULL') {
                //only works if in the map file and the query_layer's query the uploading is LEFT JOINED
                $w = "date_trunc('day',uploading_date)=$v";
                $_SESSION['uploading_join_filter'] = 1;
            } 
            elseif ($mc == 'obm_uploader_user' and $test_string!='NULL') {
                //only works if in the map file and the query_layer's query the uploading is LEFT JOINED
                $w = "uploader_id=$v";
                $_SESSION['uploading_join_filter'] = 1;
            } 
            elseif ($mc == 'obm_uploading_id' and $test_string!='NULL') {

                if ($test_string == 'last_data') {
                    $cmd = sprintf("SELECT id FROM system.uploadings WHERE project_table=%s ORDER BY uploading_date DESC LIMIT 1",quote($_SESSION['current_query_table']));
                    $res = pg_query($ID,$cmd);
                    if (!pg_num_rows($res)) {
                        //log_action($cmd,__FILE__,__LINE__);
                        $v = 0;
                    } 
                    else {
                        $row = pg_fetch_assoc($res);
                        $v = $row['id'];
                    }
                } 
                else {
                    //overwrite qtable_alias based on uploading_id's table
                    $cmd = sprintf("SELECT project_table FROM system.uploadings WHERE project='%s' AND id=%s",PROJECTTABLE,$v);
                    $res = pg_query($ID,$cmd);
                    $row = pg_fetch_assoc($res);
                    $sqla = sql_aliases($row['project_table']);
                    $qtable_alias = $sqla['qtable_alias'];
                }
                $w = $sqla['qtable_alias'].".obm_uploading_id=$v";

            } 
            elseif ($mc == 'obm_uploading_id_mine' and $test_string!='NULL') {
                if ($test_string == 'last_data') {
                    $cmd = sprintf("SELECT id FROM system.uploadings WHERE project_table=%s AND uploader_id=%d ORDER BY uploading_date DESC LIMIT 1",quote($_SESSION['current_query_table']),$_SESSION['Trole_id']);
                    $res = pg_query($ID,$cmd);
                    if (!pg_num_rows($res)) {
                        //log_action($cmd,__FILE__,__LINE__);
                        $v = 0;
                    } 
                    else {
                        $row = pg_fetch_assoc($res);
                        $v = $row['id'];
                    }
                }
                $w = $sqla['qtable_alias'].".obm_uploading_id=$v";
            } 
            elseif ($mc == 'obm_id' and $test_string!='NULL') {
                if (is_quoted($test_string)) {
                    $test_string = unquote($test_string);
                }
                if (preg_match('/^last_data_?(\d*)?_?(\d*)?$/', $test_string, $m)) {

                    $num = $modules->get_params('box_load_last_data');
                    $limit = ($num!='' and is_numeric($num)) ? $num : 10;
                    if (is_numeric($m[1])) {
                        $limit = (int)$m[1];
                    }
                    
                    $offset = (is_numeric($m[2])) ? (int)$m[2] : 0;

                    $table = $_SESSION['current_query_table'];
                    $cmd = "SELECT ARRAY_TO_STRING(array( SELECT obm_id FROM $table ORDER BY obm_id DESC LIMIT $limit OFFSET $offset ),',') as id";
                    $res = pg_query($ID,$cmd);
                    $row = pg_fetch_assoc($res);
                    $v = $row['id'];
                }
                $qtable_alias = $sqla['qtable_alias'];
                //obm_id query : map apply text query over spatial query
                $w = sprintf("$qtable_alias.\"obm_id\" IN (%s)",$v);

            } 
            elseif ($mc == 'hasphoto' and $test_string!='NULL') {
                $w = 'obm_files_id IS NOT NULL';
            } 
            elseif (substr($mc,0,9) == 'obm_terms' and $test_string != 'NULL') {
                if (count($ja) == 1) {
                    $w = '';
                }
                else {
                    $relation = ($ja[0] == 'OR') ? '&&' : '@>' ;
                    $v = substr($v,strpos($v,',') + 1);
                    $subject = substr($mc,10);
                    // faj_ids DOES NOT EXISTS!!!!
                    $w = "linn.{$subject}_ids $relation ARRAY[$v]";
                }
            }


        }

        return $w;

    }
}

class data_query {
    private $defaults = [
        'database' => 'gisdata',
        'schema' => 'public',
        'table' => '',
        'table_alias' => '',
        'columns' => [],
        'filter' => [],
        'frel' => '=',
        'filter_rel' => 'AND',
        'distinct' => false,
        'where' => '',
        'orderby' => '',
        'debug' => false,
    ];


    function __construct($args) {
        if (!isset($args['table']))
            $this->defaults['table'] = $_SESSION['current_query_table'];
        foreach ($this->defaults as $prop => $value) {
            $this->args[$prop] = (!isset($args[$prop])) ? $value : $args[$prop] ;
        }
        if ($this->args['debug']) log_action($this->args,__FILE__,__LINE__);
    }

    public function getData() {
        global $ID, $BID;
        $distinct = ($this->args['distinct']) ? 'DISTINCT' : '';

        $aliases = $this->isAssoc($this->args['columns']);
        $columns = [];
        foreach ($this->args['columns'] as $alias => $col_name) {
            $columns[] = ($aliases) ? "$col_name AS $alias" : $col_name;
        }
        $relation = (isset($this->args['filter']['relation'])) ? $this->args['filter']['relation'] : null;
        $filter = (($this->args['where'] === '') && !empty($this->args['filter'])) ?
            "WHERE " . prepareFilter($this->args['filter']['column'], $this->args['filter']['value'],$relation) :
            '';

        if ($this->args['debug']) log_action("FILTER: $filter",__FILE__,__LINE__);

        //ha a where jon parameterkent be lehetne egybol tenni

        $orderby = ($this->args['orderby'] !== '') ? "ORDER BY \"{$this->args['orderby']}\"" : "";

        $rules = $this->sensitivity_check();

        $cmd = sprintf('SELECT %1$s %2$s FROM %3$s.%4$s %5$s %8$s %6$s %9$s %7$s;', $distinct, implode(', ',$columns), $this->args['schema'], $this->args['table'], $this->args['table_alias'], $filter, $orderby, $rules['join'], $rules['filter']);

        if ($this->args['debug']) log_action($cmd,__FILE__,__LINE__);

        $conn = ($this->args['database'] === 'biomaps') ? $BID : $ID;
        $res = pg_query($conn, $cmd);
        $results = [];
        while($row = pg_fetch_assoc($res)) {
            $results[] = $row;
        }

        return $results;
    }

    private function isAssoc(array $arr) {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    private function sensitivity_check() {
        $tid = (!isset($_SESSION['Tid'])) ? 0 : $_SESSION['Tid'];
        $tgroups = (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='') ? 0 : $_SESSION['Tgroups'];
        $st_col = st_col($this->args['table'],'array');

        $rules['filter'] = '';
        $rules['join'] = '';
        if ($st_col['USE_RULES']) {
            if (!has_access('master')) {
                $con = (($this->args['where'] === '') && !count($this->args['filter'])) ? 'WHERE' : 'AND';
                $rules['filter'] = " $con ( (sensitivity::varchar IN ('1','no-geom','3') AND ARRAY[$tgroups] && read)  OR sensitivity::varchar IN ('0','public') ) ";
                $rules['join'] = sprintf("LEFT JOIN %s_rules r ON (obm_id=r.row_id AND data_table = %s)",PROJECTTABLE,quote($this->args['table']));
            }
        }
        return $rules;
    }
}

class filter_input extends input_field {
    private $defaults = [
        'table_alias' => '',
        'optionsSchema' => 'public',
        'valueColumn' => '',
        'labelColumn' => '',
        'filterColumn' => '',
        'filterValue' => '',
        'labelAsValue' => false,
        'allOptions' => false,
        'orderby' => '',
        'html_id' => '',
        'size' => '',
        'multiple' => '',
        'autocomplete' => false,
        'empty_line' => true,
        'access_class' => '',
        'tdata' => [],
        'onlyoptions' => false,
        'filter' => '',
        'boolen' => false,
        'format_query' => '',
        'data' => [],
        'reference' => '',
    ];

    function __construct($args) {

        // reading the arguments
        $this->defaults['optionsTable'] = $_SESSION['current_query_table'];
        foreach ($this->defaults as $prop => $value) {
            $this->args[$prop] = (!isset($args[$prop])) ? $value : $args[$prop] ;
        }

        //preparing the columns to be queried
        $columns['value'] = (($this->args['labelAsValue']) && ($this->args['labelColumn'] !== '')) ? $this->args['labelColumn'] : $this->args['valueColumn'];
        $columns['label'] = ($this->args['labelColumn'] !== '') ? $this->args['labelColumn'] : $this->args['valueColumn'];

        if ($this->args['allOptions']) {
            $stcol = st_col("{$this->args['optionsSchema']}.{$this->args['optionsTable']}",'array');
            $columns['id'] = (!isset($stcol['ID_C']) || $stcol['ID_C'] == '') ? 'obm_id' : $stcol['ID_C'];
        }

        //html_id
        if ($this->args['html_id'] == '') {
            if ($this->args['reference'] !== '') {
                $this->args['html_id'] = preg_replace('/\./','-',$this->args['reference']);
            }
            else
                $this->args['html_id'] = implode('-',[$this->args['table_alias'],$this->args['column']]);
        }
        $input_elem_arguments = [
            'id' => $this->args['html_id'],
            'tdata' => $this->args['tdata'],
            'onlyoptions' => $this->args['onlyoptions'],
            'class' => "qf {$this->args['access_class']}",
            'multiple' => $this->args['multiple'],
            'size' => $this->args['size'],
        ];

        if ($this->args['autocomplete']) {
            $input_elem_arguments['class'] = 'qf qfautocomplete';
        }
        else {

            // imporved filtering

            if (($this->args['filterColumn'] !== '') and ($this->args['filterValue']) === '') {
                //this case is for nested children elements, which do not have eny data at inital load
            }
            else {
                //getting the data of select input field
                $filter = (($this->args['filterColumn'] !== '') and ($this->args['filterValue']) !== '') ?
                     ['column' => preg_replace('/-/','.',$this->args['filterColumn']), 'value' => $this->args['filterValue'],'relation' => NULL] :
                     [];
                     
                $q = new data_query([
                    'schema' => $this->args['optionsSchema'],
                    'table' => $this->args['optionsTable'],
                    'columns' => $columns,
                    'orderby' => $columns['label'],
                    'distinct' => true,
                    'filter' => $filter,
                    'debug' => false,
                ]);

                // excluding null values
                $data = array_filter($q->getData(), function($d) {
                    return $d['value'] !== null;
                });

                for ($i = 0; $i < count($data); $i++) {
                    if (isset($data[$i]['id'])) {
                        $data[$i]['tdata'] = ['id' => $data[$i]['id']];
                        unset($data[$i]['id']);
                    }
                }
                $input_elem_arguments['data'] = $data;
            }
            $input_elem_arguments['element'] = 'select';

        }

        parent::__construct($input_elem_arguments);
    }

}

?>
