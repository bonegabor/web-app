<?php
#' ---
#' Module:
#'   results_asKML
#' Files:
#'   [results_asKML.php]
#' Description: >
#'   Export results as KML
#' Methods:
#'   [print_data, export_as]
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class results_asKML extends module {
    var $error = '';
    var $retval = '';
    
    function __construct($action = null, $params = null,$pa = array()) {
        global $ID;
        
        $this->params = $this->split_params($params);

        if ($action) {
            $this->retval = $this->$action($this->params,$pa);
        }
    }

    protected function moduleName() {
        return __CLASS__;
    }
    
    public function print_data($params,$pa) {

        $st_col = st_col($_SESSION['current_query_table'],'array');
        if (!isset($st_col['GEOM_C']) or $st_col['GEOM_C']=='') {
            echo common_message("fail", "No geometry column defined.");
            return;
        }
        
        $de = new DataExport((int)$pa['de_id']);
        extract($this->read_params());
        
        $col_idxs = $this->get_column_indexes(array_keys($de->colnames), $st_col['GEOM_C'], $SPECIES_C, $NUM_IND_C);
        
        if (!$col_idxs) {
            echo common_message("error", "Column missing");
            return;
        }
        
        $kmltmp = OB_TMP . $de->filename . ".kml.csv";
        if (($handle = fopen($de->tmpfile(), "r")) && ($out = fopen($kmltmp, 'w'))) {
            
            fputcsv($out, ['wkt', 'Name', 'Description']);
            
            while (($row = fgetcsv($handle, $de->maxlength)) !== FALSE) {
                $tmprow = [];
                
                $tmprow[] = $row[$col_idxs[0]]; // wkt
                $tmprow[] = $row[$col_idxs[1]]; // Name
                $tmprow[] = implode(' | ', array_map(function ($val) {
                    return defined($val) ? constant($val) : $val;
                }, array_intersect_key($row, array_flip(array_slice($col_idxs, 2))))); // DEscription imploded
                
                fputcsv($out, $tmprow);
            }
            fclose($handle);
            fclose($out);
            
            $filename = $de->filename . '.kml';
            $file_path = OB_TMP . $filename;
            //itt még ki kell dolgozni, hogy hogyan jelenjenek meg a mezők a kml-ben, de most működik
            $res = exec("ogr2ogr -a_srs EPSG:4326 -f KML /vsistdout/ $kmltmp -lco ENCODING=UTF-8 | grep -Ev 'ExtendedData|SimpleField|SimpleData|SchemaData' > $file_path" );
            unlink($kmltmp);
            
            if (file_exists($file_path)) {
                //    track_download($track_id);
                # header definíció!!!
                header("Content-type: text/xml"); 
                header("Content-Disposition: attachment; filename=$filename");
                header("Content-length: " . filesize($file_path));
                header("Pragma: no-cache"); 
                header("Expires: 0"); 
                $handle = fopen($file_path, "rb");
                while (!feof($handle)){
                    echo fread($handle, 8192);
                }
                fclose($handle);
                
                $de->increment_dl_count();
                
                unlink($file_path);
                return;
            }
            
            echo "Error. See the system log.";
            return;
            
        } else {
            echo "Error. See the system log.";
            return;
        }

    }

    private function read_params() {
        $st_col = st_col($_SESSION['current_query_table'],'array');
        
        if (isset($this->params['name'])) {
            $SPECIES_C =  $this->params['name'];
        }
        else {
            if (!isset($st_col['SPECIES_C']) or $st_col['SPECIES_C']=='') {
                $SPECIES_C = "'SPECIES_C'";
            }
            else {
                $SPECIES_C = (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['SPECIES_C'],$m)) ? $m[2] : $st_col['SPECIES_C'];
            }
        }
        
        // special case obm_taxon - for multilingual taxon names
        if ($SPECIES_C === 'obm_taxon') {
            $SPECIES_C = $st_col['SPECIES_C'];
        }
        
        if (isset($this->params['description'])) {
            $NUM_IND_C = (is_array($this->params['description'])) ? $this->params['description'] : [$this->params['description']];
        }
        else {
            if (!isset($st_col['NUM_IND_C']) or  $st_col['NUM_IND_C']=='') {
                $NUM_IND_C = [];
            }
            else {
                $NUM_IND_C = (preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$st_col['NUM_IND_C'],$m)) ? [$m[2]] : [$st_col['NUM_IND_C']];
            }
        }
        
        return compact('SPECIES_C', 'NUM_IND_C');
    }
    
    private function get_column_indexes($header, $GEOM_C, $SPECIES_C, $NUM_IND_C) {
        $indexes = [];
        foreach (array_merge([$GEOM_C, $SPECIES_C], $NUM_IND_C) as $col) {
            if ($idx = array_search($col, $header)) {
                $indexes[] = $idx;
            }
            else {
                return false;
            }
        }
        return $indexes;
    }
}
?>
