<?php
#' ---
#' Module:
#'   box_load_coord
#' Files:
#'   [box_load_coord.php]
#' Description: >
#'   Show given coordinates position on the map
#' Methods:
#'   [print_panelbox, print_js, map_js]
#' Examples: >
#'   {
#'     "wgs84": "4326",
#'     "google": "900913",
#'     "eov": "23700"
#'   }
#' Module-type:
#'   table
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   1.0
class box_load_coord extends module {

    public $error = '';
    function __construct($action = null, $static_params = null, $dynamic_params = array()) {
        global $BID;

        if ($action)
            $this->retval = $this->$action($static_params,$dynamic_params);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }

    public function print_panelbox ($params='') {

        if ($params=='')
            $params = "WGS84 (epsg 4326)=4326";

        $p = $this->split_params($params,'pmi');
        $epsg_list = array();
        foreach ($p as $key=>$value) {
            $epsg_list[] = $key.'::'.$value;
        }
        
        $options = selected_option($epsg_list,'');
        $sout = "<select class='' id='epsg_set'>$options</select> ";
        $sout .= "Lon, Lat: <input type='text' size='12' class='custom-mouse-position' style='vertical-align:middle;max-height:26px;border:1px solid lightgray;border-radius:3px;padding:4px;' id='coord_query_xy'> ";
        $sout .= "<button id='coord_query' class='button-gray button-small pure-button'><i class='fa-map-pin fa-lg fa'></i></button>";
        return sprintf("%s",$sout);
    }

    public function map_js ($params) {
        return '';
    }

    public function print_js ($params) {
        return '
$(document).ready(function() {
    //replace mouse-position-mcbox
    //$("#mouse-position-mcbox").hide();

    let inputProj = "EPSG:"+$("#epsg_set").val();
    if (inputProj == "EPSG:23700") {
        proj4.defs(inputProj, "+proj=somerc +lat_0=47.14439372222222 +lon_0=19.04857177777778 +k_0=0.99993 +x_0=650000 +y_0=200000 +ellps=GRS67 +towgs84=52.17,-71.82,-14.9,0,0,0,0 +units=m +no_defs");
        new ol.proj.proj4.register(proj4);
    }
    
    $("#coord_query").click(function(){

        markerLayer.getSource().clear();
        
        var XY = $("#coord_query_xy").val();
        var p = new ol.geom.Point(XY.split(","));


        p.transform(inputProj, map.getView().getProjection());

        const iconFeature = new ol.Feature({
            geometry: p,
            name: "LonLat Marker",
        });
        const iconStyle = new ol.style.Style({
            image: new ol.style.Icon({
                anchor: [0.5, 25],
                anchorXUnits: "fraction",
                anchorYUnits: "pixels",
                src: "js/img/marker.png"
            })
        });
        iconFeature.setStyle(iconStyle)
        markerLayer.getSource().addFeature(iconFeature);
        map.getView().setCenter(p.getCoordinates());
    });
    
    /*$("#map").on("change",".custom-mouse-position",function(){
        var e = $(this).text();
        if (e.trim()!="") {
            coords = e.split(", ");
            var lonlat = new ol.proj.fromLonLat([coords[0], coords[1]], inputProj );
            $("#coord_query_xy").val(lonlat[0] + "," + lonlat[1])
        }
    });*/
});';}
}
?>
