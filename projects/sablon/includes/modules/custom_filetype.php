<?php
#' ---
#' Module:
#'   custom_filetype
#' Files:
#'   [custom_filetype.php, custom_filetype.js]
#' Description: >
#'   Custom file preparation. E.g. observado style CSV
#'   $pa =$file = '',$filetype = null
#' Methods:
#'   [option_list, custom_read, print_js, adminPage, init, getMenuItem, custom_filetype]
#' Module-type:
#'   project
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   1.0
class custom_filetype extends module {
    
    public $error = '';
    
    function __construct($action = null, $params = null, $pa = array()) {
        global $BID;

        $this->params = $this->split_params($params);

        if ($action)
            $this->retval = $this->$action($this->params,$pa);
    }
    
    public function adminPage($params) {
        ob_start();
        $in_db = $this->get_imported_fts();
        $as_json = $this->list_dictionaries();
        
        $tbl = new createTable();
        $tbl->def(['tid'=>'','tclass'=>'resultstable']);
        $tbl->addHeader(array(t('str_imported'), t('str_file'), t('str_actions')));
        
        $importbtn = '<button type="button" title="Import from file" class="pure-button button-primary import-ft" id="%s"> <i class="fa fa-cloud-upload"></i> </button>';
        // TODO: export nincs befejezve
        $exportbtn = ''; //'<button type="button" title="Export to file" class="pure-button button-primary export-ft" id="%s"> <i class="fa fa-cloud-download"></i> </button>';
        
        foreach (array_unique(array_merge($in_db, $as_json)) as $ft) {
            $tbl->addRows([
                (in_array($ft, $in_db)) ? $ft : '-',
                (in_array($ft, $as_json)) ? $ft : '-',
                ((in_array($ft, $as_json)) ? sprintf($importbtn, $ft) : "") . ((in_array($ft, $in_db)) ? sprintf($exportbtn, $ft) : "")
            ]);
        }
        ?>
        
        <button class="module_submenu button-success button-xlarge pure-button" data-url="includes/project_admin.php?options=custom_filetype"> <i class="fa fa-refresh"></i> </button>
        <?php echo $tbl->printOut() ?>
        
        <?php
        return ob_get_clean();
    }
    
    private function list_dictionaries() {
        $dir = getenv('OB_LIB_DIR') . 'modules/private/custom_filetypes/';
        $filetypes = [];
        if (file_exists($dir)) {
            foreach (new FilesystemIterator($dir) as $fileinfo) {
                if ($fileinfo->getExtension() === 'json') {
                    
                    $filetypes[] = $fileinfo->getBasename('.json');
                }
            }
            
        }
        return $filetypes;
    }
    
    private function read_dictionary($filename) {
        $dir = getenv('OB_LIB_DIR') . 'modules/private/custom_filetypes/';
        
        $file = $dir . $filename . '.json';
        
        $content = (file_exists($file)) ? json_decode(file_get_contents($file), true) : [];
                    
        return $content;
    }
    
    public function ajax($params, $request) {
        
        if (isset($request['action']) && method_exists(__CLASS__, $request['action'])) {
            $method = $request['action'];
            echo $this->$method($params, $request);
        }
    }
    
    private function import_ft($params, $request) {
        $dict = $this->read_dictionary($request['ft']);
        extract($dict);
        $i = 0;
        $action_order = 1;
        if (!isset($file_type) || !isset($variant) || !isset($rules)) {
            return common_message('error', 'json error');
        }
        $cmd = [];
        while ($i < count($rules)) {
            extract($rules[$i]);
            if (!isset($from_column) || !isset($action)) {
                continue;
            }
            $form = $form ?? 0;
            $to_column = $to_column ?? 'NULL';
            
            $values = $values ?? ['NULL' => 'NULL'];
            
            foreach ($values as $from_value => $to_value) {
                $cmd[] = sprintf("INSERT INTO %s_custom_filetype (file_type, variant, from_column, action, to_column, from_value, to_value, form, action_order) VALUES (%s, %s, %s, %s, %s, %s, %s, %d, %d);",
                            PROJECTTABLE,
                            quote($file_type),
                            quote($variant),
                            quote($from_column),
                            quote($action),
                            quote($to_column),
                            quote($from_value),
                            quote($to_value),
                            (int)$form,
                            $action_order
                        );
                $action_order++;
            }
            foreach (['from_column', 'values', 'action', 'to_column', 'from_value', 'to_value', 'form'] as $var) {
                unset($$var);
            }
            $i++;
        }
        
        global $ID;
        
        $deleteCmd = sprintf("DELETE FROM %s_custom_filetype WHERE file_type = %s AND variant = %s;",
            PROJECTTABLE,
            quote($file_type),
            quote($variant)
        );
        if (!$res = pg_query($ID, $deleteCmd)) {
            return common_message('error', 'delete query_error');
        }
        if (!$res = pg_query($ID, implode('', $cmd))) {
            return common_message('error', 'insert query_error');
        }
        
        return common_message('ok','ok');
    }
    
    // TODO: export ezt nem fejezem most be mert most nem sürgős
    private function export_ft($params, $request) {
        global $ID;
        $ft = explode('.',$request['ft']);
        $file_type = $ft[0];
        $variant = $ft[1] ?? null;
        $t = $this->get_ft($file_type, $variant);
        
        //extracting unique rules
        $rules = array_map(function ($r) {
            $ca = explode('.', $r);
            return [
                'from_column' => $ca[0],
                'to_column' => $ca[1],
                'action' => $ca[2],
                'form' => $ca[3]
            ];
        }, array_values(array_unique(array_map(function ($r) {
            return "{$r['from_column']}.{$r['to_column']}.{$r['action']}.{$r['form']}";
        }, $t))));

        foreach ($rules as $rule) {
            $dict = array_filter($t, function ($row) use ($rule) {
                return (
                    $row['from_column'] === $rule['from_column'] && 
                    $row['to_column'] === $rule['to_column'] && 
                    $row['action'] === $rule['action'] &&
                    $row['form'] === $rule['form']
                );
            });
        }
        $exp  = compact('file_type', 'variant', 'rules');
    }
    
    private function get_imported_fts() {
        global $ID;
        $cmd = sprintf("SELECT DISTINCT file_type, variant FROM %s_custom_filetype", PROJECTTABLE);
        if (!$res = pg_query($ID,$cmd)) {
            $this->error = 'query error: ' . $cmd;
            return []; 
        }
        
        if (pg_num_rows($res) === 0) {
            return [];
        }

        $results = pg_fetch_all($res);
        
        $filetypes = array_map(function ($f) {
            return (isset($f['variant'])) ? $f['file_type'] . '.' . $f['variant'] : $f['file_type'];
        } , $results);
        
        return $filetypes;
    }
    
    public function getMenuItem() {
        return ['label' => 'Custom filetype', 'url' => 'custom_filetype' ];
    }

    protected function moduleName() {
        return __CLASS__;
    }
    
    public function init($params, $pa) {
        global $ID;
        
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_custom_filetype');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        $cmd = [];
        if ($result['exists'] == 'f') {
            $cmd[] = sprintf('CREATE SEQUENCE %1$s_custom_filetype_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;',PROJECTTABLE);
            $cmd[] = sprintf('CREATE TABLE "public"."%1$s_custom_filetype" (
                                "id" integer DEFAULT nextval(\'public.%1$s_custom_filetype_id_seq\') NOT NULL,
                                "file_type" character varying(32) NOT NULL,
                                "from_column" character varying(32) NOT NULL,
                                "action" character varying(32) NOT NULL,
                                "to_column" character varying(32),
                                "from_value" character varying(128),
                                "to_value" character varying(128),
                                "form" integer,
                                "action_order" integer,
                                "comments" character varying,
                                "variant" character varying(32),
                                CONSTRAINT "%1$s_custom_filetype_pkey" PRIMARY KEY ("id")
                            ) WITH (oids = false); ', PROJECTTABLE );

            if (!query($ID,$cmd)) {
                $this->error = __CLASS__ .' init failed: table creation error';
                return;
            }
        }
    }
    
    public function option_list($params) {
        global $ID;
        return $this->get_imported_fts();
    }

    private function get_ft($file_type, $variant = null, $form_id = null) {
        global $ID;
        
        $form_q = (is_numeric($form_id)) ? sprintf(" AND form IN (0, %d)", $form_id) : "";
        $variant_q = ($variant) ? sprintf(' AND variant = %s',quote($variant)) : "";
        
        $cmd = sprintf("SELECT from_column, action, to_column, from_value, to_value, form, action_order FROM %s_custom_filetype WHERE file_type = %s %s %s ORDER BY action_order;",PROJECTTABLE,quote($file_type), $variant_q, $form_q);
        if (!$res = pg_query($ID,$cmd)) {
            log_action('query error', __FILE__, __LINE__, __CLASS__, __FUNCTION__);
            return;
        }
        
        return pg_fetch_all($res);
        
    }
    
    public function custom_read($params, $pa) {
        global $ID;
        $file = $pa[0];
        $filetype = $pa[1];
        
        $ft = explode('.',$filetype);
        $form_id = $_SESSION['current_page']['form'];

        //build the transformation array from the database
        $t = $this->get_ft($ft[0], $ft[1] ?? null, $form_id);
        
        $j = 0;
        for ($i = 0, $l = count($t); $i < $l; $i++) {
            // TODO: what is this?
            if (
                $j > 0 && (
                    $transform[$j - 1]['from_column'] == $t[$i]['from_column'] && 
                    $transform[$j - 1]['action'] == $t[$i]['action'] && (
                        isset($transform[$j - 1]['to_column']) && 
                        $transform[$j - 1]['to_column'] == $t[$i]['to_column']
                    )
                )
            ) {
                $transform[$j - 1]['pairs'][$t[$i]['from_value']] = html_entity_decode($t[$i]['to_value']);
            }
            else {
                $transform[$j]['from_column'] = $t[$i]['from_column'];
                $transform[$j]['action'] = $t[$i]['action'];
                $transform[$j]['to_column'] = $t[$i]['to_column'];
                $transform[$j]['pairs'][$t[$i]['from_value']] = html_entity_decode($t[$i]['to_value']);
                $j++;
            }
        }

        $deleteRows = [];
        $has_complete_list = false;
        $complist = FALSE;
        $list_id = 0;

        for ($i = 0, $l = count($file); $i < $l; $i++) {

            for ($j = 0, $ll = count($transform); $j < $ll; $j++) {
                $column = $transform[$j]['from_column'];
                $action = $transform[$j]['action'];
                switch ($action) {

                case 'user name':
                case 'user_name':
                    $file[$i]['user_name'] = $_SESSION['Tname'];
                    break;

                case 'rename':
                    $newcol = $transform[$j]['to_column'];
                    $file[$i][$newcol] = $file[$i][$column];
                    unset($file[$i][$column]);
                    break;

                case 'filter':
                    $drop_rows = $transform[$j]['pairs'];
                    if (isset($drop_rows['_all_except'])) {
                        $to_keep = explode(',', $drop_rows['_all_except']);
                        if (!in_array($file[$i][$column], $to_keep)) {
                            $deleteRows[] = $i;
                        }
                    }
                    elseif (isset($drop_rows[$file[$i][$column]]))
                        $deleteRows[] = $i;
                    break;

                case 'drop': 
                    unset($file[$i][$column]);
                    break;

                case 'replace':
                    $toColumn = $transform[$j]['to_column'];

                    if (isset($file[$i][$column])) {
                        if (isset($file[$i][$toColumn])) {
                            if ($file[$i][$column] != '') {
                                $file[$i][$toColumn] =  $file[$i][$column];
                            }
                        }
                        else {
                            $file[$i][$toColumn] =  $file[$i][$column];
                        }
                    }
                    break;

                case 'append':
                    $toColumn = $transform[$j]['to_column'];
                    $toValue = (isset($file[$i][$toColumn])) ? $file[$i][$toColumn] : '';

                    $condition = $transform[$j]['pairs'];
                    $appendValue = '('.$column . ': ' . trim($file[$i][$column],'"') . ')';

                    if (isset($condition['_all'])) {
                        $toValue = ($file[$i][$column] != '') ? $toValue . $appendValue  : $toValue;
                    }
                    elseif (isset($condition['_all_except'])) {
                        $exceptions = explode(',', $condition['_all_except']);
                        $toValue = (!in_array($file[$i][$column], $exceptions)) ? $toValue . $appendValue  : $toValue;
                    }
                    else {
                        $toValue = (isset($condition[$file[$i][$column]])) ? $toValue . $appendValue  : $toValue;
                    }

                    $file[$i][$toColumn] = $toValue;
                    break;

                case 'coord':
                    $file[$i][$column] = str_replace(',','.',$file[$i][$column]);
                    break;

                case 'new column':
                case 'new_column':
                    $newcol = $transform[$j]['to_column'];
                    $pairs = $transform[$j]['pairs'];
                    
                    $from_value = trim($file[$i][$column],'"');

                    // if the new column was already populated, the firstly created values can be overridden if necessary
                    $else = (isset($file[$i][$newcol])) ?  $file[$i][$newcol] : '';
                    
                    if (isset($pairs['_else'])) {
                        $else = $pairs['_else'];
                    }
                    if (isset($pairs['_else_original'])) {
                        $else = $from_value;
                    }

                    if (isset($pairs[$from_value])) {
                        $to_values = explode(',',$pairs[$from_value]);

                        $file[$i][$newcol] = $to_values[0];

                        /* conditional values based on other columns value. 
                         * eg.: str_recently_fledged_dawny_young,age=str_juvenile:str_recently_fledged_juvenile,age=str_pull:str_dawny_young
                         */
                        if (isset($to_values[1])) {
                            for ($k = 1, $m = count($to_values); $k < $m; $k++) {
                                $cond = preg_split("/[:=]+/",$to_values[$k]);
                                if (count($cond) != 3) {
                                    log_action("conditions not formated properly at column: $column, value: $from_value!",__FILE__,__LINE__);
                                    break;
                                }
                                if ($file[$i][$cond[0]] == $cond[1]) {
                                    $file[$i][$newcol] = $cond[2];
                                    break;
                                }
                            }
                        }
                    }
                    else 
                       $file[$i][$newcol] = $else;

                       //log_action($file[$i][$newcol]);
                    break;

                case 'complete list':
                case 'complete_list':
                    $remarks = preg_split("/[\s,.]+/",trim($file[$i][$column],'"'));
                    $pairs = $transform[$j]['pairs'];

                    if (isset($pairs['_single'])) {

                        $only_duration = TRUE;
                        $has_complete_list = true;

                    }

                    if (isset($pairs['_multiple']) || isset($pairs['_alternative']) || isset($pairs['_alternative_noduration'])) {

                        $dur = TRUE;
                        if (isset($pairs['_alternative_noduration']))
                            $dur = FALSE;

                        $list[] = $i;

                        if (!$complist)
                            $this->complist_drop($file,$list,$dur);

                        if (in_array('stop',array_map('strtolower',$remarks))) {
                            // Ha hiányzik a start, akkor törölje a megkezdett teljes listát
                            if ($complist) {
                                $this->complist_drop($file,$list,$dur);
                            }
                            $complist = TRUE;
                            $list[] = $i;
                        }


                        if (in_array('start',array_map('strtolower',$remarks)) && $complist) {
                            $has_complete_list = true;
                            $this->complist_validate($file,$list,$list_id,$dur);
                            $complist = FALSE;
                        }

                        // Ha hiányzik a start az utolsó listából, akkor törölje a megkezdett teljes listát
                        if ($i == $l-1 && $complist)
                            $this->complist_drop($file,$list,$dur);
                    }
                    break;
                }
            }
        }

        if ($has_complete_list) {
            if ($list_id == 0) {
                $this->only_duration($file,$dur);
            }
            if (isset($only_duration) && $only_duration) {
                $this->only_duration($file);
            }
        }


        foreach ($deleteRows as $del) {
            unset($file[$del]);
        }

        unset($deleteRows);
        return array_values($file);
    }

    private function only_duration(&$file,$dur) {

        $starttime = new DateTime($file[count($file)-1]['exact_time']);
        $endtime = new DateTime($file[0]['exact_time']);
        $duration = $endtime->diff($starttime);

        for ($i = 0, $l = count($file); $i < $l; $i++) {
            $file[$i]['time_of_start'] = $starttime->format('H:i');
            $file[$i]['time_of_end'] = $endtime->format('H:i');
            if ($dur)
                $file[$i]['duration'] = $duration->format("%H:%I");
        }

    }
    
    private function complist_validate(&$file, &$list, &$list_id, $dur) {

        $list_id++;

        $starttime = new DateTime($file[end($list)]['exact_time']);
        $endtime = new DateTime($file[reset($list)]['exact_time']);
        $duration = $endtime->diff($starttime);

        foreach ($list as $row) {
            $file[$row]['time_of_start'] = $starttime->format('H:i');
            $file[$row]['time_of_end'] = $endtime->format('H:i');
            if ($dur)
                $file[$row]['duration'] = $duration->format("%H:%I");
            $file[$row]['complete_list'] = 1;
            $file[$row]['list_id'] = $list_id;
        }
        $list = [];
    }

    private function complist_drop(&$file, &$list, $dur) {
        foreach ($list as $row) {
            $file[$row]['time_of_start'] = '';
            $file[$row]['time_of_end'] = '';
            if ($dur) 
                $file[$row]['duration'] = '';
            $file[$row]['complete_list'] = 0;
            $file[$row]['list_id'] = '';
        }
        $list = [];
    }
}
?>
