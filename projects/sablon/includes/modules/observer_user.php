<?php
#' ---
#' Module:
#'   observer_user
#' Files:
#'   [observer_user.php]
#' Description: >
#'   ???
#' Methods:
#'   [moduleName, getMenuItem, adminPage, displayError, ajax, init, print_js]
#' Module-type:
#'   project
#' Author:
#'   Bóné Gábor <gabor.bone@milvus.ro>
#' Version:
#'   0.1
class observer_user extends module {
    
    public $error = '';
    
    function __construct($action = null, $params = null, $pa = array()) {
        global $BID;

        $this->params = explode(';',$params);

        if ($action)
            $this->retval = $this->$action($params,$pa);
    }
    
    protected function moduleName() {
        return __CLASS__;
    }
    
    public function init($params, $pa) {
        global $ID, $BID;

        // A list_manager nem general module, és itt nem tudjuk eldönteni, hogy melyik táblára kéne lekérdezni...
        #if (!$m->is_enabled('list_manager')) {
        #    $this->error = 'The list_manager module not enabed. Please enable it!';
        #    return ['error' => $this->error];
        #}
        //checking terms table existence
        $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.tables WHERE table_schema = 'public' AND table_name = '%s_users');", PROJECTTABLE);
        $res = pg_query($ID,$cmd);
        $result = pg_fetch_assoc($res);

        if ($result['exists'] == 'f') {
            pg_query($ID, "BEGIN;");
            $cmd = sprintf("CREATE TABLE %1\$s_users ( id integer NOT NULL, username character varying(100), institute character varying(255), email character varying(64), role_id integer NOT NULL, CONSTRAINT %1\$s_users_email_key UNIQUE (email) ) WITH (oids = false);", PROJECTTABLE);

            if (!pg_query($ID,$cmd)) {
                $this->error = 'observer_user init failed: table creation error';
                return ['error' => $this->error];
            }
            
            $cmd = sprintf("SELECT u.id, u.username, u.institute, u.email, pr.role_id FROM project_roles pr LEFT JOIN users u ON u.id = pr.user_id WHERE pr.project_table = 'teszt' AND pr.user_id IS NOT NULL;", PROJECTTABLE);
            if (!$res = pg_query($BID,$cmd)) {
                $this->error = 'observer_user init failed: table creation error';
                return ['error' => $this->error];
            }
            $users = array_map(function ($user) {
                return sprintf("(%s)", implode(',', array_map('quote', $user)));
            }, pg_fetch_all($res) );
            
            $cmd = sprintf("INSERT INTO %s_users (id, username, institute, email, role_id) VALUES %s;", PROJECTTABLE, implode(',', $users));
            if (!$res = pg_query($ID,$cmd)) {
                $this->error = 'observer_user init failed: inserting users failed';
                return ['error' => $this->error];
            }
            
            $cmd = sprintf("SELECT EXISTS ( SELECT 1 FROM information_schema.columns WHERE table_name = '%s_terms' AND column_name = 'user_id');", PROJECTTABLE);
            $res = pg_query($ID,$cmd);
            $result = pg_fetch_assoc($res);
            if ($result['exists'] == 'f') {
                $cmd = sprintf("ALTER TABLE %s_terms ADD COLUMN user_id INTEGER NULL;", PROJECTTABLE);
                if (!$res = pg_query($ID,$cmd)) {
                    $this->error = 'observer_user init failed: inserting users failed';
                    return ['error' => $this->error];
                }
            }
            pg_query($ID, "END;");
        }
        
    }
    
    public function adminPage($params, $pa) {

        if (!has_access('master')) return;

        return "<div>observer_user module admin page</div>";
    }

    public function getMenuItem() {
        return ['label' => t('str_observer_user'), 'url' => 'observer_user' ];
    }
}
?>
