<?php
#' ---
#' Module:
#'   results_asCSV
#' Files:
#'   [results_asCSV.php, results_asCSV.sql]
#' Description: >
#'   Export results as CSV file
#' Methods:
#'   [print_data, export_as]
#' Examples: >
#'   {
#'   "sep":";",
#'   "quote":"'",
#'   "encoding":"latin2"
#'   }
#' Module-type:
#'   table
#' Dependencies:
#'   results_buttons
#' Author:
#'   banm@vocs.unideb.hu
#' Version:
#'   2.0
class results_asCSV extends module {
    var $error = '';
    var $retval;
    
    var $sep = ',';
    var $q = '"';
    var $enc = "";
    
    function __construct($action = null, $params = null,$pa = array()) {
        global $BID;
        
        $this->params = $this->split_params($params);
        
        foreach($this->params as $key=>$value) {
            if ($key === 'sep')
                $this->sep = $value;
            elseif($key === 'quote')
                $this->q = $value;
            elseif($key === 'encoding')
                $this->enc = $value;
        }
        
        if ($action) {
            $this->retval = $this->$action($this->params,$pa);
        }
    }

    protected function moduleName() {
        return __CLASS__;
    }
    
    /**
     * This functions prints out the csv file for download
     **/
    public function print_data($params,$pa) {

        $de = new DataExport((int)$pa['de_id']);
        if (isset($de->export_options['sep'])) {
            $this->sep = $de->export_options['sep'];
        }
        if (isset($de->export_options['quote'])) {
            $this->q = $de->export_options['quote'];
        }
        if (isset($de->export_options['encoding'])) {
            $this->enc = $de->export_options['encoding'];
        }
        
        # header definíció!!!
        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=\"{$de->filename}.csv\";");
        header("Expires: -1");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);

        //easter eggs
        if (date('m-d')=='12-04' and isset($_GET['42'])) {
            if (file_exists(getenv('OB_LIB_DIR').'ascii')) {
                $f = getenv('OB_LIB_DIR').'ascii';
                $str = array();
                $handle = fopen($f, "r");
                if ($handle) {
                    $i = 0;
                    while (($line = fgets($handle)) !== false) {
                        if ($line!="\n") {
                            $str[$i] .= $line;
                        } else {
                            $i++;
                        }
                    }
                    fclose($handle);
                }
                echo base64_decode($str[array_rand($str)]);
                exit;
            }
        }
        
        if (($handle = fopen($de->tmpfile(), "r")) && ($out = fopen('php://output', 'w'))) {
            
            $header_row = $this->iconv_row(array_values($de->colnames));
            
            fputcsv($out, $header_row, $this->sep, $this->q);
            
            while (($data = fgetcsv($handle, $de->maxlength))) {
                $data = $this->iconv_row($data);
                
                fputcsv($out, $data, $this->sep, $this->q);
            }
            
            fclose($handle);
            fclose($out);
            
            $de->increment_dl_count();
            return;
        }
        return false;
    }

    private function iconv_row($row) {
        if ($this->enc != "") {
            return array_map(function ($cell) {
                return iconv("UTF-8", $this->enc, $cell);
            }, $row);
        }
        return $row;
    }
}
?>
