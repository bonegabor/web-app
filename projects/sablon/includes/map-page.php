<?php

    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

    if (!isset($_SESSION['current_query_table'])) {
        $_SESSION['current_query_table'] = PROJECTTABLE;
    }

    #debug('include map-page.php',__FILE__,__LINE__);

    $filter = "";

    # This is an ugly hack of modules output order, would be better to pass this order argument to modules, and get that here somehow
    $box_order = array('grid_view','text_filter','text_filter2','box_load_selection','box_load_last_data', 'box_custom');
    
    $boxes = array();

    // Module hook: print_box
    // print map side box (search box)
    foreach (array_intersect($box_order, $modules->which_has_method('print_box')) as $mm) {
        if ($f = $modules->_include($mm,'print_box',[],true)) {
            $boxes[$mm] = $f;
        }
    }
    // Module hook: print_panelbox
    // print map bottom panel box (function box)
    foreach ($x_modules->which_has_method('print_box') as $mm) {
        if ($f = $x_modules->_include($mm,'print_box',[],true)) {
            $boxes[$mm] = $f;
        }
    }

    $bk = array_keys($boxes);
    foreach ($box_order as $bo) {
        if (array_search($bo,$bk)!==false) {
            $filter .= $boxes[$bo];
            unset($boxes[$bo]);
        }
    }
    # Add the unordered boxes
    foreach ($boxes as $b)
        $filter .= $b;

    $button = 0;
    // Module hook: query_button
    // ...
    foreach ($modules->which_has_method('query_button') as $module)  {
        $button++;
    }

    if ($button) {

        $filter .= "<script>
        $(document).ready(function() {
            $('#mapfilters').append('<button class=\"pure-button button-xlarge button-href pure-u-1\" id=\"mapfilters-roller\"><i class=\"fa fa-angle-double-down\"></i></button>');
            $('#bheader').append('<button class=\"pure-button button-xlarge button-href\" id=\"mapfilters-show\"><i class=\"fa fa-angle-double-up\"></i></button>');
            $('#mapfilters').on('click','#mapfilters-roller',function(){
                $('#mapfilters').hide();
                $('#mapfilters-show').show('slow');
                $('#mapbox').show();
            });
            $('#bheader').on('click','#mapfilters-show',function(){
                $('#mapfilters').show();
                $('#mapfilters-show').hide('slow');
                $('#mapfilters-roller').show('slow');
                $('#mapbox').hide();
            });
        });
        var geometry_query_obj;
        var geometry_query_neighbour = 0;
        var geometry_query_session = 0;
        var geometry_query_selection = 0;
        var skip_loadQueryMap_customBox = 0;
        var text_filter_enabled = 0;
        var last_data_enabled = 0;

        /* Text query with text filter box module 
         * */
        $('#mapfilters').on('click','#sendQuery',function(){
            $('#navigator').hide();
            //vector layer destroy features
            vectorLayer.getSource().clear();
            markerLayer.getSource().clear();
            $('#navigator').hide();

            if (text_filter_enabled) {
                myQVar = getParamsOf_text_filter();
            }
            
            // geometry selection
            if (geometry_query_session!=0) {
                myQVar.geom_selection = \"session\";
                myQVar.neighbour = geometry_query_neighbour;
                myQVar.ce = JSON.stringify(geometry_query_obj);
            }
            
            // box load last data
            /* modules/box_load_selection.php
             * Box filter
             * query last data
             * last upload, last 10 rows, ...
             * */
            if (last_data_enabled) {
                var qname = ''
                qname = $('#last_data_query_option').val();
                if (qname != '')
                    myQVar['qids_' + qname] = 'last_data';

            }

            // shared polygons
            if (geometry_query_selection!=0) {
                myQVar.geom_selection = 'shared_polygons';
                myQVar.spatial_function = $('input[name=\"spatial_function[]\"]:checked').map(function() {
                    return $(this).val();
                }).get();
            }

            if ($('#ignoreJoins').prop('checked') === true)
                var ignoreJoins = $('#ignoreJoins').val();
            else
                var ignoreJoins = 0;

            myQVar.ignoreJoins = ignoreJoins;
            if ( skip_loadQueryMap_customBox ) {

                var customBox_elements = $('.custom_box');

                $.when.apply($, customBox_elements.map(function(item) {
                    var custom_box_name = $(this).val();
                    var custom_function_name = 'custom_' + custom_box_name;
                    var oVar = window[custom_function_name]();
                    
                    myQVar = {...myQVar, ...oVar }
                })).then(function() {
                    // create map with custom box parameters
                    loadQueryMap(myQVar);
                    // all ajax calls done now
                });
            }

            // create map
            if (skip_loadQueryMap_customBox==0) {
                loadQueryMap(myQVar);
            }
        });
</script>";
        
        // this is not a text filter module thing!!!!
        $sqla = sql_aliases(1);
        if ($sqla['joins'])
            $ignore_joins_tr = "<tr><td colspan=2 class='title'><input id='ignoreJoins' value=1 type='checkbox' style='vertical-align:bottom'> ignore table JOINS</td></tr>";
        else
            $ignore_joins_tr = "<tr><td colspan=2><input id='ignoreJoins' value=0 type='hidden'></td></tr>";

        $filter .= "<table class='mapfb'>
            <tr><td colspan=2 class='title'><button class='button-gray button-success pure-button pure-u-1' style='font-size: 200%;' id='sendQuery'><i class='fa-search fa'></i> ".str_query."</button></td></tr>
            <tr><td colspan=2 class='title'><button id='clear-filters' class='pure-button button-href' style='float:left'><i class='fa fa-eraser'></i> ".str_clear_filters."</button></td></tr>
            $ignore_joins_tr
            </table>";
            
        //$t->cell("<button id='select_with_shared_geom' class='button-gray button-xlarge pure-button'><i class='fa-search fa'></i> ".str_query."</button>",2,'title');
        //$t->cell("<button id='last_data_query' class='button-gray button-xlarge pure-button'><i class='fa-search fa'></i> ".str_query."</button>",2,'title');
    }

    
    if(defined('ZOOM_WHEEL_ENABLED') and constant("ZOOM_WHEEL_ENABLED")!='false') {
        $zw_checked = 'checked';
    } else {
        $zw_checked = '';
    }
    if (isset($_COOKIE[ "wz" ])) {
        if ($_COOKIE[ "wz" ]==='true') {
            $zw_checked = 'checked';
        } else {
            $zw_checked = '';
        }
    }

    $buff = CLICK_BUFFER;

    //<img src='http://localhost/cgi-bin/mapserv?LAYER=dinpi_points&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&MAP=/var/www/html/biomaps/projects/dinpi/private/private.map'>
    $legend = "";
    if (SHOW_LEGEND and isset($_SESSION['ms_layer'])) {
        $legend .= "<div id='legend'>";
        if (defined('CUSTOM_LEGEND_IMG'))
            $legend .= "<img src='$protocol://".URL."/".CUSTOM_LEGEND_IMG."'>";
        else
            $legend .= "<img src='$protocol://".PRIVATE_MAPSERV."?LAYER={$_SESSION['ms_layer']}&FORMAT=image/png&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetLegendGraphic&MAP=PMAP'>";
        $legend .= "</div>";
    } 

    $out = "
    <div id='mapholder'>
    <div class='mapholder-cell' id='mapbox'>$legend
        <div id='map' class='maparea'></div> 
        <div id='mapcontrols'>
<div id='mouseXY'></div>
            <div class='mcbox' id='map-zoom-box'>
                <label style='display:inline-block;vertical-align:bottom'>
                    <input type='checkbox' name='mouse-wz' id='wz' $zw_checked onclick='toggleWZ(this);' />
                    <span title='".str_mouse_zoom."' style='background-image:url($protocol://".URL."/css/img/mouse_wheel_zoom-onoff.png);width:24px;height:22px;background-repeat:no-repeat;display:block;'></span>
                </label>
            </div>
            <div class='mcbox'> <button id='reset-map' class='button-small pure-button button-href'><i class='fa fa-refresh'></i> ".str_reset_map."</button></div>
            ";

    $out .= "<div class='mcbox' id='map-tools-box' style='position:relative'>";
    $out .= "   <div style='display:none;position:absolute;top:24px;width:400px' id='spatial-query-settings-box'>
                    <div class='mcbox pure-form'>
".str_select_buffer_size.": <input style='vertical-align:middle' class='pure-input' type='range' id='bufferslide' min='0' max='1000' value='$buff'>&nbsp;<input type='text' id='buffer' value='$buff' style='width:5em;border:1px solid lightgray;padding:4px;border-radius:3px;max-height:26px'> ".str_meter."</div>
                </div>";

    $out .= "<span style='display:inline-block'>&nbsp;".mb_convert_case(str_spatial_query, MB_CASE_UPPER, "UTF-8").":&nbsp;</span>";
    if ($buff == 0) {
        $pointToggle = "disabled";
        $pointToggle_display = "none";
    }
    else {
        $pointToggle = "";
        $pointToggle_display = "block";
    }

    if ($modules->is_enabled('text_filter') || $modules->is_enabled('text_filter2')) {
        $out .= "<label style='display:inline-block;vertical-align:bottom'>
                    <input type='checkbox' name='editmode' value='featureEditor' id='editToggle' class='maphands' onclick='toggleControl(this);' />
                    <span title='Edit drawn features' style='display:block;background-image:url($protocol://".URL."/css/img/edit_feature_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label>
                <label style='display:inline-block;vertical-align:bottom'>
                    <input type='radio' name='draw-type' value='Point' id='pointToggle' $pointToggle class='maphands' onclick='toggleControl(this);' />
                    <span title='Draw point' id='pointToggle_icon' style='display:$pointToggle_display;background-image:url($protocol://".URL."/css/img/draw_point_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label>
                <label style='display:inline-block;vertical-align:bottom'>
                    <input type='radio' name='draw-type' value='LineString' id='lineToggle' class='maphands' onclick='toggleControl(this);' />
                    <span title='Draw line' style='display:block;background-image:url($protocol://".URL."/css/img/draw_line_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label>
                <label style='display:inline-block;vertical-align:bottom'>
                    <input type='radio' name='draw-type' value='Polygon' id='polygonToggle' class='maphands' onclick='toggleControl(this);' />
                    <span title='Draw polygon' style='display:block;background-image:url($protocol://".URL."/css/img/draw_polygon_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label>
                <label style='display:inline-block;vertical-align:bottom'>
                    <input type='radio' name='draw-type' value='Circle' id='circleToggle' class='maphands' onclick='toggleControl(this);' />
                    <span title='Draw circle' style='display:block;background-image:url($protocol://".URL."/css/img/draw_circle_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
                </label>

                <label style='display:inline-block;vertical-align:bottom;' id='spatial-query-settings' title='Add buffer'>
                    <span class='pure-button button-href' style='width:22px;height:22px;padding:0'><i class='fa fa-cog fa-lg' style='vertical-align:-25%'></i></span>
                </label>";
    }
    $out .= "</div>";
    
    $out .= "<div class='mcbox' id='map-navi-box'>";
    $out .= "<span style='display:inline-block'>&nbsp;".mb_convert_case(str_navigation, MB_CASE_UPPER, "UTF-8").":&nbsp;</span>";
    $out .= "<label style='display:inline-block;vertical-align:bottom'>
                 <input type='radio' name='type' value='zoomArea' id='zoomToggle' class='maphands' />
                 <span style='display:block;background-image:url($protocol://".URL."/css/img/zoom_box_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
             </label>
             <label style='display:inline-block;vertical-align:bottom'>
                 <input type='radio' name='type' value='none' id='noneToggle' class='maphands' checked='checked' />
                 <span style='display:block;background-image:url($protocol://".URL."/css/img/pan_onoff.png);width:24px;height:22px;background-repeat: no-repeat;'></span>
             </label></div>";

    // Apply text query switch
    $out .= "<div class='mcbox' id='apptq-mcbox'>".str_apply_text_query.":&nbsp;<button class='pure-button button-passive button-small' id='apptq'><i class='fa fa-lg fa-toggle-off'></i></button></div>";

    // Show my position
    $out .= "<div class='mcbox' id='show-my-position-mcbox'><button title='Show my position' id='show-my-position' class='pure-button'><i class='fa fa-lg fa-map-marker' style='color:#59c3e8'></i></button></div>";

    // Module hook: print_panelbox
    // print map bottom panel box (function box)
    foreach ($modules->which_has_method('print_panelbox') as $mm) {
        $out .= "<div class='mcbox'>".$modules->_include($mm,'print_panelbox',[])."</div>"; 
    }
    
    // show coordinates
    $out .= "<div class='mcbox' id='mouse-position-mcbox'>".str_position.": <div id='mouse-position'></div></div>";
    
    
    $out .= "</div>";
    
    // results-area
    $out .= "<div id='results-area'>";
        $out .= "<div id='error'></div>";
        $out .= "<div id='matrix'></div>";

        // rollable results view area
        $out .= '<div id="drbc">';
            //$out .= "<div style='background-color: #b3cee6;height:40px'></div>";
            $out .= '<div id="dynamic-results-block" style="position:absolute;display:inline-block;z-index:2">';
                $out .= '<div id="scrollbar-header" style="display:none"></div>';
                $out .= '<div id="scrollbar" class="scrollbar" style="display:none"></div>';
            $out .= '</div>'; // dynamic-results-block
        $out .= '</div>'; // drbc
    $out .= '</div>'; // results-area

    // results-area-end

    $out .= "</div>"; // mapcontrols

    $out .= "<div class='mapholder-cell' id='mapfilters'>
        <div id='smallheader'></div>";

    $cmd = sprintf("SELECT main_table,f_project_schema FROM project_queries 
LEFT JOIN header_names ON f_project_name=project_table AND main_table=f_project_table 
WHERE project_table='%s' AND f_project_table IS NOT NULL",PROJECTTABLE);
    $res = pg_query($BID,$cmd);
    $main_tables = array('');
    while ($row = pg_fetch_assoc($res) ) {
        if (defined('str_'.$row['main_table']))
            $main_tables[] = constant('str_'.$row['main_table']).'::'.$row['f_project_schema'].'.'.$row['main_table'];
        else
            $main_tables[] = preg_replace("/^".PROJECTTABLE."_/","",$row['main_table'])."::".$row['f_project_schema'].'.'.$row['main_table'];
    }
    $options = selected_option($main_tables,$_SESSION['current_query_table']);

    $filters = 1;
    if (isset($_GET['boxes']) and $_GET['boxes']===false) {
        $filters = 0;
    }
    if ($filters) {
        $out .= "
        <div class='mapfb' style='border:1px solid #eaeaea'>
            <div class='title center'>".str_query_table."</div>
            <div class='content'><select style='text-align:center;padding:0.7rem;font-size:120%' class='pure-u-1' id='current_query_table'>$options</select></div>
        </div>";
        $out .= $filter;
    }
    $out .= "<!--w1--></div><!--w2--></div>";
    // Module hook: map_js
    // Include js script in <script></script> tags loaded in map mappage
    $js = '';

    foreach ($modules->which_has_method('map_js') as $mm) {
        $js .= $modules->_include($mm,'map_js'); 
    }
    if ($js != '')
        $out .= '<script type="text/javascript">'.$js.'</script>';

?>
