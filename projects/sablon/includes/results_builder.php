<?php
/* This function set together the map query results
 * wfs_response is a json array including row ids and other wfs response elements
 *
 *
 * */
 require_once(getenv('OB_LIB_DIR').'db_funcs.php');
 
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");
if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");
if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");

require_once(getenv('OB_LIB_DIR').'modules_class.php');
require_once(getenv('OB_LIB_DIR').'common_pg_funcs.php');
require_once(getenv('OB_LIB_DIR').'data_export.php');
require_once(getenv('OB_LIB_DIR').'prepare_vars.php');
require_once(getenv('OB_LIB_DIR').'languages.php');

/* create result slides, csv, gpx and table
 * it is handling restricted data as well
 * */
class results_builder {

    private $Method;
    private $Filename;
    private $Buttons = 'off';
    private $Slide;
    private $Specieslist = 'off';
    private $Quote = "'";
    private $Sep = ",";
    private $Summary = "off";
    private $Reverse = 'off';
    private $Clear_current_rows = 'on';
    public $rq = array('columns','query','joins','joins_vnames','join_id','extent','results_count');
    public $response_id = array();
    public $error = array();
    public $modules = array();
    public $x_modules = array();
    public $current_rows = array();
    public $current_rows_length;
    public $module_ref;

    public function __construct (array $options = array()) {
        global $BID;
        $this->method = $options['method'] ?? null;
        $this->filename = $options['filename'] ?? null;
        $this->slide = $options['slide'] ?? null;
        $this->quote = $options['quote'] ?? null;
        $this->sep = $options['sep'] ?? null;
        $this->reverse = $options['reverse'] ?? null;
        $this->clear_current_rows = $options['clear_current_rows'] ?? null;
        $this->bgproc = $options['bgproc'] ?? false;
        $this->dlr = $options['dlr'] ?? null;
        $this->dlr_message = $options['dlr_message'] ?? "";
        $this->export_options = $options['export_options'] ?? "";
        $this->module_ref = $options['module_ref'] ?? "";

        //should be exists!
        //if (!isset($_SESSION['current_query_table'])) $_SESSION['current_query_table'] = PROJECTTABLE;
        /* MODULE INCLUDES HERE */

        $this->modules = new modules($_SESSION['current_query_table']);

        $this->x_modules = new x_modules();
    }

    // create list of ids from WFS/WMS response
    /*function id_queue($wfs_response) {

        if (!is_array($wfs_response)) {
            $j = json_decode($wfs_response);
            if (is_object($j)) {
                $wfs_response = array();
                $wfs_response['data'] = $j->{'data'};
                $wfs_response['id']   = $j->{'id'};
                $wfs_response['geom'] = $j->{'geom'};
                if (isset($j->{'history'}))
                    $wfs_response['history'] = $j->{'history'};
            } else {
                //save to admin
                $this->error[] = "Invalid results object.";
                return false;
            }
        }

        //get the data rows' ids into response_id Array
        //this array will be processed in the results_query
        $n = 0;
        $ids = array();
        foreach ($wfs_response['id'] as $i) {

            preg_match("/[a-z]?+([0-9]+)/i", $i,$m);
            if (isset($m) and count($m)) {
                $ids[$n] = $m[1];
                $n++;
            }
        }
        //talán segítek vele a postgresnek...
        //vagy szivatom a httpd-t
        asort($ids);
        $this->response_id = $ids;
        return true;
    }*/

    /* Update html content of results_as... modules
     *
     * */
    function updateOutput() {
        /*
         * */
        $data = new formatData();
        //$data->modules = $this->x_modules;
        //push column names to $data->csv_header
        $data->csv_head($this->rq['columns']);
        //if ($this->x_modules->is_enabled('photos') && get_option('show_attachment_filenames')) {
        if (get_option('show_attachment_filenames')) {
            $pos = array_search('obm_files_id', array_keys($data->csv_header));
            if ($pos > 0) {
                $pos++;
                $data->csv_header = array_slice($data->csv_header, 0, $pos, true) + ["filename" => str_filename] + array_slice($data->csv_header, $pos, count($data->csv_header)-$pos, true);
            }
        }
        // Open slide modal in results_asList
        if ($this->method=='slide') {
            // create_csv()
            // Internal output. Called on using results_asList in "list" method
            // creates a data output modal card
            // These will be stored in _SESSION['slide'] variables and
            if ($this->modules->is_enabled($this->module_ref)) {
                //default itegrated module
                $slide = $this->modules->_include($this->module_ref,'print_slide',array($data,$this->rq['columns'],['slide'=>$this->slide]));
                echo $slide;
            } else {
                echo common_message('error','Display results slide module not enabled');
                log_action('results_listSlide module not enabled!',__FILE__,__LINE__);
                return false;
            }
            return true;
        }
        // Load rolltable
        if ($this->method=='list') {
        
        }
    }
    /* Return status for export 
     * called only in results_buttons
     */
    function export() {
        if ($this->method == 'export') {
        }

        $data = new formatData();
        //$data->modules = $this->x_modules;
        //push column names to $data->csv_header
        $data->csv_head($this->rq['columns']);
        //if ($this->x_modules->is_enabled('photos') && get_option('show_attachment_filenames')) {
        if (get_option('show_attachment_filenames')) {
            $pos = array_search('obm_files_id', array_keys($data->csv_header));
            if ($pos > 0) {
                $pos++;
                $data->csv_header = array_slice($data->csv_header, 0, $pos, true) + ["filename" => str_filename] + array_slice($data->csv_header, $pos, count($data->csv_header)-$pos, true);
            }
        }
        $data->csv_header = array_merge(array('obm_id'=>str_rowid,'uploader_name'=>str_uploader,'uploading_date'=>str_upload_date,'uploading_id'=>str_dataupid),$data->csv_header);

        $filename = ($this->filename=='') ? $_SESSION['current_query_table'].genhash() : $this->filename;

        $de = new DataExport($filename);
        if ($de->exists()) {
            return "{$de->status}:{$de->id}";
        }
        
        if (isset($_SESSION['Tuser'])) {
            $u = unserialize($_SESSION['Tuser']);
            $uid = $u->user_id;
        } else {
            $uid = 0;
        }
            
        $de->setArr([
            'user_id' => $uid,
            'filename' => $filename,
            'requested' => date('Y-m-d h:i:s'),
            'status' => 'requested',
            'message' => $this->dlr_message,
            'options' => $this->export_options,
        ]);
        $de_id = $de->save();
        
        $fun = 'create_tmpfile';
        $run_opts = json_encode([
            'data_header' => $data,
            'session' => $_SESSION,
            'data_export_id' => $de_id,
            'dlr' => $this->dlr
        ]);
        
        if ($this->bgproc == true) {
            $fun = 'create_tmpfile_as_bgproc';
        }
        else {
            $run_opts = json_decode($run_opts, true);
        }
        return $de->$fun($run_opts);
    }
    /* Return dynamic html content by results_... modules
     * summary
     * save options
     * species-list
     * rolling tables
     */
    function printOut($params=array('content'=>'all')) {
        global $ID,$BID;
        /*
         * */
        $data = new formatData();
        //$data->modules = $this->x_modules;
        //push column names to $data->csv_header
        $data->csv_head($this->rq['columns']);
        //if ($this->x_modules->is_enabled('photos') && get_option('show_attachment_filenames')) {
        if (get_option('show_attachment_filenames')) {
            $pos = array_search('obm_files_id', array_keys($data->csv_header));
            if ($pos > 0) {
                $pos++;
                $data->csv_header = array_slice($data->csv_header, 0, $pos, true) + ["filename" => str_filename] + array_slice($data->csv_header, $pos, count($data->csv_header)-$pos, true);
            }
        }

        // Results outputs
        // Rolltable - big table
        // Set: SESSION['roll_array']
        if ($this->method=='fulltable') {

            if ($this->modules->is_enabled($this->module_ref)) {
                //default itegrated module
                return $this->modules->_include($this->module_ref,'load_fullpage_results_table',array($data));
            } else {
                echo common_message('error','Display results table module not enabled');
                log_action($this->module_ref.' module is not enabled!',__FILE__,__LINE__);
                //pg_close($ID);
                return false;
            }
            //pg_close($ID);
            return true;
        }

        $html_output = "";

        // Default action, return all html content
        // Do not print any other thing, only big table and map if we are in load Shared data mode
        if ($params['content'] == 'all' and !isset($_SESSION['load_loadquery'])) {
        
            // morefilter output
            if (isset($_SESSION['morefilter']) and $_SESSION['morefilter']!='clear') {

                array_reverse(array_filter($_SESSION['filter_type']));
                $ft = implode(" <i class='fa fa-arrow-right fa-lg'></i><br> ",$_SESSION['filter_type']);

                $button = new button(['title'=>t('str_new_filter')]);
                if ($_SESSION['morefilter']=='off')
                    $html_output .= str_filters_in_this_query.":<div style='color:#333;font-size:80%;font-weight:normal;margin:10px'>$ft</div>" .
                                        $button->print(['class'=>'pure-button button-passive button-small']);
                else
                    $html_output .= str_filters_in_this_query.":<div style='color:#333;font-size:80%;font-weight:normal;margin:10px'>$ft</div>" .
                                        $button->print(['class'=>'pure-button button-warning button-small clear-morefilter']);
                $html_output .= "<br><br>";
            }

            // Summary
            // Count records
            $summary_output = "";
            foreach ($this->modules->which_has_method('print_results_summary_count') as $mm) {
                $summary_output .= $this->modules->_include($mm,'print_results_summary_count',[]);
            }

            // Species list
            foreach ($this->modules->which_has_method('print_results_summary_specieslist') as $mm) {
                $summary_output .= $this->modules->_include($mm, 'print_results_summary_specieslist',[]);
            }

            if ($summary_output != '')
                $html_output .= sprintf('<div><span class="mtitle"><a name="nav_summary">%s</a></span><div class="options-box">%s</div></div>',
                    t(str_summary),
                    $summary_output);

            // Save options buttons
            $buttons = '';
            foreach ($this->modules->which_has_method('print_results_buttons') as $mm) {
                $buttons .= $this->modules->_include($mm,'print_results_buttons',[]); 
            }
            if ($buttons != '')
                $html_output .= sprintf('<div style="width:600px"><span class="mtitle"><a name="save_options">%s</a></span><div class="options-box" style="display:none">%s</div></div>',
                    t(str_save_options),
                    $buttons);


            // Edit options
            foreach ($this->modules->which_has_method('print_results_editor_button') as $mm) {
                $html_output .= $this->modules->_include($mm,'print_results_editor_button',[]); 
            }

            // View option buttons
            $view_options = '';
            foreach ($this->modules->which_has_method('print_results_selector_button') as $mm) {
                $view_options .= $this->modules->_include($mm,'print_results_selector_button',[]); 
            }
            if ($view_options!='') {
                $html_output .= sprintf('<div style="width:600px"><span class="mtitle"><a name="view_options">%s</a></span><div class="options-box">%s</div></div>',
                    t(str_view_options),
                    $view_options);
            }
        }

        // content = all && update

        //$st_col = st_col($_SESSION['current_query_table'],'array');
        $foo = array();
        if ($this->method == 'default_view_result_method') {
            foreach ($this->modules->which_has_method('get_default_view') as $mm) {
                $om = $this->modules->_include($mm,'get_default_view',[]);
                if ($om != '') {
                    $this->method = $om;
                    $foo[] = $om;
                }
            }
            // compatibility hack
            if (!count($foo)) {
                if ($this->modules->is_enabled('results_asStable'))
                    $this->method = 'stable';
                elseif ($this->modules->is_enabled('results_asList'))
                    $this->method = 'list';
            }

        }

        // Rolltable modules: results_asList, results_asStable
        $rolltable = array();
        foreach ($this->modules->which_has_method('print_query_results') as $mm) {

            $o = $this->modules->_include($mm,'print_query_results',[$this->method,$data,$this]);
            if ($o !== false) {
                
                if ($o['output'] == 'rolltable') {
                    $html_output .= $o['content'];
                    $rolltable = array(
                        'rowHeight'=>$o['params']['rowHeight'],
                        'cellWidth'=>$o['params']['cellWidth'],
                        'borderRight'=>$o['params']['borderRight'],
                        'borderBottom'=>$o['params']['borderBottom'],
                        'arrayType'=>$this->method,
                        'scrollbar_header'=>json_encode($o['control'])
                    );
                }
            }
        }

        session_write_close();
        echo common_message('ok',array("html"=>$html_output,"map"=>explode(',',$this->rq['extent']),"rolltable"=>$rolltable));
        return true;
    }

    function results_length() {
        return $this->rq['results_count'];
    }

    // slide function
    // fetch data from postgres result object
    // set: current_rows
    // set: current_rows_length
    // EZ még obm 1.0 xml processing volt
    public function fetch_data() {
        global $ID,$BID;
        $new_query = 1;
        //reload page clear result variables
        //default state
        //When not used?
        //print_slides.php -> read only one slide data
        //printGPX -> read current max 2000 records to export GPX
        if ($this->clear_current_rows=='on') {
            unset($_SESSION['wfs_rows']);
            unset($_SESSION['wfs_rows_counter']);
            $new_query = 1;
        }

        //prepare sql query and columns list

        // create a new current_rows array - default state
        if($new_query) {
            $error[] = $this->rq['query'];
            #log_action("query:".$this->rq['query'],__FILE__,__LINE__);
            if (isset($this->rq['query']) and $this->rq['query']!='') {
                //
                //<<<<<<<<<<<<<<<<<<< MAIN QUERY EXECUTION >>>>>>>>>>>>>>>>>>>
                //
                $result = pg_query($ID,$this->rq['query']);
            } else {
                //should be saved for admins
                $this->error[] = "An error occured while preparing the database query. Please contact the database maintainers.";
                return false;
                // error message: no query prepared
            }

            if(pg_last_error($ID)){
                // Wrong query
                $this->error[] = "Query error: ";
                $this->error[] = $this->rq['query'];
                return false;
                //should be saved for admin
            } elseif (pg_num_rows($result)==0) {
                /* Empty result
                 * Read the wfs response instead of database query.
                 */
                $this->error[] = "No database results in this query.<br>";
                log_action("results_builder: no-data:  {$this->rq['query']}",__FILE__,__LINE__);

                return false;
            } else {
                # Postgres query result
                $this->current_rows_length = pg_num_rows($result);
                //MEMORY!!!
                //while($row = pg_fetch_assoc($result)) {

                //}
                $this->current_rows = pg_fetch_all($result);
            }
        } else {
            //reread results_builder without
            //clear_current_rows == off
            //button list/stable, gpx

            if(isset($_SESSION['current_rows'])){
                $this->current_rows_length = count($_SESSION['current_rows']);
                $this->current_rows = $_SESSION['current_rows'];
            }
        }
        //reverse order
        if ($this->reverse=='on') {
            //log_action('reverse');
            $reverse_rows = array();
            for($i=0;$i<$this->current_rows_length;$i++) {
                $reverse_rows[] = array_pop($this->current_rows);
            }
            $this->current_rows = $reverse_rows;
            unset($reverse_rows);
        }

        // No rows error message
        if (!$this->current_rows_length) {
            $this->error[] = "Empty results object.";
            return false;
        }

        /* Data content restriction based on rules
         * It can remove some columns from the original table or change contents based on complex rules
         * There are other ACCESS LEVEL HANDLING mechanism for general access rule handlings!
         * Use this only for very special non standard cases! Maybe never...
         * It was very firs module for access rule handlings (for Milvus) and I keept it as special module.
         * */
        if($this->modules->is_enabled('restricted_data')) {
            $this->current_rows = $this->modules->_include('restricted_data','rule_data',array($this->current_rows));
        }

        return $this->current_rows_length;
    }
    /* Ez a függvény szedi össze a webes adatmegjelenítéshez az oszlop neveket és fűzi össze az SQL SELECT szöveget a lekérdezéshez.
     * Rafinált, mert az SQL lekérdezés már lefutott (results_query.php/proxy.php) és a visszaadott ID-k ott vannak a temp_--- táblában - itt már csak formázgatunk
     * viszont ennek a formázásnak összhangban kell lennie a mapfile formázásaival!!!
     *
     * Nem jelenít meg semmit, nem hajtja végre a lekérdezést!
     * USE user specific modules to call user functions to EXTEND the SQL query:
     * additional_columns:
     * join_extension:
     * column_names:
     * only_columns:
     * */
    function results_query($id_list = '',$do_not_create_temp_table = false) {
        global $BID,$ID;

        if (!isset($_SESSION['current_query_table']))
            $_SESSION['current_query_table'] = PROJECTTABLE;

        $st_col = st_col($_SESSION['current_query_table'],'array');

        /*
         * $this->rq['extent']
         * */
        if (isset($_SESSION['do-not-create-query'])) {
            unset($_SESSION['do-not-create-query']);
            $this->rq['columns'] = dbcolist('array',$_SESSION['current_query_table']);

            $cmd = sprintf("SELECT concat_ws(',',ST_XMIN(ST_Extent(obm_geometry)),ST_YMIN(ST_Extent(obm_geometry)),ST_XMAX(ST_Extent(obm_geometry)),ST_YMAX(ST_Extent(obm_geometry))) as extent
                        FROM temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id());
            $res = pg_query($ID,$cmd);
            $row = pg_fetch_assoc($res);
            $this->rq['extent'] = $row['extent'];
            $this->rq['results_count'] = 1;

            return true;
        }

        if (isset($st_col['SPECIES_C']))
            $SPECIES_C = $st_col['SPECIES_C'];
        else
            $SPECIES_C = "";
        if (isset($st_col['GEOM_C']) and $st_col['GEOM_C']!='') {
            $GEOM_C = $st_col['GEOM_C'];
            $GEOM_NAME = $GEOM_C;
            $GEOM_C = $_SESSION['current_query_table'].".".$GEOM_C;
        } else {
            $GEOM_C = '';
        }

        $historder = "";
        //$histcol = "";
        $main_columns_visible_names = array();
        $additional_columns_visible_names = array();
        $joined_table_columns_visible_names = array();

        // flexible data table

        $sqla = sql_aliases(1);
        $qtable = $sqla['qtable'];
        $qtable_alias = $sqla['qtable_alias'];
        $FROM = $sqla['from'];
        $GEOM_C = $sqla['geom_col'];
        $geom_alias = $sqla['geom_alias'];
        $id_col = "$qtable_alias.obm_id";

        /* QUERY COLUMNS LIST
         * first:       obm_id (qtable=PROJECTTABLE) followed by the geometry (header_names!!)
         * secondly:    uploadings columns, modifier_id
         * thirdly:     dbcolist main column names || no_login_columns
         *
         * */
        $c = array();
        $query_cmd = array();
        $c[] = $id_col;
        $grid_mki = false;

        // geometry data displayed as WKT text
        if ($GEOM_C != '') {

            if ($grid_mki = $this->modules->is_enabled('grid_view')) {
                if (isset($_SESSION['display_grid_for_map'])) {
                    $grid_geom_col = $_SESSION['display_grid_for_map'];
                    $geom_col = 'ST_AsText(qgrids.'.'"'.$grid_geom_col.'"'.')';
                } else {
                    $grid_geom_col = $this->modules->_include('grid_view','default_grid_geom');
                    $geom_col = 'ST_AsText(qgrids.'.'"'.$grid_geom_col.'"'.')';
                }

            } else
                $geom_col = "ST_AsText($GEOM_C)";
        }
        else
            $geom_col = '';

        /* module functions
         * TRANSFORM GEOMETRY: default is postgis St_SnapToGrid function for points location transformation
         *
         * */
        /*$module_name = array_values(array_intersect(array('snap_to_grid','transform_geometries','transform_geometry'), array_column($this->modules, 'module_name')));
        $mki = (count($module_name)) ? array_search($module_name[0], array_column($this->modules, 'module_name')) : false;
        if ($mki!==false) {
            //default itegrated module
            $dmodule = new defModules();
            if (!isset($_SESSION['load_loadquery'])) {
                $geom_col = $dmodule->snap_to_grid('geom_column',$this->modules[$mki]['p'],NULL);
            }
        }*/
        // what is this??
        if (preg_match("/$qtable\./",$geom_col))
            $geom_col = preg_replace("/$qtable\./","$qtable_alias.",$geom_col);


        if ($geom_col != '')
            $c[] = "$geom_col as obm_geometry";

        //$c[] = $histcol;
        $c[] = "uploadings.uploading_date";
        $c[] = "uploadings.uploader_name";
        $c[] = "uploadings.form_id as uformid";
        $c[] = "uploadings.uploader_id";
        $c[] = "$qtable_alias.obm_validation as data_eval";
        $c[] = "uploadings.id as uploading_id";
        $c[] = "$qtable_alias.obm_modifier_id";
        /* if rules table exists for the main table!!! */
        if ($st_col['USE_RULES']) {
            $c[] = sprintf("ARRAY_TO_STRING(data_rules.read,',') as obm_read");
            $c[] = sprintf("ARRAY_TO_STRING(data_rules.write,',') as obm_write");
            $c[] = sprintf("data_rules.sensitivity as obm_sensitivity");
        }

        //only column names
        $oc = dbcolist('columns',$qtable);

        //list of columns with visable names without prefix
        //it can be longer than the real column list....
        $main_columns_visible_names = dbcolist('array',$qtable);

        //kidobjuk a formázatlan geometriát
        if (isset($GEOM_NAME))
            unset($oc[array_search($GEOM_NAME,$oc)]);

        //table_name.column formatting
        array_walk($oc, 'prefix', $qtable_alias);
        $c = array_merge($c,$oc);

        /* module functions
         *
         * ADDITIONAL COLUMNS
         * The joined table's columns
         * */
        if ($this->modules->is_enabled('additional_columns')) {
            // send module params
            $ret = $this->modules->_include('additional_columns','return_columns');
            $c = array_merge($c,$ret[0]);
            $additional_columns_visible_names = $ret[1];
        }

        /* JOIN with UPLOADING table */
        $query_cmd[] = $FROM;
                /* cmd CONDITION
         * */
        if ($id_list=='') {
            $id_filter = "1 = 1";
            $query_cmd[] = sprintf("INNER JOIN temporary_tables.temp_%s_%s tt ON \"%s\".obm_id = tt.obm_id",PROJECTTABLE, session_id(), $qtable_alias);
        }
        elseif (is_array($id_list)) {
            $id_filter = sprintf('"%s".obm_id IN (%s)', $qtable_alias, implode(',', array_map(function($v) {
                return (int)$v;
            }, $id_list)));
        }
        else {
            # only one row -> e.g. result_slide
            $id_filter = sprintf('"%s".obm_id = %d', $qtable_alias, $id_list);
        }

        $query_cmd[] = join_table('uploadings', compact('qtable_alias') );

        /* JOIN with grid table if module enabled */
        if ($grid_mki) {
            $query_cmd[] = join_table('grid',compact('id_col','qtable') );
        }

        /* if rules table defined */
        /* JOIN with RULES table */
        if ($st_col['USE_RULES']) {
            $query_cmd[] = join_table('rules', compact('id_col','qtable') );
        }

        //if ($this->x_modules->is_enabled('photos') && get_option('show_attachment_filenames')) {
        if (get_option('show_attachment_filenames')) {
            $query_cmd[] = join_table('filename', compact('qtable'));
            $c[] = 'fj.filename';
        }

        /* On the fly JOIN tables IN the results query
         * not nice, complicated
         * module functions
         * DEPRECATED - will be removed the join_atble module?!
         *
         * JOIN_EXTENSION: join other tables to the main table
         * extend the the query command
         * Here we can extend our query result with data which not exists in the original query (project_queries)
         * */
        /*$this->rq['joins_vnames'] = '';
        $defined_visible_names_array = array();
        $mki = array_search('join_tables', array_column($this->modules, 'module_name'));
        if ($mki!==false) {
            $dmodule = new defModules();
            // send module params
            $ret = $dmodule->join_tables('return_joins',$this->modules[$mki]['p']);

            //this will be used for modifying joined table data on data_sheet_loader
            $query_cmd[] = $ret[0]; // appending the join_tables to the query's JOIN part
            $c = array_merge($c,$ret[1]); // table.column array
            $defined_visible_names_array = $ret[3]; // visible names array of array
            $this->rq['joins_vnames'] = $ret[2];
            $this->rq['joins'] = $ret[0];
        }*/

        /* Drop the non unique names!!!
         * which has a small chance thanx to the prefixes, however they can cause problems while creating the temporary table because the duplicated prefixed column names there will non-unique again!!!
         * */
        $cu = array_unique($c);
        if (count($c) != count($cu)) {
            log_action("The `where` non-unique column names in the query which automatically filtered: ".implode(',',array_diff_key($c,$cu)),__FILE__,__LINE__);
            $c = $cu;
        }


        /* sofisticated column name modification for joined tables if there are the same names in the different tables
         * the main problem is, that these names break the temporary table creation, therefore here I change the table.column style to
         * table_column
         * this should be triggered in the visible column names: table_column AS predefined names
         * */
        $cfk = array();
        foreach($c as $cf) {
            // split the columns array along the prefix separator
            $cfp = preg_split('/\./',$cf);
            if (count($cfp)==2) {
                $cfk[] = $cfp[1];
            } else
                $cfk[] = $cfp[0];

        }
        // if this condition is true
        // we should rename the non unique columns names and give AS name for them
        if (count($cfk) != count(array_unique($cfk))) {
            //we are here because there are some non unique names
            //difference array
            //$d[3] = 'foo'
            // Itt felvesszük azokat az oszlopokat amik azonos néven vannak a JOIN táblában mint a MAIN táblában
            $d = array_diff_key($cfk,array_unique($cfk));
            foreach(array_keys($d) as $k) {
                if(preg_match('/([a-z0-9_]+)\.([a-z0-9_]+)/i',$c[$k],$m)) {
                    $l = sprintf("%s",preg_replace('/\./','_',$c[$k]));
                    $label = $m[2];
                    //if (isset($defined_visible_names_array[$m[1]])) {
                    //    $label = $defined_visible_names_array[$m[1]][$m[2]];
                    //}
                    $joined_table_columns_visible_names[$l] = $label;
                    $c[$k] = sprintf("%s AS %s",$c[$k],$l);
                }
            }
            // Itt felvesszük azokat amik eltérő néven vannak
            /*foreach($defined_visible_names_array as $k) {
                foreach($k as $key=>$value) {
                    $joined_table_columns_visible_names[$key] = $value;
                }
            }*/

        } /*else {
            foreach($defined_visible_names_array as $k) {
                foreach($k as $key=>$value) {
                    $joined_table_columns_visible_names[$key] = $value;
                }
            }
            }*/

        /* merge the three sources of columns
         * */
        $this->rq['columns'] = array_merge(array('hist_time'=>t(str_last_modify)),$main_columns_visible_names,$additional_columns_visible_names,$joined_table_columns_visible_names);
        //log_action($this->rq['columns'],__FILE__,__LINE__);

        //eliminates the empty elements if there are some!
        array_unshift($query_cmd,implode(',',array_filter($c)));
        array_unshift($query_cmd,"SELECT");

        /* ORDER BY
         *  used by modules, e.g. results_asStable
         *  */

        if (!isset($_SESSION['orderby'])) {
            $_SESSION['orderby'] = 'obm_id';
            $_SESSION['orderad'] = 1;
            $_SESSION['orderascd'] = '';
        }
        if (!isset($_SESSION['orderad']) or $_SESSION['orderad']==0) {
            $_SESSION['orderad'] = 1;
        }

        if ($this->reverse != '') {
            $_SESSION['orderad'] = $_SESSION['orderad']*-1;
            $OD = ($_SESSION['orderad']<0) ? 'ASC' : 'DESC';
            $_SESSION['orderby'] = $this->reverse;
            $_SESSION['orderascd'] = $OD;
        }
        elseif ($SPECIES_C != '') {
            // Is it good to use species name ordering in very default way??
            if ($_SESSION['orderby'] == $this->reverse) {
                $_SESSION['orderad'] = $_SESSION['orderad']*-1;
                $OD = ($_SESSION['orderad']<0) ? 'ASC' : 'DESC';
            } else {
                $OD = 'ASC';
            }
            $_SESSION['orderby'] = $SPECIES_C;
            $_SESSION['orderascd'] = $OD;
        } else {
            if (!isset($_SESSION['orderby'])) {
                // fall back
                $_SESSION['orderby'] = 'obm_id';
            }
            if (!isset($_SESSION['orderascd'])) {
                // fall back
                $_SESSION['orderascd'] = '';
            }
        }


        // sensitive data handling in queries
        $sensitive = '';
        if (!isset($_SESSION['Tid']))
            $tid = 0;
        else
            $tid = $_SESSION['Tid'];
        if (!isset($_SESSION['Tgroups']) or $_SESSION['Tgroups']=='')
            $tgroups = 0;
        else
            $tgroups = $_SESSION['Tgroups'];

        // visible columns restriction
        $main_cols = dbcolist('columns',$_SESSION['current_query_table']);
        $main_cols[] = 'obm_id';
        $main_cols[] = 'obm_uploading_id';
        $main_cols[] = 'filename';
        $allowed_cols = $main_cols;

        if ($this->modules->is_enabled('allowed_columns')) {
            //default itegrated module
            $allowed_cols = $this->modules->_include('allowed_columns','return_columns',$main_cols);
        } else {
            $ACC_LEVEL = ACC_LEVEL;
            if (!isset($_SESSION['Tid']) and ( $ACC_LEVEL and ("$ACC_LEVEL" == '1' or "$ACC_LEVEL" == '2' or "$ACC_LEVEL" == 'login' or "$ACC_LEVEL" == 'group' ))) {
                // drop all column for non logined users if access level is higher
                $allowed_cols = array('obm_id');
            }
        }

        if (!has_access('master') and $st_col['USE_RULES']) {
            $sensitive = " AND ( (sensitivity::varchar IN ('3','only-owner') AND ARRAY[$tgroups] && read) ";
            $sensitive .= " OR (sensitivity IS NULL OR sensitivity::varchar IN ('0','public') ) ";
            $sensitive .= " OR (sensitivity::varchar IN ('2','restricted','sensitive') AND ARRAY[$tgroups] && read)";
            $sensitive .= " OR (sensitivity::varchar IN ('1','no-geom'))";
            
            /* drop lines if sensitivity problems meet with allowed columns restriction
             *
             * */
            if (isset($_SESSION['current_query_keys'])) {
                $intersected_allowed_columns_count = count(array_intersect($_SESSION['current_query_keys'],$allowed_cols));
                $query_column_count = count($_SESSION['current_query_keys']);
                if ($intersected_allowed_columns_count != $query_column_count) {
                    $sensitive .= " OR (sensitivity::varchar IN ('2','restricted','sensitive') AND 1=0 AND (read && ARRAY[$tgroups])=FALSE) ";
                } else {
                    $sensitive .= " OR (sensitivity::varchar IN ('2','restricted','sensitive')) ";
                }
            } else {
                $sensitive .= " OR (sensitivity::varchar IN ('2','restricted','sensitive')) ";

            }
            $sensitive .= " ) ";
        }

        //$query_cmd[] = sprintf('WHERE "%s".obm_id IN (%s) %s %s',$qtable_alias,$id_filter,$sensitive,$ORDERBY);
        $query_cmd[] = sprintf('WHERE %s %s',$id_filter,$sensitive);

        // implode final sql command
        $this->rq['query'] = implode(" ",$query_cmd);

        // data sheet page call
        // skip creating temp table, because we do not fire mapserver action
        if ($do_not_create_temp_table)
            return true;
        
        $this->x_modules->_include('download_restricted', 'delete_cache');
        // push selected result into temp_ table
        // for map query
        pg_query($ID,sprintf("DROP TABLE IF EXISTS temporary_tables.temp_query_%s_%s",PROJECTTABLE,session_id()));
        // It is fast - ok!
        $cmd = sprintf("CREATE UNLOGGED TABLE temporary_tables.temp_query_%s_%s AS %s",PROJECTTABLE,session_id(),$this->rq['query']);
        //debugging query
        $res = pg_query($ID,$cmd);
        if(!pg_affected_rows($res)) {
            if (pg_last_error($ID) or $res===false) {
                log_action(pg_last_error($ID),__FILE__,__LINE__);
                log_action($cmd,__FILE__,__LINE__);
            } else {
                if (isset($_SESSION['filter_type'])) {
                    unset($_SESSION['filter_type']);
                }

                // empty query
            }
            $this->rq['results_count'] = 0;
        } else {
            $this->rq['results_count'] = pg_affected_rows($res);
        }

        $cmd = sprintf("GRANT SELECT ON temporary_tables.temp_query_%s_%s TO %s_admin",PROJECTTABLE,session_id(),PROJECTTABLE);
        pg_query($ID,$cmd);


        $cmd = sprintf("SELECT concat_ws(',',ST_XMIN(ST_Extent(obm_geometry)),ST_YMIN(ST_Extent(obm_geometry)),ST_XMAX(ST_Extent(obm_geometry)),ST_YMAX(ST_Extent(obm_geometry))) as extent
                        FROM temporary_tables.temp_query_%s_%s %s",PROJECTTABLE,session_id(),$qtable_alias);
        if (!has_access('master') and $st_col['USE_RULES']) {
            $cmd .= join_table('rules', compact( 'qtable', 'id_col'));
            $cmd .= " WHERE  (sensitivity::varchar IN ('1','3','no-geom','only-owner') AND ARRAY[$tgroups] && read) ";
            $cmd .= "   OR sensitivity IS NULL OR sensitivity::varchar IN ('0','2','public','restricted','sensitive') ";
        }

        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);
        $this->rq['extent'] = $row['extent'];

        update_temp_table_index('query_'.PROJECTTABLE.'_'.session_id());

        return true;
    }
}



# results list csv table printout class functions
class formatData {
    public $modules = array();
    public $def=array('r_id'=>'','u_id'=>'','u_date'=>'','upl_id'=>'','upl_name'=>'','r_spec'=>'');
    public $header_ref;
    public $body_ref;
    public $csv_rows=array(); // two dimensional associate array contains all queried data, the
    public $csv_header=array();
    //public $uspecies=array();
    public $cite=array();

    function def($arr) {
        foreach ($arr as $key=>$val) {
            if ($key == 'r_id') $this->def['r_id'] = $val;
            elseif ($key == 'u_id') $this->def['u_id'] = $val;
            elseif ($key == 'u_date') $this->def['u_date'] = $val;
            elseif ($key == 'upl_id') $this->def['upl_id'] = $val;
            elseif ($key == 'upl_name') $this->def['upl_name'] = $val;
            elseif ($key == 'r_spec') $this->def['r_spec'] = $val;
            elseif ($key == 'r_valid') $this->def['r_valid'] = $val;
        }
    }

    //cite persons
    //function cite_push($e) {
    //    $this->cite[]=$e;
    //}
    //data header columns
    function csv_head($e) {
        $this->csv_header=$e;
    }
    // data rows
    // put lon,lat and other fields in a row of a table
    function csv_push($geom,$e) {
        $n = count($this->csv_header);
        $csv = new SplFixedArray($n);//array();
        // correct header should be prepared previously
        // without it there will be no data in the csv table
        $i = 0;
        foreach($this->csv_header as $cn=>$cv) {
            if (isset($e[$cn]))
                //array_push($csv,$e[$cn]);
                $csv[$i] = $e[$cn];
            else
                //array_push($csv,'');
                $csv[$i] = '';
            $i++;
        }
        if($geom!='')
            $csv[0] = $geom;
        array_push($this->csv_rows,$csv->toArray());
    }
} // class end


?>
