<?php
/* maintenance scheduler code */
$schedule_day = "2011/12/13";
$from = "23:00";
$length = "2 hours";
$local_mconf = 0;
$system_mconf = 0;
$mconf = "../maintenance.conf";

if (file_exists('./maintenance.conf'))
    $local_mconf = filemtime('./maintenance.conf');

if (file_exists('../maintenance.conf'))
    $system_mconf = filemtime('../maintenance.conf');

if ($local_mconf>$system_mconf)
    $mconf = "./maintenance.conf";

$contents = file($mconf); 

$capture = array("day"=>0,"from"=>0,"length"=>0);

foreach ($contents as $line) {

    if ($capture['day'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
        $schedule_day = trim($line);
        $capture = array_fill_keys(array_keys($capture), 0);
    }
    elseif ($capture['from'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
        $from = trim($line);
        $capture = array_fill_keys(array_keys($capture), 0);
    }
    elseif ($capture['length'] and  trim($line)!='' and !preg_match('/^#/',$line)) {
        $length = trim($line);
        $capture = array_fill_keys(array_keys($capture), 0);
    }
    
    if (preg_match('/^## start day/',$line)) {
        $capture = array_fill_keys(array_keys($capture), 0);
        $capture['day'] = 1;
    }
    elseif (preg_match('/^## from/',$line)) {
        $capture = array_fill_keys(array_keys($capture), 0);
        $capture['from'] = 1;
    }
    elseif (preg_match('/^## length/',$line)) {
        $capture = array_fill_keys(array_keys($capture), 0);
        $capture['length'] = 1;
    }
}

require_once('local_vars.php.inc');
$maintenance_head = "Database maintenance on ".PROJECTTABLE."!";

$date = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
$date_start = DateTime::createFromFormat('Y/m/d H:i',$schedule_day." ".$from);
$date_start_format = $date->format('Y-m-d H:i');
$length = preg_replace("/hour.?/","H",$length);
$length = preg_replace("/minute.?/","M",$length);
$date_end = $date->add(new DateInterval('PT'.str_replace(" ","",$length)));
$date_end_format = $date_end->format('Y-m-d H:i');

# automatic exend time with 2 hours, if exceeded
$date_now = new DateTime();
if ($date_end < $date_now) {
    $date_end = $date_now->add(new DateInterval('PT2H'));
    $date_end_format = $date_end->format('Y-m-d H:00');
}
    
$date_now = new DateTime();
if ($date_end < $date_now or $date_start > $date_now) {
    # non maintenance window
    return;
}

$time =   "Planned schedule:<br><br>between <span style='color:violet'>$date_start_format</span> and <span style='color:violet'>$date_end_format</span>!";

?>
<!DOCTYPE html>
<html>
<head>
<style>
body {
    font-family: "Lucida Grande", Verdana, Geneva, Lucida, Arial, Helvetica, sans-serif !important;
    font-size:12px;
    color: #3f3f3f;
    background-color: slategray;
    margin:0;
    position:relative;
    line-height:1.5;
    background-color:
}
#bheader {
    margin:0;
    display:table;
    width:100%;
    padding:0;
    line-height:1em;
    vertical-align:bottom;
    position:relative;
    border-bottom:1px solid #7c7c7c;
    box-shadow: 0 3px 8px rgba(0,0,0,.24);
    background-color:#cacaca;
    z-index:1100;
    min-width:300px;
}
#bheader .htitle {
    vertical-align:top;
    padding:15px 0 15px 30px;
    float:left;
}
#bheader .htitle p {
    font-family:georgia;
    letter-spacing:1px;
    color:#4d4d4d;
    font-size:250%;
    padding:1em 1em 1em 1em;
    line-height:50px;
    vertical-align:middle;
    margin:0;
    background: -moz-linear-gradient(left, rgba(250,250,250,1) 0%, rgba(250,250,250,0.35) 65%, rgba(0,0,0,0) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left, rgba(250,250,250,1) 0%,rgba(250,250,250,0.35) 65%,rgba(0,0,0,0) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right, rgba(250,250,250,1) 0%,rgba(250,250,250,0.65) 35%,rgba(0,0,0,0) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fafafa', endColorstr='#00000000',GradientType=1 );
    border-radius:5px;
}
</style>
</head>
<body>
    <div id='bheader'>
        <div class='htitle'>
            <p><?php echo $maintenance_head; ?></p>
        </div>
    </div>
    <div style='text-align:center;font-size:3em;margin-top:2em'><?php echo $time; ?></div>
</body>
</html>

<?php
 exit;
?>
