<?php

function server_choice() {
    $user = $_SESSION['Tmail'];
    
    if (!defined('COMPUTATIONAL_SERVERS'))
        return "No Computational servers available (should be added into /etc/openbiomaps/system_vars.php.inc)";

    else {
        $servers = COMPUTATIONAL_SERVERS;
        $running_servers = array();
        $ports = array('http'=>80,'https'=>443);
        $content = "<p>";
        foreach ($servers as $s) {
            $p = parse_url($s);
            $host = $p['host'];
            $scheme = $p['scheme'];
            if ($this->serverSate($host,$ports[$scheme])) {
                $content .=  "$host is online<br>";
                $running_servers[] = $s;
            } else
                $content .= "$host is down<br>";
        }
        $content .= "</p>";
        $content .= "<p>";
        $loadstates = array();
        
        $ciphertext = sslEncrypt(COMPUTATIONAL_CLIENT_SEC, COMPUTATIONAL_CLIENT_KEY);
        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=>"client-key: $ciphertext\r\n" . 
                      "client-name: ".COMPUTATIONAL_CLIENT_NAME."\r\n" 
          )
        );
        $context = stream_context_create($opts);

        foreach ($running_servers as $s) {
            $p = parse_url($s);
            $host = $p['host'];
            $l = json_decode(file_get_contents($s."?load",false,$context),true);
            //var_dump($l);
            if ($l['status']=='success') {
                $loadavg = explode(" ",$l['data']['loadavg']);
                $mem = preg_replace('/^\d+(\w+)$/','',$l['data']['mem_available']);
                $ncpu = $l['data']['ncpu'];
                $load_index1 = $loadavg[0] / $ncpu;
                $load_index5 = $loadavg[1] / $ncpu;
                $load_index15 = $loadavg[2] / $ncpu;

                $load_index = $load_index1 + $load_index5 + $load_index15;

                if ( $load_index > 1 and $load_index1 > 0.8 ) {
                    $loadstates[] = $load_index1 + $load_index5 + $load_index15;
                    $content .= "$host is overloaded<br>";
                } else {
                    $loadstates[] = $load_index;
                    $content .= "$host is calm (cpu: {$l['data']['ncpu']} mem:{$l['data']['mem_available']})<br>";
                }
            } else
                    $content .= "$host state is unknown<br>";
        }
        $content .= "</p>";
        $content .= "<p>";

        // use log($mem) !!!
        //
        arsort($loadstates);
        $k = array_key_last($loadstates);
        $url = $running_servers[$k];
        $content .= "$url will be used for running computations<br>";
        $content .= "</p>";

        return $content;
    }

?>
