<?php
/* ***************************************************************************  /
    
GENERAL and GLOBAL FUNCTIONS
Do not change them
Use the local_funcs.php if you need specific functions

****************************************************************************** */
/*        (SELECT * FROM project_forms WHERE project_table=\'%1$s\' AND
        draft=false AND form_access<3 AND active=1 AND \'api\'=ANY(form_type)
        ORDER BY published DESC LIMIT 1)
            UNION
        SELECT * FROM project_forms WHERE project_table=\'%1$s\' AND
        draft=true AND form_access<3 AND active=1 AND \'api\'=ANY(form_type) AND user_id=%d
        ORDER BY form_name*/

function pds_form_choose_list($project,$user,$groups) {
    #var_dump(debug_backtrace());
    global $BID;
    $list = array();
    /* összes id lekérdezése */
    //$cmd = sprintf("SELECT form_id FROM project_forms WHERE project_table=%s AND form_access<3 AND active=1 AND 'api'=ANY(form_type) AND form_name!='nullform' ORDER BY form_name",quote($project));

    $cmd = sprintf('
            (
             SELECT form_id, form_name, published_form_id
             FROM project_forms p1 
             WHERE project_table=%1$s AND draft=true AND form_access<3 AND active=1 AND \'api\'=ANY(form_type) AND user_id=%2$d
             ORDER BY form_name
            )
            UNION
            (
              SELECT t1.form_id,t1.form_name,t1.published_form_id FROM project_forms t1
              JOIN 
              (
               SELECT form_name,MAX(last_mod) AS MAXDATE
               FROM project_forms
               WHERE project_table=%1$s
               GROUP BY form_name
              ) t2
              ON (t1.form_name = t2.form_name AND t1.last_mod = t2.MAXDATE)
              WHERE t1.project_table=%1$s AND form_access<3 AND active=1 AND \'api\'=ANY(form_type) AND draft=false
            )
            ORDER BY form_name',
        quote($project),$_SESSION['Tid']);
     
    $res = pg_query($BID,$cmd);
    $n = array();
    while($row = pg_fetch_assoc($res)){
        //hozzáférés ellenőrzése
        if (pds_form_access_check($row['form_id'],$user,$groups)===1) {
            $n[] = array('id'=>$row['form_id'],'pubid'=>$row['published_form_id']);
        }
    }
    //lista a megengedett form azonosítókról
    return $n;
}
function pds_form_access_check($form_id,$user,$groups) {
    if(!$form_id) return;
    global $BID;

    $f1 = $f2 = $ft= $fn=$ft_str='';
    $acc = 0; #0:public, 1:logined, 2:closed, 3:inactive
    $group_filter='';
 
    //access level - we create a filter for it
    if ($user) {
        $acc = 1;
        /* the form is available only for specified groups
         * and the logined user is member of these groups */
        $group_filter = sprintf(' OR (form_access=2 AND \'{%s}\'&&groups)',$groups);
    }

    // van-e olyan aktív form a projektben aminek input_form_id az azonosítója és a hozzáférés kisebb/egyenlő mint acc(0|1)
    // több ilyen form is lehet!!!
    $cmd = sprintf("SELECT form_id,user_id,draft 
                    FROM project_forms 
                    WHERE  form_id=%s AND (form_access<='%s' %s)",
                        quote($form_id),
                        $acc,
                        $group_filter);

    #$cmd = sprintf("SELECT form_id,'api' as form_type, user_id, CASE WHEN published_form_id IS NOT NULL AND published IS NULL THEN 1 ELSE 0 END as draft 
    #        FROM project_forms 
    #        WHERE project_table=%s AND form_id=%s AND 'api'=ANY(form_type) AND (form_access<='%s' $group_filter)",
    #            quote($project),quote($form_id),$acc);

    $res = pg_query($BID,$cmd);
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        // draft form only for its owner
        if ( $row['draft']=='t' and (!$user or $row['user_id']!=$user))
            return 0;

        return 1;
    }
    return 0;
}
// List of API forms
function pds_form_element($form_id,$project) {
    global $BID;
    $cmd = sprintf("SELECT form_id,form_name,'api' as ft,round(extract(epoch from last_mod)) as last_mod 
        FROM project_forms 
        WHERE project_table=%s AND 'api'=ANY(form_type) AND form_id='%d' and active=1",
            quote($project),$form_id);
    $res = pg_query($BID,$cmd);
    $row = pg_fetch_array($res);
    return $row;
}
// kisegítő függvény: Array to JSON object
function array2json($arr) {
    //$json = array();
    return json_encode($arr, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT|JSON_FORCE_OBJECT);
}
    
// kisegítő függvény: PRS
// Return with an array of Column's names of a table 
function glue($table) {
    global $ID;
    $list = '*';
    $cmd = "SELECT * FROM $table WHERE 1=1 LIMIT 1";
    $res = pg_query($ID,$cmd);
    while($r=pg_fetch_assoc($res)){
       $list = array_keys($r);
    }
    return $list;
}

// PFS: Get user profile data with access_token
function pds_profile($token) {
    global $BID,$ID;
    
    # crypted userid
    #  	
    $cmd = sprintf("SELECT username, givenname, familyname, email, 
                        id, user, status, institute, address, inviter, validation, orcid, \"references\" 
                    FROM users u LEFT JOIN oauth_access_tokens oa ON (u.email =oa.user_id)
                    WHERE access_token=%s",quote($token));

    if ( $res = pg_query($BID,$cmd)) {
        return pg_fetch_assoc($res);
    }
    else {
        log_action(pg_last_error($BID));
        log_action($cmd,__FILE__,__LINE__);
        return false;
    }
}

// PFS: history
// getFunction: history
function histRows($table,$value) {
    global $ID;
    $qtable_history = sprintf("%s_history",preg_replace("/_history$/","",$table)); 
    $genid = quote($value);

    $cmd = "SELECT COUNT(hist_id) as cn FROM $qtable_history WHERE obm_id=$genid LIMIT 1";
    #print $cmd.'<br>';
    $res = pg_query($ID,$cmd);
    return $res;
}

// PFS: history
// History header function
function histHeader($table,$value) {
    global $ID;
    $qtable_history = sprintf("%s_history",preg_replace("/_history$/","",$table)); 
    $genid = quote($value);

    $cmd = "SELECT to_char(hist_time,'YYYY-MM-DD HH24:MI') as hist_time,modifier_id FROM $qtable_history WHERE obm_id=$genid ORDER BY hist_time DESC LIMIT 1";
    #print $cmd.'<br>';
    $res = pg_query($ID,$cmd);
    return $res;
}

// PFS functions
// load saved queries from project repository
function loadq($value) {
    global $BID;
    list($id,$ses) = preg_split('/@/',$value);
    $id = preg_replace("/[^0-9]/","", $id);
    $ses = preg_replace("/[^a-zA-Z0-9]/","", $ses);
    if (strlen($ses)>30) return;
    $cmd = "SELECT result,project FROM project_repository WHERE id='$id' and sessionid='$ses'";
    $res = pg_query($BID,$cmd);
    $post_id = array();
    $table = '';
    if (pg_num_rows($res)) {
        $row=pg_fetch_assoc($res);
        $table = $row['project'];
        $xml = simplexml_load_string($row['result']);
        $namespaces = $xml->getDocNamespaces();
        foreach($xml->children($namespaces['gml']) as $child) {
            foreach($child->children($namespaces['ms']) as $data){
                foreach($data->children($namespaces['ms']) as $i){
                    if ($i->getName() == 'id') {
                        $post_id[] = (string) $i;
                    }
                }
            }
        }
        return array($table,$post_id);
    } else {
        return common_message('fail',$cmd);
    }
}

// array_walk function:
// quotes column's names
function wquote(&$i, $key) {
    if ($i=='obm_comments')
        /* array_to_string FUNCTION should be exists!! */
        $i = "array_to_string(obm_comments,'~~') as obm_comments";
    #elseif ($i=='validation_')
    #    $i = "array_to_string(validation_,',') as validation_";
    else
        $i = '"'.$i.'"';
}

// PRS function
// adat lekérdezés elmentett egyedi SQL  alapján: service->PRS
function reportQuery($key,$table) {
    global $ID,$BID;

    $cmd = sprintf("SELECT id FROM users 
                LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email) 
                LEFT JOIN project_users pu ON (pu.user_id=id)
                WHERE access_token=%s AND pu.project_table=%s",quote($_POST['access_token']),quote($table));

    $res = pg_query($BID,$cmd);

    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);

        $cmd = sprintf("SELECT command FROM bookmarks WHERE user_id=%d AND key=%s",$row['id'],quote($key));
        $res = pg_query($BID,$cmd);
        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);

            $command = $row['command'];

            #$command = "SELECT comment, datum FROM public_nestbox_data_observations WHERE datum=(SELECT datum FROM public_nestbox_data_observations ORDER BY datum DESC LIMIT 1)";
            $cmd = sprintf("SELECT row_to_json(t) FROM ( %s ) t",$command);
            return common_message('ok',$cmd);
        } else {
            return common_message('error','No reports for the requested user and key');
        }
    } else {
        return common_message('fail','access denied');
    }
}

function validate_shared_link($link) {

    global $BID;

    $cmd = sprintf('SELECT project,project_table,command FROM bookmarks WHERE link=%s AND project_table=%s',quote($link),quote(PROJECTTABLE));
    $res = pg_query($BID,$cmd);

    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        return common_message('ok',$row);
    }

    return common_message('fail','link verified failed');
}

// PRS function
// adat lekérdezés function: service->PRS
// table: requested table
// value: filtering value (text)
// clmn: filtered column (a valid column  name in table or --id-- for primary key)
// request_type: JSON | CSV | ...? - not used yet
// filter: additional where statements
// limit: LIMIT OFFSET settings
// exclude: array of excluded columns
function dataQuery($table,$value,$clmn,$request_type='JSON', 
                    $filter='',$limit='',$exclude=array('obm_comments','obm_validation'),$shared_link='') {
    global $ID,$BID;
    $columns = array();
    $ref_column = '';

    $st_col = st_col($table,'array');

    $obm_columns = dbcolist('columns',$table);
    if (!in_array("obm_id",$exclude))
        $obm_columns[] = "obm_id";
    if (!in_array("obm_uploading_id",$exclude))
        $obm_columns[] = "obm_uploading_id";
    if (!in_array("obm_validation",$exclude))
        $obm_columns[] = "obm_validation";
    if (!in_array("obm_comments",$exclude))
        $obm_columns[] = "obm_comments";

    $geom_idx = array_search('obm_geometry',$obm_columns);

    $READ_ACC = ACC_LEVEL;

    // ACCESS LEVEL Handling
    $cmd = sprintf("SELECT layer_query FROM project_queries WHERE project_table=%s AND layer_type='query' AND rst IN (0,1) AND enabled=TRUE AND main_table=%s",
                    quote(PROJECTTABLE),quote($table));
    $res = pg_query($BID,$cmd);
    $query = '';
    if (pg_num_rows($res)) {
        $row = pg_fetch_assoc($res);
        $query = $row['layer_query'];
    }

    $uid = 0;
    $rule_join = '';
    $rules = "";


    if ($READ_ACC and ($READ_ACC == '1' or $READ_ACC === 1 or $READ_ACC == 'login')) {
        if(!isset($_SESSION['Tid'])) {
            return common_message('error','Insufficient rights');
        }
    }
    elseif ($READ_ACC and ($READ_ACC == 2 or $READ_ACC == 'group')) {
        $cmd = sprintf("SELECT id,user_status FROM users 
                LEFT JOIN oauth_access_tokens oa ON (oa.user_id=email) 
                LEFT JOIN project_users pu ON (pu.user_id=id)
                WHERE access_token=%s AND pu.project_table=%s",quote($_POST['access_token']),quote(PROJECTTABLE));

        $res = pg_query($BID,$cmd);

        if (pg_num_rows($res)) {
            $row = pg_fetch_assoc($res);

            if ($row['user_status']===0 or $row['user_status']=='banned')
                return common_message('error','Insufficient rights');
            else {
                if ($st_col['USE_RULES']) {
                    $tgroups = $_SESSION['Tgroups'];
                    $rule_join = sprintf('LEFT JOIN "%s_rules" ON (obm_id=row_id)',PROJECTTABLE);
                    $rules = "AND (sensitivity::varchar IN ('0','public') OR 
                                  (sensitivity::varchar IN ('1','no-geom') AND read && ARRAY[$tgroups]) OR 
                                  (sensitivity::varchar IN ('2','restricted','sensitive') AND read && ARRAY[$tgroups]) OR 
                                  (sensitivity::varchar IN ('3','only-owner') AND read && ARRAY[$tgroups]))";
                    $rules .= sprintf(" AND %s_rules.data_table=%s",PROJECTTABLE,quote($table));
                }
            }
        } else {
            $cmd = sprintf("SELECT client_id FROM oauth_access_tokens WHERE access_token=%s",
                quote($_POST['access_token']));

            $res = pg_query($BID,$cmd);

            if (pg_num_rows($res)) {
                $row = pg_fetch_assoc($res);
                if ($row['client_id'] == 'obm') {
                    $cmd = sprintf('SELECT command FROM bookmarks WHERE project_table=%s AND link=%s',
                        quote(PROJECTTABLE),quote($shared_link));
                    $res = pg_query($BID,$cmd);
                    if (pg_num_rows($res)) {
                        $row = pg_fetch_assoc($res);

                        $cmd = sprintf("SELECT ROW_TO_JSON(t) FROM (%s) t",
                            $row['command']);
                        return common_message('ok',$cmd);
                    }
                }
            }
            return common_message('error','access denied');
        }
    } elseif (!$READ_ACC or $READ_ACC == '0' or $READ_ACC === 0 or $READ_ACC == 'public') {
        # Public access

    } else {

        return common_message('error','shit in the soup');
    }

    $cmd = '';

    $obm_column_list = $obm_columns;
    # quote column' names
    array_walk($obm_columns, 'wquote');

    if ($geom_idx!==false) {
        $obm_columns[$geom_idx] = "st_asText(obm_geometry) AS obm_geometry";
    }

    if (count($obm_columns)) {

        $a = array();
        // SET requested column
        if ($clmn == '--id--') {

            // limit is ignored here

            // Should be extended to use any primary key??
            
            // data preprocessing
            // numeric value range
            if (preg_match("/(\d+):(\d+)/",$value,$m)) {
                $value = range($m[1],$m[2]);
            }
            
            // list of ids
            if (!is_array($value)) $value = array($value);
            foreach ($value as $var) {
                $var = preg_replace("/[^0-9]+/","", $var);
                $a[] = quote($var);
            }

            $request_column = 'obm_id';

            $query = preg_replace('/%morefilter%/',"",$query);
 
            // text or spatial filter string
            // applied in query layers
            // point - line - polygon

                     // query set parameters
            $q = sprintf("%s IN (%s)",$request_column,implode(",",$a));
            $q = preg_replace('/\$/',"\\\\$",$q);
            $query = preg_replace('/%qstr%/',"AND $q",$query);
            // if I could use this query it would more clear workflow

            $cmd = sprintf("SELECT ROW_TO_JSON(t) FROM ( SELECT %s FROM \"public\".\"%s\" %s WHERE %s IN (%s) %s) t",
                implode(",",$obm_columns),$table,$rule_join,$request_column,implode(",",$a),$rules);

        }
        elseif ($clmn == '--history--') {
            // limit is ignored here
            // lekérdezendő sorok azonosítói egy tömbbe pakolva 
            if (preg_match("/(\d+):(\d+)/",$value,$m)) {
                $value = range($m[1],$m[2]);
            }
            if (!is_array($value)) $value = array($value);

            foreach ($value as $var) {
                $var = preg_replace("/[^0-9]+/","", $var);
                $a[] = quote($var);
            }

            $request_column = "row_id";

            $cmd = sprintf("SELECT %s FROM \"public\".\"%s\" WHERE %s IN (%s)",
                implode(",",$obm_columns),$table,$request_column,implode(",",$a));

        }
        elseif ($clmn == '--species--') {
            // UPGRADE NECESSARY !!!
            foreach ($value as $var) {
                // nincs definiálva hogy tömbbként jöjjön több fajnév
                $var = preg_replace("/[+]/","",$var);
                $var = preg_replace("/[_]/","",$var);
                $var = preg_replace("/[ ]/","",$var);
                $a[] = quote(strtolower($var));
            }
            $taxon_table = sprintf("%s_taxon",$table);
            $cmd = sprintf("SELECT lang FROM %s WHERE LOWER(meta) IN (%s)",$taxon_table,implode(",",$a));
            $res = pg_query($ID,$cmd);
            $row=pg_fetch_assoc($res);

            $species_array = $st_col['ALTERN_C'];
            if (!count($species_array)) $request = $st_col['SPECIES_C'];
            else $request = array_search($row['lang'],$species_array);
            $cmd = sprintf("SELECT %s FROM \"public\".\"%s\" WHERE replace(lower(%s),' ','') IN (%s) %s",
                implode(",",$obm_columns),$table,$request,implode(",",$a),$limit);

        } else {
            
            // normal data query
            //if (in_array($clmn,glue($table)) or $clmn === 1) {
            if (in_array($clmn,$obm_column_list) or $clmn === 1 or $clmn == '--filter--') {
                
                if (preg_match("/(\d+):(\d+)/",$value,$m)) {
                    $value = range($m[1],$m[2]);
                    $clmn = 'obm_id';
                }
                if (!is_array($value)) $value = array($value);

                foreach ($value as $var) {
                    $a[] = quote($var);
                }
                $WHERE = sprintf("\"%s\" IN (%s)",$clmn,implode(",",$a));

                if ($clmn == '--filter--')
                    $WHERE = $filter;
                
                // ask all data from the database
                if ($clmn === 1) 
                    $WHERE = "1=1";


                if ($request_type == 'geojson') 
                    $cmd = sprintf("
                SELECT jsonb_build_object(
                    'type',     'FeatureCollection',
                    'features', jsonb_agg(features.feature)
                )
                FROM (
                  SELECT jsonb_build_object(
                    'type',       'Feature',
                    'id',         obm_geometry,
                    'geometry',   ST_AsGeoJSON(obm_geometry)::jsonb,
                    'properties', to_jsonb(inputs) - 'obm_geometry'
                  ) AS feature
                  FROM (SELECT %s FROM \"public\".\"%s\" %s WHERE %s %s %s) inputs) features",implode(",",$obm_columns),$table,$rule_join,$WHERE,$rules,$limit );
                
                elseif ($request_type == 'export') {
                    $prefix = "c.";

                    $obm_columns = array_map(function($elem) use ($prefix) {
                        return $elem === '"reference"' || $elem === '"access"' ? $prefix . $elem : $elem;
                    }, $obm_columns);

                    $files_join = "LEFT JOIN system.file_connect ON conid=obm_files_id LEFT JOIN system.files f ON f.id=file_id";
                    $obm_columns[] = 'f.reference as obm_filename';
                    $obm_columns[] = 'f.sum as obm_sha1';

                    $cmd = sprintf("SELECT ROW_TO_JSON(t) FROM ( SELECT %s FROM \"public\".\"%s\" c %s %s WHERE %s %s %s) t",
                        implode(",",$obm_columns),$table,$rule_join,$files_join,$WHERE,$rules,$limit);

                } else
                    //$cmd = sprintf("select row_to_json(t) from ( SELECT %s FROM \"public\".\"%s\" WHERE %s ) t",$obm_columns,$table,$WHERE);
                    $cmd = sprintf("SELECT ROW_TO_JSON(t) FROM ( SELECT %s FROM \"public\".\"%s\" %s WHERE %s %s %s) t",
                        implode(",",$obm_columns),$table,$rule_join,$WHERE,$rules,$limit);

            } elseif ($filter != '') {

                $cmd = sprintf("SELECT ROW_TO_JSON(t) FROM ( SELECT %s FROM \"public\".\"%s\" k %s WHERE %s %s %s) t",
                    implode(",",$obm_columns),$table,$rule_join,$filter,$rules,$limit);

            } else
                # no logging to the user
                log_action('Illegal query attempt',__FILE__,__LINE__);
        }
//debug($cmd);
        return common_message('ok',$cmd);
    } else {
        return common_message('error','Query columns from table '."$table".' failed.');
    }
    return common_message('fail','Unexpected event');
}

// A Computational package function
function saveURL($url,$package) {
    $path = getenv('PROJECT_DIR')."computational_packages/".$package;
    #$cmd = sprintf("UPDATE computation...");
    $y = yaml_emit_file("$path/computation_state.yml",array("working-server"=>$url));
}

function resize_image($image_name, $w) {
    
    if (!file_exists($image_name)) return false;

    $type = exif_imagetype($image_name);

    if ($type === 2)
        $image = imagecreatefromjpeg($image_name); // For JPEG

    elseif ($type === 3)
        $image = imagecreatefrompng($image_name);   // For PNG

    elseif ($type === 1)
        $image = imagecreatefromgif($image_name);   // For GIF

    else {
        log_action('unsopproted image type to resize');
        return false;
    }
    $imgResized = imagescale($image , $w);
    ob_start();
    imagejpeg($imgResized);
    $imageContent = ob_get_contents();
    ob_end_clean();

    return $imageContent;
}
// A function to tranform GeoJSON to mobil-app type gemoetry list
# transform given WKT to geometry
## point - polygon - line
/* from API v2.5
                
        {
          "type":"point",
          "positions":
            [
             {
               "latitude":37.3301,
               "longitude":-122.032,
               "accuracy":5,
               "timestamp":1719768268328.5588
             }
            ]
        }

        VS GeoJSON

        {
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [37.3301, -122.032]
          },
          "properties": {
            "timestamp":1719768268328.5588,
            "accuracy":5
         }
        }

*/
function transformGeoJson($geoJson) {
    $decoded = json_decode($geoJson, true);

    if (isset($decoded['geometry']))
        $d_geom = $decoded['geometry'];
    else
        $d_geom = $decoded;

    $type = strtolower($d_geom['type']);
    if ($type == 'linestring') $type = 'line';
    $properties = (isset($decoded['properties'])) ? $decoded['properties'] : array();
    $coordinates = $d_geom['coordinates'];

    $output = ['type' => $type, 'positions' => []];

    if ($type == 'point') {
        $output['positions'][] = array_merge(
            ['latitude' => $coordinates[1], 'longitude' => $coordinates[0]],
            $properties
        );
    } else if ($type == 'line') {
        foreach ($coordinates as $coords) {
            $output['positions'][] = array_merge(
                ['latitude' => $coords[1], 'longitude' => $coords[0]],
                $properties
            );
        }
    } else if ($type == 'polygon') {
        foreach ($coordinates[0] as $coords) { // First ring (exterior ring)
            $output['positions'][] = array_merge(
                ['latitude' => $coords[1], 'longitude' => $coords[0]],
                $properties
            );
        }
    }

    return $output;
}
?>
