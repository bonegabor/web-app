<?php

/* Allowed GET | POST request
 *
 * */

$general_keys = array(
    'service',
    'debug',
    'table',
    'access_token'
);
$function_keys = array(
    'header',
    'project',
    'user',
    'get_data_rows',
    'filters',
    'species',
    'history',
    'header',
    'get_profile',
    'track_data',
    'track_download',
    'track_citation',
    'get_specieslist',
    'Share',
    'type',
    'upload',
    'upload_test',
    'm_comment',
    'm_datum',
    'm_geometry',
    'get_form_list',
    'get_form_data',
    'put_data',
    'form_id',
    'set_rules',
    'get_bookmark',
    'put_header',
    'soft_error',
    'get_tables_data',
    'get_project_list',
    'get_project_vars',
    'get_trainings',
    'get_training_questions',
    'training_results',
    'training_toplist',
    'get_mydata_rows',
    'ic_post_data',
    'ic_accept_key',
    'ic_request_key',
    'ic_slave_project',
    'ic_slave_server',
    'pg_user',
    'get_tile_list',
    'get_tile_zip',
    'get_message_count',
    'request_time',
    'get_attachments',
    'get_tracklogs',
    'post_tracklogs',
    'tracklog',
    'computation',
    'connect_with_shared_link',
    'shared_link',
    'use_repo',
    'get_notification'
);

$valid_requests = array_merge($general_keys,$function_keys);

?>
