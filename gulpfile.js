
const { series, parallel, src, dest, watch } = require('gulp');
const sass = require('gulp-sass')(require('sass'))
const cleanCSS = require('gulp-clean-css')
const sourcemaps = require('gulp-sourcemaps');

async function css() {
    return await src('resources/styles/app/evolvulus/scss/page_style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(sourcemaps.write('.'))
        .pipe(dest('resources/styles/app/evolvulus/'))
}

exports.css = css
exports.watch = function () {
    watch('resources/styles/app/evolvulus/scss/*', css);
}
exports.default = css
